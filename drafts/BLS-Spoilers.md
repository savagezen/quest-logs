### Quest Planning:

Structure: 

* Entrance / Guardian
* Role Play / Puzzle
* Red Herring
* Climax Battle / Conflict
* Plot Twist

Encounter Building: aim for 100% resource use / long rest:

*   Deadly = 40% resources
*   Hard = 20% resources
*   Medium = 10% resources
*   Easy = 5% resources or less

---

### Loose Ends and Meta:

**Meta / Lore:**

Zoltun Kulle:
> Kulle is the main protagonist of the BLS campaign.  After the [Reaper of Souls](https://diablo.fandom.com/wiki/Diablo_III:_Reaper_of_Souls) events he once again descended into madness; believing that [Nephalem](https://diablo.fandom.com/wiki/Nephalem) (either ascended or purebred forebearers of humanity) had a rightful place beyond Sanctuary and ruling even the [High Heavens](https://diablo.fandom.com/wiki/High_Heavens) and the [Burning Hells](https://diablo.fandom.com/wiki/Burning_Hells).  Of course, Kulle sees himself as the only one intelligent enough to lead humanity on this crusade.

> In undeath, post Reaper of Souls, Kulle has virtually no threats and unabated attention -- not a good thing for a mad wizard who's achieved lich-dom.  In this down time Kulle learned of the once Dragon Pharaoh, [Nicol Bolas](https://mtg.fandom.com/wiki/Nicol_Bolas), one of the rare beings in existence Kulle considers to be his equal.

> Kulle's grand plan is to recreate the [Black Soulstone](https://diablo.fandom.com/wiki/Black_Soulstone) and trap all manner of Nephalem and [Planeswalkers](https://mtg.fandom.com/wiki/Planeswalker) inside it -- including Bolas.  Of course, Kulle again believes that only he is the one intelligent and wise enough to be responsible for dispensing such great power to humanity -- after serving himself first.

> With both the High Heavens and the Burning Hells in disarray, [omenpaths](https://mtg.fandom.com/wiki/Omenpath) have been relatively easy to create, or even occurring at random, appearing to be a combination of [planeshifting](https://roll20.net/compendium/dnd5e/Plane%20Shift#content) and [banishment](https://roll20.net/compendium/dnd5e/Banishment#h-Banishment) magic.

> Seeing summoning a [Greater Evil](https://diablo.fandom.com/wiki/Great_Evils) as both risky and difficult, Kulle's ire is drawn towards the [Lesser Evils](https://diablo.fandom.com/wiki/Lesser_Evils); specifically [Belial](https://diablo.fandom.com/wiki/Belial) due to the insidious and manipulative potential of the Lord of Lies.  But, why do the heavy lifting yourself?  Even easier is resurrecting Belial's primary disciple, [Maghda](https://diablo.fandom.com/wiki/Maghda) and allowing her to search for Belial's [Soul Shard](https://diablo.fandom.com/wiki/Soul_Shards).

> Through blood magic and human sacrifices, Kulle is able to create a [beacon accross realms](https://diablo.fandom.com/wiki/Andariel#Andariel's_Return).  However, his first contact isn't Bolas, but instead the de-sparked lithomancer [Nahiri](https://mtg.fandom.com/wiki/Nahiri).  Kulle's meddling with space-time-fabric has caused other planeswalkers to be "dropped" in the world of Sanctuary.  This is quite favorable to Kulle's plan.

> In Kulle's research, he learns of Nahiri's feud with [Sorin Markov](https://mtg.fandom.com/wiki/Sorin_Markov), Sorin's presence in Sanctuary, as well as the elder dragon [Niv-Mizzet](https://mtg.fandom.com/wiki/Niv-Mizzet)'s possession of a [map of omenpaths](https://mtg.fandom.com/wiki/Loot_(Fomori)); with Kulle believes will help him communicate with and ultimately summon Bolas.

> Once Belials' soul shard (the Dregs of Lies) is retrieved  by Maghda, Kulle is able to lie to Maghda -- he has no intention of summoning Belial any time soon, only using Belial's essence.  Primarily, this is to continue to manipulate Nahiri, as a lithomancer is a quite effective tool in hiding a lich's phylactery -- should any of the summoned Nephalem or Planeswalkers turn out to be problematic for Kulle himself.


Nichol Bolas:

> The once great God Pharaoh (CR28) is now de-sparked, nameless, and imprisoned (CR15) in a meditation realm; guarded by his brother, [Ugin](https://mtg.fandom.com/wiki/Ugin) ([CR24](https://imgur.com/mVh9eyi)), the Spirit Dragon.  While the removal of his name means that Bolas cannot be summoned, it is still theoretically possible to communicate with (or even visit) him in the spirit realm.  Of course, Ugin would likely immediately be alerted to this as his essence is "spread across the realm" to keep it guarded.


---


**Loose Ends:**

* How does Kulle get the Omenpath Map from Niv?
* The exact locations of Kulle's phylactery or soulstone laboratory aren't known.  In Diablo 4 a soulstone is "fixed" in a Horadrim lab in Khejekstan -- which is where Kulle's library is.  However, Tristram's association with summoning powerful beings also makes sense.
* Re-Assess Personal Stakes:
  * Sorin needs to level up, but has a feud with Nahiri.
  * Liliana has a feud with Kulle.
  * Zakk needs to "study" and level up and is probably game.
	  * Plot-Based Experience: +0.5 levels every time a Black Label Society or Black Sabbath reference is made (up to level 20).
	  * He will definitely get plenty of time to study, if the party secures the manor.
  * What about Digoria?  She has ties to Liliana, but isn't exactly the "create a detailed plan, and keep family bonds" type.
  * Serra can part ways with the rest of the angels.

---

**Noteworthy Items and Add-Ons:**

* 4 x [Riding Horses](https://roll20.net/compendium/dnd5e/Monsters:Riding%20Horse?expansion=0#content) are currently owned by the party.
* Serra Angel ([CR13](https://i.pinimg.com/originals/3a/00/9c/3a009c3f1d14a23986d2e188c35a44af.png)) (*temporary party member*)
* Liliana (*temporarily captive in Westmarch*)
	* [Bottomless Bag of Coal](https://www.tribality.com/2019/12/17/dd5e-christmas-themed-magic-items-to-gift-your-players/)
	* [Scroll of Naughty and Nice](https://www.wallydm.com/dnd-holiday-magic-items-christmas/#item6), 11 charges remaining
* Sorin
	* [Vampiric King's Greatblade](https://www.dandwiki.com/wiki/Vampiric_King%27s_Greatblade_(5e_Equipment))


---

### Quest 2: Liches Be Schemin'

* Defeating Nicolo:
	* His study unlocks and reveals a collection of holy and unholy musical instruments.  Zakk is dawn to one, which brings him some sobriety, and restores some of his apparently missing powers.
	* Can Zakk find other tools and / or practice music to get from LVL9 to LVL20ish?  Curse?
		* DMG131: going from LVL9 to LVL20 would cost 360 days and 720 GP
		* The gold isn't the issue here, especially if Maghda and Leoric are defeated and the party occupies the manor.  We can't wait a whole year to fight Kulle.
	* The party can also find notes on devilish deals which include how Sorin can return to full power:
		* DMG131: LVL15 to LVL20 costs 190 days and 380 GP; again, time is the issue, but the curse can be lifted by:
		* Drinking the blood of a willing planeswalker.
		* Drinking tears of a King
* Defeating Leoric:
	* One of the things for Zakk
	* Tears for fear -- for Sorin to collect.
	* Letters from Maghda to Kulle trying to collaborate on summoning nameless entities, though they differ in whom (due to their varying allegiances) -- may contain information about Bolas and other planeswalkers, black soulstone, etc.
	* Letters and historical references about [Adria](https://diablo.fandom.com/wiki/Adria) (beholden to Diablo, Lord of Terror) and Maghda (beholden to Belial, Lord of Lies), [King Leoric](https://diablo.fandom.com/wiki/Leoric), and [Queen Asylla](https://diablo.fandom.com/wiki/Queen_Asylla).
	* Asylla can serve as an aid "haunting" the house if the party wants to set up home base there after defeating Maghda
* In Pandemonium:
  * Digoria tries to obtain a summoning spell for Adria.  Serra and Sorin will especially not be a fan of this!  However, Digs' point is that Kulle is an immediate threat and Bolas -- or Diablo -- are "tomorrow problems for tomorrow."
    * This (Adria) may be how the BLS is able to protect the manor from celestials as their home base.  Why would they need to do that?  See BLS-Q02-P5

---

### Quest 3:  7 Soul Shards to the Black Soulstone (need clever title)

**Quest Prompt:**

> Give me a d&d 5e quest prompt for summoning (and banishing) nicol bolas from the MTG universe. Bolas is currently imprisoned in the spirit realm and nameless (so he cannot be summoned by his name). The spirit realm, a pocket "meditation" realm, is also guarded by the spirit dragon Ugin.

**Chat Response:**

> **Quest Prompt: "Chains of the Dragon Tyrant"**

Your party has uncovered a hidden cabal that seeks to free a nameless, ancient dragon imprisoned in the Spirit Realm—a dragon of cunning and cruelty whose power rivals the gods. This dragon, known only by his titles, "The Eternal Tyrant" and "The Dragon of Ambition," was once the mightiest planeswalker, bound to realms beyond. However, the cabal has found a way to unlock his prison, believing they can control him to conquer entire worlds.

The party's first task is to disrupt the cabal's ritual, but they learn that to truly keep the tyrant imprisoned, they must themselves travel to the Spirit Realm, an ethereal plane that exists only in meditation and guarded by none other than Ugin, the Spirit Dragon. Ugin’s distrust of mortals is well-known, and he’s unlikely to let any pass into his realm without a test of wisdom, humility, and restraint—qualities that differ starkly from his prisoner’s own ruthless drive.

Once within the Spirit Realm, the party will encounter manifestations of Bolas’s influence—shadows of ambition, distorted landscapes twisted by his lingering presence, and constructs that test each member’s loyalty to one another. Ugin will demand they prove their worthiness in a contest of magical and mental fortitude. Only if they succeed will he grant them the guidance to seal Bolas’s essence permanently, binding his power beyond mortal reach.

But beware: if any party member is swayed by the dragon’s whispered promises of ultimate power, they could undo the prison themselves, releasing Bolas. The party must resist these temptations and work together to ensure Bolas remains trapped—or risk becoming his new unwitting servants, carrying his ambitions back to the material plane.

> Additional Summoning Details:

Summoning a nameless lesser deity protected by a "spirit dragon" from another realm in *Dungeons & Dragons* is no small feat. Here’s a framework to accomplish such a ritual in a way that could fit into a high-level campaign with heavy stakes. This scenario could incorporate both arcane ritual mechanics and DM-created elements to make it a challenging and memorable encounter.

### 1. **Find the Deity’s Unique Signature or Sigil**
   - Since the deity is nameless, summoning them will require something else to establish a mystical connection. This could be a **unique sigil**, **symbol**, or **ancient relic** associated with the deity, perhaps created by their followers or by the deity themself.
   - Obtaining this sigil or relic could be a quest in itself. For example, it might be hidden in a long-abandoned temple guarded by cultists, celestial beings, or wards that only powerful magic users can bypass.

### 2. **Locate a Convergence Point Between Realms**
   - To summon a being from another realm, you may need to be in a place where the boundary between realms is thin. These places could include **sacred sites**, **leyline nexuses**, or **ancient ruins** dedicated to forgotten deities.
   - A temple of a similar alignment, or an ancient altar once used by the deity's followers, could work as a summoning location. If you’re playing in the Forgotten Realms, consider something like a ruined temple in the Shadowfell or a place where the **Veil between the Material Plane and the deity's plane** is naturally thin.

### 3. **Gather Rare Components and Allies**
   - Summoning a deity, even a lesser one, should involve powerful and rare components. For instance:
     - **Dragon bones or scales** (especially potent if they are remnants of a "spirit dragon")
     - **Celestial or Fiendish ichor** (the blood of powerful outsiders, depending on the deity’s alignment)
     - **An enchanted crystal or gem** capable of holding divine energy temporarily.
   - Allies like **clerics** or **warlocks** dedicated to similar deities, or those who specialize in planar magic, may be helpful to channel divine energy and stabilize the summoning process.

### 4. **Conduct a Ritual and Face the Spirit Dragon Guardian**
   - When the ritual begins, the **spirit dragon guardian** would sense the disturbance and appear to protect the deity. This could be an ethereal dragon, a quasi-celestial being, or even a draconic construct. The spirit dragon will attempt to halt the ritual by:
     - **Disrupting magic** around the summoning site
     - **Attacking the summoners**
     - **Calling upon lesser spirits or minions** to add complexity to the encounter.

   Defeating or subduing this guardian could be achieved by:
   - **Befriending or bargaining with it** (if the party is respectful and persuasive enough)
   - **Finding a specific weakness** or **artifact** that suppresses its power long enough to complete the ritual
   - Using **protective spells** or **barriers** to hold it at bay, allowing time to finish the summoning.

### 5. **Channel Divine Energy**
   - Completing the summoning ritual may require a **powerful incantation** or even a **blood sacrifice** to channel the deity’s energy into the Material Plane. This could mean:
     - A **sacrifice of a valuable or rare magical item** aligned with the deity’s essence.
     - Using **cleric spells** like *Divine Intervention* (Clerics of level 10 or higher) or ritual spells like *Gate* if the players have access to them.
   - During this time, the summoning circle or runes would glow with divine energy, and the players might face continued attacks from residual guardians, unstable energy surges, or magical backlash.

### 6. **Contact the Deity and Make an Offering**
   - When the deity arrives, they may appear as a vague, shifting figure or an apparition rather than a physical being. To communicate with or gain the favor of this deity, the party may need to offer a gift or demonstrate alignment with the deity’s purpose.
   - At this point, the players could:
     - Present the rare relic or sigil, proving their dedication.
     - Swear an **oath** or **service** to the deity in exchange for their guidance or power.
     - Offer **divine or spiritual knowledge** they’ve gathered to gain the deity’s trust.

This ritual could serve as a climactic event in a campaign, involving multiple challenges, NPC allies, and unique resources. With the successful completion of the summoning, the party might gain powerful new spells, divine knowledge, or a special boon directly from the deity they’ve summoned—if they survive the spirit dragon and the ritual's dangers!


---

### Quest 4:  Cathedral Showdown with Kulle

4. Plot Point No. 4:  Killing Kulle (will probably need Bolas's ancient wisdom to learn how / where phylactery is and how to destroy it)
	* Objective 4.1:
	* Objective 4.2:
	* Objective 4.3:
	* Objective 4.4:
	* Objective 4.5:

---

### Quest 5: Revenge

> Kulle's phylactery is in his library, not The Tristram Cathedral.

5. Plot Point No. 5:  Battle Bolas?
	* Objective 5.1:
	* Objective 5.2:
	* Objective 5.3:
	* Objective 5.4:
	* Objective 5.5:  Bolas escapes somehow... too strong for party