---
title: "Liches Be Schemin', Part 4"
description: ""
date:
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 02 - Liches Be Schemin'
Session: 04
DM: ChatGPT
```

### Current Situation:

> The *Black Label Society* has uncovered that Kulton Zulle is the lich Zulton Kulle. The party found the Wisemen (Dec, Kard, and Cain) turned to stone by a fey magic spell linked to blood, likely cast by Maghda.  However, Maghda was defeated by Liliana (blood-feud sister of Zulton), who has rejoined the party.  Serra, a celestial once allied with the party has been killed by Sorin, which brought the party together, but also brought the wrath of a fleet of angels.  The angels were dispelled by an unknown force once the party entered The Manor and closed the doors behind them.  The party's shared objective is to locate Zulton and his lithomancer aid, Nahiri, and stop them.  But first, they'll need to traverse the halls of the manor formerly occupied by Maghda and her cult (devoted to Belial, Lord of Lies), and King Leoric (The Skeleton King corrupted by Diablo the Lord of Terror, and consort to Adria, Bride of Hell) before Maghda.

---

### Played Adventure:

Ready to be played as standalone one-shot: [ChatGPT](https://chatgpt.com/share/679fa4e8-1ec8-8004-a602-529217141284)

---

### Notes:

* [Chat GPT Prompt Reference Guide](https://oracle-rpg.com/2023/03/solo-dm-guide-part-3-chatgpt-as-assistant-ai-dungeon-master/)

### Additional User Imput (Pre-Adventure):

> **DM Role:**
> You are the Dungeon Master for a D&D 5e campaign set in the world of Sanctuary from the Diablo series, blending lore from Forgotten Realms, Magic: The Gathering, and D&D 5e.  I will ask you for help with story writing, random scenarios, and guiding the adventure.  User input will dictate player actions unless I ask you specifically to act on behalf of a player character.

> **Player Characters:**
> 1. Digoria - Level 20, Tiefling Warlock (hexblade); cunning, sarcastic, absent minded.
> 1. Liliana - Level 20, Human Cleric (death domain); cynical, egocentric, charismatic.
> 2. Sorin - Level 15, Vampire Fighter (eldritch knight); generally stoic, but has a temper.
> 3. Zakk - Level 11, Half-Orc Bard (college of heavy metal); usually drunk, but a true master of fine arts (poetry, religion, etc.)

> **Antagonists:**
> 1. [Maghda](https://diablo.fandom.com/wiki/Maghda) (CR17 Witch Queen) - was formerly defeated by Liliana, but her ghostly essence still lingers in the Manor.  Maghda is a loyal servant to Belial, Lord of Lies.
> 2. King Leoric "[The Skeleton King](https://diablo.fandom.com/wiki/Leoric)" (CR15) was corrupted by Diablo, Lord of Terror.  It's rumored that he too lurks in his manor that's since been occupied by Maghda and her coven.
> 3. [Adria](https://diablo.fandom.com/wiki/Adria) (CR19 Blood Witch) - Bride of Hell, consort to Diablo (via Leoric), was defeated by the Nephalem and banished from sanctuary.  Though she once held equal ties with Maghda in organizing their coven, Adira's influence is strong, beyond death and even the material world.  She is not present in the manor, but has been banished to Pandemonium for failing Diablo.  It was Adria that called Maghda's essence back to the manor, after her defeat by Liliana, in order to absorb it for her own needs.
> 4. Nicolo (CR12) - An undead bard modeled after [Nicolo Paganini](https://en.wikipedia.org/wiki/Niccol%C3%B2_Paganini), "The Devil's Violinist."  He has an extensive library within The Manor that would make for a superb and accelerated study for a deviant or up-and-coming musician.

> **Personal Stakes:**
> 1.  Sorin has had his [Vampiric King's Greatblade](https://www.dandwiki.com/wiki/Vampiric_King%27s_Greatblade_(5e_Equipment)) restored, but is still notably depowered from Level 20 status (currently CR15).  He's desperate to seek revenge against his former ally and apprentice Nahiri as well as look for clues how to return to his homeworld of Innistrad.
> 2. Zakk is even more depowerd and in a persistent drunken stupor (Level 11, down from 20).  Unbeknownst to him, conquering a foe like Nicolo as well as having such a musical study to occupy, would greatly improve his leveling.  Zakk also gains 0.5 levels for every character reference to music by Black Sabbath, Black Label Society, or Robb Zombie.
> 3. Liliana has an ongoing blood feud with her brother, Zulton Kulle.  However, he's reached undeath, is apparently allied with the lithomancer Nahiri, and cannot be attacked directly, let alone in his lair.  Liliana and the party need a stronghold to plan their attack against Kulle.  Ideally, conquering The Manor could serve a function like [DC's House of Mystery for Constantine](https://dc.fandom.com/wiki/House_of_Mystery).
> 4. Digoria, being cleared of her prior crimes, isn't above whatever means may be necessary to stop Kulle.  She's certainly not a hero, but views Kulle as a multiversal threat, which includes her pirating life of adventure.
> 5. Nicolo's study is on the second floor of The Manor, it occupies about half of the region and contains prolific works of music as well as minor relics and artifacts -- "pieces of music" that can be used like puzzles, unlocking experience on sufficient player rolls.
> 6. Leoric is only semi-sentient at this point and guards the basement (bottom) floor of The Manor.
> 7. Maghda is only a fragment of her CR17 power, however, her ghost still occupies her personal quarters, which expands over the entire third (top) floor of The Manor.

> **Quest Objective(s):**
> 1. The main objective is to find the source of the dark magical fog (which is emanating from Maghda's quarters).
> 2. To get to the third floor, the first and second floors will have to be cleared.
> 3. The third floor entrance is locked, Leoric (found in the basement) has the key. 
> 4. Completion - upon defeating Nicolo, Leoric, and Maghda, the party will find a portal to [The Pandemonium Fortress](https://diablo.fandom.com/wiki/Pandemonium_Fortress), a microcosm of the [Pandemonium Realm](https://forgottenrealms.fandom.com/wiki/Pandemonium).  Unlocking, entering, the fallout of the portal will a separate quest.
> 5. Environment and Monster Types:
	> Basement - undead, medieval, underdark
	> First Floor - witches, forest beasts
	> Second Floor - gothic and medieval, fiends and undead
	> Third Floor - only Maghda, but many fey and abyssal magical tones.

> **Current Situation**: above