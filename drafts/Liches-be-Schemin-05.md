---
title: "Liches Be Schemin', Part 5"
description: ""
date:
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 02 - Liches Be Schemin'
Session: 05
```

### Current Situation:

> The party has returned to Sanctuary, in Maghda and Leoric's Manor.  They're greeted by a confused Liliana who's returned from Westmarch after defeating Maghda.

### Played Adventure:

in development ...

played adventure summary here...

---

### Notes:

* [Full Chat Transcript](coming soon)

**User Input:**

> DM Role:

> Player Characters:

> Antagonists:

> Personal Stakes:

> Current Situation:  (above)


* They can further inform the party on their research that Kulle will need to gather 7 [Soul Shards](https://diablo.fandom.com/wiki/Soul_Shards); which is probably easier to find, protect, and forge into a Black Soulstone with the help of a lithomancer (Nahiri).
* Why or how didn't Maghda kill / fight Zakk when she re-entered the Manor to get to the rest of the party at the Pandemonium Fortress portal?

> Success Conditions:



> * role playing set up for soul shards
> * party meets back up with Lily
> * the stone heads of the wisemen are thrown at the party
> * Nahiri and Kulle are in the distance sealing caverns and mountains around the Leoric's Manor; do they have the [Dregs of Lies](https://diablo.fandom.com/wiki/Soul_Shards)
> * certain disagreement between party about whether to stay and prep in the manor, or not waste any more time and go after Kulle and Nahiri now.

---

### Played Adventure:

Coming soon...

---

### Notes:

* Literal Transcript: [Chat Link](coming soon)