### Quest Logs

> A simple text-based blog for narrative storytelling of tabletop RPG adventures.

> Hosted at [https://savagezen.gitlab.io/quest-logs](https://quest-logs-d0b519.gitlab.io/)

---

* Source Code: [savagezen/quest-logs](https://gitlab.comm/savagezen/quest-logs)
* Built with [Hugo](https://gohugo.io/), [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), and [Hugo-Ficurinia Theme](https://gitlab.com/gabmus/hugo-ficurinia).

---

To Do List:

* Custom Domain Name URL