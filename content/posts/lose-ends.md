---
title: "Lose Ends"
description: "Opportunities left untold for one-shot adventures and future campaigns!"
tags: [DND5E, Dawn Treaders, Black Label Society]
---

> Opportunities left untold for one-shot adventures and future campaigns!

---

### Campaign Opportunities:

**Game Setting: Sanctuary**

* Low Level:
  * Any of the one-shot adventures below could be combined to start a full campaign.
  * Much of the Eastern Hemisphere (north deserts and south jungle / swamps) in Sanctuary remains unexplored.
* Mid Level:
  * *King Odin and The Dark Wander*: Given the context from the Dawn Treaders campaign, a retro-campaign could take place where King Odin, in his youth, fulfills the role of The Warrior in [Diablo 1](https://diablo-archive.fandom.com/wiki/Diablo_I).  In the Dawn Treaders campaign he wanders -- hence The Dark Wanderer -- which could also leaven an opportunity to play out Diablo II (see Mount Arete below).
  * [Karasu](/posts/homebrew/npc-karasu/) is still at large and unresolved from the Dawn Treaders Campaign.  While the Council of Dexa is not happy about Liliana's escape during *Born Under a Bad Sign*, this may more immediately occupy their attention.
* High Level:
  * Cas and The Twilight Company venture through the Feywilds to battle Sitariel.
  * While watching over Ensteig, Alodel finds that several [Ancient Leshens (CR20)](https://i.pinimg.com/736x/92/2a/aa/922aaa4f8d5500af040ab1953468d193.jpg) threaten the neighboring Kokiri Forest.

---

### One-Shot Adventures:

**Game Setting: Sanctuary**

* *Tomb of Queen Paragrini* (CR5): Bordering the forests north of Tristram, and the southern edge of the Dreadlands, there are a series of mines, never opened during a quest in the Dawn Treaders campaign.
* *The Abyss and Caldeum* (CR5-8): The Dawn Treaders campaign left an Abyssal portal engulfing the castle and threatening the entire city.  The original party fled the scene.
* *The Dreadland Channel* (CR any): A channel was being built to bridge the western and central seas, spanning from south of Cellan to east of Heythorp.  However, any and all able bodied fighters were recruited to fight in the north with the barbarians of Bastion’s Keep after the Supernatural Winter set in.  Further, the limited labor forces gave a foothold to a bitter business dispute between Cellan and Heythorp which resulted in cessation of the channel’s construction.
* *Home Improvement* (CR any):  In the middle of the Dawn Treaders campaign, when the company inherited control of the citadel of Entsteig; the townsfolk were tasked with procuring several enhancements to the city within 75 days:  Training Stable / Facility (1,000 GP budget), DC15 Vault (750 GP budget), Warning / Alarm system.  This could be further expanded as Alodel (CR20) presides over the city after the conclusion of the Dawn Treaders campaign.
*Bastion's Keep and Mount Arete (~CR12-15):* King Odin ventured into the Mount Arete and has not returned.  Before leaving he made the warrior of Bastion's Keep (Marvel's Avengers) vow to not come for him.  His son, Prince Thor, has been driven to maddening grief, but recently came into possession of two swords made of legendary stone which may be re-forged into a new hammer for the prince; should he choose to disobey his father and venture into the perils of the Arete Crater.  This could be a nice throwback to [Diablo II](https://diablo-archive.fandom.com/wiki/Diablo_II#Story); alternatively King Odin could fulfill the role of [The Dark Wanderer in Diablo I](https://diablo-archive.fandom.com/wiki/Diablo_I) for an entirely different campaign.
* Cas's gladiator battle bracket from *Born Under a Bad Sign*.
* Avengers and GOTG vs. Jadis's Winter Army, concurrent with *Born Under a Bad Sign*.  Creatures include: duergar, werewolves, wriaths, bheur hags, frost giants and one white dragon.
* Cas, Alodel, Digoria, and Liliana vs. [Jadis](/posts/homebrew/NPC-jadis-visage).
* Cas and Alodel travel by ship from Xiansai to Entsteig, but the seas are no longer calm.