---
title: "Link"
description: "A half-elf adventurer, protector of the Kokiri Forest, who is still growing into his adventuring boots."
tags: [DND5E, Characters]
---

```
Race: Half-Elf
Class: Ranger
Background: Folk Hero
Level: 5
Alignment: Neutral Good
```

### Character Description:

Regardless of incarnations in other universes, the Link of this universe is somewhat immature and boyish.  While he idealizes heroism and bravery, his actions often betray the fact that he is still growing into his adventurer role.

Like a good anime support character, he's loyal to the team, will support the leader and the mission, and occasionally gets caught with heavy perverted glances and comments.

![](/images/link.png)

---

### Character Sheet:

* [archived Character Sheet](/docs/PC-link.pdf)

---

### Notes:

**Campaigns:**

* [Dawn Treaders](/tags/dawn-treaders)

**Character Inspiration:**

* [Link, Legend of Zelda](https://zelda.fandom.com/wiki/Link)