---
title: "Rhogar McGee"
description: "A lewd and lovable dwarf with a fondness for hammers, dogs, heavy ale, and chewing tobacco." 
tags: [DND5E, Characters]
---

```
Race: Mountain Dwarf
Class: Barbarian / Druid
Background: Beastmaster
Level: 1
Alignment: Neutral Good
```

### Character Description:

Rhogar is stout and strong, just as he likes his ale...and women.  He has a talent for training, and is constantly accompanied by one or more [blink dogs](https://forgottenrealms.fandom.com/wiki/Blink_dog), whether they're visible to bystanders or not.

Rhogar's love for many this is heaver than the hammer he wields.  He extremely loyal to his comrades and has an endearing personality in spite of his incessant spitting of tobacco juice.  Rhogar is also fond of nature and views himself as both a steward to and protector of his homeland.

![](/images/rhogar.png)

---

### Character Sheet:

* [Temporary Character Sheet](/docs/PC-rhogar.pdf)

### Notes:

* Character Inspiration: [Scotsman, Samourai Jack](https://samuraijack.fandom.com/wiki/Scotsman)