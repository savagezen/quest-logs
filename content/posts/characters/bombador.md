---
title: "Balfador Bombador"
description: "A hill dwarf rogue and reformed thief skilled in demolition."
tags: [DND5E, Characters]
---

```
Race: Hill Dwarf
Class: Rogue
Background: Demolition, Burglar (reformed)
Level: 3
Alignment: Chaotic Neutral
```

### Character Description:

If you need a job done, and don't much care about who or what is left standing, Balfador is the man to call.  Balfador is well acquainted with the criminal underground and thievery of many cities and towns.  Uncharacteristically, he abstains from drinking alcohol, but is a connoisseur of both magical and non-magical cigars.

![](/images/bombador.jpg)

---

### Character Sheet:

* [temporary character sheet](/docs/PC-balfador.pdf)

---

### Notes:

**Campaigns:**

* [Dawn Treaders](/tags/dawn-treaders), *Fane of the Drow (only)*