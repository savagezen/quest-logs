---
title: "Alodel Erwer"
description: "Alodel is a high elf war priest with traits of both sun and moon elves. He’s strong and intelligent, wielding maces and magical gear, seeking a place to belong."
tags: [DND5E, Characters]
---

```
Race: High-Elf
Class: Cleric
Background: War Priest (Kiri-Jolith)
Level: 20
Alignment: Neutral Good
```

### Character Description:

Alodel is a high elf. His specific subrace is not known. His personality is similar to that of moon elves, but his appearance is more akin to a sun elf. It is possible that his family line is a mixture of both, as sometimes occurs. His is on the upper end of elves in height, at just over 6 feet tall.

He is abnormally heavy compared to his kin, weighing in at 170 pounds. This could be due to his war priest training, which focuses heavily on physical strength, an uncommon occurrence in elven communities.

Alodel prefers to fight lightly armored, typically on wearing studded leather armor. He does carry a magical shield known as a Sentinel Shield, which enhances his perceptiveness while adorned. He recently acquired a robe of stars that covers most of his form. Maces are his weapon of choice, and he currently alternates between a Mace of Disruption and a Heavenly Mace, depending on the situations he expects to encounter.

![](/images/alodel.jpg)

Alodel is intelligent and often not afraid to display this fact. While he doesn't refer to himself as such, he often shows his smarts through his quick recall of obscure facts and rapid problem solving. Although many consider him up-tight, he has exhibited a sense of humor on several occasions.

He is nearly always reasonable and level-headed, although when the right buttons are pressed, he can quickly become ill-tempered and stand-offish. Alodel appears to be as comfortable operating in the background as he is coming to the fore and leading others. While he mostly looks out for the welfare of others, there have been occasions where lapses of judgement occur and he puts the innocent at risk when combating a greater evil.

He is also a misfit, leaving both his family and his clergy due to ideological differences. He doesn't speak of these problems much, but he secretly wonders when and where he will find his place in the world.

---

### Character Sheet:

* [Archived Character Sheet](/docs/PC-alodel.pdf)

---

### Notes:

**Campaigns:**

* [Dawn Treaders](/tags/dawn-treaders)

**Creator / Player:**

* Tyler
* [original character description](https://dtgl.fandom.com/wiki/Alodel_Erwer)