---
title: "Cas"
description: "Cas is a fierce fighter with a strong moral code and deep loyalty to his allies. Driven by his past, he faces danger head-on and strives to protect those around him."
tags: [DND5E, Characters]
---

```
Race: Human
Class: Fighter
Background: 
Level: 20
Alignment: Chaotic Good
```

### Character Description:

Despite his origin, Cas is anatomically a human. He is large among his kind, at 6'3" and 250 pounds. In his earlier days, his hair was nearly brown. As time has passed, it has become more blonde. When transformed, it is nearly white. His eyes, however, have maintained their sharp green hue across his life. Cas is well muscled and lean, playing much to his companion's chiding him as a lunkhead.

While often caught in a less adorned state, he always wears his battle-worn chainmail and carries his Twin Swords of Fury into battle. He is also currently sporting a pair of less than masculine Boots of the Winterlands that were a gift from Digoria after leaving her allies stranded for the time being. Cas has jokingly named the furry boots Ted and Fred because he believes they look like bunnies.

![](/images/cas.jpg)

Cas is righteous, but reckless at times. He is an intelligent combatant but often makes foolish errors when his blood isn't pumping. He tends to be sarcastic and resistant to authority, primarily when said authority is haughty. He can be a bit unforgiving if he feels that his companions are being taken advantage of or his kindness is being abused.

He also becomes grouchy and sharp-tongued when starved of food, alcohol, or daylight. Despite his flaws and brutish size, Cas is kind and willing to put himself at great risk to help others whom he deems allies or the innocent. He can also be accepting to a fault of those he considers friends. His amnesia leaves him secretly distressed and he worries about having to part ways with his companions over ideological differences since he has nobody else. He is also concerned by the fact that nobody from his past has even attempted to make contact with him.

This has caused him to wonder what kind of a person he must have been before losing his memory and is compounded by the nightmares that plague him during sober sleep. The dreams are never very clear, but always violent and unfailingly end with him looking into his own eyes. The face behind those eyes shadowed but covered in the blood of the innocent.

---

### Character Sheet:

* [Archived Character Sheet](/docs/PC-cas.pdf)

---

### Notes:

**Campaigns:**

* [Dawn Treaders](/tags/dawn-treaders)

**Creator / Player:**

* Ryan
* [character description and personality](https://dtgl.fandom.com/wiki/Cas)