---
Title: "Raiden"
description: "A Liberian-American mercenary and former special forces soldier; now known for his mostly cybernetic body and swordsmanship."
tags: [MHRP, Characters]
---

```
Game: Marvel Heroic Roleplaying
```

![](/images/raiden.png)



**Power Sets:**

* Cybernetic Body:
  * Enhanced Strength / Sword (D10)
  * Enhanced Reflexes (D8)
  * Enhanced Durability (D8)
  * Enhanced Speed (D6)
  * Cybernetic Senses (D8)
* Foxhound Special Agent
  * Electromagnetic Rail Gun (D10)
  * Assault Rifle, silenced (D8)
* Limits:
  * 100% power cell charge lasts ~1 scene pending intensity.  Can charge up to 200%.

**Specialities:**

* Combat Master (D10)
* Technology Expert (D8)
* Acrobatic Expert (D8)
* Weapons Expert (D8)

**Milestones:**

* 1xp MGS Soundobard.
* 3xp Forgotten Memories.
* 10xp Get betrayed or leave a team.

**Affiliations:**

* Solo: 10
* Buddy: 8
* Team: 6

**Distinctions:**

* Acrobatic and stealth fighting.
* "Time for Jack the Ripper to let 'er rip!"
* Morally conflicted and mentally unstable.

**Abilities and Resources:**

> Raiden's weapon of choice is a high frequency blade that can cut buildings and even adamantium with enough force.  Raiden has a cybernetic body, granding him 70% faster reflexes (90 w/ upgrade), 50 ton strength (100 ton with upgraded body), enough speed to run on walls, and invulnerable to physical pain.  Raiden requires frequent blood transfusions (oil change) and must frequently consume electromagnetic / nuclear energy to power his cybernetic body.

**Personality:**

> Contemplative and morally torn.  He suffers from post-traumatic stress and psychotic breaks / berserker rages.

**History:**

> Raiden was raised as a child soldier in Liberia, commanding troops and earning a notorious reputation and the nickname "Jack the Ripper" by age 10.  He was later abandoned by the terorrists who trainined him and was further trained by the secret government FOXHOUND unit.  From there he took part in numerous special forces operations against the secret society known as "The Patirots" -- as well as their war machines known as Metal Gear(s).  However, he was eventually captured and experiemented on; thus giving him a cybernetic body with only his brain and spinal cord remaining human.  Upon returning to "civilian life."  He began working "security" of Stark Industries, often tasked with taking out lower level criminals while Iron Man was tending to other matters.  he usues his clearance at Stark Industries to give himself a "pay raise" in the form of upgrading his cybernetics.    However, when Raiden learns of Stark's history and on and off support of "the war economy" the two have a falling out shortly after the events of The Civil War.

**Inspiration:**

> [Metal Gear Solid / Rising - Raiden](https://metalgear.fandom.com/wiki/Raiden)
