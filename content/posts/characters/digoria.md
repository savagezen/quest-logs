---
title: "Digoria Antica"
description: "An elemental sorceress-rogue turned hexblade warlock."
tags: [DND5E, Characters]
---

```
Race: Tiefling
Class: Warlock
Background: Hexblade
Level: 20
Alignment: Chaotic Neutral
```

### Character Description:

Digoria is mischievous, sly, and cunning.  Here early experiences of constantly being persecuted form all sides has molded her into a lone survivor, with difficulties fully trusting anyone other than herself.  Having been tormented by various entities she's betrayed and been tossed and torn between realms her sanity seems to always be in question.

![](/images/digoria.jpg)

While often portrayed as whimsical to the point of near psychosis -- *I may be the worst pirate you've ever heard of, but you have heard of me!* -- Digoria's ability to conceive and employ layered contingency plans should never be underestimated -- *I never make mistakes, even when I do.*

She is notorious for bargaining with demons, fiends, and all manner of ill-company for various artifacts and favors whilst double-crossing them -- *Magic has a price, and I"m willing to pay, or let someone else pay.*  Though she has a knack for always coming out on top and saving herself first, Digoria isn't beyond teamwork any more than stooping to whatever means necessary to achieve an objective.

### Character Sheet(s):

* [Google Drive Backup](/docs/PC-digoria.pdf)
* [D&D Beyond (temporary)](https://www.dndbeyond.com/characters/113863530/x6WxST)
* [Archive - Sorcerer/Rogue](/docs/PC-digoria-old.pdf)

---

### Notes:

**Campaigns:**

* [Dawn Treaders](/tags/dawn-treaders)
* [Black Label Society](/tags/black-label-society)

**Character Inspiration:**

* [Jack Sparrow, Pirates of the Carribean](https://pirates.fandom.com/wiki/Jack_Sparrow)
* [Bela Talbot, Supernatural](https://supernatural.fandom.com/wiki/Bela_Talbot)
* [John Constantine, DC](https://dc.fandom.com/wiki/John_Constantine)
* [original character description](https://dtgl.fandom.com/wiki/Digoria_Antica)