---
title: "NPC - Paganini"
description: "CR12 Devil's Violinist"
tags: [DND5E, Homebrew]
---

> *Medium Undead (Undead Bard), Chaotic Neutral*

---

**Armor Class** 16 (*Studded Leather Armor*)  
**Hit Points** 140 (20d8 + 40)  
**Speed** 30 ft.

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 10 (+0) | 20 (+5) | 14 (+2) | 12 (+1) | 13 (+1) | 22 (+6) |

---

**Saving Throws** Dex +11, Cha +12  
**Skills** Performance +12, Persuasion +12, Deception +12, Insight +7, Sleight of Hand +11  
**Damage Resistances** Necrotic, Psychic  
**Condition Immunities** Charmed, Frightened, Poisoned  
**Senses** Darkvision 60 ft., Passive Perception 13  
**Languages** Common, Elvish, Draconic  
**Challenge** 12 (8,400 XP)  
**Proficiency Bonus** +4  

---

### **Bardic Features**

**Bardic Inspiration (d10, 5/Day).** Niccolò can inspire others through stirring music or oration. As a bonus action, he can choose one creature within 60 feet that can hear him. That creature gains an inspiration die (1d10). The creature can add that die to one ability check, attack roll, or saving throw it makes. The inspiration die can be used once within the next 10 minutes.

**Undead Fortitude.** If damage reduces Niccolò to 0 hit points, he must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant. On a success, he drops to 1 hit point instead.

**Spellcasting.** Niccolò is a 10th-level spellcaster. His spellcasting ability is Charisma (spell save DC 20, +12 to hit with spell attacks). He has the following spells prepared:

- **Cantrips (at will):** *vicious mockery*, *dancing lights*, *prestidigitation*, *minor illusion*  
- **1st level (4 slots):** *charm person*, *disguise self*, *healing word*  
- **2nd level (3 slots):** *invisibility*, *suggestion*, *shatter*  
- **3rd level (3 slots):** *fear*, *hypnotic pattern*, *counterspell*  
- **4th level (3 slots):** *greater invisibility*, *dimension door*, *freedom of movement*  
- **5th level (2 slots):** *modify memory*, *dominate person*  
- **6th level (1 slot):** *mass suggestion*  
- **7th level (1 slot):** *project image*  

---

### **Actions**

**Multiattack.** Niccolò can make two attacks with his *Violin of Enchantment*.

**Violin of Enchantment.** *Melee Weapon Attack:* +12 to hit, reach 5 ft., one target. *Hit:* 30 (6d10 + 5) psychic damage, and the target must succeed on a DC 20 Wisdom saving throw or become charmed by Niccolò for 1 minute. The charmed target regards Niccolò as a trusted friend to be heeded and protected. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

**Bardic Performance (Recharge 5-6).** Niccolò plays an enthralling melody. Each creature of his choice within 30 feet of him must make a DC 20 Wisdom saving throw or be incapacitated until the end of their next turn.

**Spectral Ensemble (1/Day).** Niccolò conjures a spectral band of undead musicians that lasts for 1 minute. He can create three spectral undead musicians that mimic the effects of the *Conjure Animals* spell, but with undead creatures. These musicians provide the following benefits: 
- Each undead musician can take a single action to assist Niccolò, providing advantage on an attack roll or ability check.

---

### **Reactions**

**Fascinating Performance.** When a creature Niccolò can see within 30 feet makes an attack roll, he can use his reaction to impose disadvantage on that roll by performing a distracting flourish on his violin.

---

### **Background & Personality**

**Background:** Niccolò is a wandering minstrel who made a dark pact with forces beyond the grave, granting him immortality at the cost of his humanity. Now an undead bard, he draws on the essence of life to fuel his enchanting music. His performances often border on the supernatural, captivating audiences with haunting melodies that evoke deep emotions, sometimes siphoning the very life from his listeners.

**Personality:** Niccolò is charismatic, enigmatic, and somewhat unpredictable. He thrives on attention and enjoys the thrill of performance, often incorporating elements of danger and allure into his acts. Though he seems carefree, he harbors a dark side, using his music to manipulate or control others for his amusement and sustenance.

**Ideal:** "Life is but a grand performance; play it to the fullest and seize every opportunity to shine."  
**Bond:** He feels a deep connection to his music and will go to great lengths to protect his violin, seeing it as an extension of his very soul.  
**Flaw:** Niccolò’s desire for fame and admiration can lead him to make reckless choices, often prioritizing spectacle over safety.

---

### **Combat Strategy**

In combat, Niccolò prefers to control the battlefield with his spells and abilities, using *hypnotic pattern* to incapacitate enemies while inspiring allies with *Bardic Inspiration*. He engages in melee with his *Violin of Enchantment* when necessary, aiming to charm opponents and turn them against each other. His ability to move in and out of combat with spells like *greater invisibility* allows him to stay elusive while causing chaos on the battlefield. Additionally, his **Undead Fortitude** grants him resilience against damage, making him a tenacious adversary.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Nicolo Paganini](https://en.wikipedia.org/wiki/Niccol%C3%B2_Paganini)
