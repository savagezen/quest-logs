---
title: "NPC - Jadis, the White Witch"
description: "CR23 Fey Frost Giant"
tags: [DND5E, Homebrew]
---

> *Large Fey Giant, Lawful Evil*

---

**Armor Class** 22 (*Natural Armor, Frost Ward*)  
**Hit Points** 425 (34d10 + 238)  
**Speed** 40 ft., *Ice Glide* (40 ft.)

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 24 (+7) | 18 (+4) | 24 (+7) | 22 (+6) | 18 (+4) | 26 (+8) |

---

**Saving Throws** Str +14, Con +14, Int +13, Wis +11, Cha +15  
**Skills** Arcana +13, Deception +15, Intimidation +15, Persuasion +15, Perception +11  
**Damage Resistances** Cold; bludgeoning, piercing, and slashing from nonmagical attacks  
**Damage Immunities** Cold, Psychic  
**Condition Immunities** Charmed, Frightened, Paralyzed, Petrified  
**Senses** Truesight 120 ft., Passive Perception 21  
**Languages** Common, Giant, Elvish, Sylvan, Infernal  
**Challenge** 23 (50,000 XP)  
**Proficiency Bonus** +7  

---

### **Traits**

**Legendary Resistance (3/Day).** If Jadis fails a saving throw, she can choose to succeed instead.

**Icewalker.** Jadis is immune to difficult terrain created by ice or snow. She can move across and stand on icy surfaces without needing to make an ability check.

**Regal Presence.** Creatures of Jadis's choice within 60 feet that can see or hear her must succeed on a DC 23 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. A creature that succeeds on the saving throw is immune to this feature for 24 hours.

**Frost Ward.** When a creature within 30 feet of Jadis hits her with an attack, they take 14 (4d6) cold damage as ice shards burst from her skin.

**Magic Resistance.** Jadis has advantage on saving throws against spells and other magical effects.

**Innate Spellcasting.** Jadis's innate spellcasting ability is Charisma (spell save DC 23, +15 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

- **At will:** *frostbite*, *ray of frost*, *ice knife*, *misty step*  
- **3/day each:** *cone of cold*, *ice storm*, *wall of ice*, *hypnotic pattern*  
- **1/day each:** *control weather*, *globe of invulnerability*, *mass suggestion*, *power word kill*

---

### **Actions**

**Multiattack.** Jadis makes three attacks with her *Icicle Spear* or uses her *Frozen Blast*.

**Icicle Spear.** *Melee or Ranged Weapon Attack:* +14 to hit, reach 10 ft. or range 60/180 ft., one target. *Hit:* 21 (3d8 + 7) piercing damage plus 18 (4d8) cold damage.

**Frozen Blast.** *Ranged Spell Attack:* +15 to hit, range 120 ft., one target. *Hit:* 32 (5d10 + 5) cold damage, and the target must succeed on a DC 23 Constitution saving throw or become restrained in ice for 1 minute. A restrained creature can make a DC 23 Strength check at the end of each of its turns, breaking free on a success.

**Wrath of Winter (Recharge 5-6).** Jadis summons a blizzard centered on a point she can see within 120 feet. Each creature within a 20-foot-radius sphere must make a DC 23 Dexterity saving throw, taking 54 (12d8) cold damage on a failed save, or half as much on a successful one. The area becomes difficult terrain for 1 minute, and any creature that starts its turn in the area takes 14 (4d6) cold damage.

---

### **Legendary Actions**

Jadis can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time, and only at the end of another creature’s turn. Jadis regains spent legendary actions at the start of her turn.

- **Cantrip.** Jadis casts a cantrip.
- **Move.** Jadis moves up to her speed without provoking opportunity attacks.
- **Chilling Gaze (Costs 2 Actions).** Jadis targets one creature she can see within 60 feet of her. The target must succeed on a DC 23 Wisdom saving throw or become paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
- **Summon Ice Elemental (Costs 3 Actions).** Jadis summons an *Ice Elemental* that appears in an unoccupied space within 30 feet of her. The elemental obeys her commands and acts immediately after her in initiative order. It disappears after 1 minute or when reduced to 0 hit points.

---

### **Background & Personality**

**Background:** Jadis hails from a realm of eternal winter, where she learned to harness the raw power of ice and darkness. Born from a lineage that mingles the ancient power of the fey with the strength of frost giants, she grew into a being of overwhelming magical might. After seizing control of a kingdom through manipulation and cruelty, she maintains a rule of iron and ice, crushing any resistance with her frostbitten hand.

**Personality:** Jadis is cold, calculating, and utterly ruthless. Her charisma and intelligence make her a master manipulator, delighting in bending others to her will and ruling with an iron fist. She views most mortals as beneath her, believing herself a rightful ruler destined to rule all she surveys. Despite her evil nature, she can exhibit a twisted sense of beauty and elegance, which she uses to charm and deceive.

**Ideal:** "The strong were meant to rule; the weak were meant to serve or perish."  

**Bond:** She believes herself the rightful ruler of all realms that touch upon winter, and she will not rest until her domain stretches over all.  

**Flaw:** Her arrogance makes her prone to underestimating her enemies, and her pride can lead her to overreach in her ambitions.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Chronicles of Narnia, Jadis](https://narnia.fandom.com/wiki/Jadis)
* Deity Ranks: Demigods ~CR20, Titans ~CR23, Lesser Gods ~CR30, Greater Gods are beyond mortal comprehension, ([reference (dmg.11)](https://dungeonsdragons.fandom.com/wiki/Divine_rank#D&D_5th_edition))
* 3.5e example of [Jadis as a demigod](https://www.dandwiki.com/wiki/Jadis_(3.5e_Deity))
* See also [Forgotten Realms, Auril](https://forgottenrealms.fandom.com/wiki/Auril) for comparison.