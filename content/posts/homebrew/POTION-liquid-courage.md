---
title: "Bottomless Flask of Liquid Courage"
description: "Wondrous Item (uncommon), potion"
tags: [DND5E, Homebrew, Item]
---

> *Potion, uncommon*

### Description:

A normal looking steel flask with an enchanted emblem.

---

### Use and Effects:

Drinking from the flask requires use of a *bonus action.*

One "charge" equates to consuming one ounce of fluid from the flask.  Once per day, at dusk, the flask refills itself with 1d4 charges.

The effects of each ounce of fluid consumed last for one hour and grant the following to the consumer:

* +1 to Strength checks
* +1 to Charisma checks
* -1 to Dexterity checks
* -1 to Intelligence checks
* curse of hiccups

If more than one ounce is consumed within one hour the effects stack (e.g. +2 STR/CHA and -2 DEX/INT) for the duration, however, the consumer must make also make a Constitution save (DC10 + number of charges consumed) or spend a bonus action vomiting each turn until the save is made, or the duration expires. 

---

* [D&D Beyond, Bottomless Flask Version 1](https://www.dndbeyond.com/magic-items/6002091-liquid-courage-bottomless-flask)