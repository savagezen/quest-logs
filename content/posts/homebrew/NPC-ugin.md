---
title: "NPC - Ugin, Spirit Dragon"
description: "CR24 Elder Dragon"
tags: [DND5E, Homebrew]
---

> *Gargantuan Dragon (Celestial), Neutral*

---

**Armor Class** 23 (*Natural Armor, Ethereal Shield*)  
**Hit Points** 465 (30d20 + 150)  
**Speed** 50 ft., fly 90 ft.

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 26 (+8) | 18 (+4) | 20 (+5) | 24 (+7) | 22 (+6) | 22 (+6) |

---

**Saving Throws** Str +15, Dex +11, Con +12, Int +14, Wis +13, Cha +13  
**Skills** Arcana +14, History +14, Insight +13, Perception +19  
**Damage Resistances** Radiant, Psychic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Force, Necrotic  
**Condition Immunities** Charmed, Frightened, Paralyzed  
**Senses** Truesight 120 ft., Passive Perception 29  
**Languages** Common, Draconic, Celestial, Telepathy 120 ft.  
**Challenge** 24 (62,000 XP)  
**Proficiency Bonus** +7  

---

### **Traits**

**Ethereal Presence.** Ugin exists partially in the Ethereal Plane. He can move through objects and creatures as if they were difficult terrain. If he ends his turn inside an object, he is immediately shunted to the nearest unoccupied space, taking 5 (1d10) force damage for every 5 feet he is moved.

**Master of the Ethereal.** Ugin's spells and abilities that deal force or radiant damage ignore resistance to those damage types, and treat immunity as resistance instead.

**Innate Spellcasting.** Ugin’s spellcasting ability is Intelligence (spell save DC 22, +14 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

- **At will:** *detect magic*, *etherealness*, *invisibility*, *dispel magic*  
- **3/day each:** *banishment*, *plane shift* (self only), *disintegrate*, *telekinesis*  
- **1/day each:** *maze*, *time stop*, *prismatic spray*, *sunbeam*  

**Legendary Resistance (3/Day).** If Ugin fails a saving throw, he can choose to succeed instead.

---

### **Actions**

**Multiattack.** Ugin makes three attacks: one with his *Claw*, one with his *Tail*, and one with *Spirit Breath* if available.

**Claw.** *Melee Weapon Attack:* +15 to hit, reach 15 ft., one target. *Hit:* 20 (3d8 + 8) slashing damage.

**Tail.** *Melee Weapon Attack:* +15 to hit, reach 20 ft., one target. *Hit:* 26 (3d10 + 8) bludgeoning damage.

**Spirit Breath (Recharge 5-6).** Ugin exhales ethereal fire in a 90-foot cone. Each creature in that area must make a DC 22 Dexterity saving throw, taking 66 (12d10) force damage on a failed save, or half as much on a successful one. Creatures reduced to 0 hit points by this breath weapon are immediately transported to the Ethereal Plane and become trapped there for 1 minute.

**Astral Pulse (Recharge 5-6).** Ugin releases a wave of spiritual energy. Each creature of his choice within 60 feet must make a DC 22 Wisdom saving throw, taking 55 (10d10) psychic damage on a failed save, or half as much on a successful one. A creature that fails the save is also stunned until the end of its next turn.

---

### **Legendary Actions**

Ugin can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time, and only at the end of another creature’s turn. Ugin regains spent legendary actions at the start of his turn.

- **Detect.** Ugin makes a Wisdom (Perception) check.
- **Spiritual Displacement.** Ugin shifts into the Ethereal Plane until the end of his next turn, reappearing in an unoccupied space within 30 feet of his original location.
- **Astral Strike (Costs 2 Actions).** Ugin makes a *Claw* attack, and the target must succeed on a DC 22 Intelligence saving throw or take an additional 22 (4d10) psychic damage.

---

### **Lair Actions**

When fighting within his lair or a location of great spiritual significance, Ugin can invoke the following lair actions. On initiative count 20 (losing initiative ties), he can take one lair action to cause one of the following effects:

- **Spirit Ward.** Ugin creates a spectral barrier in a 20-foot radius sphere centered on a point he can see within 60 feet. The barrier lasts until initiative count 20 on the next round. Creatures within the sphere have advantage on saving throws against spells and magical effects, and creatures outside cannot see into or through the sphere.
- **Ethereal Surge.** Ugin causes the boundary between the Material Plane and the Ethereal Plane to weaken. Each creature within 30 feet of him must make a DC 22 Charisma saving throw or be shifted into the Ethereal Plane until the end of its next turn.
- **Shifting Shadows.** Ugin creates areas of shifting, ghostly light in up to three 10-foot cubes within 60 feet of him. Each area is heavily obscured and filled with spectral apparitions. The apparitions last until initiative count 20 on the next round.

---

### **Background & Personality**

**Background:** Ugin is a primordial spirit dragon, ancient and wise beyond mortal understanding. He has dedicated himself to maintaining the balance between worlds and understanding the fundamental forces that govern the multiverse. Though his approach to balance is often cold and detached, he is willing to intervene when he sees threats to the natural order.

**Personality:** Ugin is contemplative, calculating, and often aloof, focusing on the bigger picture and long-term consequences over immediate concerns. He values knowledge and understanding, sometimes to the point of seeming indifferent to mortal struggles. He sees himself as a guardian of cosmic order, yet he is not without compassion for those who seek knowledge or understanding.

**Ideal:** "The multiverse must be balanced. All things must find their place in the weave of existence."  
**Bond:** He feels a deep connection to the spirit realms and those who seek to maintain order in the face of chaos.  
**Flaw:** His detachment from mortal concerns can lead to underestimating the emotional complexities of those he encounters, which sometimes blinds him to threats that operate on a more personal level.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration [Magic the Gathering, Ugin](https://mtg.fandom.com/wiki/Ugin)
* [Archived - Original Ugin CR24](/docs/archive/ARCHIVE-ugin-CR24.jpeg)