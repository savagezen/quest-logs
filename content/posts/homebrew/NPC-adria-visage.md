---
title: "NPC - Adria, visage of"
description: "Astral projection of the blood demon consort of Diablo"
tags: [DND5E, Homebrew]
---

*Large Fiend (Demon), Chaotic Evil*  
**Armor Class:** 16 (*Natural Armor*)  
**Hit Points:** 178 (17d10 + 85)  
**Speed:** 30 ft.

---

| STR | DEX | CON | INT | WIS | CHA |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 14 (+2) | 18 (+4) | 20 (+5) | 18 (+4) | 16 (+3) | 19 (+4) |

---

**Saving Throws:** Dex +9, Con +10, Wis +8, Cha +9  
**Skills:** Arcana +9, Deception +9, Insight +8, Persuasion +9  
**Damage Resistances:** Cold, Fire; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities:** Poison, Necrotic  
**Condition Immunities:** Charmed, Frightened, Poisoned  
**Senses:** Darkvision 120 ft., Passive Perception 13  
**Languages:** Abyssal, Common, Infernal, telepathy 60 ft.  
**Challenge Rating:** 12 (8,400 XP)  
**Proficiency Bonus:** +5

---

### **Traits**

**Fiendish Nature.** Adria’s Visage doesn’t require air, food, drink, or sleep.

**Magic Resistance.** Adria's Visage has advantage on saving throws against spells and other magical effects.

**Aura of Blood.** At the start of each of Adria's turns, creatures of her choice within 10 feet take 7 (2d6) necrotic damage as their blood is siphoned by her dark influence. This also grants Adria temporary hit points equal to the damage dealt.

**Blood Sacrifice.** When a creature within 30 feet of Adria's Visage drops to 0 hit points, she can use her reaction to regain 15 hit points.

---

### **Actions**

**Multiattack.** Adria's Visage makes two *Claws* attacks or casts a spell and makes one *Claws* attack.

**Claws.** *Melee Weapon Attack:* +9 to hit, reach 10 ft., one target.  
*Hit:* 14 (2d8 + 5) slashing damage plus 10 (3d6) necrotic damage.

**Corrupting Bolt (Recharge 5-6).** *Ranged Spell Attack:* +9 to hit, range 120 ft., one target.  
*Hit:* 45 (10d8) necrotic damage. On a failed DC 17 Constitution saving throw, the target is paralyzed until the end of its next turn.

**Blood Curse (1/Day).** Adria's Visage targets one creature within 60 feet that she can see. The target must succeed on a DC 17 Wisdom saving throw or be cursed with blood decay for 1 minute. While cursed, the target takes 10 (3d6) necrotic damage at the start of each of its turns and has disadvantage on attack rolls and ability checks. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

---

### **Bonus Actions**

**Sanguine Shift.** Adria's Visage teleports up to 30 feet to an unoccupied space she can see, leaving behind a pool of dark blood. Any creature that enters the area for the first time on a turn or starts its turn there takes 10 (3d6) necrotic damage.

---

### **Legendary Actions** (3/Day)

Adria's Visage can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Adria's Visage regains spent legendary actions at the start of her turn.

- **Claws Attack.** Adria's Visage makes one *Claws* attack.
- **Blood Drain (Costs 2 Actions).** Adria's Visage targets a creature within 30 feet. The target must succeed on a DC 17 Constitution saving throw or take 18 (4d8) necrotic damage, and Adria regains hit points equal to the damage dealt.
- **Blood Mist (Costs 3 Actions).** Adria's Visage creates a thick mist of blood in a 20-foot radius centered on herself. The area is heavily obscured until the end of her next turn, and creatures inside take 10 (3d6) necrotic damage at the start of their turns.

---

### **Spellcasting**

Adria's Visage is a 14th-level spellcaster. Her spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). She has the following warlock spells prepared:

- **Cantrips (at will):** *Chill Touch,* *Mage Hand,* *Minor Illusion,* *Toll the Dead*  
- **1st level (4 slots):** *Armor of Agathys,* *Hex,* *Hellish Rebuke*  
- **2nd level (3 slots):** *Misty Step,* *Mirror Image*  
- **3rd level (3 slots):** *Counterspell,* *Fear*  
- **4th level (3 slots):** *Greater Invisibility,* *Blight*  
- **5th level (2 slots):** *Cloudkill,* *Hold Monster*  
- **6th level (1 slot):** *Circle of Death*  
- **7th level (1 slot):** *Finger of Death*

---

### **Lair Actions**

When fighting inside her lair, Adria’s Visage can invoke lair actions. On initiative count 20 (losing initiative ties), she can take one of the following lair actions:

- **Darkness Ascendant:** A wave of darkness fills a 20-foot radius centered on a point Adria can see within 60 feet. Creatures in the area must succeed on a DC 17 Dexterity saving throw or be blinded until the end of their next turn.
- **Blood Surge:** Adria’s Visage draws on the power of the blood spilled in her lair. She regains 20 hit points.
- **Shadow Tendrils:** Black tendrils erupt from the ground in a 20-foot radius centered on a point Adria can see within 60 feet. Creatures in the area must make a DC 17 Strength saving throw or be restrained until the end of Adria’s next turn.

---

### **Combat Tactics**

Adria's Visage uses her *Blood Curse* to weaken foes, prioritizing targets that are isolated or pose a significant threat. She uses *Sanguine Shift* to reposition and maintain her advantage in combat, often leaving behind pools of damaging blood to restrict enemy movement. In dire situations, she retreats into the shadows using *Greater Invisibility* or surrounds herself with *Blood Mist* to create chaos. She is a formidable foe even in this diminished state, capable of draining the life of her enemies while maintaining a dark, eerie control over the battlefield.

---

**Background & Personality:**  
Even in this diminished form, Adria’s Visage is a cunning and manipulative entity, constantly seeking a way to restore her former power. She remains dedicated to the demonic powers she once served, using her influence and blood magic to bend others to her will. Her presence is a shadow of what she once was, yet her cruelty and ambition remain intact. She revels in the fear she creates, using it as a tool to bend others to her dark desires.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Diablo 3, Adria](https://diablo.fandom.com/wiki/Adria)