---
title: "NPC - Jadis, visage of"
description: ""
tags: [DND5E, Homebrew]
---

> *Large Fey (Giant), Lawful Evil*

---

**Armor Class** 19 (*Natural Armor*)  
**Hit Points** 310 (23d10 + 161)  
**Speed** 30 ft., *hover* 30 ft. (levitates above the ground in a supernatural glide)

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 22 (+6) | 14 (+2) | 24 (+7) | 18 (+4) | 16 (+3) | 21 (+5) |

---

**Saving Throws** Con +13, Wis +9, Cha +11  
**Skills** Arcana +10, Deception +11, Intimidation +11, Persuasion +11  
**Damage Resistances** Cold, Psychic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Cold, Poison  
**Condition Immunities** Charmed, Frightened, Paralyzed, Poisoned  
**Senses** Truesight 60 ft., Darkvision 120 ft., Passive Perception 13  
**Languages** Common, Giant, Sylvan, Primordial, Abyssal, Telepathy 120 ft.  
**Challenge** 18 (20,000 XP)  
**Proficiency Bonus** +5  

---

### **Traits**

**Aura of Frost.** A frigid aura surrounds Jadis' Visage in a 10-foot radius. Creatures that start their turn within this area take 10 (3d6) cold damage. Additionally, the ground in this area becomes icy and difficult terrain for all creatures except Jadis' Visage.

**Magic Resistance.** Jadis' Visage has advantage on saving throws against spells and other magical effects.

**Magic Weapons.** The attacks made by Jadis' Visage are considered magical.

**Unyielding Presence.** As a mere visage of her former power, Jadis can project an aura of fearsome authority. Creatures of her choice within 30 feet must make a DC 19 Wisdom saving throw at the start of their turn or become frightened of her until the end of their next turn.

---

### **Actions**

**Multiattack.** Jadis' Visage makes three attacks: one with *Frost Staff* and two with *Frost Touch*.

**Frost Staff.** *Melee Weapon Attack:* +11 to hit, reach 10 ft., one target. *Hit:* 19 (3d8 + 6) bludgeoning damage plus 14 (4d6) cold damage.

**Frost Touch.** *Melee Spell Attack:* +11 to hit, reach 5 ft., one target. *Hit:* 22 (4d8 + 6) cold damage, and the target must succeed on a DC 19 Constitution saving throw or become paralyzed until the end of its next turn.

**Ice Shard Barrage (Recharge 5-6).** Jadis' Visage conjures a storm of razor-sharp ice shards. Each creature in a 30-foot cone must make a DC 19 Dexterity saving throw, taking 45 (10d8) cold damage on a failed save, or half as much damage on a successful one. Creatures that fail the saving throw are also restrained by ice until the end of their next turn.

**Winter's Command (1/Day).** Jadis' Visage channels the power of winter, summoning a blizzard in a 60-foot radius centered on herself for 1 minute. The area becomes heavily obscured, and nonmagical flames in the area are extinguished. Each creature of her choice in this area must make a DC 19 Constitution saving throw at the start of each of its turns or take 18 (4d8) cold damage and have its speed reduced to 0 until the start of its next turn.

---

### **Reactions**

**Glacial Rebuke.** When a creature within 10 feet of Jadis' Visage hits her with an attack, she can use her reaction to release a burst of freezing energy. The attacker must make a DC 19 Constitution saving throw, taking 21 (6d6) cold damage on a failed save or half as much damage on a successful one.

---

### **Legendary Actions** (3/Day)

Jadis' Visage can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Jadis' Visage regains spent legendary actions at the start of her turn.

- **Ice Step.** Jadis' Visage teleports up to 20 feet to an unoccupied space she can see, leaving behind a patch of ice that counts as difficult terrain until the end of her next turn.
  
- **Frost Touch.** Jadis' Visage makes one *Frost Touch* attack.

- **Chilling Command (Costs 2 Actions).** Jadis' Visage targets one creature she can see within 60 feet. The target must succeed on a DC 19 Charisma saving throw or be charmed by her for 1 minute. While charmed in this way, the target takes 7 (2d6) cold damage at the start of each of its turns. The charmed creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

---

### **Lair Actions**

If Jadis' Visage is encountered within her domain or a location that reflects her power, she may take lair actions on initiative count 20 (losing initiative ties). She can use each lair action once per round:

- **Ice Mirror.** Jadis' Visage creates an illusory double of herself in an unoccupied space she can see within 60 feet. The mirror lasts until destroyed (AC 15, 10 HP) or until Jadis dismisses it as a bonus action. The mirror projects an aura of cold, dealing 5 (1d10) cold damage to any creature that starts its turn within 5 feet of it.

- **Icy Winds.** Bitter winds sweep through the area. Each creature of her choice within 60 feet must succeed on a DC 19 Strength saving throw or be pushed 20 feet away from her and knocked prone.

---

### **Background & Personality**

**Background:** A once-mighty lesser deity, Jadis' Visage is the lingering shadow of her former self. In life, she ruled with a cold, iron fist, blending the raw strength of her frost giant bloodline with the enchanting and manipulative nature of her fey heritage. Now reduced to a mere spectral echo, she roams desolate, icy realms and despoiled landscapes, still clinging to fragments of her power and regal presence.

**Personality:** Jadis' Visage is haughty, domineering, and cruel. Her ambitions of conquest and dominance linger, but she is now constrained by the limitations of her weakened state. She despises those who remind her of her diminished power, but she remains calculating and cunning, using her remaining strength to manipulate and control others.

**Ideal:** "Only those with true power are fit to rule."  
**Bond:** She is obsessed with restoring her former might and views any who could aid or hinder her goals as tools or obstacles.  
**Flaw:** Her pride often blinds her to potential threats, leading her to underestimate those she sees as inferior.

---

### **Combat Strategy**

Jadis' Visage prefers to control the battlefield, using abilities like *Winter's Command* and *Ice Shard Barrage* to limit the movements of her enemies while remaining mobile with *Ice Step*. She uses *Unyielding Presence* to keep foes at bay and prefers to engage enemies at a distance, only moving into melee when she is certain of an advantage. Her use of *Glacial Rebuke* ensures that those who strike her feel the sting of winter's wrath.

---

### Notes:

* Generated by ChatGPT
* Original Inspiration: [Chronicles of Narnia, Jadis](https://narnia.fandom.com/wiki/Jadis)