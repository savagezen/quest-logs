---
title: "NPC - Zulton Kulle"
description: ""
tags: [DND5E, Homebrew]
---

> *Medium Undead, Chaotic Neutral*  

---

**Armor Class:** 19 (*Natural Armor*)  
**Hit Points:** 235 (27d8 + 108)  
**Speed:** 30 ft., fly 40 ft. (hover)

---

| STR | DEX | CON | INT | WIS | CHA |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 14 (+2) | 16 (+3) | 18 (+4) | 22 (+6) | 16 (+3) | 18 (+4) |

---

**Saving Throws:** Int +12, Wis +9, Cha +10  
**Skills:** Arcana +16, History +12, Insight +9, Deception +10  
**Damage Resistances:** Cold, Lightning; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities:** Necrotic, Poison  
**Condition Immunities:** Charmed, Exhaustion, Frightened, Paralyzed, Poisoned  
**Senses:** Truesight 120 ft., Passive Perception 13  
**Languages:** Common, Infernal, Abyssal, Draconic, telepathy 120 ft.  
**Challenge Rating:** 21 (33,000 XP)  
**Proficiency Bonus:** +6

---

### **Traits**

**Legendary Resistance (3/Day).** If Zoltun Kulle fails a saving throw, he can choose to succeed instead.

**Rejuvenation.** If Zoltun Kulle is destroyed, his body reforms in 1d10 days near his phylactery unless it is destroyed. His phylactery is a large gem containing a fragment of his essence, hidden in a distant, well-guarded chamber.

**Arcane Reservoir.** Zoltun Kulle has an additional pool of 100 arcane points that he can expend to empower spells, restore spell slots, or amplify magical effects (like metamagic). Once depleted, the reservoir replenishes at dawn.

---

### **Actions**

**Multiattack.** Zoltun Kulle makes three *Arcane Blast* attacks.

**Arcane Blast.** *Ranged Spell Attack:* +12 to hit, range 120 ft., one target.  
*Hit:* 18 (4d8) force damage.

**Gravitational Vortex (Recharge 5-6).** Kulle creates a 20-foot-radius sphere of swirling gravitational force centered on a point he can see within 90 feet. Each creature in the area must succeed on a DC 20 Strength saving throw or be pulled 15 feet towards the center and take 27 (6d8) force damage. A creature that saves takes half damage and isn't pulled.

**Soulburn.** *Melee Spell Attack:* +12 to hit, reach 5 ft., one target.  
*Hit:* 32 (5d10 + 6) necrotic damage. The target must succeed on a DC 20 Constitution saving throw or be cursed for 1 minute. While cursed, the target has disadvantage on saving throws against spells.

---

### **Bonus Actions**

**Teleport (1/Day).** Zoltun Kulle magically teleports, along with any equipment he is wearing or carrying, up to 60 feet to an unoccupied space he can see.

---

### **Legendary Actions** (3/Turn)

Zoltun Kulle can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Zoltun Kulle regains spent legendary actions at the start of his turn.

- **Cantrip.** Kulle casts a cantrip.
- **Arcane Shield (Costs 2 Actions).** Zoltun Kulle surrounds himself with an arcane barrier. Until the start of his next turn, he gains +4 to AC and resistance to all damage.
- **Temporal Distortion (Costs 3 Actions).** Zoltun Kulle bends time, forcing one creature he can see within 60 feet to succeed on a DC 20 Wisdom saving throw or be stunned until the end of its next turn.

---

### **Spellcasting**

Zoltun Kulle is a 17th-level spellcaster. His spellcasting ability is Intelligence (spell save DC 20, +12 to hit with spell attacks). He has the following spells prepared:

- **Cantrips (at will):** *Chill Touch,* *Mage Hand,* *Ray of Frost,* *Mind Sliver*  
- **1st level (4 slots):** *Shield,* *Magic Missile,* *Mage Armor,* *Identify*  
- **2nd level (3 slots):** *Mirror Image,* *Misty Step,* *Hold Person*  
- **3rd level (3 slots):** *Counterspell,* *Dispel Magic,* *Lightning Bolt*  
- **4th level (3 slots):** *Greater Invisibility,* *Dimension Door,* *Blight*  
- **5th level (3 slots):** *Wall of Force,* *Cloudkill,* *Cone of Cold*  
- **6th level (1 slot):** *Disintegrate,* *Globe of Invulnerability*  
- **7th level (1 slot):** *Plane Shift,* *Finger of Death*  
- **8th level (1 slot):** *Power Word Stun*  
- **9th level (1 slot):** *Time Stop*  

---

### **Lair Actions**

When fighting in his lair, Zoltun Kulle can invoke the ambient magic to take lair actions. On initiative count 20 (losing initiative ties), he can use one of the following lair actions:

- **Arcane Surge.** Magical energy pulses in a 30-foot radius around Kulle. Each creature of his choice within this area must make a DC 20 Dexterity saving throw, taking 14 (4d6) force damage on a failed save, or half as much on a successful one.
- **Temporal Displacement.** Kulle distorts time around a point he can see within 60 feet. Each creature within 10 feet of the point must succeed on a DC 20 Wisdom saving throw or have its speed halved until the end of its next turn.
- **Golem Summoning.** Kulle summons two stone golems or constructs of a similar nature. They appear in unoccupied spaces within 30 feet of him and vanish if reduced to 0 hit points or if Kulle uses this lair action again.

---

### **Combat Tactics**

Zoltun Kulle is a strategic and manipulative opponent, using his control over time and space to keep enemies off-balance. He maintains distance with *Gravitational Vortex* and *Arcane Blast*, while using his *Arcane Shield* and spells like *Counterspell* to protect himself from threats. His ability to manipulate time with *Temporal Distortion* and *Time Stop* allows him to control the battlefield and target vulnerable enemies. When cornered, he employs his *Teleport* ability to reposition and continue his relentless assault.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration, [Diablo 3, Zulton Kulle](https://diablo.fandom.com/wiki/Zoltun_Kulle)