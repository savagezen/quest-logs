---
title: "Snow Globe of the Winter Cabin"
description: "Wondrous Item (rare)"
tags: [DND5E, Homebrew, Item]
---

> *Wondrous Item (rare)*

---

### Description:

This small orb features a wintery scene of a wooden cabin with snow on the ground. When the snow globe is shaken and placed on a flat surface, you may teleport yourself and up to five willing creatures inside the extradimensional space of the snow globe. Once inside, you will have access to a cabin complete with water, rations, wood, and a stove to keep warm.

If the snow globe is shaken while creatures are inside of it, if the orb is destroyed, or after 8 hours has elapsed, all creatures within it will be ejected and appear in an unoccupied space closest to the orb. Any items left inside the orb also appear in a space outside of the snow globe.

---

### Notes:

* Original inspiration: [wallydm.com](https://www.wallydm.com/dnd-holiday-magic-items-christmas/#item6)