---
title: "The Chain Veil"
description: "Artifact (legendary), requires attunement"
tags: [DND5E, Homebrew, Item]
---

> *Artifact (legendary), requires attunement*

---

### Description:

The Chain Veil is an ancient and powerful artifact that was hidden within the ancient catacombs of the Onakke at Shandalar.  The Chain veil is made of burnished gold and is extremely exquisitely crafted, so fine it appears to have the texture of silk.  When worn it covers the face.

It was created to turn someone into a vessel for the resurrection of the long-dead Onakke.  It kills its users if they're not strong enough.  Others are turned into a demon.  It masks the wearer's soul from those who would take it.

---

### Abilities:

**Protection of the Onakke:**

The wearer of The Chain Veil has resistance to damage from all sources.

**Snuff Out:**

Whenever the wearer of The Chain Veil deals necrotic damage, they deal the maximum amount of damage and that damage ignores any resistances or immunities that would normally apply.

**Choice of Damnation:**

At the end of each turn, while wearing The Chain Veil, the wearer loses 3 hit points and must succeed on a DC 13 Constitution saving throw or gain one level of exhaustion.  The hit point loss and saving throw DC increases by 1 each consecutive round that The Chain Veil is worn.

---

### Notes:

* [Liliana Vess NPC](/docs/NPC-liliana-vess.pdf)
* [MTG Fandom - Chain Veil](https://mtg.fandom.com/wiki/Chain_Veil)