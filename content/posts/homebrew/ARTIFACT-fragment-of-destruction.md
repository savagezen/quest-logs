---
title: "The Fragment of Destruction"
description: "One of the 7 Soul Shards of the Black Soulstone, Baal's Essence"
tags: [DND5E, Homebrew]
---

> Wondrous Item (Soul Shard), Legendary (Requires Attunement)

The Fragment of Destruction channels the essence of Baal, the Lord of Destruction, causing chaos and devastation.

### Abilities:

* Destructive Wave: As an action, create a 30-foot cone of force damage (8d6). DC 18 Constitution saving throw or be knocked prone. Recharges after a long rest.
* Ruinous Might: When you hit a target with a melee attack, deal an extra 3d6 force damage. Structures take double damage.
* Chaos Pulse: Cast shatter at 4th level without using a spell slot (DC 18). Recharges after a short rest.

### Cursed Power:

The wielder becomes reckless, suffering disadvantage on all Wisdom saving throws and being compelled to attack targets until they are destroyed.

### Awakened Power:

When reduced to 0 hit points, the wielder can release a devastating shockwave, dealing 10d6 force damage to all creatures within 30 feet. Usable once per week.

### Destroying the Fragment of Destruction:

It must be shattered using a celestial hammer forged by the hands of an angel, under a solar eclipse.

---

### Notes

* Generated by ChatGPT
* Original Inspiration: [Diablo 3, 7 Soul Shards](https://diablo.fandom.com/wiki/Soul_Shards)
