---
title: "NPC - King Leoric, The Skeleton King"
description: "CR16 Undead Knight"
tags: [DND5E, Homebrew]
---

> *Large Undead, Lawful Evil*  
___  
**Armor Class** 18 (Plate Armor)  
**Hit Points** 275 (22d10 + 154)  
**Speed** 40 ft.  
___  
### **STR 22 (+6) | DEX 12 (+1) | CON 24 (+7) | INT 14 (+2) | WIS 16 (+3) | CHA 18 (+4)**  
___  
**Saving Throws** Str +11, Con +12, Wis +8  
**Skills** Athletics +11, Intimidation +9, Perception +8  
**Damage Resistances** Cold, Lightning, Necrotic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Poison, Psychic  
**Condition Immunities** Charmed, Exhaustion, Frightened, Poisoned  
**Senses** Darkvision 120 ft., Passive Perception 18  
**Languages** Common, Infernal, Abyssal  
**Challenge** 16 (15,000 XP)  
___  

### **Legendary Undead.**  
Leoric has advantage on saving throws against being turned. If turned, he is affected for only 1 round.  

### **Undying Wrath.**  
When reduced to 0 hit points, Leoric’s skeletal form collapses but begins to reform over the next 1d4 rounds. If his remains are not consecrated with radiant energy (50 points total), he returns to unlife with half his hit points.  

### **Reign of the Black King (Recharge 5-6).**  
Leoric swings his massive greataxe in a whirlwind. All creatures within a 15-foot radius must make a DC 19 Dexterity saving throw, taking 45 (10d8) slashing damage on a failed save or half as much on a success.  

### **Royal Decree (Recharge 5-6).**  
Leoric calls forth spectral knights to aid him. Three spectral knights (CR 6 each) appear and fight until destroyed or dismissed.  

### **Actions**  

### **Multiattack.**  
Leoric makes three attacks with his **Tyrant’s Greataxe**.  

### **Tyrant’s Greataxe.** *Melee Weapon Attack:* +11 to hit, reach 10 ft., one target.  
*Hit:* 26 (4d8 + 6) slashing damage plus 10 (3d6) necrotic damage.  

### **Tomb Charge.**  
Leoric moves up to his speed in a straight line. Each creature in his path must make a DC 19 Strength saving throw or take 22 (4d8 + 4) bludgeoning damage and be knocked prone.  

### **Legendary Actions** (3 per round)  
Leoric can take 3 legendary actions, using one at a time at the end of another creature’s turn.  

- **Grave Smite.** (Costs 1 Action) Leoric makes one **Tyrant’s Greataxe** attack.  
- **Phantom Step.** (Costs 1 Action) Leoric teleports up to 30 feet to an unoccupied space he can see.  
- **Rise Again.** (Costs 2 Actions) Leoric animates one fallen humanoid as a skeleton or zombie under his control for 1 minute.  

___  
Leoric is a terrifying force of undeath, bound by madness and rage. His skeletal form refuses to rest, ensuring that those who challenge him will never forget his name.  

---

**Notes:**

* Character Inspiration: [King Leoric / Skeleton King](https://diablo.fandom.com/wiki/Leoric) from the Diablo series