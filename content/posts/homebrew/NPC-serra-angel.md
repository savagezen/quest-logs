---
title: "NPC - Serra Angel"
description: "CR14 Celestial Paladin"
tags: [DND5E, Homebrew]
---

> *Large Celestial, Lawful Good*

---

**Armor Class** 20 (*Plate Armor*, *Shield*)  
**Hit Points** 225 (18d10 + 108)  
**Speed** 30 ft., *fly* 60 ft.

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 22 (+6) | 16 (+3) | 22 (+6) | 14 (+2) | 18 (+4) | 20 (+5) |

---

**Saving Throws** Wis +10, Cha +11, Con +12  
**Skills** Insight +10, Perception +10, Persuasion +11  
**Damage Resistances** Radiant; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Poison  
**Condition Immunities** Charmed, Frightened, Exhaustion, Poisoned  
**Senses** Truesight 60 ft., Passive Perception 20  
**Languages** Celestial, Common, telepathy 60 ft.  
**Challenge** 14 (11,500 XP)  
**Proficiency Bonus** +5  

---

### **Traits**

**Angelic Weapons.** The Serra Angel's weapon attacks are magical. When it hits with any weapon, the weapon deals an extra 13 (3d8) radiant damage (included in the attack).

**Divine Smite (3/Day).** When the Serra Angel hits a creature with a melee weapon attack, it can expend one use to deal an extra 27 (6d8) radiant damage to the target.

**Aura of Radiance.** The Serra Angel emits an aura of radiant light in a 10-foot radius. Allies within this radius gain a +2 bonus to saving throws and are immune to being frightened. The aura also deals 10 radiant damage to any fiend or undead that starts its turn within this area.

**Magic Resistance.** The Serra Angel has advantage on saving throws against spells and other magical effects.

---

### **Actions**

**Multiattack.** The Serra Angel makes three attacks: two with its *Radiant Blade* and one with its *Shield Bash*.

**Radiant Blade.** *Melee Weapon Attack:* +12 to hit, reach 5 ft., one target. *Hit:* 14 (2d6 + 6) slashing damage plus 13 (3d8) radiant damage.

**Shield Bash.** *Melee Weapon Attack:* +12 to hit, reach 5 ft., one target. *Hit:* 12 (2d4 + 6) bludgeoning damage, and the target must succeed on a DC 20 Strength saving throw or be knocked prone.

**Healing Light (Recharge 5–6).** The Serra Angel touches a creature within 5 feet of it. The target regains 40 (8d8 + 4) hit points and is cured of one disease or condition, such as blindness, deafness, or paralysis.

**Blinding Smite (1/Day).** The Serra Angel channels a burst of holy energy into its weapon. The next time it hits with a melee attack before the end of its turn, the target must make a DC 19 Constitution saving throw or be blinded for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

---

### **Reactions**

**Holy Rebuke.** When a creature within 10 feet of the Serra Angel hits it with an attack, the Serra Angel can use its reaction to unleash a burst of radiant energy. The attacker must make a DC 19 Constitution saving throw or take 18 (4d8) radiant damage and be pushed 10 feet away.

---

### **Legendary Actions** (3/Day)

The Serra Angel can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The Serra Angel regains spent legendary actions at the start of its turn.

- **Radiant Strike.** The Serra Angel makes one *Radiant Blade* attack.

- **Radiant Wings.** The Serra Angel flies up to half its flying speed without provoking opportunity attacks.

- **Protective Shield (Costs 2 Actions).** The Serra Angel calls forth a shield of radiant energy around an ally it can see within 30 feet. The target gains a +2 bonus to AC until the end of its next turn.

---

### **Background & Personality**

**Background:** The Serra Angel is a divine servant of light and protection, created by the planeswalker Serra to uphold justice and guard the innocent. Known for their unwavering resolve and dedication to their sacred duty, these angels are the embodiment of valor and purity. This particular Serra Angel has taken the mantle of a paladin, using its holy might to protect those in need and vanquish darkness wherever it is found.

**Personality:** The Serra Angel is resolute, compassionate, and fiercely protective of those under its care. It values duty and order, but its kindness shines through in moments of peace, offering hope to the downtrodden and inspiring courage in others. However, its zeal for justice can make it uncompromising and unyielding in the face of evil.

**Ideal:** "Justice and mercy walk hand in hand; the righteous must protect those who cannot protect themselves."  
**Bond:** It is devoted to preserving the ideals of Serra and will go to great lengths to prevent corruption from spreading in the realms.  
**Flaw:** Its uncompromising nature can lead it to be inflexible, and it often struggles with understanding the shades of gray in mortal struggles.

---

### **Combat Strategy**

The Serra Angel prefers to fight on the front lines, using its *Radiant Blade* and *Shield Bash* to smite foes while protecting allies with its *Aura of Radiance*. It uses *Divine Smite* to deal devastating blows to powerful enemies, especially fiends and undead, while keeping weaker allies safe with *Healing Light*. In battle, the Serra Angel focuses on taking down key threats swiftly, using its legendary actions to keep up pressure and protect allies with *Protective Shield*.

---

### Notes

* Character generated by ChatGPT
* Original Inspiration: [Magic the Gathering, Serra's Angels](https://mtg.fandom.com/wiki/Angel#Serra_Angels)