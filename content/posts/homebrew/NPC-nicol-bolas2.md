---
title: "NPC - Nicol Bolas, Nameless"
description: "CR15 Imprisoned Dragon"
tags: [DND5E, Homebrew]
---

> *Large Dragon (Shapechanger), Neutral Evil*

---

**Armor Class** 19 (*Natural Armor, Damaged Scales*)  
**Hit Points** 230 (20d10 + 120)  
**Speed** 40 ft., fly 60 ft. (hover)

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 22 (+6) | 14 (+2) | 22 (+6) | 20 (+5) | 16 (+3) | 18 (+4) |

---

**Saving Throws** Str +11, Con +11, Int +10, Wis +8, Cha +9  
**Skills** Arcana +10, Deception +9, History +10, Intimidation +9, Insight +8  
**Damage Resistances** Psychic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Fire  
**Condition Immunities** Charmed, Frightened  
**Senses** Truesight 60 ft., Passive Perception 13  
**Languages** Common, Draconic, Telepathy 60 ft.  
**Challenge** 15 (13,000 XP)  
**Proficiency Bonus** +5  

---

### **Traits**

**Severed Arcane Powers.** Nicol Bolas has disadvantage on saving throws against spells and other magical effects. He can no longer cast spells of 7th level or higher and has only limited access to his former magical prowess.

**Shapechanger.** Nicol Bolas can use his action to transform into a humanoid form, appearing as a scarred and diminished version of himself. His statistics remain the same in either form, but he cannot use his breath weapon while in humanoid form.

**Residual Power.** Nicol Bolas’s remaining spell attacks ignore resistance to psychic damage.

**Innate Spellcasting.** Nicol Bolas's spellcasting ability is Intelligence (spell save DC 18, +10 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

- **At will:** *detect thoughts*, *fireball*, *mind spike*  
- **3/day each:** *dispel magic*, *dominate person*, *counterspell*  
- **1/day each:** *chain lightning*, *confusion*, *finger of death*

---

### **Actions**

**Multiattack.** Nicol Bolas makes two attacks with his claws or uses his *Weakened Mind Blast*.

**Claw.** *Melee Weapon Attack:* +11 to hit, reach 10 ft., one target. *Hit:* 18 (2d10 + 6) slashing damage.

**Weakened Mind Blast.** *Ranged Spell Attack:* +10 to hit, range 60 ft., one target. *Hit:* 27 (6d8) psychic damage, and the target must succeed on a DC 18 Wisdom saving throw or be stunned until the end of their next turn.

**Lingering Flame (Recharge 5-6).** Nicol Bolas exhales fire in a 30-foot cone. Each creature in that area must make a DC 19 Dexterity saving throw, taking 45 (10d8) fire damage on a failed save, or half as much on a successful one. The area remains ablaze with magical fire for 1 minute, causing creatures that enter the area or start their turn there to take 11 (2d10) fire damage.

---

### **Legendary Actions**

Nicol Bolas can take 2 legendary actions, choosing from the options below. Only one legendary action can be used at a time, and only at the end of another creature’s turn. Nicol Bolas regains spent legendary actions at the start of his turn.

- **Cantrip.** Nicol Bolas casts a cantrip.
- **Move.** Nicol Bolas moves up to his speed without provoking opportunity attacks.
- **Psychic Scream (Costs 2 Actions).** Nicol Bolas releases a psychic scream. Each creature within 15 feet of him must succeed on a DC 18 Wisdom saving throw or take 14 (4d6) psychic damage and be deafened until the end of its next turn.

---

### **Background & Personality**

**Background:** Stripped of his identity, name, and much of his power, Nicol Bolas exists in a state of diminished grandeur, held in a prison of his own making. Though his power is sealed, his mind remains sharp, and he endlessly contemplates his defeat, seeking a way to restore his lost glory. Even weakened, he is still a dangerous and cunning adversary, wielding remnants of his psychic and fiery might.

**Personality:** Nicol Bolas is bitter, resentful, and brimming with pride even in his broken state. He despises those who remind him of his fall from grace and dreams of vengeance against those who humbled him. Despite his loss of power, he retains his strategic mind, and his desire for control and dominance remains undiminished.

**Ideal:** "Power is meant to be wielded, not lost. I will rise again."  
**Bond:** He clings to the memory of his former power, desperate to find a way to break his bonds and regain his might.  
**Flaw:** His arrogance prevents him from accepting his current limitations, leading him to make rash decisions when challenged.

---

### Other Versions:

* [Nicol Bolas, God Pharaoh](/posts/homebrew/npc-nicol-bolas/)

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Magic the Gathering, Nicol Bolas](https://mtg.fandom.com/wiki/Nicol_Bolas)