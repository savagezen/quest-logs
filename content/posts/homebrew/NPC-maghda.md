---
title: "NPC - Maghda"
description: "CR17 Dark Coven Enchantress"
tags: [DND5E, Homebrew]
---

> *Medium Fey (Warlock), Chaotic Evil*

---

**Armor Class** 19 (*Natural Armor, Cloak of Shadows*)  
**Hit Points** 275 (33d8 + 132)  
**Speed** 30 ft., fly 40 ft. (hover)

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 12 (+1) | 16 (+3) | 18 (+4) | 16 (+3) | 14 (+2) | 22 (+6) |

---

**Saving Throws** Con +10, Wis +8, Cha +12  
**Skills** Arcana +9, Deception +12, Insight +8, Persuasion +12  
**Damage Resistances** Cold, Psychic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Necrotic  
**Condition Immunities** Charmed, Frightened  
**Senses** Darkvision 120 ft., Passive Perception 12  
**Languages** Common, Infernal, Sylvan, Telepathy 120 ft.  
**Challenge** 17 (18,000 XP)  
**Proficiency Bonus** +6  

---

### **Traits**

**Cloak of Shadows.** Maghda can use a bonus action to become invisible until the start of her next turn or until she makes an attack or casts a spell. While invisible, she leaves a faint trail of dark mist, revealing her position to creatures with Truesight.

**Dark Covenant.** When Maghda is reduced to 0 hit points for the first time, she explodes in a burst of dark magic, forcing each creature within 20 feet to make a DC 20 Constitution saving throw, taking 45 (10d8) necrotic damage on a failed save, or half as much on a successful one. Maghda then transforms into a swarm of shadowy moths, regaining 100 hit points and becoming immune to nonmagical damage until the start of her next turn.

**Fey Pact Magic.** Maghda’s spellcasting ability is Charisma (spell save DC 20, +12 to hit with spell attacks). She can innately cast the following warlock spells, requiring no material components:

- **At will:** *eldritch blast* (3 beams, 1d10+6 damage each), *mage armor*, *minor illusion*  
- **3/day each:** *counterspell*, *greater invisibility*, *hold person*, *shadow of moil*  
- **1/day each:** *circle of death*, *finger of death*, *mass suggestion*, *true polymorph*  

---

### **Actions**

**Multiattack.** Maghda makes three *Eldritch Blast* attacks or two attacks with her *Dark Blade*.

**Dark Blade.** *Melee Spell Attack:* +12 to hit, reach 5 ft., one target. *Hit:* 21 (4d6 + 6) necrotic damage, and the target must succeed on a DC 20 Wisdom saving throw or be frightened until the end of its next turn.

**Swarm of Moths (Recharge 5-6).** Maghda releases a swirling swarm of shadowy moths in a 30-foot radius centered on herself. Each creature in that area must make a DC 20 Dexterity saving throw, taking 44 (8d10) necrotic damage on a failed save, or half as much on a successful one. The area becomes heavily obscured for 1 minute.

**Eldritch Chains (1/Day).** Maghda conjures chains of shadow that lash out at up to three creatures she can see within 60 feet. Each target must make a DC 20 Strength saving throw or become restrained by the chains for 1 minute. A restrained creature can use its action to make a DC 20 Strength or Dexterity check (its choice), freeing itself on a success.

---

### **Legendary Actions**

Maghda can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time, and only at the end of another creature’s turn. Maghda regains spent legendary actions at the start of her turn.

- **Eldritch Blast.** Maghda casts *eldritch blast*.
- **Teleport.** Maghda magically teleports up to 30 feet to an unoccupied space she can see.
- **Dark Pulse (Costs 2 Actions).** Maghda releases a pulse of dark energy in a 15-foot radius around her. Each creature in that area must make a DC 20 Constitution saving throw, taking 21 (6d6) necrotic damage on a failed save, or half as much on a successful one.

---

### **Lair Actions**

When fighting inside her dark sanctuary or a location where she has prepared a ritual site, Maghda can invoke the following lair actions. On initiative count 20 (losing initiative ties), she can take one lair action to cause one of the following effects:

- **Dark Ritual.** Maghda draws power from her coven's magic, regaining 20 hit points.
- **Shadow Tendrils.** Dark tendrils rise from the ground in a 20-foot radius within 60 feet of Maghda. Creatures in that area must make a DC 20 Dexterity saving throw or be restrained until the end of their next turn.
- **Veil of Darkness.** Maghda creates an area of magical darkness, a 15-foot-radius sphere centered on a point she can see within 60 feet. The darkness lasts until initiative count 20 on the next round.

---

### **Background & Personality**

**Background:** Once a member of a shadowy coven devoted to dark and forbidden magic, Maghda gained power through a pact with an ancient, malevolent entity. She became a leader among her followers, wielding the power of shadow and necromancy to further the will of her master. Her ambition and thirst for control over dark forces led her to betray allies and manipulate those around her, seeking to ascend to even greater heights of power.

**Personality:** Maghda is manipulative, prideful, and utterly devoted to the dark forces she serves. She delights in tormenting those she views as weaker than herself, and her cruelty is matched only by her cunning. She rarely shows mercy, preferring to break the wills of her enemies and force them into servitude. Despite her evil nature, she is shrewd and understands the value of alliances—though she is quick to turn on allies when it suits her.

**Ideal:** "Power is found in the shadows and secrets of the world, and I shall command both."  
**Bond:** She is deeply bound to the dark entity that grants her power, willing to sacrifice almost anything to retain its favor.  
**Flaw:** Her arrogance makes her overconfident, and she underestimates those who stand against her, viewing them as mere playthings.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Diablo 3, Maghda](https://diablo.fandom.com/wiki/Maghda)