---
title: "NPC - Adria"
description: "CR19 Blood Witch"
tags: [DND5E, Homebrew]
---

> *Large Fiend (Demon), Chaotic Evil*

### Description:

The Blood Witch Demon is a horrifying sight, a twisted figure of a woman drenched in blood, surrounded by swirling shadows and flames. Once human, she has been consumed by dark powers, wielding blood magic to devastating effect. Her connection to the demonic plane allows her to summon fiendish servants, and her mastery of blood rituals empowers her dark spells. She is drawn to places where blood flows freely and delights in the torment of those who fall under her sway. A fight with a Blood Witch Demon is a brutal and chaotic battle, with summoned demons, curses, and blood-soaked magic making her a deadly foe.

---

**Armor Class** 18 (Natural Armor)  

**Hit Points** 247 (26d10 + 104)  

**Speed** 30 ft.

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 18 (+4) | 16 (+3) | 18 (+4) | 19 (+4) | 17 (+3) | 22 (+6) |

---

### Skills:

* **Saving Throws** Dex +9, Wis +9, Cha +12  
* **Skills** Arcana +10, Deception +12, Insight +9, Persuasion +12  
* **Damage Resistances** Cold, Lightning, Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
* **Damage Immunities** Fire, Poison  
* **Condition Immunities** Poisoned, Charmed, Frightened  
* **Senses** Truesight 120 ft., Passive Perception 13  
* **Languages** Abyssal, Common, Infernal, Telepathy 120 ft.  
* **Challenge** 19 (22,000 XP)  
* **Proficiency Bonus** +6  

---

### Traits:

**Demonic Blood Ritual.**

The Blood Witch Demon can perform a blood ritual as a bonus action, sacrificing 20 hit points to gain one of the following effects until the end of her next turn:

- **Bloodfire Aura:** Creatures within 10 feet of the Blood Witch Demon take 10 (3d6) fire damage at the start of their turn.
- **Empowered Spells:** The Blood Witch Demon deals an extra 9 (2d8) necrotic damage with her spells and attacks.

**Regeneration.**

The Blood Witch Demon regains 15 hit points at the start of her turn if she has at least 1 hit point. This feature does not function if she is in sunlight or an area consecrated by holy magic.

**Magic Resistance.**

The Blood Witch Demon has advantage on saving throws against spells and other magical effects.

**Magic Weapons.**

The Blood Witch Demon's attacks are magical.

---

### **Actions**

**Multiattack.**

The Blood Witch Demon makes three attacks: two with her *Bloodlash* and one with *Bloodflame Orb*.

**Bloodlash.**

*Melee Spell Attack:* +12 to hit, reach 10 ft., one target. *Hit:* 19 (3d8 + 6) necrotic damage. If the target is a creature, it must succeed on a DC 18 Constitution saving throw or be cursed. While cursed, the target takes 7 (2d6) necrotic damage at the start of each of its turns. The curse lasts until the target or another creature uses an action to remove the curse with a successful DC 18 Wisdom (Medicine) check, or until it is dispelled with magic such as *remove curse*.

**Bloodflame Orb.**

*Ranged Spell Attack:* +12 to hit, range 90 ft., one target. *Hit:* 22 (4d8 + 6) fire damage, and the target must succeed on a DC 18 Dexterity saving throw or take 11 (2d10) necrotic damage at the start of its next turn.

**Blood Ritual: Summon Demonic Minions (Recharge 5-6).**

The Blood Witch Demon performs a dark ritual, summoning up to 3 *Bloodspawn Demons* (CR 5, fiends with basic melee attacks and resistance to fire) within 30 feet of her. The minions act immediately after her in the initiative order and are hostile to all creatures except the Blood Witch Demon. The minions remain for 1 minute or until they are reduced to 0 hit points.

---

### **Legendary Actions**

The Blood Witch Demon can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature’s turn. The Blood Witch Demon regains spent legendary actions at the start of her turn.

- **Blood Burn.** The Blood Witch Demon targets one creature she can see within 60 feet of her. The target must succeed on a DC 18 Constitution saving throw or take 13 (2d12) necrotic damage and be blinded until the end of its next turn.
- **Teleport.** The Blood Witch Demon magically teleports up to 60 feet to an unoccupied space she can see.

- **Blood Sacrifice (Costs 2 Actions).** The Blood Witch Demon sacrifices 20 hit points to regain one use of her *Blood Ritual: Summon Demonic Minions* feature or to recharge her *Bloodflame Orb* ability.

---

### **Lair Actions**

If the Blood Witch Demon is encountered in her lair (a demonic blood-soaked temple or ritual chamber), she can take lair actions on initiative count 20 (losing initiative ties). She can’t use the same effect two rounds in a row.

- **Blood Pool Eruption.** A pool of blood within 30 feet of the Blood Witch Demon erupts in a 10-foot radius, forcing each creature within the area to make a DC 18 Dexterity saving throw. On a failure, a creature takes 14 (4d6) fire damage and 14 (4d6) necrotic damage, or half as much on a success.

- **Blood Puppet.** The Blood Witch Demon targets one humanoid she can see within 60 feet of her. The target must succeed on a DC 18 Wisdom saving throw or become charmed until initiative count 20 on the next round. While charmed in this way, the target moves up to half its speed and makes a single attack against a creature of the Blood Witch Demon's choice.

- **Veil of Shadows.** The Blood Witch Demon calls forth a veil of shadowy mist, causing the area within 20 feet of her to become heavily obscured until initiative count 20 on the next round. Creatures within this area cannot regain hit points.

---

### Notes:

* Charcter generated by ChatGPT
* [Diablo Wiki, Adria](https://diablo.fandom.com/wiki/Adria)
