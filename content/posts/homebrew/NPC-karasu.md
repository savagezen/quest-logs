---
title: "NPC - Karasu"
description: "CR15 Vampire Spellcaster"
tags: [DND5E, Homebrew]
---

> *Medium Undead (Shapechanger), Lawful Evil*

---

**Armor Class** 18 (*Natural Armor*)  
**Hit Points** 210 (20d8 + 120)  
**Speed** 30 ft.

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 18 (+4) | 20 (+5) | 22 (+6) | 18 (+4) | 14 (+2) | 20 (+5) |

---

**Saving Throws** Dex +11, Int +10, Wis +8, Cha +11  
**Skills** Arcana +10, Deception +11, Perception +8, Stealth +11  
**Damage Resistances** Necrotic; bludgeoning, piercing, and slashing from nonmagical attacks  
**Senses** Darkvision 120 ft., Passive Perception 18  
**Languages** Common, Infernal, Abyssal, Elvish  
**Challenge** 15 (13,000 XP)  
**Proficiency Bonus** +5  

---

### **Traits**

**Shapechanger.** If Karasu isn't in sunlight or running water, he can use his action to polymorph into a Tiny bat or a Medium cloud of mist, or back into his true form. While in bat form, his flying speed is 30 feet. In mist form, he can’t take actions, speak, or manipulate objects. He is weightless, has a flying speed of 20 feet, can hover, and can pass through small openings and cracks. He can’t pass through water. He has advantage on Strength, Dexterity, and Constitution saving throws, and he is immune to all nonmagical damage, except damage dealt by sunlight.

**Regeneration.** Karasu regains 20 hit points at the start of his turn if he has at least 1 hit point and isn’t in sunlight or running water. If Karasu takes radiant damage or damage from holy water, this trait doesn’t function at the start of his next turn.

**Meld into Darkness.** While in dim light or darkness, Karasu can use a bonus action to become invisible until he makes an attack, casts a spell, or is in bright light.

**Spellcasting.** Karasu is a 13th-level spellcaster. His spellcasting ability is Charisma (spell save DC 19, +11 to hit with spell attacks). He has the following spells prepared:

- **Cantrips (at will):** *chill touch*, *minor illusion*, *mage hand*
- **1st level (4 slots):** *shield*, *magic missile*, *detect magic*
- **2nd level (3 slots):** *misty step*, *mirror image*, *shatter*
- **3rd level (3 slots):** *counterspell*, *fireball*, *vampiric touch*
- **4th level (3 slots):** *greater invisibility*, *blight*
- **5th level (2 slots):** *cloudkill*, *dominate person*
- **6th level (1 slot):** *disintegrate*
- **7th level (1 slot):** *finger of death*

---

### **Actions**

**Multiattack.** Karasu makes two attacks, only one of which can be a bite attack.

**Unarmed Strike.** *Melee Weapon Attack:* +10 to hit, reach 5 ft., one target. *Hit:* 13 (2d6 + 6) bludgeoning damage.

**Bite (Bat or Vampire Form Only).** *Melee Weapon Attack:* +10 to hit, reach 5 ft., one willing creature, or a creature that is grappled by Karasu, incapacitated, or restrained. *Hit:* 9 (1d6 + 6) piercing damage plus 14 (4d6) necrotic damage. The target's hit point maximum is reduced by an amount equal to the necrotic damage taken, and Karasu regains hit points equal to that amount. The reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

**Explosive Curse (Recharge 5–6).** Karasu infuses a small object or creature within 30 feet of him with explosive energy. At the end of Karasu’s turn, the target takes 28 (8d6) force damage, and each creature within 10 feet of the target must make a DC 19 Dexterity saving throw, taking 28 (8d6) force damage on a failed save, or half as much on a successful one. If used on a creature, the creature can attempt a DC 19 Wisdom saving throw to resist the effect.

---

### **Reactions**

**Shadow Step.** When an attacker Karasu can see targets him with an attack, he can use his reaction to teleport up to 30 feet to an unoccupied space he can see, causing the attack to miss.

**Unholy Deflection.** When Karasu is targeted by a spell that requires a saving throw, he can use his reaction to add +5 to his save. 

---

### **Legendary Actions**

Karasu can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time, and only at the end of another creature’s turn. Karasu regains spent legendary actions at the start of his turn.

- **Cantrip.** Karasu casts a cantrip.
- **Move.** Karasu moves up to his speed without provoking opportunity attacks.
- **Blood Drain (Costs 2 Actions).** Karasu makes a bite attack.

---

### **Background & Personality**

**Karasu** is known for his unsettling calm and sadistic tendencies, using his powers to sow fear and destruction. Unlike many vampires, he lacks the overt arrogance typical of his kind, instead adopting a detached and analytical demeanor. He takes great pleasure in playing with his opponents, using his magic to manipulate and control them before delivering a devastating blow. His affinity for explosive magic reflects his twisted desire to see his victims suffer and break apart, both physically and mentally.

**Ideal:** "Power lies in the subtle manipulation of fear and the thrill of destruction."  
**Bond:** Karasu has a strange attachment to a shattered ancient artifact, believing it holds the secret to surpassing his own undead nature.  
**Flaw:** He is obsessed with tormenting his enemies, often prolonging a battle when he could have finished it quickly.

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Yu Yu Hakusho, Karasu](https://yuyuhakusho.fandom.com/wiki/Karasu)
* [Archive - Original Karasu CR15](/docs/archive/ARCHVE-karasu-CR15.md)