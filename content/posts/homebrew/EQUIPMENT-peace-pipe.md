---
title: "Wizard's Peace Pipe"
description: "Wondrous Item (uncommon)"
tags: [DND5E, Homebrew, Item]
---

> Wondrous Item (uncommon)

### Description:

An unusually long, beautifully carved wooden pipe typically used for smoking leafy substances.

---

### Effects and Abilities:

**Loud Smoke:**

Smoking the pipe requires 1 Action and has the effects of the *Fog Cloud (1st level)* spell.

**Blitz:**

You can smoke the peace pipe once per short rest without penalty.  To use the peace pipe more than once per short rest, make a Constitution save (DC 14).  On a failed save you have disadvantage on all Wisdom saves / checks for the next 4 hours.  On a successful save you have advantage on all Wisdom saves / checks for the next 4 hours.

---

### Notes:

* Original Inspiration: [D&D Beyond, Peace Pipe Version 1](https://www.dndbeyond.com/magic-items/6002257-wizards-peace-pipe)