---
title: "Mempo of Twilight"
description: "Wonderous Item (helmet), legendary (requires attunement)"
tags: [DND5E, Homebrew, Item]
---

> *Wonderous Item (helmet), legendary (requires attunement)*

### Description:

This ornate helm is forged from dark, twisted metal and adorned with intricate engravings depicting scenes of twilight skies and storms at sea. Shadows seem to cling to the helmet, and its polished surface reflects the world through a dusky, surreal haze. When worn, the Mempo of Twilight radiates a palpable aura of otherworldly power, instilling fear in the hearts of those who face its bearer.

---

### Properties:

**AC Bonus:**

While wearing the Mempo of Twilight, you gain a +2 bonus to AC.

**Darkvision:**

The helm grants darkvision out to a range of 120 feet. If you already have darkvision, its range increases by 60 feet.

**Twilight Shroud:**

As a bonus action, you can surround yourself in shadows, becoming heavily obscured for up to 10 minutes. While in this form, you have advantage on Dexterity (Stealth) checks and attack rolls against you have disadvantage. This effect ends if you attack or cast a spell. This ability recharges after a long rest.

**Aura of Dread:**

Creatures of your choice within 30 feet that can see you must succeed on a DC 18 Wisdom saving throw or become frightened of you for 1 minute. A frightened creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. This ability recharges after a short or long rest.

---

### Captain of the Entropy Tide:

The Mempo of Twilight allows its wearer to command the legendary ship, *The Entropy Tide*. The ship is a vessel that drifts between the planes, appearing as a shadowy ghost ship on dark waters. Its sails are made of ethereal mist, and its hull is carved from ancient, otherworldly wood that whispers with the voices of the deep.

**Command of the Entropy Tide:**

While attuned to the Mempo of Twilight, you gain the ability to telepathically communicate with and issue commands to The Entropy Tide, guiding its course through material and ethereal waters alike. The ship follows your commands and can navigate to locations known only to those who have mastered its secrets. You gain proficiency with water vehicles if you do not already have it, and can add double your proficiency bonus when commanding The Entropy Tide.

---

### Curse of the Twilight:

With great power comes a heavy price, as the helm's dark energy exacts a toll on the wearer’s mind.

**Short Term Madness:**

Once per day, the wearer suffers the effects of short-term madness (roll on the Short-Term Madness table in the Dungeon Master’s Guide) for 10 minutes. This occurs at a random time each day, as the helm’s influence claws at the wearer’s sanity.

**Long Term Madness on the Entropy Tide:**

If the wearer is aboard The Entropy Tide, the helm's connection to the ship intensifies. Once per day, the wearer suffers the effects of long-term madness (roll on the Long-Term Madness table in the Dungeon Master’s Guide) for 10 hours. The nature of the ship and helm warp the wearer’s mind, making reality blur with nightmarish visions from the sea of shadows.

---

### Destroying the Mempo of Twilight:

The Mempo of Twilight can only be destroyed if submerged in the heart of a planar maelstrom—one that rips apart the boundaries between the Material Plane and the Shadowfell. If destroyed, *The Entropy Tide* will lose its connection to the Material Plane, forever vanishing into the mists of the ethereal sea.

---

### Notes:

* Item generated with ChatGPT
* [Diablo 3, Mempo of Twilight](https://diablo.fandom.com/wiki/Mempo_of_Twilight)
* [Archived - Original Mempo of Twilight](/docs/archive/ARCHIVE-mempo-of-twilight)