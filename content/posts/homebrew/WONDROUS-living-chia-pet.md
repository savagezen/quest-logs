---
title: "Living Chia Pet"
description: "Wondrous Item (uncommon)"
tags: [DND5E, Homebrew, Item]
---

> *Wondrous Item, uncommon*

---

### Description:

An American styled terracotta figurine that typically sprouts vegetation, where the vegetation grows to resemble the animal's fur or hair.  On the underbelly of this figurine are the worlds "just add water", written in Common language.

When the figurine is submerged in water, instead of sprouting vegetation, it transforms into the unaligned beast or fey creature (CR 1d4) that it resembles.

---

### Notes:

* [D&D Beyond, Chia Pet Version 1](https://www.dndbeyond.com/magic-items/6002154-living-chia-pet)