---
title: "NPC - Nicol Bolas, God Pharaoh"
description: "CR28 Elder Dragon"
tags: [DND5E, Homebrew]
---

> *Gargantuan Dragon (Shapechanger), Lawful Evil*

---

**Armor Class** 25 (*Natural Armor, Arcane Shield*)  
**Hit Points** 760 (40d20 + 320)  
**Speed** 60 ft., fly 120 ft.

---

| STR  | DEX  | CON  | INT  | WIS  | CHA  |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 30 (+10) | 18 (+4) | 26 (+8) | 30 (+10) | 22 (+6) | 28 (+9) |

---

**Saving Throws** Str +18, Dex +12, Con +16, Int +18, Wis +14, Cha +17  
**Skills** Arcana +18, Deception +17, History +18, Insight +14, Intimidation +17, Perception +14  
**Damage Resistances** Psychic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Fire, Necrotic  
**Condition Immunities** Charmed, Frightened, Paralyzed  
**Senses** Truesight 120 ft., Passive Perception 24  
**Languages** Common, Draconic, Celestial, Infernal, Telepathy 120 ft.  
**Challenge** 28 (120,000 XP)  
**Proficiency Bonus** +8  

---

### **Traits**

**Legendary Resistance (4/Day).** If Nicol Bolas fails a saving throw, he can choose to succeed instead.

**Arcane Mastery.** Nicol Bolas’s spell attacks ignore resistance to damage types and treat immunity as resistance.

**Psionic Domination.** When a creature starts its turn within 30 feet of Nicol Bolas, he can use his reaction to attempt to dominate their mind. The creature must succeed on a DC 25 Wisdom saving throw or be charmed by Nicol Bolas until the end of its next turn. While charmed in this way, the creature is incapacitated and obeys Nicol Bolas’s mental commands. A creature that succeeds on the saving throw is immune to this effect for 24 hours.

**Shapechanger.** Nicol Bolas can use an action to magically transform into a humanoid or back into his true form. While in humanoid form, his statistics are the same, but he cannot use his breath weapon or multiattack. 

**Magic Resistance.** Nicol Bolas has advantage on saving throws against spells and other magical effects.

**Innate Spellcasting.** Nicol Bolas's spellcasting ability is Intelligence (spell save DC 26, +18 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

- **At will:** *detect thoughts*, *dominate monster*, *fireball*, *mind spike*  
- **3/day each:** *disintegrate*, *feeblemind*, *globe of invulnerability*, *mass suggestion*, *time stop*  
- **1/day each:** *meteor swarm*, *power word kill*, *true polymorph*, *wish*

---

### **Actions**

**Multiattack.** Nicol Bolas makes three attacks: one with his Bite and two with his Claws, or he casts a spell in place of one of these attacks.

**Bite.** *Melee Weapon Attack:* +18 to hit, reach 15 ft., one target. *Hit:* 31 (4d10 + 9) piercing damage plus 14 (4d6) necrotic damage.

**Claw.** *Melee Weapon Attack:* +18 to hit, reach 10 ft., one target. *Hit:* 25 (4d6 + 10) slashing damage.

**Frightful Presence.** Each creature of Nicol Bolas’s choice that is within 120 feet and aware of him must succeed on a DC 25 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to Nicol Bolas’s Frightful Presence for the next 24 hours.

**Mind Shatter Breath (Recharge 5-6).** Nicol Bolas exhales a blast of psionic energy in a 90-foot cone. Each creature in that area must make a DC 25 Intelligence saving throw, taking 88 (16d10) psychic damage on a failed save, or half as much on a successful one. A creature that fails the saving throw is stunned for 1 minute. A stunned creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

---

### **Legendary Actions**

Nicol Bolas can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time, and only at the end of another creature’s turn. Nicol Bolas regains spent legendary actions at the start of his turn.

- **Cantrip.** Nicol Bolas casts a cantrip.
- **Tail Swipe.** Nicol Bolas makes a tail attack: +18 to hit, reach 20 ft., one target. *Hit:* 26 (4d8 + 8) bludgeoning damage.
- **Telekinetic Crush (Costs 2 Actions).** Nicol Bolas targets a creature he can see within 60 feet. The target must succeed on a DC 26 Strength saving throw or take 33 (6d10) force damage and be restrained for 1 minute. A restrained creature can make a DC 26 Strength check at the end of each of its turns, freeing itself on a success.
- **Cast a Spell (Costs 3 Actions).** Nicol Bolas casts a spell of 5th level or lower.

---

### **Lair Actions**

When fighting inside his lair, Nicol Bolas can invoke the ambient magic to take lair actions. On initiative count 20 (losing initiative ties), Nicol Bolas can take one lair action to cause one of the following effects:

- **Mind Warp.** Nicol Bolas creates a 20-foot-radius sphere of psychic distortion at a point he can see within 120 feet. Creatures in the sphere must make a DC 25 Wisdom saving throw or take 22 (4d10) psychic damage and have disadvantage on saving throws until the end of their next turn.
- **Arcane Rift.** A rift opens in the fabric of reality within 60 feet of Nicol Bolas, causing chaotic magical energy to surge in a 10-foot radius. Creatures in that area must make a DC 25 Dexterity saving throw or take 18 (4d8) force damage and be pushed 15 feet away from the rift.
- **Mana Drain.** Nicol Bolas targets a creature he can see within 60 feet. The target must succeed on a DC 25 Constitution saving throw or have one of its spell slots of 5th level or higher drained, and Nicol Bolas regains 20 hit points.

---

### **Background & Personality**

**Background:** Nicol Bolas is an ancient being who has walked the planes for millennia, growing ever more powerful in his quest for dominion over all. He is the last of a line of godlike dragons, and his cunning, patience, and power have allowed him to manipulate entire worlds to serve his ambitions. He views lesser beings as mere tools or obstacles to his plans.

**Personality:** Ruthless, prideful, and endlessly ambitious, Nicol Bolas is a master strategist who sees countless steps ahead in any plan. He has little patience for weakness and delights in breaking the will of those who oppose him. He enjoys playing with his enemies, savoring their despair before delivering the final blow. While he can be charming when it serves his needs, his true nature is that of a tyrant who sees all life as beneath his own.

**Ideal:** "All who live exist to serve those who possess true power. I am that power."  
**Bond:** He craves ultimate dominion, seeking to reshape reality itself to his will.  
**Flaw:** His arrogance and sense of invincibility lead him to underestimate those he considers beneath him, and he despises losing control.

---

### Other Versions:

* [Nicol Bolas, Nameless](/posts/homebrew/npc-nicol-bolas2/)

---

### Notes:

* Character generated by ChatGPT
* Original Inspiration: [Magic the Gathering, Nicol Bolas](https://mtg.fandom.com/wiki/Nicol_Bolas)
* [Archived - Original Reference](/docs/archive/ARCHIVE-nicol-bolas-CR28.jpg)