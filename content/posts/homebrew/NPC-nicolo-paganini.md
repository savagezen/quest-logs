---
title: "NPC - Nicolo Paganini, The Devil's Violinist"
description: "CR12 Undead Fiend-Touched Musician"
tags: [DND5E, Homebrew]
---

> *Medium Undead (Fiend-Touched), Lawful Evil*  
___  
**Armor Class** 17 (Unholy Grace)  
**Hit Points** 210 (20d8 + 120)  
**Speed** 40 ft.  
___  
### **STR 12 (+1) | DEX 22 (+6) | CON 22 (+6) | INT 18 (+4) | WIS 16 (+3) | CHA 24 (+7)**  
___  
**Saving Throws** Dex +11, Con +11, Cha +12  
**Skills** Performance +15, Deception +12, Arcana +9, Insight +8, Perception +8  
**Damage Resistances** Cold, Fire, Psychic, Thunder; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks  
**Damage Immunities** Necrotic, Poison  
**Condition Immunities** Charmed, Frightened, Poisoned, Exhaustion  
**Senses** Darkvision 120 ft., Passive Perception 18  
**Languages** Common, Infernal, Abyssal, Celestial  
**Challenge** 12 (8,400 XP)  
___  

### **Legendary Musician.**  
Paganini’s musical performances have supernatural influence. Creatures within 120 feet of him have disadvantage on saving throws against his enchantment and illusion effects.  

### **Unholy Grace.**  
Paganini adds his Charisma modifier (+7) to his AC and all saving throws.  

### **Virtuoso’s Curse.**  
Any creature that willingly listens to Paganini’s violin for more than one minute must succeed on a DC 20 Wisdom saving throw or be charmed for 24 hours. While charmed, the creature is obsessed with Paganini’s music and will refuse to attack him unless provoked.  

### **Soul-Touched Strings.**  
Paganini’s violin is infused with infernal energy. Any creature that hears it can feel the haunting echoes of its previous owners’ souls.  

---

### **Actions**  

### **Multiattack.**  
Paganini makes two attacks with **Infernal Bowstrike** or casts one spell and plays a violin effect.  

### **Infernal Bowstrike.** *Melee Weapon Attack:* +11 to hit, reach 5 ft., one target.  
*Hit:* 22 (4d6 + 6) necrotic damage. The target must succeed on a DC 18 Constitution saving throw or have disadvantage on attack rolls and ability checks until the end of Paganini’s next turn.  

### **Hellish Cadenza.** (Recharge 5-6)  
Paganini plays an otherworldly solo. All creatures within 30 feet must make a DC 18 Wisdom saving throw or be stunned for 1 minute. A creature may repeat the saving throw at the end of each of its turns.  

### **Fiend’s Waltz.** (Recharge 4-6)  
Paganini moves in an ethereal blur, vanishing and reappearing up to 60 feet away, leaving behind haunting echoes of violin music. Any creature within 10 feet of his starting or ending location takes 21 (6d6) psychic damage.  

### **Spells.**  
Paganini casts spells with Charisma (spell save DC 20, +12 to hit). He can cast the following:  

**At will:** *Vicious Mockery (5d4), Minor Illusion, Thaumaturgy*  
**3/day:** *Suggestion, Fear, Enthrall, Phantasmal Force*  
**1/day:** *Dominate Person, Otto’s Irresistible Dance*  

---

### **Legendary Actions** (3 per round)  
Paganini can take 3 legendary actions, using one at a time at the end of another creature’s turn.  

- **Haunting Melody.** (Costs 1 action) A chosen creature within 60 feet must succeed on a DC 18 Wisdom saving throw or be frightened for 1 minute.  
- **Blinding Flourish.** (Costs 1 action) Paganini moves up to half his speed without provoking opportunity attacks. Creatures within 10 feet must succeed on a DC 18 Dexterity saving throw or be blinded until the end of their next turn.  
- **Demonic Crescendo.** (Costs 2 actions) Paganini releases a violent surge of music, dealing 18 (4d8) necrotic damage to all creatures within 20 feet.  

---

### **Lair Actions**  
If Paganini is performing in a grand concert hall or an infernal court, he can take lair actions. On initiative count 20 (losing ties), he may:  

- **Phantom Orchestra.** An eerie orchestra plays alongside him. All spells he casts have their range doubled for 1 minute.  
- **Dance of the Damned.** Spectral figures rise and force creatures within 30 feet to make a DC 18 Dexterity saving throw or be restrained by ghostly hands until the end of Paganini’s next turn.  
- **Sinister Encore.** Paganini regains one use of **Hellish Cadenza** or **Fiend’s Waltz.**  

---

**"Music is not just sound—it is power, it is seduction, it is damnation. Listen well, and you may never listen again."**  

---

**Notes:**

* Original Reference: [Nicolo Paganini](https://en.wikipedia.org/wiki/Niccol%C3%B2_Paganini)