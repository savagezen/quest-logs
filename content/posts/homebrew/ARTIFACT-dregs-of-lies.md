---
title: "The Dregs of Lies"
description: "One of the 7 soul shards of the Black Soulstone, Belial's Essence"
tags: [DND5E, Homebrew]
---

> Wondrous Item (Soul Shard), Legendary (Requires Attunement)

The Dregs of Lies contains the essence of Belial, the Lord of Lies, granting the wielder the ability to weave intricate deceptions.

### Abilities:

* Web of Deceit: Cast major image (DC 18) at will without expending a spell slot. The illusion can last up to 24 hours or until dispelled.
* Forked Tongue: Gain advantage on Charisma (Deception) checks, and you can use charm person (DC 18) at will.
* Mists of Illusion: As an action, create a 20-foot radius of dense mist that confuses creatures within. They must make a DC 18 Wisdom saving throw or treat all creatures as hostile for 1 minute. Recharges on a long rest.

### Cursed Power:

Whenever the wielder speaks, they must succeed on a DC 18 Wisdom save or be compelled to lie, twisting the truth to suit their own purposes.

### Awakened Power:

When the wielder is reduced to 0 hit points, they can become invisible, teleporting up to 60 feet away and stabilizing with 1 hit point. This effect can occur once per week.

### Destroying the Dregs of Lies:

The shard must be shattered beneath the light of the first dawn of the year, in a sacred grove guarded by treants, while a true oath of loyalty is spoken by a pure-hearted being.

---

### Notes:

* Genereated by ChatGPT
* Original Inspiration, [Diablo 3, 7 Soul Shards](https://diablo.fandom.com/wiki/Soul_Shards)
