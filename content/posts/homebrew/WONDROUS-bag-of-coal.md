---
title: "Bottomless Bag of Coal"
description: "Wondrous Item (common)"
tags: [DND5E, Homebrew, Item]
---

> *Wondrous Item (common)*

---

### Description:

The bag is made to look like a tiny sack you can keep in your pocket. While holding the sack, you can use an action to take 2 pounds of coal from it. The retrieved coal consumes itself after 12 hours. The sack never runs out of coal.

*Designer note: This item was just a funny one that can also be useful for setting camp. If you’ve been a naughty child this year you won’t only get coal as a Christmas present, but also have it disappear before you get to use it.*

---

### Notes:

* Original inspiration: [tribality.com](https://www.tribality.com/2019/12/17/dd5e-christmas-themed-magic-items-to-gift-your-players/)