---
title: "Scroll of Naughty and Nice"
description: "Wondrous Item (rare), scroll"
tags: [DND5E, Homebrew, Item]
---

> *Wondrous Item (rare), scroll*

---

### Description:

This scroll has 12 charges. Use one charge to see where someone might be on the spectrum of evil and good in the past year. To use a charge, write their name in the left column of the scroll. This must be someone that you know and have met within the past year.

The scroll will reveal the accumulation of the creature’s deeds in the past year by displaying the word Good (nice) in green or Evil (naughty) in Red. The brighter the color, the further left or right on the spectrum they are. The fainter/lighter the color, the closer to the middle. After all charges have been used, the scroll will crumble and is destroyed.

---

### Notes:

* Original inspiration: [wallydm.com](https://www.wallydm.com/dnd-holiday-magic-items-christmas/#item6)