---
title: "Entropy Tide"
description: "Gargantuan Vehicle (ship), legendary (requires attunement to the Mempo of Twilight)"
tags: [DND5E, Homebrew, Item]
---

> *Gargantuan Vehicle (ship), legendary (requires attunement to the Mempo of Twilight)*

### Description:

The Entropy Tide is a shadowy, fearsome ship with dark wood that seems almost black under any light. Its hull is covered in eldritch carvings that glow with faint purple and blue light, especially when in the presence of magic or during stormy weather. Mist clings to its sides, and the sails—made from shadowy, semi-transparent material—appear tattered, yet no wind is needed to propel it through water or ethereal planes.

The ship gives off an aura of mystery and dread, chilling those who lay eyes upon it. It is known to move with uncanny speed, even in the roughest seas, and can vanish into mist, slipping between planes.

---

### Ship Statistics:

* Size: Gargantuan (100 ft. long, 25 ft. wide)
* Armor Class: 20
* Hit Points: 500 (damage threshold 15)
* Speed: 8 miles per hour (70 ft. per round on water)
* Crew Required: None (see Cursed Crew feature)
* Cargo Capacity: 100 tons
* Travel: The Entropy Tide can traverse oceans, seas, and even cross into the Ethereal Plane. It can maintain its speed regardless of wind conditions due to its spectral sails.

---

### Features & Abilities:

**Ghostly Speed:**

The Entropy Tide can move through rough waters, stormy seas, and even ethereal currents without slowing down. It can use a bonus action to Dash, moving at double speed for 1 minute. This ability recharges after a short or long rest.

**Veil of Shadows:**

As an action, the captain of the Entropy Tide (the attuned wearer of the Mempo of Twilight) can cause the ship to become heavily obscured by shadowy mist for up to 10 minutes. Creatures outside the ship have disadvantage on Wisdom (Perception) checks to detect it. The mist also imposes disadvantage on attack rolls against the ship and its crew. This ability recharges after a long rest.

**Ghostly Evasion:**

When targeted by an area effect, such as a fireball or a breath weapon, the Entropy Tide can use a reaction to phase partially into the Ethereal Plane, taking only half damage from the effect. This ability recharges after a short rest.

**Cursed Crew:**

* Skeleton Crew: The Entropy Tide is crewed by 30 undead skeletons, bound to the ship and compelled to serve its captain. These skeletons have the following traits:
  * AC 13, HP 15, Speed 30 ft., Str +2, Dex +2, Con +1, Wis -1, Int -2, Cha -3.
  * They are immune to exhaustion, frightened, and poison, and do not require air, food, drink, or sleep.
  * Languages: Understands all languages known by the captain but cannot speak.
  * Obedience to the Captain: The skeletons will only obey commands from the attuned wearer of the Mempo of Twilight. If the wearer loses attunement or if the helm is removed, the skeletons become dormant and will not perform any duties until attunement is restored. They can perform all ship tasks, such as raising and lowering the anchor, adjusting sails, and firing cannons or ballistae (if equipped).

**Aura of Undeath:**

While aboard the Entropy Tide, undead allies within 60 feet of the ship (including the skeleton crew) have advantage on saving throws against being turned or frightened.

---

### Special Properties:

**Plane Shift (1/week):**

Once per week, the Entropy Tide can travel between the Material Plane and the Ethereal Plane, taking all creatures and cargo on board with it. This ability is activated through a ritual performed by the captain, which takes 1 hour to complete. The ship remains in the chosen plane until the ritual is performed again.

**Cursed Captain’s Bond:**

The Entropy Tide’s magic binds it to the attuned wearer of the Mempo of Twilight. The ship cannot be controlled, steered, or otherwise operated by any other creature. If another creature attempts to take the helm, the ship remains inert and unresponsive until the rightful captain returns.

**Curse of Madness:**

The dark powers that bind the Entropy Tide to the Mempo of Twilight come at a heavy cost to the captain’s mind.

**Madness at Sea:**

When aboard the Entropy Tide, the captain suffers from long-term madness once per day for 10 hours (roll on the Long-Term Madness table in the Dungeon Master’s Guide). The sea’s whispers and the shadows that haunt the ship weigh heavily on the captain’s mind, twisting reality into nightmarish visions.

**Madness on Land:**

Even when not aboard, the captain is not free of the ship's influence. They suffer from short-term madness once per day for 10 minutes (roll on the Short-Term Madness table).

---

### Destroying the Entropy Tide:

The Entropy Tide is nearly impossible to destroy through conventional means. It must be torn apart in a maelstrom between planes, where the boundaries between reality and shadow are weakest. If the ship is destroyed, the skeleton crew crumbles into dust, and the whispers that accompany it vanish into the void.

---

#### Notes:

* Item generated with ChatGPT
* [Pirates of the Caribbean, The Black Pearl (ship)](http://pirates.wikia.com/wiki/Black_Pearl)
* [Archived - Original Entropy Tide](/docs/archive/ARCHIVE-entropy-tide.md).  The original version also required 4 [Fey Rings of Atlantis]() to power the ship.

---
