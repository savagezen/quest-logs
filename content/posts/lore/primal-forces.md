---
title: "The Primal Forces"
description: "Explore the Primal Forces' origins, emanations, and their role in magic. From Ein Sof to the elemental chaos, these forces shape existence in Sanctuary."
tags: [DND5E, Lore]
---

```
Author: Ryan
```

### Origins of the Primal Forces

Before the world, there was only the infinite and the emptiness. These are now known to us as Ein Sof and Da’at, respectively. Much mystery still surrounds these entities. All that is really known is that the infinite comprises all that is, and all that isn’t belongs to the emptiness. They take no clergy and only interact with the world when balance must be restored. Even then, it is only through indirect means. All that we know of this pair is from the ramblings of men left mad after channeling higher entities, the men recalling bits of the powerful being’s thoughts that echo in the person’s mind. It appears that Ein Sof began, for some unknown reason, emanating the progenitors of the primal forces. This emanation congealed into two beings, The Crown and the Adversary. We know these as Anu and Tiamat, but they have many names across our multiverse. What remains constant is that the Crown is seen as the true authority of our existence and the Adversary is exactly that, the enemy of the Crown. The emanations didn’t end there, however. Keter produced two emanations, Chokmah the Divinity and Binah the Nature. Thamiel produced Chaigidel the False and Sathariel the Corrupt. The emanations produced from these four were the true origin of the Primal Forces.

---

### Emanations of Chokmah

**Akhul, The Sanctified Force**

Akhul is the primal force of all things good and holy. The heavenly planes and celestials harbor the greatest concentrations of this force. It has the power to heal and purify the righteous as well as smite the evil. The greatest of celestial beings are also know as emanations, which are quite alien when compared to the celestials mortals have commonly dealt with. These are served by the celestial beings known as the seraphim, who rule the heavens, and the watchers, who protect the material world. These are followed by the archangels and the rulers of the lesser celestials. Beneath them are the angels and celestial princes, and the lower celestials form the bottom ranks.

**Mardukhul, the Soul Force**

Mardukhul is the primal force that grants immortal life. Small amounts form the soul of mortals, while large concentrations create immortals and even deities. It has frequently been referred to as the spark of divinity. The traditional theological structure of greater, intermediate, and lesser deities followed by demigods and quasideities displays the concentrations of this force beyond the mortal levels.

---

### Emanations of Binah

**Karashul, the Life Force**

Karashul is the primal force that grants life. Small amounts breathe life into the flora and fauna of all nature-based worlds. Large concentrations create beings and worlds that seem to embody the purest forms of nature, even if they seem contradictory to our conception of nature. The feywild and its beings thrive almost entirely on the Life Force. The most powerful of these are the Forgotten Ones, called so because their names are lost to mortals. Whether it is because they are slumber, are truly lost, or unknown due to the virtually nonexistent record-keeping of the faeriefolk, the identities remain a mystery. The following ranks are comprised by the Fey Royalty, the Nobility, the lower Archfey, and finally, the lesser fey.

**Rashul, the Material Force**

Rashul is the Primal Force that creates physical substance of living creatures. The larger the concentration of the Material Force, the bigger the creature. Transfer of this force is quite possibly the simplest. All that is required is the consumption of another corporeal creature. This force is most abundant on the material plane, but several outer planes also contain large stores of it. Due to the truly primal nature of the creatures associated with this force, a classification system is difficult. We propose the following, based purely on loose requirements of size. The lowest tier is comprised of the largest of “natural” beasts. Beyond that, we refer to those larger as behemoths, followed by titans. The next step we use a term borrowed from the oriental phrase for giant beasts, the Kaiju. The largest, those truly capable of global destruction, we call the World Eaters.

--

### The Emanations of Chaigidel

**Shardhul, the Vile Force**

Shardhul is the Primal Force of all things evil and unholy. The Lower Planes and Fiends comprise the largest concentrations of this force. This forces pains and poisons the minds and souls of mortals. The greatest fiendish beings are known as the false gods. The name can be a bit of a misnomer because these beings are nearly unknown to the mortals. They do draw a twisted clergy, but it is comprised of other fiendish beings. The next tier is similar. About half are called the Dark Ones and are great fiends that rule large expanses of the fiendish planes. The other half are the Nephilim, some of which walk the mortal worlds, corrupting and destroying, and most of which are sealed or slumbering. These are followed by the rulers of the lesser fiendish races, such as the Archdevils and Demon Princes. The fourth tier follows suit, composed of the dukes of hell, demon lords and the like. The remainder of the powerful lesser fiends forms the lowest tier. 

**Palaghul, the Entropic Force**

Palaghul is the Primal Force of chaos and the raw elements. The inner planes are the central sources of this power. Alternately multiple elemental planes or a single elemental chaos, little true life can exist here.  Much of the inhabitants are merely elemental energy infused with just enough Soul Force to grant a sentient existence. This force functions similarly to the Material Force, in that higher concentrations tend to yield bigger elementals. Exceptions do exist, but they are generally the result of infusions of other Primal Forces. The hierarchy is as follows starting with the most mighty of elementals:  The Primordials, The Elemental Lords, Elemental Princes, Primal Elementals, a lastly the remainder of the elemental beings.

---

### Emanations of Sathariel

**Chanakhul, the Psychic Force**

Chanakhul is the Primal Force of mind and madness. This force allows for the higher mental status and unconventional thinking that creates innovation in lower doses. In higher concentrations, it creates everything from psychosis to psionic powers to purely aberrant beings, depending on the amount and the presence of other Primal Forces. The Far Realm, holds large stores of the Psychic Force. The Force has the strange property of becoming more powerful in the absence of other Primal Forces and can often mimic some of the properties of the other Forces when they are not present. This may be why the most powerful Eldritch beings resides so deep in the Far Realm, far away from the material world. The most powerful of these titans are called the Elder Evils. They rarely venture far from their homes, but when they do apocalyptic terrors are sure to ensue. The next tier down are called the Progenitors, beings who propagate armies and races in their own twisted image. This is followed by the Old Ones, who are the eldest of the Progenitors progeny. The fourth tier is composed of the paragons of the eldritch beasts, known as the Xoria. The remaining aberrant beasts from the final group.

**Baghul, the Shadow Force**

Baghul is the Primal Forces of rot and decay. This Force severs the bonds of the other Forces and recycles them into the multiverse. It performs its function very well is relatively small doses. Several things can cause it to accumulate naturally, including a heavy concentration of other forces and even an unusual lack of them. Generally, however, the accumulations occur by unnatural means by the hands of a powerful entity. In any of these cases, higher amounts of the Shadow Force will mimic functions of the Life, Soul and Material Forces. These simulacrums can create both corporeal and incorporeal forms, and impart the semblance of life unto these forms. Baghul is the creator of undead, and these foul beings contain the highest levels of the Shadow Force. No satisfactory classification has been presented before so we present the following starting the most powerful: The Reapers, The Heretics, The Undying, the Undead Lords, and the remaining sentient undead.

---

### The Origin of Magic

All magic is powered by drawing energy from the Weave. This is a network of ether formed from an infinite number of tiny undetectable interactions between the Primal Forces. It is accessed in a variety of ways. 

**Arcane Magic:**

Arcane magic draws directly on the Weave. Bards and wizards manipulate the ether through practiced techniques while sorcerers and warlocks actually function as a conduit of this energy. The major difference between the sorcerer and warlock is that a sorcerer’s heritage instills an innate link to the Weave while a warlock’s patron instills the ability upon them. 

**Divine Magic:**

All divine magic is also sourced at the Weave, but the ether is channeled and filtered through the divine beings before reaching the user. This is beneficial to both the deity and the user as the former can decide what constitutes acceptable use of the power while the latter is protected from some of the hazards of accessing raw ether. 

**Psionics:**

Psionic energy is similar to magic but is not powered by the weave. The ability is actually sourced in higher concentrations of the Psychic Force in the user. As such, it ignores certain pitfalls of traditional magic, like dead magic zones and antimagic fields. 

**Other Forms of Magic:**

Nearly all of the Primal Forces, when concentrated, will produce magical effects like the Psychic Force. In these other cases, however, the range of effects are usually very limited or require access to the weave to function properly. For instance, the Sanctified Force can be used to heal by itself. It can also be used to smite foes, but this usage requires the input of magical energy from the weave to function. 

---

### Boundaries and Limits of the Primal Forces

As mighty as these forces are, they do have their limits. As the accumulation required to achieve the next level of power follows an exponential trend, it becomes impossible to advance beyond the highest tiers listed through normal means. As the concentrations reach such high levels, interactions with the other Primal Forces causes massive frictional losses. A good example are the true deities. To maintain their status, the gods require continual input of Soul Energy from devoted worshippers. Without this continual input, they would eventually regress to demigods. Other high tier entities face similar restrictions, although their needs are met in different ways. Powerful celestials and fiends, for instance, must regularly return to their home planes to recharge. Another limitation to high concentrations of Primal Forces is inter-Force interactions within a being. As a being accumulates more of one type of Primal Force, even small levels of the others seriously impede the function of the primary Force of the being. As such, it is impossible to accumulate significant amounts of more than one type through normal means.

---

### Breaking These Rules

Power-hungry beings have always sought ways to break these limitations. The most commonly attempted methods are continual infusion and magical stabilization. These are exactly as they sound. The former case has the entity continuously consuming the Forces to maintain the high levels. This invariably fails because the input of supporting Force for high concentrations of one type is difficult enough. These requirements are magnified as when even one other Force is added at high levels. The latter has users attempting to the stabilize the accumulations using magical means. This can work to some extent at lower levels. As the levels get higher, however, diminishing returns ensue as the efficacy of support per unit of magical energy is constant while the frictional loss from the other forces is compounded. There have been whispers among the higher beings of one solution that should work. To ascend the limitations of the Primal Forces, one must go beyond those Forces and seek assistance from their source. That is, drawing upon the energy of Ein Sof and Da’at could prove to be an answer. Their energies, which we have colloquially dubbed the Infinite Force and Void Force, respectively, should bind the Primal Forces sustainably, at minimum, or empower the being beyond what the other Forces could do, at maximum. Attaining these powers illicitly is the largest hurdle, and this surely occupies the nefarious intentions of at least a few powerful beings at present. It has not yet been discussed if the beings in between Ein Sof and Da’at and the paragons of the Primal Forces, for instance Thamiel or Chaigidel, could serve a similar role. It would stand to reason that it would most likely only work for those Forces that found their origin in the relevant being. 