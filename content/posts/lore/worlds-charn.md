---
title: "World of Charn"
description: "Jadis, The White Witch, a cruel warrior mage of royal Djinn and Frost Giant lineage, annihilated Charn's life to seize its throne, seeking power across worlds."
tags: [DND5E, Lore]
---

```
Author: Austin
```

### The White Witch:

There are stories from long ago, worlds away, that tell of a cruel warrior mage named Jadis, The White Witch, who represented the tyrannical nature of sin in her world.  Born of a royal bloodline of Djinn and Frost Giants, Jadis was born in the city of Charn, the capital city of a world bearing the same name.  Eventually there came a dispute over the throne of Charn between Jadis and her sister.

However, Jadis had long since been practicing dark magic "beyond what was proper" in her Charn.  With her army defeated, rather than surrender, Jadis cast a spell -- known only as The Deplorable Word -- to end all life in Charn except her own.  Being the sole living resident of her world, she declared herself queen.

Jadis since attempted to conquer other worlds, but was ultimately defeated and her world sealed away from The Wood Between Worlds (a temporal connection to the Astral Plane).  Jadis eventually found a weakness between worlds and found that she was able to have influence over differing domains and their respective worlds.  Legends say that she has had influence influence in the world of Sanctuary before and has done so under the guise of the deity Auril -- contemptuous as she is cruel only ascending to quasi-godhood.

The White Witch is known for her preference of cold weather (here mere presence instigating regional climate change), strictly enforced rules, and executions.  It is said that she cannot tolerate treason (even against her enemies as legends describe her as fighting even against other evil deities), the slightest suggestion of disloyalty, celebrations, or rule breaking of any kind.  Rumors state that in her human form Jadis is menacing presence of at least 8 ft. tall clad in a white fur robe lined in ice blue trim.

**References:**

* [Forgotten Realms, Auril](https://forgottenrealms.fandom.com/wiki/Auril)
* [Jadis, 3.5e Deity](https://www.dandwiki.com/wiki/Jadis_(3.5e_Deity))
* [Narnia Wiki, Jadis](https://narnia.fandom.com/wiki/Jadis)

### Charn

Charn was once accessible from the Wood Between the Worlds, but the pool dried up after the defeat of the world's ruler Jadis.  The world has since been recreated as a pocket dimension existing parallel to The Grey Waste.  The sun of Charn is old, red, and far larger than that of the material plane.  The sky is blue so dark that it is nearly black.

There is one moon that hangs adjacent to the sun.  The air is thin and cold and there has been no visible water since the extinction of Charn's people.  The capital city, also named Charn, contains a Hall of Images of past rulers that initially began in an age of wisdom and benevolence, but eventually turned away into corruption, cruelty, and evil.  In another age and universe, Jadis was initially imprisoned in the world after destroying her people.

She was accidentally awoken by a curious humanoid who rang a magical bell, unknowingly waking her.  At it's height the city of Charn was a war-like empire was described as a city of the King of Kings, the wonder of the world, perhaps all worlds.  The natural inhabitants of Charn are descended from Djinn and Giants and magic use was common practice when the world was inhabited."

**References:**

* [Narnia Wiki, World of Charn](https://narnia.fandom.com/wiki/World_of_Charn)