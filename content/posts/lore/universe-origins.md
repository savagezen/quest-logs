---
title: "Origins of the Universe"
description: "The origins of the universe and the world of Sanctuary."
tags: [DND5E, Lore]
---

```
Author: Austin
```

### Anu:

 "... according to legend, was the very first being in creation.  It was Anu who fought the battle with the prime Evil Tathament (sometimes called Tiamat), their struggle leading to their deaths, but also the births of the heavens, hells, angels, and demons.  Anu once resided in a single perfect pearl and was the sum of all things:  good and evil, light and dark, physical and mystical, joy and sadness -- all reflected across the crystalline facets of its form.  Seeking a state of total purity and perfection, Anu cast all evil from itself.

All dissonance was gone... for a time.  These discordant parts formed into the being Tathamet / Tiamat, the first Prime Evil.  Though separate beings, Anu and Tathamet were bound within the pearl.  There they warred against each other in an unending clash of light and shadow for ages uncounted, neither gaining the upper hand.  At last, their energies spent after countless millennia of battle, they delivered their final blows.  The energies unleashed by their impossible fury ignited an explosion of light and matter so vast and terrible that it birthed the very universe itself."

**References:**

* [Diablo Wiki, Anu](http://diablo.wikia.com/wiki/Anu)