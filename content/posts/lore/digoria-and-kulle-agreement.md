---
title: "Digoria and Kulle Agreement"
description: "Cas and Alodel battle to stop an army opening the Abyss in Caldeum, while Digoria’s alliance with Zulton Kulle complicates their quest. Magic and betrayal loom large."
tags: [DND5E, Lore]
---

```
Author: Austin
```

Before Cas and Alodel set out to explore the recent happenings in Lut Ghoelin, Digoria parted ways with them, stating that she had other business to attend to.  She also reported that she’d robbed them in their sleep, but tossed a bag of equipment to them as a parting gift -- they’d only weigh her down.

After saving  Lut Gholein’s sultan -- Jehryn -- with the help of Gachev, the party was allowed to view a magical bucket of water from Jehryn’s seeing well.  A well that, once per day, can glimpse and reveal potential futures of the world.  In one imagery created in the water, Cas and Alodel saw Digoria talking to Zulton Kulle and some other fiendish looking creature -- as if making some kind of deal.

Afterwards, at the imploring words of Jehryn, Cas, Alodel, and Gachev borrow a ship from Jehryn to stop the evil forces that have sieged Cladeum.  An army, it seems, has gathered there to open the Abyss!

Meanwhile, Digoria set sail on The Entropy Tide with her crew of 20 spies -- now empowered by two Fey Rings of Atlantis mounted to the ship’s helm.  She returned to Tristram to consult with Zulton Kulle.  Eagerly he speaks of their prior arrangement for her to complete a task to gain his favor in return for assistance of her own.

Digoria reveals that the helmet that grants her captainhood of The Entropy Tide is “driving her more madness than usual.”  Kulle suspects that the helmet “can’t create something that isn’t there”, but “may be exaggerating what thoughts, fears, feelings, doubts, and demons are already crawling around in her mind.”  She further reveals that she has felt “restricted” in some ways since she returned from Charn all those years ago and offers to let Kulle use his magic to search her and her mind for any curses or otherwise magical inhibitions placed upon her.

In return Kulle asks that she make a sacrifice for him.  Specifically -- thanks to Digoria’s bartering he reveals more information -- he is not at his full Lich power until he is “well fed”; and that requires souls to be sacrificed to his phylactery.  Digoria agrees to “try” to help him.  Suspicious of her wording, Kulle begrudgingly accepts the offer.  

As auras flare and the ethers flow, Kulle performs a ritual and is able to confirm that Jadis appears to have indeed blocked some of Digoria’s abilities -- likely until their accord is fulfilled (bringing the Entropy Tide to Charn).  Digoria immediately asks Kulle to remove the blockage, to which he declines and quickly asserts that he will need “more commitment from her” on their agreement.  Kulle wants Digoria to offer him 9 souls.  However, she argues for the “quality” or rather “amplitude” of some souls -- suggesting that perhaps they want some of the same people dead, “after all bounty hunting is part and parcel with being a pirate.”

Kulle declines the offer, and asserts that 9 souls is the number that must be attained.  Next, seeing an opportunity, Digoria attempts to persuade Kulle to give her some insight as to the whereabouts of her nemesis Phage (being the highest on Digoria’s hit list) -- however, the persuasion fails.  In a more desperate persuasion now, Digoria convinces Kulle to “unblock” her abilities so that she can use them to gather the souls he requires.  He agrees, though he is not keen on potentially crossing an interplanar quasi-deity like Jadis.

Kulle summons a pact devil to bind the agreement.  However, the devil reveals that Digoria’s soul (the current of contracting demons) is “not available to barter” as it “is already on another contract.”  Digoria passes a snide look to a (for once) “put aback and in his place” and confuses Zulton Kulle.  “It looks like you’ll be crossing Jadis either way.  Now you just have to decide if you want to do so as a half-ass lich or a full powered one.”

Kulle dismisses the pact devil and, there in the catacombs beneath the Tristram Cathedral, begins the ritual to unblock the power barricading curse placed upon Digoria.  This requires Kulle to open the mummy tomb, revealing that he has some control over the mummy lord within, and further opening some sort of spectral portal located within the mummy’s heart -- as if it were a doorway to a pocket dimension containing a wealth of Kulle’s former power.  Possibly, Digoria’s insight tells her the mummy’s body contains an artifact that leads to Kulle’s archives and thereby his precious phylactery.

Once the ritual is completed, Zulton Kulle reveals while in Charn Digoria gained the abilities of the frost-touched.  However, it seems that this has cost her some of her other bloodline origin abilities as it is known that inter-dimensional travel can warp, alter, and perhaps even re-write time.  The favorment of the cold seems to have also cost Digoria her inherited hellish resistance to fire, but also left much “untapped potential” as a mage as far as her memory to recall spells for innate casting.

With the ritual completed, Digoria prepares to depart the catacombs.

> Kulle:  Where are you going now?

> Digoria:  Thanks to you, I’m going to stop an army of ghouls.

> Kulle:  Your attempts will be in vein…

> Digoria:  Perhaps.  But where’s the adventure in living forever anyway?  Immortality is a coward’s way out. (begins walking out the door, as Kulle grimaces at the cutting words)

> Kulle:  Why do you protect the fighter and the cleric?  Mages like you and I could rule this realm you know?

> Digoria:  They remind me of a band of comrades that died battling a sea serpent that attacked a ship I was on long ago… a serpent that I had to make the killing blow myself, even as a child.

> Kulle:  So, there is honor among thieves after all…

> Digoria:  …and predictability is a weakness.

> Kulle:  At any rate… We have an accord, yes?

> Digoria:  We have an accord.  (leaving for real now) … Oh, and one more thing Kulle.  If you harm the cleric or the fighter in my company… I’ll kill you myself.

> Kulle:  Very well then…

As Digoria departs, a lanky and shadowy figure steps forth from the shadows.  Karasu speaks out to Kulle, “You know she will find a way to betray you….”  Kulle replies, “It will not matter.  We can leave this world to be torn in conflict between The Abyss and The Nine Hells.  The magic in this world has all but been used up compared to the Fey Wilds.  Imagine Karasu… if you fed on fey instead of men?  And you, Phage, there would be a comparatively endless world to rampage and ravage through.  Now, let us proceed.”

Back on the shores of Caldeum, Digoria docks the Entropy Tide and looks up on the city, swarming with ghouls flooding into it’s gates… It’s an ambush!  She curses out loud that “that bloody lunkhead Cas has walked straight into a trap! (with a drow priest in the center and more than 50 ghouls blocking the way out).” 

Before stepping of the ship, Digoria looks up at the sky an notices that the sun appears to carry an unusually read tone and appears larger than usual, though perhaps this is an effect of the hazy damp sky sprinkling snow to the group -- though that in itself suggest something amiss here in the dessert.

As heavy winds rise Digoria rallies her crewmen to charge into battle and fight her way into Caldeum in pursuit of Cas and Alodel.  The first adversaries, two packs of 20 ghouls prove somewhat worthy as the undead crewmen and ghouls exchange blades.  Digoria unleashes a fury of lightning and snow spells to fell the first mob, though the second mob does not fall without taking the first rank of crewmen with them.  The second rank of crewmen act quickly to avenge the others and defeat the second mob.

Before anyone can catch their breath, two more mobs stand between Digoria and the city gates.  Swords continue to clash and more snow and lighting flash as thunder echoes throughout the city.  The second rank of crewmen fall before either the third or fourth mobs can be killed, leaving Digoria to fend for herself.  She gives no sympathy to the fallen crewmen as she knows that -- per the curse of The Entropy Tide -- the crewmen will once again take form upon the ship on sunset next.

Digoria decides to go down swinging and fights until she herself falls against the army of ghouls.  When she falls to the ground, neigh unconscious, a final epic and explosive ice storm erupts as she does.  The storm, magically enchanted seemingly beyond Digoria’s own capabilities, freezes and pummels the remaining ghouls outside the city walls -- protecting Digoria from immediate danger.  However, the spell also erupts so thunderously that is breaks a hole in part of the city’s more brittle stonework, revealing it to the harsh elements outside.

At last, Cas, Alodel, and Gachev are left to fend off the remaining ghouls and stop The Abyss from being opened.  In short order to follow, the party will surely become aware that they’ve gained the uneasy and glaring attention of some great elemental force of reckoning.


