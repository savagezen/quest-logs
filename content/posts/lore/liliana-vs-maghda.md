---
title: "Battle at Westmarch: Liliana vs. Maghda"
description: "A 1v1 death battle between a fey warlock and human necromancer in the gothic city of Westmarch."
date: 2024-10-24
tags: [DND5E, Lore]
---

```
Game: Dungeons & Dragons 5e
Relevant Campaign: Black Label Society
Relevant Quest: Liches Be Schemin'
```

### Part 1:

> Author: ChatGPT

**The Battle of Westmarch: Liliana Vess vs. Maghda**

The air above Westmarch was thick with a grim, oppressive silence, the kind that comes only after tragedy. The gothic spires of the once-thriving city lay in ruins, the streets abandoned save for the remnants of battles long fought. Malthael’s attack had stripped the city of life, leaving its grand cathedrals and crumbling facades as hollow as the souls that had perished here.

But tonight, the silence would be broken.

Standing at the heart of the desolate square was Liliana Vess, her violet eyes scanning the dark horizon. Clad in her flowing black robes, the necromancer’s beauty was ethereal, framed by raven hair that fell over her shoulders like a shroud. She radiated a cold, calculated confidence—a summoner of the dead, a mistress of necromancy.

Across from her stood Maghda, the twisted fey witch. Her grotesque wings twitched, her dark eyes gleaming with sadistic glee. Draped over her shoulders, pulsing with cursed power, was the Chain Veil. The artifact’s aura rippled around her like a black storm, giving her the air of a predator who had already decided the outcome of the hunt.

“I must admit,” Maghda said, her voice sickly sweet, “I admire your power, Liliana. The Chain Veil and I could make you even greater. Join me. Together, we could bring Belial back to the world.”

Liliana smirked, her lips curling in disdain. “You’re as deluded as ever, Maghda. And I don’t share power.”

Maghda’s eyes darkened, and without another word, she extended her hands, unleashing a vile swarm of flies that buzzed toward Liliana, their tiny wings carrying death.

---

**The Clash of Summoners**

With a single motion, Liliana waved her hand, and a shield of necromantic energy burst from the ground, incinerating the swarm before it could reach her. But she didn’t stand still. Even as Maghda cackled, Liliana was already weaving a spell, calling forth the fallen spirits that lingered beneath Westmarch’s cobblestone streets.

“Rise,” she commanded in a cold whisper.

The earth trembled as skeletal knights, their bones clattering and their hollow eyes glowing with unnatural light, clawed their way out of the ground. Former paladins of Westmarch, long forgotten, now stood at Liliana’s command, their spectral weapons ready.

Maghda’s grin faltered, but only for a moment. With a sharp, guttural hiss, she spat another cloud of flies at the summoned undead, and this time, they hit their mark. The paladins shrieked as the swarm consumed them, tearing through their bones and reducing them to dust in seconds.

Maghda laughed darkly. “You’ll need more than that to stop me.”

Liliana’s eyes narrowed. She knew that Maghda’s power was vast—bolstered by the Chain Veil, the fey witch was more dangerous than ever. Worse, with each critical strike, Maghda’s abilities allowed her to summon fanatical cultists, more bodies to throw into the fray.

But Liliana was nothing if not a tactician. She didn’t need to overwhelm Maghda with raw power—she needed to outthink her.

---

**A Moment’s Distraction**

Liliana’s sharp gaze flickered across the ruins. There, not far from where she stood, was Bailey’s tavern—a half-destroyed building still standing defiantly against the devastation around it. In the doorway, she spotted Bailey himself, the burly bartender who had somehow survived it all. She knew him—knew his admiration for her. His presence was a complication, and she hoped he would stay out of sight.

But Bailey, oblivious to the danger, cupped his hands around his mouth and shouted, “Liliana! The cemetery! It’s near the east gate! You can raise them all!”

Liliana’s heart raced. The cemetery. If she could reach it, she could raise an army of paladins and crusaders—the very ones who had died defending Westmarch. With their strength, she could crush Maghda’s forces. But first, she had to survive long enough to get there.

Maghda’s head snapped toward Bailey, her eyes gleaming with malice. “How sweet. A pathetic human, still clinging to hope.”

With a flick of her wrist, Maghda unleashed her dark magic toward Bailey. Liliana acted instantly. Her hands wove a spell, and a thick fog of shadow engulfed the area, obscuring Bailey from view and sending Maghda’s attack crashing harmlessly into the stone walls of the tavern.

“You’ll have to deal with me first,” Liliana growled.

---

**Maghda’s Cultists and the Corruption**

Angered by the interference, Maghda shrieked, and the Chain Veil flared with power. In an instant, a group of robed cultists materialized from the shadows, summoned by Maghda’s will. They brandished crude weapons, their faces twisted with zealotry as they charged toward Liliana. 

Liliana gritted her teeth. Summoning her own forces now would be a waste of time—they would be overwhelmed by Maghda’s reinforcements. No, she needed to distract the witch, buy herself the time to reach the cemetery.

As the cultists closed in, Liliana raised her hand and summoned a pack of shadowy beasts, their forms indistinct but terrifying, with glowing red eyes and dripping fangs. They lunged at the cultists, tearing into their ranks and scattering them, but Liliana didn’t wait to watch the battle unfold.

She ran.

---

**A Desperate Race**

The east gate was within sight, and beyond it, the cemetery lay shrouded in mist and silence. Liliana could feel the energy of the dead radiating from beneath the soil, calling to her. She could feel the power that awaited her—an army of warriors who had once served the Light, now ready to serve her command.

But Maghda wasn’t far behind.

As Liliana sprinted through the narrow streets, Maghda soared above, her wings flapping with unnatural speed. From her mouth, she spat another swarm of flies, and they buzzed through the air, closing in on Liliana. The necromancer barely had time to react, throwing up a barrier of shadow that absorbed the swarm just in time.

“You can’t run forever, Liliana!” Maghda’s voice echoed through the streets. “Come back and face me!”

But Liliana wasn’t listening. She was almost there.

---

**The Cemetery and the Army of the Dead**

The gates of the cemetery loomed before her, black and wrought iron, creaking open as Liliana approached. She could feel the weight of the spirits buried beneath the ground, their restless souls yearning for release. Without hesitation, she raised both hands and whispered the incantation she had been saving.

“Rise, and serve.”

The earth trembled, and the graves split open as the dead surged from their resting places. Hundreds of paladins and crusaders, still clad in their gleaming armor, rose from the soil, their eyes burning with the fires of undeath. Their swords gleamed in the dim light as they stood ready, waiting for Liliana’s command.

Maghda landed at the gate, her cultists scrambling behind her. She took in the sight of Liliana’s army and sneered. “You think these pathetic ghosts will stop me?”

Liliana smiled coldly. “They’re not ghosts. They’re the ones who once fought your kind—and now, they’ll fight again.”

With a wave of her hand, Liliana unleashed her undead army. The paladins charged forward, their spectral blades cutting through Maghda’s cultists with ease. The battle raged around the cemetery, Maghda’s forces being pushed back by the sheer force of Liliana’s summoned legion.

---

**The Final Confrontation**

Maghda screeched in rage. With a desperate surge of power, she unleashed a wave of dark magic, corrupting the very ground beneath her. Bystanders who had hidden in the shadows screamed as Maghda’s magic took hold, twisting their bodies and warping their minds. They lunged at Liliana, now mere puppets in Maghda’s grip.

But Liliana’s will was unbreakable. With a single, deadly gesture, she summoned a torrent of necromantic energy that ripped through the possessed townspeople, severing Maghda’s hold and laying waste to all in her path. 

Maghda, now alone, her forces decimated, faced Liliana in the center of the cemetery. The Chain Veil pulsed on her shoulders, but it was no longer enough to tip the balance. Liliana’s army surrounded her, their weapons ready to strike.

Liliana raised her hand once more. “This is your end, Maghda.”

The paladins descended on the fey witch in a furious onslaught, their blades cleaving through her dark magic. Maghda screamed, her wings flailing, but she was overwhelmed. The Chain Veil fell from her shoulders, its power draining as its master crumbled under the might of the undead.

With a final, agonized shriek, Maghda fell, her body dissolving into dust. The battle was over.

---

**Epilogue**

Liliana stood amidst the ruins of Westmarch’s cemetery, the Chain Veil lying at her feet. She stared at it for a long moment, feeling its dark allure. It had almost been too tempting.

The Chain Veil’s dark power still pulsed faintly, whispering promises of unimaginable strength. Liliana could feel its seductive pull, the same pull that had consumed Maghda and so many before her. For a moment, she considered it—what it could mean to wield the Veil’s power, to command death and life itself with even greater ease.

But Liliana was no stranger to temptation. She had danced with it before and had always emerged the victor. With a sharp breath, she turned her back on the artifact, letting it lie forgotten in the dust of Westmarch.

Her army of undead crusaders and paladins stood at attention, awaiting their next command, but Liliana gave none. She had won the battle, defeated Maghda, and survived the temptation of the Veil. There was no need for more destruction.

As the celestial onlookers watched silently from above, Liliana walked away, leaving the ruins of Westmarch behind her. She had proven her strength, not by the power of an artifact, but by her own will.

And in the shadows, she knew she would always be ready for the next battle.

---

### Part 2:

> Author: Austin

In her fleeting breaths, as Maghda's physical form dissipates into a conglomerate of ash, moths, and butterflies, she spews blood and coughs:

> "The prophecy has been seen in Jehryn's well, that [hatred will return to Sanctuary](https://diablo.fandom.com/wiki/Diablo_IV), but it is always [lies](https://diablo.fandom.com/wiki/Belial) and [terror](https://diablo.fandom.com/wiki/Diablo) that sew the seeds for hatred to grow.  The Horadrim that swore to protect Sanctuary will succumb to their own [sins](https://diablo.fandom.com/wiki/Azmodan) -- isolation (Lorath), fear (Donnan), pride (Tyrael), zeal (Elias), and greed (Zoltun) -- and bring about unspeakable and otherworldly devastation upon Sanctuary."

The Serra Angels descend upon Liliana to retrieve the Chain Veil.  It's unclear if Maghda is truly vanquished or not.  Nevertheless, Liliana wafts her hand through the air to collect a sample of butterfly-ash and comments to herself, *"You never known when you'll need some fey blood-magic."*

A dozen Serra Angels have surrounded Liliana.  She stands tall and faces them and asserts herself:

> "There... you have it.  I am neither in possession of The Chain Veil, nor am I need of such artifacts to relieve Sanctuary of what threatens it.  Am I dismissed?"

The angels stand silent in judgment.  They appear able to telepathically, or otherwise, consult with Niv-Mizzet himself and / or with Alodel.  After a long silence, one angel retrieves the Chain Veil from the ground, and the other motions that Liliana is free to go.

Liliana comments:

> "I don't suppose you've any insight where the others have gone looking for that thing (The Chain Veil)?"

Bartender Bailey, in his slobbering, boyish affection for Liliana, brings her a horse, and reminds her that the party was headed for Tristram for leads, however *"... they might have had better luck stopping in Leoric's Hunting Grounds to search The Manor that's rummored to have been occupied by Maghda and her cult."*

Liliana offers the closest thing to a compliment she can, that Bailey *"really is quite insightful and well read for slavish lump."*  Bailey blushed and winks.  Liliana shutters and mounts the horse, riding off east towards Tristram.

However, the chilling aura of Maghda's last words remain.  What evils have been at work since the fall of the high heavens?  Surely Liliana's brother and sworn enemy, the lich now known as Zoltun Kulle, has not been idle while Liliana was imprisoned in Dexa.  Nevertheless, the rest of the party is likely searching Maghda's Manor in Leoric's Hunting Grounds for the Chain Veil which is not there.

In other words, *it's a trap!*

Without the oversight or probation of the Serra Angels, Liliana no longer has any restrictions and aims to catch up with the rest of the party, before Maghda -- what remains of her -- or any other foul traps of fate may beset them.
