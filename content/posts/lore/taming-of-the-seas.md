---
title: "The Taming of the Seas"
description: "In Sanctuary, adventurers face an uneventful sea, disrupted by demigod Jadis, whose connection to the Primal Forces reshapes the oceans and risks chaos."
tags: [DND5E, Lore]
---

### The Taming of the Seas

Adventurers in the world of Sanctuary have, in recent decades, found adventures at sea to be generally uneventful especially compared to the stories of yore.  While landfall has remained host to frequent exploration, excavation, and conflict, the oceans of the world have been strangely uneventful -- relatively speaking of course.  But why?

The Primal Forces … they are the physics by which all things move, they are the wind that blows the flat of the weave between space and time; extending beyond and between godhood and mortality, divinity and profanity, structure and disarray. 

Many powerful entities in ascent to godhood or who have fallen from their godly ranks have sought the primal forces to restore their former powers or to seek out new ones.  While lesser and greater deities gain power from the strength of their worshipers and by conquering other domains; deceased deities from our realm and from others find themselves powerless and adrift among the Astral Plane.

Wanting connection to either the material or godly worlds, the Primal Forces provide a conduit to focus what remains of their energy as the Primal Forces manifest in all planes of existence and even perhaps extend to and penetrate the outer planes themselves.

The demigod Jadis of Charn had her home world destroyed and was banished to the Astral Plane by her conqueror.  However, she has found connection to Rashul, the material primal force.  It is no secret in the lore that it is well within her domain to “devour worlds.”

As such, the growing presence of Jadis in the world of Sanctuary has brought a little solidarity to the seas of the world.  That is, her lawful and structured nature has pulled from the once restless and tumultuous seas to   build ice and snow consolidating near her summoning point.

Legends say that Jadis has destroyed her own race and home world in order to conquer it for herself and that that world too was destroyed by her hand and eventually annihilated by her “final” (most recent) defeat.  The strengthening of Rashul in the world of Sanctuary explains the “taming of the seas” quite well as even the energies of the Primal Forces are not without an economy of transaction.  This is all the more true when considering deities such as Jadis coming nearer to the plane(s) of elemental chaos (and thereby an elemental alliance of their choosing).

It seems as well that as the presence and competition for these Primal Forces grows on the material plane, the barriers between other planes of existence (for example Feywild and Shadowfell), making transitions between them much easier and lucid.

Other Primal Forces may be sought by other quasi divine beings (Celestials for example) in pursuit of their own ascent to power for various reasons.  History is rifely storied with tales of many a necromancer and dark wizards, sorcerers, and warlocks pursuant of vile forces as well.

Caution should be taken, as the relenting of the solidarity held by Jadis’s frigid structure could result in a backlash of entropic force that was temporarily held at bay by  the inertia of her frosty presence.
