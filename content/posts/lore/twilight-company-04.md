---
title: "The Twilight Company - The Day Heaven Died"
description: "Izalea, Fenler, and their party join forces with Charlemagne to stop a rogue angel wreaking havoc. Amid deadly fungal threats and dark magic, they navigate dangers in a quest to save the realm."
tags: [DND5E, Lore]
---

```
Lore: Ryan
```

### Chapter 4: The Day Heaven Died

> Izalea: So, Charlemagne, was it? We hear that you bring dire news related to our quest.

> Charlie:  Please call me Charlie. And yes, my superior sent me to inform you, specifically, about an angel that has gone rogue and now wreaking havoc on the material plane. 

> Fenler: Why us? I don’t see what we could do about a pissed off agent of divine wrath…

> Izalea: You don’t know what a human is, but you know about angels…?

> Fenler: …… I know things, I guess.

> Izalea: Right.., anyways the lump brings up a good point. 

> Charlie: I don’t know why the bossman asked for you. You barely survived getting me out, no offense, but he says you may have the key to stopping that winged brat before his temper tantrum turns the world to ash. Regardless, a collaborative effort to form a resistance group by our fey brethren and the celestials has started. Apparently, the angel has been probing some messy magics to increase his power. Again, I don’t know the details, but it’s a major threat. The bossman has requested I bring you to the resistance group. Maybe they know more about what we can do. 

> Izalea: What do y’all think? I don’t know how this will progress our goals, but I don’t think we can really ignore a monumental threat like this. 

> Galinis: Well, we aren’t particularly following any real leads anyway. Plus, how many chances does one get to duke it out with an angel?

> Altair: I’d venture fewer still get away alive from one. In any case, I think it’d be worth making some contacts with both powerful fey and celestials. They might be able to provide us with info that gives us a tangible lead. 

> Luvese: And seeing as how the mother was a fey goddess with angelic agents at her disposal, it’s worth it to my cause. 

All eyes turn toward Fenler, who is staring blankly at a tree and the pair of squirrels playing in the branches. He snaps to attention under their impatient glare.

> Fenler: Huh!? Oh, yes. It sounds dire indeed! We must go and help. 

With all in agreement, the party treks back to the southern continent. Charlie is able to guide them east across the land to the location he was supposed to meet the resistance. With the major delay, the group has moved on eastward. The angel’s presence has more than left its mark, however. Through the scars of battle and the tales of the locals, the party is able to follow the trail of cataclysm to small series of tunnels weaving through the shallows of the Underdark. 

The group warily follows the magical trail through the tunnels, finding a region rich with fungal growth. Many of these saprophytic lifeforms are mobile and appear to be very agitated by the residual magic energy. As the party crosses the threshold of a chamber teeming with sentient fungal beings, a trio of them becomes enraged and attacks. Fenler, sensing the attack, strikes first by slinging his hunting shuriken into the diving fungus bat. Galinis follows suit and throws his intelligent sword Joffrey at the bat. The weapon hits and disintegrates the creature. Luvese catches site of a russet mold to her side and, unthinking, attempts her poison spray cantrip on it.

The mold is, of course, immune to poisons and suffers no ill effects. Another creature, a humanoid mushroom aptly called a shroom, casts a stinking cloud spell on the group. Only Charlemagne and Altair are out of its range. Altair urgently seeks to help his friends and shoots the shroom with an arrow. The shrooms chitinous skin seems only marginally affected by the ammunition, however. Gal rushes by Fenler, who is choking on the stinking cloud, and chops away at the shroom with Jeff. Luvese resists the stink as well and tries her poison spray again, this time on the shroom. The fungus man is stung by the poison and lets his focus slip on the cloud, which immediately dissipates. Izalea positions herself between the shroom and the russet mold and shouts a word of radiance. The shroom resists the magic, but the mold crumbles under its force.

The shroom retaliates by using a powered up thunderwave to blast Iza and Luv away from it. Gal is able to resist and gears up for another swing. His strike is off mark due to his target taking blows from Altair’s bow and Fenler’s short sword. Despite the miss, Luv is able to finish the shroom with another poison spray. As the mushroom man melts to the ground, Izalea spots another creature fast approaching. It is an algoid, a living colony of algae instilled with a modicum of intelligence. Iza sees the creature’s aggressive charge and immediately releases a divine bolt. The crackle of her magic is followed up with the satisfying thump of Altair’s arrow slamming into the creature’s center. Undaunted, the algoid continues its advance and unleashes a psionic mind blast. The dull wave washes over the whole party, and leaves all but Galinis and Altair stunned and completely immobile.

Sensing the severity of the situation, Galinis draws forth his bubbling rage and nearly drops the creature with his magic greatsword. Another of Altair’s arrows thumps into the creature as it slams a mighty arm into Gal’s chest. The nimble eladrin lands in a roll and springs back at the algoid. He carries the momentum into his swing and nearly bifurcates the algal monster, killing it soundly. The battle over for now, Altair and Galinis wait on their allies to sober up before progressing.

> Galinis:  I don’t like mushrooms…

> Fenler:  I don’t like caves….

> Luvese: I know fungi need dead things to eat and fungal creatures will hunt when food is scarce, but something seems odd. 

> Altair: As in the fact that the shroom and its pet attacked us when so obviously outnumbered and outclassed? The algoid, the bigger one that charged us, isn’t the brightest of cave dwellers, so I can see it making a foolhardy move. Shrooms are very intelligent, though and I’d venture it’d be wiser. Plus, they usually work in groups.

> Luvese: Even beyond all of that, they just seemed more agitated. 

> Izalea: Could it have something to do with the residual magic energy from that fallen angel fellow? 

> Charlie: Or maybe also this. 

All eyes turn to Charlemagne, who has wandered into a nearby opening. Inside the companions discover mounds of fungal corpses, mostly shrooms but including the other inhabitants of these caverns. Upon closer investigation of the surrounding central caverns, the party discovers signs of violent combat. Scorch marks on the wall, humanoid blood, portions of the cavern wall crumbled to dust. 

> Luvese: (On the verge of tears) This is awful, it looks like the poor things couldn’t even fight back.

> Izalea: We must catch up and try to put a stop to this monster’s rampage before any other innocent beings become collateral damage.

> Fenler: We might be delayed a bit on that, we’ve got more pissed off mushrooms coming our way.

The groups conversation and general lack of finesse has attracted more hostile and animalistic fungal creatures. This group consists of three monsters, ranging from the size of a halfling to the size of an ogre. The smallest is a Phycomid, nothing more than a blob of decomposing matter covered in toadstools. The second is a Fungoid, a humanoid shaped being of fungal tissue with gills on its anterior side that resemble fur. The largest is an Ascomoid, a boulder-sized brownish-green puffball. They are advancing quickly on the group. Altair springs into action and plugs the phycomid with an arrow, hoping to kill it immediately.

The monster is resilient, however, and Altair barely has another arrow nocked when the ground ruptures underneath him and tentacles tear into his side. The culprit is a mandragora, a roughly humanoid shaped creature, with fungal mass at its core and twisted roots forming its appendages. Though surprised, Altair is able to slip free of the tentacles encircling him and opens the way for Fenler to chop the thing to bits with his swords. Izalea blasts the phycomid with her divine bolt, leaving only a tiny fraction of the thing remaining to continue its advance. It slows to a final stop, though, as Luvese’s frostbite spell chills the last of its life force out. Galinis charges the fungoid, unaware of the silent death that is an olive slime looming above him. Gal delivers a mighty slash with Jeff, but the fungoid retorts with an equally mighty punch that nearly staggers the eladrin.

Charlie watches from a sheltered corner as the Ascomoid contracts and blasts the group with its toxic spores. Iza, Fenler, and Altair all reel over nauseated and unable to act for the moment. Galinis, caught in the heat of the moment, doesn’t notice the plop of the olive slime’s droplet as it drips on him. Galinis then hacks another chunk of the fungoid away, whilst nimbly ducking both of the thing’s punches. The ascomoid tries to take advantage of its sickened foes and charges, attempting to trample Gal in the process. Galinis is already mobile and slips out of the way, but Fenler isn’t so lucky. The giant puffball slams him with all three-hundred pounds of its bulk. With the monstrosity so close, Fenler, Izalea and Luvese focus on it with their swords, divine bolt, and punishing strike. However, the soft mass of the ascomoid seems to cushion the blows of the mundane weapons. Meanwhile, Altair assists his brother by sticking the fungoid with another arrow in what appears to be the thing’s face.

The recoil spurs Galinis to finish it with a final horizontal swing, while inadvertently dodging another drip by the olive slime. The ascomoid slams Fenler again and is punished by the black burst from Luvese’s last spell. It is further punished as an arrow, scimitar, and punishing strike chip away at its life. Galinis tries to join fray, being temporarily interrupted as olive slime plops right on his head. He ignores it and joins the others hacking the ascomoid to death. Afterward, he slings Jeff full force into the hanging olive slime and obliterates it. What he doesn’t realize is that the thing lives on, slowly spreading on his body and trying to infect his nervous system with its foul magic. He must ignore it for now, however, because even as Luvese is trying to heal Fenler’s fresh wounds, another large group of humanoid creatures approach. 

Altair shouts that they come peacefully and wish no conflict. The creatures seem no less dissuaded, so he repeats himself in undercommon. The group halts and turns to the largest two members. The heroes study them as the creatures silently communicate.  They are Vegepygmies, man-shaped monsters that seem to be equal parts plant and fungus. The two large ones, presumably the chiefs, turn away from each other and the remaining workers and warriors then turn in unison to the comrades and begin the assault. With a shrug, Altair and Galinis tag team and kill one of the workers, while another claws at Gal and misses to the side. Fenler slings his hunting shuriken with so much force that it splits the attackers head.

One of the chiefs unleashes a cone of spores on the men, leaving both Galinis and Altair paralyzed. Luvese and Izalea combo their frostbite and divine bolt to end another worker. Meanwhile, two of the guards skewer the immobile Galinis with their javelins and leave him to collapse unconscious. Another guard throws two javelins in succession at Fenler, but something magnificent happens upon seeing his ally fall. A blast of light envelopes him momentarily and he emerges in flight on two white feathered wings. He is easily able to dodge both javelins and whisks right by the still-paralyzed Altair. He slams into one of the guards that dropped Galinis and explodes into a fury of blades. He unleashes his divine energy into both swords, the short sword striking especially true and piercing deep into the monster’s face. The onslaught annihilates the guard but gives its partner the opening to hit Fenler with a ferocious claw, knocking him out of the air.

Luvese thinks quickly and casts a Lifetap spell to siphon the guard’s life force back into Fenler. Izalea follows with a Tearing Claws spell that minces the already injured guard and wounds another guard and one of the chiefs. The chief comes after Izalea with both claws, but she gracefully dodges them. The injured guard’s javelin manages to glance her, but she continues her stride to evade the follow-up jab. Altair still watches, helpless and paralyzed, as his brother bleeds out only a few yards out of reach. Fenler dives into the remaining guard, again charging both weapons with divine power. The guard barely survives and joins the uninjured chief as they both spear Fenler. Izalea nukes the guard with her divine bolt while simultaneously channel her healing light into the much-wounded Fenler. Luvese sees the other chief approaching Galinis to finish him and quickly calls on her healing magic. Gal opens his eyes just in time to avoid the lethal claw attack.

He goes into a rage and avenges himself by slashing the chief. The chief tries to retaliate with another pair of claw strikes, but Galinis is able to jump clean over them and slam his blade into the chief, killing it. Altair frees himself from the paralysis but is still visible ill. Even in this pain, he still manages to put down the remaining guard with a well-placed shot. The final chief stands alone against the group now and tries a last-ditch spore blast to paralyze everyone. It utterly fails, however, and even Charlie joins in as the group batters the thing. It does manage to spear Fenler one last time before Galinis splits it in two. Worn out, the group then sets up camp a short distance away and takes some much-needed rest. Luvese is luckily able to identify the olive slime infecting Galinis and cleans its away before it invades his psyche and slowly eats him alive. The night goes by without incident, as does the rest of the groups subterranean journey. Soon enough, they spot daylight in the next cavern and burst into the sun peeking through the trees of a very old forest. 

This hidden wood teems with magic. Upon setting foot into the daylight, the party can feel the life flowing through it. The trail of the angel’s destruction is clear though, both physically and magically. The group follows the swath of desolation deeper into the forest. 

> Izalea:  I think we are getting close; the corruption is becoming worse.

> Luvese: Yes, the poor flora here is twisted and suffering. We must put a stop to this now. 

> Fenler: I feel strange here. Something is drawing me forward, but It sickens me at the same time. 

> Galinis: I told you not to eat the mushrooms.

> Fenler: I didn’t, I promise. Well, maybe just one. But it’s more like a guilty feeling. (Altair looks at him suspiciously). 

> Izalea: Well, something’s strange about you. I mean, you did sprout wings in there. And your memory hasn’t been the most reliable thing. But you can worry about it later. We’ve got more important things to deal with presently. 

> Charlie: Rest easy, lunk, I may have some inkling as to what’s going on with you. But as milady said, bigger fish. 

With that, the group progresses. Altair hangs back, drawing Galinis to the rear as well. He whispers his concerns to his brother, unnoticed to all but Charlemagne’s enhanced feline hearing. The cat trots on, pretending not to notice. Another thing to deal with later, he thinks to himself.

The brambles to either side have thickened this deep in the wood. The path widens around a particularly grotesque tree, almost as if to give it a wide berth. Luvese recognizes it as a hangman tree, a nasty piece of flora that nourishes itself by stringing up any living thing that comes to near and dropping the strangled victims into the mouth buried in its crown. The tree uproots and slowly starts to approach. Seeing no way around, the group engages it. Galinis acts first, launching his sword and causing it to lodge into the tree’s thick bark. Izalea follows with a divine bolt that merely bounces off the bark. As the tree’s foul-smelling sap starts to ooze from the sword wound, it retaliates by releasing a cloud of a spore-like substance into the air. Charlie and Iza begin to hallucinate, turning against their allies to defend the tree.

Fenler dives in and connects with both weapons, although the short sword can’t breach the bark. Altair prepares to launch an arrow but slips his shot after Charlie pounces into his face. Luvese calls on her Frostbite to bring an early winter on the tree, but it is able to resist the cold for now. Galinis calls Jeff to break loose from the tree and grabs it as he dives and rolls up to slash the tree again, leaving another garish wound in its trunk. Izalea shoots a divine bolt right at Fenler, who is able to dodge but knocked off balance. The tree takes advantage of the situation and whips him with a pair of vines, using one to tangle him up. A third vines catches Gal, but his avoids the ensnarement of his ally. Altair throws Charlie aside and lands a shot this time, although the arrow seems minimally effective against the sturdy tree. Luvese tries to freeze the tree again, but this time with the more powerful Creeping Cold spell. The tree still resists the initial chill, but the cold begins slowly intensifying.

Gal gears up for another swing but is interrupted by another pair of vine whips lashing him and he becomes entangled. The tree tightens its hold on Fenler, while trying to whip him with a free vine. He jumps out of the way, but the tree takes the airborne foe and strings him up. Desperate, Fenler again finds himself transformed into his strange new form, although he can’t seem to cut the mobile vines away from himself. Charlie shakes himself free of the enchantment and bounds towards the still charmed Izalea to distract her. Luvese follows with a punishing strike. Galinis is again interrupted as he drops to the ground underneath Izalea’s latest friendly fire. Luvese’s creeping cold spell finally chills the tree, which has a peculiar response to icy effects. It becomes completely immobile, offering no resistance to attack. Fenler and Altair take advantage of the tree’s paralysis and execute it with a combination coup de gras. Fenler lands atop the tree, still suffocating, as Altair bounds up to join him. Twin swords pry open the thing’s mouth while Altair fires a well-placed arrow directly into the tree’s soft core. Fenler and Galinis feel their bounds go limp, and the group warily presses on. 

Not more than a score of yards later are they attacked again. The newest foe is another monstrous tree, called a bloodsuckle. Vines ending in needle-fine points writhe around the foul green leaves that are coated in a sticky noxious sap. The tree snaps a vine at Izalea, trying to wrap her up in a deadly embrace. Her alertness is rewarded, though, and she springs out of the way. Altair spots the assault and snipes the plant. The arrow finds much softer bark on this one and buries itself. The softer tree is no less resilient, however, and easily resists Luvese’s frostbite spell. Fenler tries to score hit with his hunting shuriken while he closes the gap but can’t get a clear shot with Izalea hopping around.

She lands in a roll and fires off a divine bolt upon landing that blows several of the tree’s foliage away. Galinis also hacks away, pruning several branches. The bloodsuckle also wraps him up, though, and jabs a needle point into his shoulder. He feels a vile poison try to dominate his consciousness. The willful elf shakes his mind loose and haphazardly misses another swing as Altair lands a shot. Luvese lands a punishing strike and Fenler scores with his shortsword as the trees repeats its maneuver on Iza, also injecting her with the poisonous sap. She drops her divine bolt to resist the poison, while Altair’s third arrow finishes the monster. As the monster falls, the party hears a dull rumbling in the nearby trees. Into the clearing enters a treant. The creature looks smaller than its common kin and its black bark is twisted grotesquely. Thick tears of sap run from its eyes and down its trunk, sizzling corrosively as they hit the ground. The treant speaks to the party in a deep and drawn-out hollow voice.

> Treant: Why do you harm my forest mortals!? 

> Luvese: The forest attacks us, noble treant. We fear its nature has been corrupted by a fallen angel wreaking havoc here. We seek to…

> Treant: (Cutting her off and exploding with an unnatural rage) You are with the angel. You have brought death unto my forest! I will crush you!

> Luvese: (The treant bearing down on her) No, you misunderstand! Please calm yourself! No!

The monster prepares to swing. Luvese quickly casts frostbite to slow the monster. The tree resists and swats her away. Fenler closes the gap on wing, slinging his hunting shuriken into the weeping treant. The treant returns fire by hurling a rock, but Fenler spirals out of the way. Galinis begins hacking away at the tree’s trunk with his magic sword but is splashed by the acidic tears in the process. Izalea launches a divine bolt as Luvese succeeds with a frostbite. Altair’s arrow and Fenler’s shortsword make contact but are minimally effective thanks to the treant’s thick bark. To add some juice to the blow, Fenler channels some divine energy to power up the weapon. He is also showered with acidic tears, but he and Gal both are able to continue fighting through the pain. The pain intensifies for both as the treant launches a flurry of slamming blows. Galinis dives and rolls over the first swing but is caught by the branch’s backhand. Fenler is too close to the treant’s center and takes a direct hit, bouncing off the ground before regaining his flight.

Galinis starts to spring back into melee, but the continuing effects of the acid cause him to collapse mid-stride. Izalea formulates a plan and casts Elemental Blade to conjure a blade of fire. Altair takes steady aim and plants an arrow in the treant’s eye, where the armor is thinner. Luvese dives into the melee to awaken Gal with a healing spell. Fenler tries to cover her by stabbing and slicing the tree with his swords. The tree is relentless, though, and knocks Galinis back out just as he stands, with Fenler joining his sleep this time. The third slam swats Luvese almost into Izalea as she darts in to slash the treant with her flaming sword. The treant howls in agony as it catches fire, then falls in mighty heap as it tries to douse the flames. Altair lands another shot on the downed foe, while Luvese finishes it with another Frostbite. She says a short prayer for the corrupted guardian of nature and whips over to Galinis. Her Cure Wounds spell awakens him, but he is still looking very rough. Desperate, she attempts a trick taught by the Circle of Dreams, called Balm of the Summer Court.

It succeeds and Galinis is restored to near perfect condition. Izalea also calls a trick of her trade, the Healing Light to restore Fenler. He also is less than optimal, however, and channels a fraction of his divine energy to use a Healing Touch on himself. Healed but weary, the group continues down the path of destruction until they reach its end. Here, they come up a monster that looks like a cave giant covered in fungal hyphae in place of hair. The thing booms at them as they approach. 

> Lanose: Ah, lunch has arrived. 

> Galinis: Move aside giant. I detest your kind and will happily slay you otherwise. 

> Altair: And yes, we are good at bringing giant’s tumbling down. 

> Lanose: (Laughing boisterously) Fools! I am no giant, this is merely a shell. Your tricks will not work against me. My master says to let no one pass into the glade beyond, so I will feast on your still bleeding corpses. 

The lanose spots Altair and Luvese each casting a spell and begins to pull its greataxe from the ground. The cerviataur shoots a Hail of Thorns empowered arrow at the beast, but the missile is deflected by the emerged axe. Luvese completes her Creeping Cold spell, but the lanose resists the initial chill. The creeping frost sets in however and starts to climb the creature. Galinis roars a battle cry as he flies into a rage and sends Jeff flying toward the monster. The lanose moves surprisingly nimbly and sidesteps the zooming sword. The monster’s body hair seems to grow and thicken as it moves and ends up forming a chitinous armor around its core. Izalea calls upon a Cloud of Daggers spell to surround the head of the giant creature, just as Fenler’s shuriken and Altair’s arrow bounce harmlessly off the newly formed armor. Luvese approaches and casts Poison Spray and Galinis recovers and swings his greatsword but both attacks are again blocked by the armor. The lanose roars its rage as the daggers mince its face and the creeping cold starts to deaden parts of its body. It vents this fury by slamming its greataxe into the nearby Galinis.

The eladrin surely would have been unable to continue fighting if his own rage hadn’t exceeded his foe’s. Izalea’s divine bolt, Luvese’s poison spray, and Altair’s arrow again fail to penetrate the fungal armor, but the coat is weakening. Fenler sees a damaged portion the armor and dives in, plunging his shortsword through the fracture. The armor still absorbs most of the blow, but the godmaegen is able to rip away a chunk of the chitin as he removes his blade. Galinis sneaks a massive swing into the lanose’s leg to cause it to stumble a bit as it swings at him with its axe. The creature follows through the swing’s momentum, however, and slams the axe into Fenler as he pulls away. The stumbled creature takes more damage from the cloud of daggers and creeping cold.

In its pain, it reveals the damage in its armor made by Fenler and Izalea fires a divine bolt into the opening. Altair and Luvese repeat their previous move to soften the area around the armor’s hole, allowing Fenler to pry more of the chitin away with his swords. He clears out just in time for Gal to recklessly, but effectively, spring and slam his sword downward into the opening. The monster stiffens upward and releases a cry, just as the daggers and creeping cold finish their work. Its pale eyes go dark as it tumbles backward into the opening it was guarding. The group temporarily crashes against nearby trees to catch their breath before facing the challenges that lie beyond the fallen lanose.

After resting and recharging, the group promptly enters a scarred vale behind the last foe. The clearing seems to have been caused by tremendous magical energy. Several plants remain, but all appear warped or withered with most of the smaller ones reduced to cinders. 

> Fenler:  So, this is where the climax occurred, I suppose. That feeling has reached an all-time high, guys.

> Izalea:  It appears that way. Fenler, can you don those wings at will? You seem to fight more effectively that way and I get the feeling the fighting’s not over yet.

> Luvese:  I agree, the flora is crying out in pain to me and I don’t think we’ll make it to the clearing without combat. 

> Fenler:  Sure thing, ladies. I think I got it figured out… (He flaps his arms for a second and tenses awkwardly, but succeeds in transforming.)

> Altair: It doesn’t seem like the battle is even here anymore. Shouldn’t we just turn back. 

> Galinis: Ah, come on, Alt! Don’t you wanna see what’s at ground zero up ahead?
As they chat, several of the remaining plants seem to be following their movement.

> Charlie: Yes, we at least need to check the aftermath of the battle. There may be survivors or if we are lucky, a dead angel.

> Altair:  Not likely, they would have brought it down long before reaching here if they had the strength to do so at all. But I concede the point that we should at least investigate. 

> Izalea: It seems to be centered around that huge tree, maybe we should make that our goal.

> Fenler: (Noting the humanoid figures surrounding them) Are those people that are approaching? Ho! Are you friends?

There is no response. As the things approach, their true nature is revealed. They are blights, twisted plant creatures that take a variety of forms. Three of them are needle blights, monsters that have a surface covered of needles that resemble larger sharper pine leaves. The remaining two are wood blights, whose covering is made of sturdy wood. The blights’ intention is revealed as they begin to pick up speed in a shambling charge on the party. Izalea fires a divine bolt into one of the wood blights, causing the thing to fly into a primal rage as it continues advancing. Luvese targets the same blight with a poison spray, followed by Galinis cutting it down with his sword. With an ally down, one of the needle blights sprays needles at Gal, but misses ever so slightly. A second needle blight decides to pick on Charlie and swats him into the bushes with a claw, causing the cat to issue a series of complaints from his new fortress of foliage.

Fenler starts to avenge the cat by sticking the needle blight with his hunting shuriken. The final needle blight fires needles on Fenler and misses as well, while Iza fires on the wounded needle blight and kills it. Luvese attempts a poison spray on the remaining wood blight, but it is able to resist for now. Altair pegs the first needle blight, while Gal scores of mighty blow on the wood blight that nearly cleaves it in half. The wood blight roars its primal fury, and almost on cue, several of the surrounding plants jump to life and attack. Three of these new foes are twig blights, that previously resembled shrubs, but have now twisted into a vaguely humanoid shaped. The other are vine blights, mobile man-shaped masses of vines and tendrils. One of the twig blights surprises Luvese and claws her right across the neck and nearly hits a main artery.

The first needle blight continues to punish Luvese by coating her in its needle spray. The first vine blight begins by diving into the center of the group and unleashing a mass of entangling vines. Only the airborne Fenler becomes caught up and he immediately cuts himself free. Gal is ganged up on by the second needle blight, using its needle spray, and the wood blight, who slams him. The second vine blight lashes out at Izalea, trying to constrict her with a vine, but she backs out of the way and fires on it with a divine bolt. This vine blight falls immediately afterward to Altair’s well-placed arrow. The second two twig blights spot Fenler tangled and jump him as he frees himself. He avoids one, but the other’s claw rakes his neck just like happened to Luvese. Gal tries to overcome his outnumbered status by hacking at the raging wood blight. Luvese is still struggling with the twig blight in her face, but with a short ripple she wild shapes into a great ape.

She takes a twiggy claw, but evades another needle spray, and constricting vines from the first vine blight. Fenler, still surrounded by two twig blights, cuts one in half with a scimitar swing, and kills the second with his shortsword. Galinis is still preoccupied with dodging the wood blight’s continued onslaught and takes a needle spray in the back. Altair spots his brother’s predicament and takes the uninjured needle blight down with a single shot. Meanwhile, Izalea and ape Luvese take on the vine blight harassing them. Izalea’s divine bolt thumped soundly into the thing while Luv’s claw almost finishes it. Her other arm does kill something, however, and that thing is the last of the twig blights. The last needle blight fire needles into Altair, who responds by slaying the monster with his bow. Galinis has his swing against the wood blight interrupted by the monster slamming him first. But ape Luvese steps in and claws the thing with both arms, while Galinis gets a good swing to finish it. The last creature, the remaining vine blight, tries desperately to catch Iza with a constricting vine, but is literally cut short by Fenler’s scimitar.

No sooner than Luvese has finished another round of balm of the summer court on Galinis, do the surrounding plants attack again. This time, a pair of branch piles spring emerge as bramble blights. These blights appear almost like a humanoid warrior armored with wood and each carries a crafted wooden pike that drips with poison produced with the monster. Taking advantage of the surprise, they each jab Galinis and leave him wounded and staggered from the poison. Luvese, still in ape form, claws one of the bramble blights twice, one hit being particularly crushing to the monster’s wooden body. Following suit, two masses of vines spring on Izalea as well. These are jubokko bone blights, masses of vines tangled around the bones of its progenitor’s victims. Izalea takes one slam and is nearly knocked to the ground, but Fenler intercepts the other bone blight and slashes it with both his swords.

Altair snaps an arrow into the injured bramble blight that leaves it barely alive, until Galinis finishes it by grabbing the arrow and forces it deep into the creature’s vitals. He then deftly ducks the other bramble blight’s pike. This blight is promptly raked by Luvese’s ape claws. Iza ducks away from the bone blight attacking her and calls her healing light on Galinis. The bone blight facing Fenler slams him, knocking him more than a foot backward. He maintains his footing, however, and counters with his shortsword. The other bone blight charges the unguarded Altair and knocks him unconscious with a severe blow. The cervitaur manages to get his shot off into the remaining bramble blight before going down. The timing couldn’t have been more perfect, as Galinis also connected with the bramble blight just after it jabbed Luvese with its pike. She returns to her normal form and kills the monster with her punishing strike. The bone blight facing against Fenler attacks with a frenzy at seeing its bloodied foe and smashes him with a nasty blow. The other bone blight tries to finish the dying Altair but is slapped aside by Gal’s greatsword. Izalea attempts to peg the bone blight in Fenler’s range with a divine bolt but cannot get a clear shot and misses.

Luvese has better luck, though, and uses frostbite to chill the monster. She also calls once again on her balm of the summer court to awaken Altair. Even through the cold, the bone blight connects with Fenler again, but he returns with both blades this time. The other blight knocks Galinis unconscious just as Altair rises and catches it with a longbow shot. Izalea misses again but uses her healing light to patch up Galinis. Fenler, prepared this time, dodges his foe’s blow and slays it with a twin sword attack. The remaining blight swats Gal again, this time knocking him into Altair and making him loose his shot. Izalea hits this time, as does Fenler with both weapons. The blight grabs for Altair but is knocked to the ground by Gal’s mighty swing. The creature reaches up for the ranger once more before it freezes in a death pose by Luvese’s frostbite.

The group plops down, exhausted, to regain their composure before entering the final stretch of forest before the clearing, knowing that more blights await them. In addition to receiving some healing magic, Galinis and Fenler decide to power up with some reserved potions before the facing this last section. Galinis downs his potion of heroism to instill some extra vigor and a heroic boost to fighting abilities. Fenler drinks Izalea’s beetle elixir, causing his skin to harden into an iridescent shell and antennas to sprout from his head. He also consumes his own potion of bear’s endurance to add a few to prepare himself for a few extra hits. Rested and prepared, they advance into the seemingly innocuous section of forest ahead.

The group’s caution is rewarded as they approach another pair of suspicious looking tangles, one of branches and the other of vines. Altair, ready for the worst, casts hail of thorns and steadies his aim on the branch pile. Galinis brings his sword to bear and approaches the same pile. Sure enough, the pile explodes into the form of a branch blight, a hauntingly beautiful tangle of long, twisted branches. The sheer force of the creature’s emergence causes Gal to stumble and fail his attack. Altair fares better and his enchanted arrow makes contact just as the branch blight unleashes its own attack. The engaged blight has no chance to avoid the hail of thorns attack and is pummeled by the magical spikes. The blights attack, however, proves equally effective.

It smashes down with a mass of branches that knocks both Altair and Galinis to the ground. The commotion turns all heads away from the tangle of vines, which subsequently emerges and reveals itself to be a jubokko skull blight. This monster is similar to the bone blight, only much deadlier. The bones entangled in its form are purely skulls, and the creature is infused with the dark energy of its progenitor. It tries to take advantage of the downed ranger by wrapping him in crushing vines, but the cervitaur kicks himself out of the way. Fenler charges in, swords whirling, and scores a nasty combo on the branch blight. Izalea and Luvese stand back-to-back and unleash spells on each blight. Izalea singes the branch blight with a divine bolt, while Luvese calls her creeping cold spell on the skull blight. Altair and Galinis each climb to their feet and both score another hit on the branch blight.

The same monster switches its tactics, deciding to focus on finishing Fenler first. It whacks him with a sturdy branch and wraps his midsection up with smaller branches from its body. The skull blight follows a similar strategy, only it wraps Altair in vines and immediately tightens the constriction on him, all while dealing with the ice creeping up its form. Fenler again score his combination attack on the branch blight, but Izalea cannot aim her bolt between the mess of ally and blight. Luvese avoids the same mistake by diving in a slaying the monster with her punishing strike. As Fenler untangles himself, Altair still struggles with his bonds. He still manages to fire a shot into his captor, but the arrow seems quite ineffective against the blights shifting form. Galinis attempts to slash at the blight but cannot maintain his balance in the mess of overgrowth he is fighting in. The blight, still suffering from the icy grip of Luvese’s spell, tries to bury its vine into Altair to siphon some of his life force. The quick-thinking cervitaur senses the intent and shifts himself to direct the stabbing vine into his armor, thus deflecting the attack. Fenler pounces on the blight in a flurry of swings.

His shortsword seems disadvantaged like the arrow against the writhing mass of vegetation, but his scimitar slices true. Izalea scores a hit with her divine bolt, but notices something strange. The blight is coursing with visible necrotic energy that seems to grow with each hit. Realizing the danger, she calls her allies to retreat before the monster detonates. Luvese, already a safe distance away, uses her frostbite to hamper the creature’s grasping vines. Altair tears himself free and ducks the slowed vines as he back away. Galinis also disengages and backs away defensively. The icy chill of Luvese’s spell reaches its peak on the blight, as does the pulsating vile energy. It tries a last-ditch attempt to wrap the only foe in reach, Fenler, and let him suffer its death throes. The godmaegen predicts the attack, though, and swats the vine away with a sword as he retreats. Just as Fenler gets safely away from the monster, Izalea pops it with another divine bolt. It immediately explodes in disgusting, but harmless, burst of vines, skulls, and purple energy. Both foes finally slain, the group calls on some light healing magic and finally enters the scarred clearing near the center of the woods.

The clearing is dominated by an enormous and ominous tree. While this tree is center of the group’s attention, a much smaller dead tree is also of note. Even in awe of the forest giant, the group’s eyes spring to the smaller tree as it comes to life and intercedes in their path to the center. The tree blight shambles toward the group and swings a branch into Luvese and Galinis. It also tries to unleash grasping roots on the both of them. Galinis dodges out of the way but Luvese is entangled by them. Galinis goes into a rage and slash the blight. Luvese, being crushed by the roots, manages to chant out a creeping cold spell. The tree blight is able to resist the initial chill, but the ice begins its deadly creep. Izalea joins the spellcasting by adding a cloud of daggers to the mix. An arrow, courtesy of Altair’s bow, whizzes by Fenler into the tree as the godmaegen charges to make his own attack. He charges both weapons with a divine smite and gouges the tree with his two swords. The tree begins its own assault after taking fresh wounds from the cloud of daggers and the creeping cold. It slams Galinis with its branch but misses Fenler with the other, but he is entangled by the tree’s roots.

Izalea, however, is able to jump over the roots coming her way. She rolls up to witness Luvese being dragged into the creature’s waiting maw. It clamps down on her and tosses her asides, leaving her unmoving on the bloodstained ground. Izalea screams as she fires a divine bolt that cuts into the blight, just as Gal’s sword is doing the same. The blight, oozing blood colored sap from a dozen wounds starts to wobble as another arrow thuds into the monster. It looks as though it is about to tumble over but the roots wrapped around Fenler continue to tighten. He drags against the dying monster hold jabs his shortsword into the tree’s center of mass to finally put it down for good. Fenler uses the last of his mystical energy to heal and wake Luvese and the party carries themselves on wearied legs to the giant tree at the center of the clearing.

The ground around this massive tree is stained with blood. Nearer to it, the bodies of the victims of the recent battle litter the ground. Skulls and bones, stripped of flesh, intertwine with the roots. The thing twists with hunger as the party approaches it. Knowing what they must do, the exhausted group starts to advance on the tree when a powerful voice booms behind them.
 
> Voice: Stop!! You will be slain if you battle with this monster. A jubukko is far beyond the pitiful skills you have displayed thus far. 

The voice belongs to a mighty solar angel. Its magnificence is only matched by the combat ability it displays as it descends on the corrupted tree. It dives towards the jubokko, stopping short and cutting two enormous gashes in the plant with its heavenly greatsword. The tree oozes humanlike blood as it returns the attack by attempting to slam the angel with two branches. The angel whisks away from the first but is caught by the second. Even so, the airborne angel barely budges under the impact. As the branch twists to entangle the solar, it unleashes a searing burst. The burst, comparable to the sun in its radiance, scorches the tree. He then bursts his tightening bonds and slashes a gash in the jubokko that causes it to collapse under its own weight as though a woodsman had felled it. Victorious, the angel approaches the beaten and bloody group. He reaches out and heals them all with a touch of his sanctified power.

> Solar: I am Anariel. You have come in hunt of the rogue. I have been commanded to assist you. 

> Fenler: A little late, huh? We nearly died a hundred times getting here. 

> Anariel: But you live. I was seeking my brother. He was sent to destroy the rogue here. He has failed. 

> Izalea: Speaking of the rogue, what happened here exactly? I assume the resistance failed. I mean, you said your brother failed to destroy the rogue. What is this fallen angel’s name, anyway?

> Anariel: (annoyed) Inquisitive faerie! You ask more questions than need to be answered now. We have much ground to cover. 

> Galinis: (sarcastically) Excuse me, your holiness. I think you should show the lady a bit more respect than that. She is one of the favorites of the Queen of Stars, you know?

> Altair: Gal, maybe we don’t piss off the majorly powerful and increasingly aggravated angel…

> Fenler: No, I’m with Galinis. I think we deserve some answers before we go gallivanting into harm’s way. You want our trust and cooperation, then earn it. Now who is this fallen angel and what the hell happened here. 

> Anariel: (conceding, surprisingly) I guess I have no other option. The triviality of free will aggravates the mission so often. We do not speak the names of the fallen. That I cannot relent on. 

To grant a name is to relinquish sovereignty. The rogue seeks to acquire more power by accessing heinous and forbidden magics. My brother and his comrades pursued the abomination to this forest, where it was performing a ritual on a captive. All were slain in the conflict but my brother, the rogue and his captive. My brother succeeding in escaping with the captive. Why, I do not know. His link with our collective has been damaged in the battle somehow. The forest was corrupted by the vile magic the rogue was working. The jubokko, the tree, was spawned from these magics and strengthened by the blood shed from so many mighty warriors. The beast was the progenitor of the blights you combatted up till now. I know the general location of my brother. We must seek him out. Are you satisfied with my answers enough for us to continue this urgent quest…?

> Luvese: Nearly, but what can we do against such powerful foes. As you said, we are nowhere near the strength of you and your allies. And what of this forest? It may never heal if not attended to immediately.

> Anariel: The powers that be know that you will play a role in the trials to come. That is all I know of it. I do not ask questions as you mortals do. Absolute obedience is demanded of us, lest we fall and join the rogue in disgrace. As for the forest. If it troubles you so, I can easily erase it and prevent the suffering you seem to fear. Otherwise, you must leave it to machinations of nature, for we must depart. 

> Luvese: (somberly) No, leave it as it is. Hopefully, some of my kin will tend to the damage. 

> Anariel: Then let us go, we have much ground to cover. I will sustain you, but we musn’t stop. 

> Fenler: (Whispers to Galinis) This angel is kind of a prick. I wonder if they are all this way? 

> Galinis: I agree. Talk about pushing your weight around. 

> Charlie: (A bit too loudly) And yes, they are all that way. Bunch of winged monkeys with righteous ego sticks up the behind.

> Anariel: Did you say something, cat? 

> Charlie (wide-eyed and terrified) Uh… No?

> Anariel: I must have been mistaken.

> Charlie: (under his breath) whew…

> Anariel: And as an aside. Most don’t know this of my kind, but we can read thoughts and know when mortals are lying.

> Izalea: Great, guys. This is going to be a long and awkward journey now….

It was.