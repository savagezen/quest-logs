---
title: "The Twilight Company - Grace of the Fallen"
description: "A mysterious shooting star draws adventurers from safety into a dark forest, where they face deadly beasts and discover an unconscious man in ruins. Battling undead, a cunning goblin ambush, and dark creatures, they struggle for survival while uncovering hidden dangers in the wilderness."
tags: [DND5E, Lore]
---

```
Author: Ryan
```

### Chapter 2 - Grace of the Fallen

Soon after the shooting star blazes over their heads, the group bids farewell and thanks Oganda for his hospitality. The allure of the spectacular event draws them from the safety of the centaur camp and back into the wilderness. After a short hike through the woods, the ruins come into view. The forest is alive with the panicked cries of the resident animals. Warily the group approaches the crumbling structure. From the bush bursts a terrified giant badger, followed shortly by a blood hawk of a similar temperament. The pair of animals is spooked by the group’s approach and attacks.

Meanwhile a pair of young leucrotas stalks the trees watching the scene unfold. The beasts had been stalking the badger, but then caught scent of sentient prey, a favorite of leucrotas, and diverted the animals to distract the group so they can feast on a straggler. Fortunately, Altair catches sight of one of the leucrota and warns the group of their presence. The blood hawk dives on Luvese with talons outstretched.

Its panic interferes with its accuracy, and she is able to divert the hawk and it smacks into a tree. Izalea takes advantage and blasts it with a divine bolt, killing it. One of the young leucrota tries to bite Galinis, who intercepts the attack with his greataxe and cleaves the monster’s skull in two. The giant badger strikes true with its own bite and clamps onto Luv.

She dodges its razor-sharp claw but cannot land a blow of her own. Altair sees Luv in distress and sticks the badger with an arrow followed by a divine bolt from Iza to silence the beast. The remaining leucrota kicks Gal as it tries to bolt for the tree line. Gal barely stumbles and catches the leucrota in the thigh. Luv misses her swing but slows the beast enough to give Altair a clear shot to kill the leucrota. 

> Galinis:  Well, I’m awake now. If the woods are this bad, are you sure we should go into these dark, crumbling ruins?

> Izalea: Yes, I can’t explain it, but something inside is telling me I must go in there. You’re welcome to stay out here in the woods with all the riled-up fauna, but I must go in. 

> Galinis: I was only kidding, I can’t just let y’all take on those tight quarters without my axe to clear the way. 

The group anxiously enters the dark opening to the ruins. They follow the narrow corridor ahead without incident until the whole structure starts to shake. As they stumble forward and approach an intersection, parts of the ceiling collapse and block the path ahead, forcing them to take the another route off the main path. An unearthly howl greets them, and a wight and zombie attack. The wight opens up by sticking Gal with two arrows.

Iza returns fire by planting a divine bolt deep into the wights emaciated form. Luvese and Galinis charge and slam the wight with weapon strikes. The zombie shambles over and slams Gal while the wight draws its sword and slashes Luv. The Wight also reaches out and drains the life force from Luvese with a touch of its hideous fingers. She is able to shake off the drain but is unable to land a blow. Fortunately, Iza is able to peg it with another divine bolt and Gal finishes it with a mighty swing. The zombie tries to slam Gal again, but he is ready and dodges this time. Iza launches another bolt on the undead and Luvese finishes it with a scimitar slash. The group then discovers what the undead were after. A crater still hums from the heat of the fallen stars impact. 

> Altair: I think we found where the star hit… Is that a man in there?

> Izalea: Why, yes.. And he is naked… It’s all you, Gal. Go poke him with your axe and see if he’s alive. 

(Gal obliges and prods the unconscious man. The man mumbles something incoherent and springs awake)

> Naked Man: What the hell! Who are you? Ugh…! (The man grabs his stomach and sinks back down.)

> Galinis: What’s wrong with you? I barely poked you, you wuss!

> Naked Man: No. Sorry. I’m wicked hungry, like haven’t eaten in days hungry. (He gets back up.) What am I doing here?

> Galinis: How should we know, From the looks of it, you’ve taken to snoozing in undead infested ruins. Apparently riding stars and what not. Why don’t we talk outside where less things will try to kill us?

> Izalea: And please find some clothes….

> Altair:  I believe I have some things that will help. (He retrieves a shirt, a towel, and some rope.) Its not ideal, but it should work. Also you might need a weapon, I have a spare short sword you can borrow. 

> Naked Man: (Dons the shirt, ties the towel around his waist and takes the sword) Alright, I think I’m good. Oh wait, that dead thing has another sword. Two are better than one, right? Great, let’s move. By the way, I’m.. uh.. I don’t remember. Must’ve bumped my head real good. 

> Izalea: Why don’t we just call you Fenler for now. Seems fitting based on the circumstances. I’ll explain what it means later.

After a giggle issues from Luvese, the group tries to backtrack towards the entrance but stops short when the see a horde of undead shambling down the corridor. The heavy load of the undead cause parts of the floor beneath them to collapse. Causing a domino effect that also drops the floor of the caved-in corridor, clearing the path. They hurriedly jump the newly formed pit and plunge deeper into the ruins away from the horde pursuing them. More undead await across a crevasse in a deeper chamber, although they are far fewer in number.

This group only consists of two feral undead cats and a jackal of darkness. These beasts are more alert than the zombies and spring straight into action at the sight of a potential meal. The jackal vaults across the pit and bites Luvese. It also summons a small black flame under her, but she breaks free of the monster’s jaws and dodges away from the flame. One cat follows suit and tries to claw Fenler, but the attack misses.

Altair cannot get a clear shot on the jackal and the arrow just whizzes passed the beast. Fenler sees Izalea trying to line up and shot and opens the way for her divine bolt to fry the cat. The other cat clears the gap and slashes Gal with both claws. The wounds are shallow, but Gal feels a paralysis creeping up on him from the animals filthy nails. Luckily, the eladrin is tough and shakes it off. Luvese ducks aside and cast a healing spell on herself, to close the bite wounds. Fenler skewers the cat with his shortsword and slays it, the follows the momentum to sling the cat’s body at the jackal and attack it with his other blade.

The slice hits true, but the jackal is able to shrug off part of the damage thanks to the magic infused in its body. The beast bites Luvese again and calls the black flame on Fenler, who also dodges it. The jackal takes an arrow and divine bolt before resisting Luvese’s frostbite spell. Fenler finishes the beast with two deadly sword strikes. Just as they stop to breathe, they hear the moaning horde has passed the collapsed floor and is upon them. They rush onward and stumble onto a degraded bit of flooring. It falls away and they tumble to the cave below. The entire room starts to crumble upon the horde, killing or separating the mass from the heroes. Finally somewhat safe, the tired and injured group rests for a moment before trekking into the cave to find a way out. 

> Izalea: So.. Fenler, care to explain what a human was doing unconscious and naked in those decrepit ruins? There’s no way you were actually on the falling star. No mere man could survive that impact without some seriously powerful magic. And from the looks of you, I doubt you have that at your disposal.

> Fenler: Honestly don’t know. Also don’t know my name, where I’m from, or anything really. About the only thing I know is that I’m still hungry. Anybody got food handy? Doesn’t look like these caves will have anything tasty. 

> Luvese: Hehe, I may have something. You’re quite friendly and open for a human. Your kind doesn’t tend to be so comfortable as the minority in a group. 

> Fenler: Well, I guess fighting a few undead beasts makes racial difference seem a lot less significant. 

> Galinis: I guess you don’t remember where you learned to fight like that, either. I’ve never seen a style that uses two weapons in such a manner. You seemed to meld both grace and power. Most usually must sacrifice one for the other. 

> Fenler: It was really just instinct. Muscle memory I guess. So, we getting out of here?

> Altair: Slight complication… Dark Elves incoming. Wait.. One is an elf, I can’t make out what the other is. Must be its pet giant spider. Looks like they’ve spotted us, in any case. 

The other beast is actually a choldrith, an elf transformed by a horrible drow ritual into a spiderlike creature that is completely loyal to the dark elves. The group charges the drow patrol. Galinis smashes into the drow with a mighty swing while Fenler jabs the choldrith with both swords. The drow ducks Gal’s follow-up swing as it retreats. It spins and launches a poisoned bolt from its crossbow, but Gal blocks it with the flat of his axe.

Altair misses the choldrith with his longbow, but Iza strikes true with her trusty divine bolt to slay the drow. The choldrith senses its peril and draws up a shield of faith to protect itself. The shield blocks Luvese’s punishing strike, but Gal is able to cut through. The hit rattles the beast’s concentration and the shield fades. Fenler connects with both weapons, one of which produces a garish wound in the spider-beast’s abdomen. It also catches an arrow from Alt and barely dodges a divine bolt from Iza. The choldrith uses hold person on Gal, freezing him in place and blocks two swings from Fenler.

Luvese flanks the monster with her punishing strike, quickly followed by Alt sticking it with another arrow. The cholrith tries a last-ditch attempt to save itself by casting sanctuary. This doesn’t stop Luvese and Fenler, however, and they both strike the monster regardless to kill the beast. The group decides they should make haste and find a way out of the cave, when they spring a trap. A blast of noxious gas knocks the group unconscious. As they gasp for air and slip into oblivion, they see a feral group of small humanoids approaching. They awake some time later in a shoddily built wooden cage surrounded by a group of cave goblins excitedly ranting in their choppy language. A large cooking fire is a short distance away from the cage with room for five on its roasting spits. 

> Fenler: I’m literally going to suffer some permanent brain damage if I keep getting knocked out. And it literally smells death in here.

> Luvese: (Points out the spits) Looks like we’re on the menu, guys….

> Altair: These foolish goblins didn’t even strip our gear and this cage looks very poorly built. Looks like there are three exits, but they are all protected.

> Galinis: Help me bust this cage, Fenler. I’m not going to be any savage’s dinner. Everyone else, weapons at the ready.

Gal flies into a rage and the pair musters all of their strength before slamming the cage. The entire structure explodes under the impact. The most well-guarded entrance is to the west with a feral cave goblin, a cave goblin brute and a standard cave goblin. The southern exits are protected by a cave goblin runt and an afflicted cave goblin, respectively. As soon as the cage bursts, Altair looses an arrow into the feral cave goblin. The runt rushes Fenler and tries to dagger him, but he diverts the blow. From the western entrance, the cave goblin king appears and starts barking orders. It seems his commands are driving the other goblins to attack faster and more recklessly.

Luvese swings on and misses the feral cave goblin, just as Gal chops into the afflicted cave goblin. The afflicted one responds by vomiting spores of some kind all over Fenler and Galinis and follows up with a swipe of its claw on Gal. Gal dodges the claw, and neither seem to be affected by the spores, besides the disgusted looks on their faces. The cave goblin brute unleashes its jawbone club thrice on Iza but can’t connect. The regular cave goblin launches two arrows at Luvese. Only one hits, but it plunges deep into her chest. Fenler easily takes the runt down with his two swords. The feral cave goblin unleashes a flurry of clawing and biting on the girls. One claw rakes Iza and another drops Luvese. Iza quickly channels her healing light to revive Luvese and cast celestial fist on the feral cave goblin.

The fist closes on the goblin and absolutely annihilates the beast, leaving only its crumpled form behind. Meanwhile, the brute catches an arrow in the shoulder from Altair. Across the chamber, the king and Luvese struggle to hit each other with their clubs and scimitar, respectively. Galinis recklessly attacks the afflicted cave goblin, trying to finish it off. He pays for his impatience, as he is bitten and clawed twice by the monster, one of which strikes a soft spot. The brute again misses every strike attempted on Iza, while regular cave goblin plants another well-placed shot on Luvese to drop her again. Fenler goes to assist Luvese, and feels healing energy flowing from him into her through his touch. As she springs up, Iza cast cure wounds on her.

The king tries to club her back down and strikes her twice, taking an arrow in the back from Altair in the process. Luvese cast her own cure wounds on herself and Gal slays the afflicted cave goblin with a sturdy axe swing. As the goblin hits the ground, it explodes in a cloud of spores that showers everyone present. The brute continues to struggle to hit Iza, while Luvese dodges two arrows from the regular cave goblin. Fenler, Iza, and Altair team up to slay the brute, but the king singles out and drops Luvese again with two nasty club strikes. Galinis rushes in and hits the king with a brutal retort. Out of arrows, the remaining underling draws a shortsword and jabs Iza. Fenler slays the underling with one strike and uses the momentum to stab the king with his other sword. This is followed quickly with an arrow from Altair and a divine bolt from Iza. The king, now alone, panics and tries to flee. It is easily chased down by Galinis, who finishes the king with a might blow that would have totally bifurcated it had the creature not been so fat. Fenler rushes to Luvese’s side and stabilizes her, then slings her over his shoulder as the team regroups and finds their way to the surface.

The group finds a safe place to make camp back under the light of the sun. Luvese soon regains consciousness, but her, Izalea and Altair are starting to show respiratory distress. They also seem to be strangely drawn to Fenler and Galinis. Even through the haze, Izalea quickly recognizes that they are infected with Goblin’s Trumpet. They presumably contracted the disease from the afflicted cave goblin as it died. Thanks to her quick identification of the problem, Izalea is able to whip together a remedy that helps her and Luvese recover by the next day. Altair is not so lucky. The condition progresses, and he is exhausted, irritable, and outright violent at times for the next two days. He finally recovers after the third day, much to everyone’s relief. The group then continues to the north, to the sylvan village of Ukt’Vese. The village was formed by fey exiled from their home after the takeover of the unseelie. Several residents still have close ties to the good archfey hidden on in the Feywild. Izalea hopes these contacts may be of some use to her new comrades as they are all currently pursuing dead-ends. The trek takes nearly ten days, and during this time, Fenler becomes well acquainted with the rest of the groups histories and goals. Still being unaware of his own origins, he offers to stick around and help the others achieve their goals in return for saving his life in the ruins.
