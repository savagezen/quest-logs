---
title: "The Twilight Company - Characters"
description: "Characters to the prologue of Cas's original team."
tags: [DND5E, Lore]
---

```
Author: Ryan
```

**Izalea:**

Nymph. Loyal to Morwel. After her queen’s ascension and curse, evil fey launched a genocidal campaign against Titiana and Morwel’s supporters. Most were killed, and those that survived were imprisoned. Some escaped to the material plane but were stripped of most of their power. Izalea lost nearly all her Nymph powers, sinking nearly to the level of a mortal. Fortunately, her bond with her Queen survived and she discovered herself as a conduit for the Queen’s divine power. With her newfound channeling ability, she discovers the abode of Luvese

**Luvese Arith:**

Titanian. Survived in Solitude for some time before Izalea discovered her, growing to maturity under the watchful eye of an elder druid. Despite the druid circle’s reservations about introducing a faeriefolk proper to the druidic arts, Luvese seemed to have a preternatural connection to the nature of the material plane (probably thanks to Titiana’s connection to Oberon). They began to help her refine her gift according to their order, this was cut short after a pack of bloodthirsty lycanthropes tore through the area and slayed every member of the order. The elder druid did manage to secure Luvese away and thus saved her life. She remained a hermit until her encounter with Izalea, who instantly recognized her for her heritage and begged her to join her quest to revive her queen, while also avenging both Luvese’s progenitor and her druidic family. 

**Prince Galinnis Nevarum:**

Eladrin. Formerly crown prince to the kingdom of Nevarum, a small but prosperous Elven kingdom with a presence both on the Material Plane and the Feywild, thanks to a direct link in the form of a gate between the two worlds in the capital city. Following the Plight of the Summer Court, Unseelie launched an assault on the Feywild side of the city. Unfortunately, a strangely organized army of giantkind with a spattering of other savage races laid siege to the material side of the side. The two-pronged assault, whether by design or misfortune, was too much for the city to resist and it thus fell. The king lost his life and the young Prince Galinnis fled into the wilderness with his closest attendant and adopted brother, Altair. His limited combat training left him lacking in the finer arts of elven swordplay, but his superb athleticism, his natural combat savvy, and his seething hatred of the evil that shattered his home have bred him into a budding barbarian. He wants nothing more than to avenge his fallen kingdom, but has neither the strength to defeat those responsible or the knowledge of their identity or location. 

**Altair Nevarum:**

Cervitaur. Orphaned at a young age under mysterious circumstances, he was adopted by King Nevarum and cast to serve as the Prince’s attendant and possibly future advisor. The bond that formed was that of brothers and closest friends. Altair stills sees Galinnis as his liege, but also looks up to him as an older brother. He would gladly sacrifice himself to save his brother. Luckily, Galinnis is more than capable of handling himself in melee, while Altair always found himself more suited to striking from a distance and rapid flight if a physically powerful foe approached. He is strong and can wound an opponent in melee, but cannot shrug of blows like his brother.  He shares the same desire as Galinnis, or more appropriately to ensure his brother attains his goal. With no leads to go on, the pair wanders the wilderness, striking at the savage races existing there taking out a couple at a time. This is more a distraction, eating time and easing the Prince’s wrath until a lead on the assailants and a plan of action can be obtained.
