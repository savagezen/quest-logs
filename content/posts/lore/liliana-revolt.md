---
title: "Liliana's Revolt"
description: "Liliana Vess, a powerful necromancer, battles demons, manipulates dark forces, and allies with Levistus for greater power. Her journey is marked by betrayal, forbidden pacts, and a dangerous quest to reclaim her past."
tags: [DND5E, Lore]
---

```
Author: Austin
```

Liliana Vess, The Defiant Necromancer, a name spoke with fear and caution by any spellcaster of this age.  Liliana’s necromancing abilities have granted her a youthful (approximately 30 year old) appearance, though there are documented encounters dating at least back to the past century.  Such documents and urban legends make note of her skill as a necromancer and penchant for reanimating the dead; “she is charismatic, witty, and profoundly egocentric.”  

Throughout her ascent to power Liliana made pacts with several demons to secure her youth and power -- Kothophed, Griselbrand, Razaketh, and Belzenlok -- the result of each deal used to broker another.  She was given an opportunity to break her pact with the demon Kothophed by retrieving an artifact -- The Chain Veil.   However, instead of handing it over, Liliana used the power of the veil to destroy the demon.

Liliana proceeded to track down and kill Griselbrand (source) , but when Razaketh was confronted he took control over Liliana’s body.  With the help of other adventurers Liliana was able to overcome Razaketh’s concentration, allowing her to reanimate the hordes of corpses sacrificed to Razaketh and commanded them to “tear him apart and feast on his remains” (source).

The demon Belzenlok (source, source) turned Liliana’s brother, Josu -- a wizard -- against her and the power of the demon lord was so corrosive that the corruption turned Josu into a lich.  Belzenlok saw this as a way to retain Liliana’s loyalty -- though she demonstrated in battle that she is not opposed to slaying her own brother.  Upon noting his change of identity and submission to Belzenlok she commented; “you have become undead and I am the orchestrator of such puppets.”  However, Liliana was not able to defeat Josu due to direct interference from Belzenlok.

Liliana, again with the help of other adventurers, is able to defeat Belzenlok.  However, his corruption over Josu remained.  Josu used his new identity to build a false reputation and misguided trust with the Horadrim, a trust he eventually used to betray them.  Resilient as ever, Liliana found a natural consort in the “infernal patron of vengeance and betrayal” – Levistus.  A recently imprisoned archdevil with notoriously few allies seemed a reasonably susceptible target for bargaining.

Liliana was able to communicate with Levistus through one of his clerics and the archdevil was indeed willing to make a deal.  Having a demon lord of Belzenlok’s status slain in his honor would significantly increase Levistus’s power.

In exchange for augmented “planeswalking” ability, Liliana was impregnated by Levistus on the condition that the child would be sacrificed to him once it was born.

Having found herself in the Xiansai territory for such consorting, Liliana immediately began her ascent to power, eventually gaining a seat among the Council of Dexa; representing the necromancy school of magic.  The mages of Xiansai differ from those of Caldeum in that they do not discriminate against the origin of one’s magical prowess; however they still value the existence of their world in the material plane with the utmost sanctity.

Due in part to augmentations from Levistus, Liliana became the most skilled mage of the Council of Dexa.  However, concerns grew among the council whether her consorting with Levistus would bring wrath from greater deities such as Asmodeus (source, source).  Indeed, not to trifled with by mortals, Asmodeus developed an acquaintance with Auril who’s stronghold benefits from Asmodeus and could be brought to the realm of Sanctuary by him as well.

Eventually Liliana grew confident -- even hubris -- that she could again confront Josu in open combat.  The two did again engage in battle, however unwittingly, as Liliana drew on Levistus’s augmentation, so did Auril’s presence in the realm.  Becoming aware of this, the other council members decided to intervene in order to cease the coming of Auril and further incurring wrath from Asmodeus.

Josu was not able to be defeated, however, his identity was exposed to The Council of Dexa just before he fled.  For Liliana though, it was too late to change the minds of the other council members.  Weakened from her battle with Josu, she was overcome by the combined forces of the other council members and imprisoned within the maximum security sector of the city’s notorious Mage Prison.

At the time of her imprisonment, Liliana was still with child.  Once birthed, under much scrutiny, the council allowed the child to live and it came to the care of the sea-faring mage Ral Zarek.  Ral was oft described as “brilliant, unpredictable, and daring” in his youth; but died in battle at sea before the child reached maturity.


