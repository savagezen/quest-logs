---
title: "Fairy Lore"
description: "In the Feywild, Lolth, the Prince of Frost, and Malar conspire to overthrow the Seelie alliance, leading to Titania's fall and Morwel's cursed rise, shifting power to the dark."
tags: [DND5E, Lore]
---

```
Author:Ryan
```

Although most archfey are just that, there have been some that are arisen to divinity to rule the feywilds and those who make their homes there. For the most recent eons, there were three: Titiania, queen of the summer court and Goddess of Magic, her consort King Oberon, the fairy god of nature, and the Queen of Air and Darkness, who rules the Night Court and land of the Unseelie. The former two had forged a strong alliance with Corellon Larethian, the patron god of elves

This of course did not sit well with the Spider Queen, and her machinations aspired to remove this powerful alliance with her nemesis. Secretly she made a pact with two other beings of darkness, The Prince of Frost and Malar of the Black Blood. The Prince of Frost is a power hungry archfey, ruler of the winter court, that seeks to end the reign of the summer court and spread eternal winter across the both the Feywild and the Material Plane. 

Malar is himself the god and origin of Lycanthropes, or so the evil lycans claim. King Oberon is a sworn enemy of the Black Blooded and the corruption it spreads. The dark triad could not stand against the Seelie gods alone, but the unknown collusion brought tragedy to the Feywild.

The moment of fruition had come for Lolth’s plan as Malar released some of his most feral abominations into the tranquil glades deep in the Feywilds. King Oberon responded as usual, personally leading the counterassault and guarded by the royal fey guard. Little did he know the life of his beloved was soon to be threatened. Titiana paced her chambers, as always worried for her consort as he fought on the battlefield.

A cold chill and crawling sensation nagged at her heels, but she barely noticed. Something didn’t feel right about Malar’s attack this time. So bold, so close to Oberon’s cherished lands, Malar couldn’t honestly believe this assault would succeed.

That was the moment. Fire in her veins and ice in her heart as the dark duo attacked with venom and dagger. Somehow she took flight, even with her once marvelous butterfly wings hanging tattered and dead from her back. Even through the fog of impending death, she called on her faerie magic to be reborn. Her physical essence spread like pollen in the wind across the Feywild and into the Material Plane. Her divinity remained in other hands.

Those hands were those of the wings that saved her, those of Morwel, queen of stars. Seated in her Court of Stars buried in the twilit sky of the feywild, she watched the dark alliance form and the scheme be hatched and prepared to end it. She sought counsel with Corellon, but even the stars cannot watch Lolth without her knowledge.

She let loose the Prince of Frost’s private force, the Hoary Hunters, to intercept Morwel. She battled her way to her goal, but precious time had been lost and the arrival of Corellon and Morwel was too late. The death blow had been stuck and Corellon knew the only way to protect assure the divinity’s safety was to remove the dying goddess from the area. Morwel did just that while the Elf Patriarch repelled his enemy. 

The Titiana’s material form fleeted. Her divinity called to Morwel, the magic of the fey needed its vessel. Without it, their world could not survive. She accepted the calling and the absorbed the divine spark. That was the day, the Feywild’s summer ended and the night began. This was a starless night, because it was not Morwel who usurped the dominant position but the Queen of Air and Darkness.

Sure, Morwel had merged with the divinity, but that divinity was envenomated with a nasty treat Lolth had prepared in the event of Corellon’s meddling. The Queen of Stars had ascended but at the cost of being cursed with eternal slumber. The faerie magic would continue to flow, but now it was an open source to fey of all natures, no longer prejudiced against those of a diabolical nature.

What the spider queen hadn’t counted on was Titiana’s persistence and Morwel’s determination. The former cause the springing forth from the Summer Queens remains were a novel race, the Titianans, each individual capable of reclaiming the mantle of her divinity or waking Morwel. The latter was Morwel’s mental shout to all her servants and champions on how to break her curse.

Muffled as the message was, those with strong connection know of the Titianans and their importance. They just need to figure out why they are important. They aren’t the only ones seeking the summer children. Lolth and her allies and minions are surely seeking to snuff these complications out as quickly as possible.

Presently, King Oberon has taken command of the Summer Court, albeit with little of the passion that he previously displayed. The tide has turned in the Feywild. Little is heard of the kinder Archfey these days. The mischievous and malicious have made bounds towards their less savory objectives, with Oberon posing little resistance. He even does little to resist the spread of Lycanthropy through the once pristine wilds of the Fairy Kingdom.
