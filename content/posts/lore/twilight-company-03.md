---
title: "The Twilight Company - Under Your Halo"
description: "The group arrives at the village of Ukt'Vese to find it in chaos, recently attacked by human marauders. As they investigate, they uncover a cult connection and plan a daring rescue mission."
tags: [DND5E, Lore]
---

```
Author: Ryan
```

### Chapter 3: Under Your Halo

After a tenday of travel, the group comes upon the village of Ukt’Vese. The find the hamlet in turmoil. Signs of violence are proliferous and there are several wounded fey. Distressed, Izalea grabs the attention of a pixie hustling through the area,

> Izalea: Hail, friend. What’s happened here? It looks like the village was recently attacked.

> Pixie: Yes milady, marauders from the northern continent attacked us…

> Izalea:  Were they orcs? Goblins? Giants? 

> Pixie: No, they were his kind. (points at Fenler).

> Fenler: Wait, my kind? I have a kind?

> Izalea: Humans? Why would they waste their efforts attacking a fey village? There’s isn’t treasure and marauders don’t care about holding land, only pillaging it. 

> Pixie: We think they were slavers. We beat them back without casualty, but they made off with one of the residents. He was a newcomer, think he said his name was Sherli Main or something…

> Izalea: Wait, Charlemagne, you say? Was he a Morwel Loyalist?

> Pixie: I believe so.

> Izalea: This isn’t good guys. I had a dream that Morwel told me to seek out Charlemagne. I thought it was just wistful dreaming, but it may have been a message. Friend, can you give me any more information?

> Pixie: Well we blockaded one of the scumbags in a storage hut. You’re welcome to him if it helps get your friend back. (Whispering to Iza) And be cautious of that human. We’ve had nothing but trouble from those greedy bastards.

> Izalea: Let me be the judge of my friends. And remember that unjustified persecution is why everyone in this village had to flee the Feywild. 

> Pixie: (looking stunned and taken aback) Oh, well, yes, of course. Apologies ma’am.

The group seeks out the hut containing the trapped marauder. En route, Fenler presses about these humans. 

> Fenler: Hold on, everyone. Can somebody explain to me what these humans are? Am I really one of them? I heard what the pixie whispered. Are they all bad people? I don’t feel like I’m a bad person.

> Luvese: Oh, of course you’re not a bad person. Don’t let that judgmental pixie get to you. Pixies should be the last ones to judge.

> Altair: Yes, and humans are one of the more unique mortal races. Whereas the other mortal races have a known creator deity, humans’ progenitor is still a mystery. They live less than a century, can fill just about any role in life. Eat everything, too. They are really the quintessential opportunists, versatile and resilient. Why the sociocultural norms and the diversity of governance systems alone…

> Izalea: (cutting him off before he builds too much steam) The important thing to remember is that all races and groups of people have their bad eggs. Some more than others. Best not to let preconceived notions about what you’re supposed to be dictate who you are. The fact that you are worried about it shows the good in you. 

> Fenler: I guess so. Screw it. No time for introspection when we got a nasty villain to interrogate. Let’s go in.

> Altair: Fenler. Maybe its best if your first contact with your own kind isn’t this particular situation. Perhaps you should remain at the door and we can call if we need your assistance.

> Fenler: Wait, really? Are you sure? (He looks around the group for any signs of opposition to Altair’s suggestion. He finds none) Alright, it’s agreed then.

The group warily enters the hut, unsure of how much resistance to expect from the human inside. The wretch they discover appears to be fanatically devoted to some foreign cult, based on his garb. The man seems calm at first, cautiously eying his unwanted guests.

> Cultist: Who the hell are you?!

> Altair: We’ve only come to talk. If you cooperate, we may be able to bargain your release and safe passage home. 

> Cultist: I think not, my master would much prefer I slay the likes of you, here and now.

The cult fanatic begins the casting of a spells and the group jump into action. Altair immediately pounces and slaps the foolhardy human with the flat of his shortsword to interrupt his spell. Before he can retaliate, Luvese unleashes a frostbite spell to dull the response. Galinis steps in and sweeps the cultist’s legs with his axe and pins the man by his neck to the ground. Izalea adds a little emphasis by firing a softened divine bolt into the fanatic’s thigh. Fenler plows through the door at the sound of combat and witnesses the pathetic man pinned to the floor and whimpering in fear and agony. 

> Fenler: Is everything okay in here? I heard fighting.

> Galinis: All is well, the man here didn’t seem too keen on talking. So, we did some convincing. Isn’t that right, friend?

The cultist groans and shifts uneasily. Fenler stares at the man, seeing the utter lack of mystique and beauty that his comrades display. He looks at his own hands, then back at the man and can’t help but feel different from this worm. He feels power and grace in his being but sees none reflected in this other individual. Could they really be of the same blood and origin? Why does he feel “better” than him? This and other thoughts begin to whirl around Fenler’s mind, when Izalea interrupts him.

> Izalea: Maybe you should return to the outside?

> Fenler: ….., yes, I think I should. (He exits)

> Galinis: (Releasing his axe, grabbing the man and pinning him against the wall.) So, friend. Ready to talk now?

The group emerges from the hut and passes the details along to Fenler. The marauders have docked two ships a half-day’s trek north in a small bay surrounded by thick jungle. The group forms a plan to sneak on board and take the crew unaware. The cultist, thanks to his reluctant cooperation, is spared for now and left at the mercy of the village’s ruling council and their judgement. With all settled in the village, the group journeys north to find the secluded bay.

The heroes come upon the bay and finds one of the marauders’ ships hardly secured amongst all of the forest and water plant. This provides them plenty of cover to sneak onto the ship and stow away in a below-deck storage area. The ship departs before they unveil themselves, at which time they are spotted by several members of the crew: three pirates (bandits), two mages (apprentice wizards), and two higher-ranking officers (thugs). The crew levels their weapons and a battle ensues.

One pirate looses a bolt from his light crossbow at Altair, but it merely whizzes by. One of the mages also shoots a firebolt at Luvese, which misses as well. The second mage follows suit, but slams Galinis right in a weak spot with his. The other two pirates simultaneously fire their crossbows and Fenler and Izalea, but both miss as well. The two heroes combine their efforts on one of the pirate officers, Iza using her divine bolt and Fenler using his new hunting shuriken.

The officer ignores them in lieu of seeing an opening in Luvese’s guard and pegs her with his heavy crossbow. Seeing this, Gal charges the enemy line. At the same time, all three pirates and both mages fire another volley. Each one misses his mark again, except for one of the mages, who catches Galinis again with his firebolt. Iza and the wounded officer exchange fire, with only Izalea’s divine bolt making contact. Luvese returns the earlier cheap shot, by blasting the distracted officer with her frostbite, killing him. The other officer draws his mace and enters into melee with Galinis, hitting him twice.

Fenler also dashes in to get a piece of the action. After seeing his brother take two nasty hits, Altair nails the officer with his longbow. This throws the villain off-balance long enough for Galinis to land an epic blow that nearly kills the officer. Luvese follows her teammates efforts and strikes the officer with her Punishing Strike. The officer tries to retaliate, but the inky black energy of Luv’s spell engulfs him and snuffs his life out. Their leaders fallen, the rest of the men fall into disarray, all missing their marks again. Iza and Fenler each take down one of the mages with their signature moves while Altair plugs one of the pirates in the heart with his razor-sharp arrows. The remaining pirates try to flee below deck.

Galinis cuts off their escape by using his Fey Step to pop up between them and their escape route, cutting one of them down in the process. The other draws his scimitar and slices Gal. The pirate’s survival instinct allows him to dodge a divine bolt and resist a frostbite, but his luck runs out when Fenler’s two blades scissor his neck and he falls dead. Now that the group has a moment to catch their breath, they think its odd that the captain and first mate didn’t make an appearance during the ruckus. A trap is most likely awaiting them below deck, so the prepare accordingly. Galinis is also quite bloodied up, so Luv, Iza and Fenler contribute a bit of healing mojo to patch him up for the next fight. 

The group nonchalantly enters the captain’s quarters below deck, fully expecting an ambush. As predicted, they spot the first mate, a swashbuckler, lying in wait. He is more lightfooted than they expected, though. The first mate dashes from hiding and sticks Gal with a dagger and Fenler with his rapier. Fortunately, Fenler is ready for the follow-up rapier jab and is able to deflect it. The first mate also takes an arrow from the quick-drawing Altair during his charge. Galinis flies into a rage, but the swashbuckling first mate is too nimble. He dodges Gal’s axe and both of Fenler’s swords.

Luvese calls upon her decomposition spell and surrounds herself with an aura of rot and decay. The pirate captain enters from his chamber and takes aim with two throwing daggers keyed in on Gal and Fenler. Just as he throws, a particularly large wave crashes into the ship and the captain’s daggers fumble harmlessly from his grasp. The first mate attempts to repeat his previous assault but cannot hit the now fully aware warriors. Gal does land a hit, however, and the gouge is infected by Luvese’s decomposing aura. The wound starts to fester and seethe with necrotic energy, causing the first mate to cry out in pain.

Fenler strikes at this opportunity, but the first mate is nimble, even in his agony, and only one swing connects. Luv follows up with a punishing strike that envelops the first mate in even more of the oozing necrotic energy.  The pirate captain again attempts his dagger throw. Gal blocks one, but the other buries itself into Fenler’s shoulder with a sickening thump. The first mate is blasted by the after-effects of Luvese’s last spell, but he manages to catch Fenler and Gal with his rapier. Luvese flits away from the mate’s dagger, though. He then disengages and tries to escape to a better position. Galinis pursues the first mate and cuts him down. Fenler enters melee with the pirate captain, landing a stab with his short sword. When he hits, he feels a strange surge of energy flow from his core to his weapon, causing it burst with radiance right as it hits. The blast further injures the captain, but he is able to force away the aura of decomposition exuded by Luvese.

She sees Fenler bleeding heavily from the dagger wound and uses her magic to heal him. Izalea calls upon her the magic to send a wave of tearing claws over the captain. The shock shakes the captain’s focus, allowing the wound to fester and causing him to miss both his rapier strikes and his dagger jab. Gal, Luv and Fenler take advantage and team up on the distracted captain. Gal’s mighty axe swing launches the captain right at Fenler, who deflects him toward Luv with his scimitar. She returns the captain with a swing of her own, and Fenler finishes the job running his short sword through the soft of the pirate’s chin and right into his brain. Following the combat, the group searches for their person of interest. While they don’t find Charlemagne, they do uncover several other prisoners and one surviving pirate. They enlist the prisoners to help them sail the ship and the pirate to guide them to the other ship, in exchange for his life.

The journey is a somber one, with so many slain at the hands of the group. Slaying monster and savage species can be trying, but to kill so many of a civilized race is truly painful. To not know whether these men were evil, or simply misguided or forced into the vile profession weighs on the conscience of all good folk. Fenler is especially distressed, seeing the first lot of his kind so cruel and malicious. Was the faerie correct about humans? Would he turn out the same way? These introspections are washed away as the “hired” hand calls out for land. In the distance, the heroes see their destination, the town of Kingsport, on a horizon obscured by the oncoming dusk. After docking and securing the boat, the heroes set foot on dry land once again. They are greeted by a priest who seems to be familiar with the boat. The priest halts in his tracks, wide-eyed, when he sees a strange bunch approaching in the darkness. 

> Galinis: (Grinning) Not who you were expecting?

> Priest: What have you done?!! You will pay for trifling with Golodnyyil! Ahnahl nathrak, uhr vahss bethud…

As the priest begins his incantation, the group springs into action. A quick arrow, shuriken and frostbite spell drop the priest dead. The combat draws the attention of a city watch patrol, comprised of two guards and an archer. Before they can even start to explain, the patrol charges. The two guards surround Galinis. He defends himself from one, but takes a nasty poke from the other guard’s spear. Izalea pegs him with a softened divine bolt to send him into a tumbling nap. The archer starts to retreat and launches a pair of arrows at Gal. One finds its mark, hitting especially true thanks to the archer’s keen eye.

Altair tries to merely subdue the archer. Because of his lack of experience in non-lethal ranged combat, however, he embarrassingly misses his foe. Luvese slices at the calf of the remaining guard and Fenler uses the chance to lay him out with a sturdy punch. Finding herself alone, the archer continues her retreat in haste, periodically stopping and launching arrows. She misses both shots this time, and Galinis uses his fey step to appear a stone’s throw away and throws a stone that smack the archer in the chest. Altair manages to hit the archer in the leg with his next shot, while Izalea misses hers. Galinis starts to sling another stone but is seriously injured by another well-placed arrow. He limps behind the line as Fenler finally catches the archer and pummels her. The archer is sent reeling into the wall, where she meets Izalea’s divine bolt. After the blast, she slumps onto the ground, barely breathing.

The group must tend to Galinis first, as he is barely conscious at this point. Two powered-up cure wounds from Luvese and Izalea improve his condition. Fenler channels some of his energy to add to that as well. They then stabilize the archer and bandage her up, leaving her at the entrance to a house and pounding on the door before running off. The group returns to the dock and examine the priest to identify his church of origin, then go to seek it out. Around an hour later, they come upon the chapel. Strangely enough, two men are conversing outside of the building. As the group approaches, the pair takes note of them and also approaches. One appears to be a barbarian or berserker of some sort, while the other a distinguished veteran. 

> Veteran: Halt! State your business on the chapel’s grounds at this hour.

> Izalea: Excuse us, gentlemen. My friends and I are merely wary travelers seeking rest and healing. Our ship was ambushed by pirates. We managed to escape but were wounded in the process. 

> Berserker: We don’t serve your kind here. Now, begone!

> Galinis: No, you just serve up our kind here. Don’t you? You filthy scum!

> Berserker: Why you insifni’cant runt! I done cut down many for less’un insult.

> Veteran: If you’ve come for trouble, you’re in the wrong place. We’re only chapel guards, doing our job and all. 

> Izalea: (Sensing his bluff) So then the phrase “Golodnyyil” means nothing to you?

> Veteran: (Looking shocked) Whuh, what do you know about that.

> Fenler: (Tosses the emblem cut from the priest’s robes to him) We know your welcoming party didn’t stand a chance.

> Berserker: Benny! Them scoundrels killed Toby! The boss is gonna be pissed, we don’t bring ‘em in. 

With that the berserker charges at Fenler and connects with a reckless swing of his greataxe. Altair, always quick on the draw, plugs him with an arrow en route. Fenler sees an opening in the clumsy berserker’s approach and stings him with his shortsword. Galinis flies into a mighty rage and slams into the berserker with his own axe while Luvese slinks up and uses her punishing strike on him. The veteran sees his companion’s predicament and unleashes a flurry of swings on Luvese. One slash of his longsword connects, but the nimble faerie is able to duck the other and twist away from the shortsword jab. Izalea tries to finish the berserker with her divine bolt, but it takes another arrow from Altair to end the oaf.

Galinis, Fenler and Luvese then surround the veteran, offering him his life for a surrender. The veteran refuses, and takes a greataxe, scimitar, and a punishing strike as punishment. He returns by slashing Fenler with his longsword and stabbing Galinis with his shortsword. Luvese’s magical energy blasts the veteran as she dodges another longsword swing. Another divine bolt, arrow and shortsword swipe later, the veteran is on his last leg. A reckless battery by Galinis takes him off that leg and ends the fight. Minutes later, the group cautiously enters the chapel to find the imprisoned Charlemagne. 

The group enters the chapel, which is messily boarded off beyond the entrance hallway. A crudely disguised staircase is found by Luvese’s sharp eyes about halfway down the hall. This leads into the basement below the mostly abandoned chapel. Sensing something suspicious, the group treads lightly. Luvese peeks around the corner to spot four individuals lounging around the chamber. Two wear the robes of spellcasters, one a priestly acolyte and the other cultist. The other two appears as rogues, one with the light attire of a scout and the other the dark leather of a spy. The spy, sitting with a heavy crossbow in her lap, spots Luvese peering around the corner and calls in alarm. Not missing her opportunity, Luvese flits into the room and calls her punishing strike upon the scout, a bolt from the spy’s heavy crossbow whizzing by her head. Galinis rushes the reloading spy and smashes his greataxe into her.

The cultist, afraid to start a spell with Gal’s massive axe whirling by, slashes the eladrin with his scimitar instead. The scout starts to retaliate against Luvese but is dropped by Altair’s rocket of an arrow. Fenler rushes the spy, who is distracted by the divine bolt that just slammed into her shoulder. She still manages to sidestep Fenler’s shortsword jab, but his scimitar tastes her blood and ends her life. The acolyte tries to rally her remaining ally by blessing him, as well as herself. She follows up by casting Sanctuary to protect herself. Luvese then chills the cultist with a Frostbite, slowing him enough for Gal to deliver the finishing blow. Altair and Iza attempt to blast the acolyte, but the spell’s magic prevents them from releasing their attacks. Fenler seems unaffected and simply waltzes up and slashes the acolyte twice to kill her. 

The sounds of combat alert the cultists in the other room. They await the intruders patiently. Luvese approaches the door to the next chamber, only for it to explode open. Another spy bursts through, using the surprise to sneak attack Luvese with his shortsword. She is able to skirt the other swing and the spy cunningly disengages and retreats to the other room. Galinis runs to take the fore, spotting the room’s other occupant, a cult fanatic. As she sees her foe enter the room, she summons a sacred flame beneath Gal. As he dodges out of the way, the cultist quickly calls on a spiritual weapon in the form of a hammer, which Galinis blocks with his axe.

The rest of the group tries to get into range before their enemies can strike again, but don’t arrive in time. The room’s final occupant, another scout, shoots two arrows at Izalea, but both are easily dodged by the channeler. Luvese steps back to magically cure her wound after the nasty sneak attack from the spy, who tries the same strategy as before on Izalea. She dodges both of these attacks as well. As the spy attempts to retreat, he is cut off by Galinis and punished for his sneaky ways. The cult fanatic chants a spell and Galinis feels a paralyzing force come upon him but is able to shake it off with his steely will. The fanatic wills his spiritual hammer to strike Altair, but it glances off his barding. Barely noticing the weapon strike, Altair sticks the spy with a fatal arrow, and he drops dead before he can cry out. With his target dead, Gal advances on the scout. The scout draws her shortswords and jabs him with one. As she prepares her second blow, Izalea cuts into her with a divine bolt. Her guard down, Galinis hacks into her shoulder with his mighty axe.

Fenler finally reaches the melee, just in time to shout a warning out to Galinis. It is too late, however, and the cult fanatic unleashes a particularly devastating inflict wounds spell that blasts Gal with necrotic energy. When the black haze clears, the mighty eladrin is lying still on the floor. Altair cries out and launches another pair of arrows at the fanatic, who smugly summons a shield of faith that deflects the missiles. Izalea decides to finish the scout before focusing on the cultist and slays the scout with her trademark divine bolt. Fenler then takes his vengeance on the fanatic. His first swing doesn’t get passed the magical shield, but his second does. The fanatic winces at the slice and lets her concentration slip, dropping the barrier. Luvese attends to Gal’s immediate need and magically heals him, restoring him to consciousness. He springs up and recklessly attacks the fanatic. The cultist retaliates with the same strategy, but Galinis is ready this time. Even with in his recklessness, he is able to dodge the magic-engulfed hand of the fanatic. Her shield of faith is once again dismembered when Altair sticks her with an arrow. Luvese pounces on the opportunity, grabbing the still vibrating arrow shaft to help draw the cultist onto her ready scimitar. The fanatic breathes her last and the combat ends, for now. 

The group seems to have come to a dead end. After some investigation, they discover that one of the walls is false. They poke around a bit, and Altair finds a switch to lower the wall. Behind is a small chamber with a locked door opposite. Luvese hears chanting on the other side of the door. Sensing the urgency, Galinis kicks the door down. On the other side, a Warlock of the Archfey awaits. While they fiddled with the door, the warlock has called upon mage armor for himself and conjured a redcap to fight by his side. Altair prepares to fire on the warlock, but the sight of the redcap distresses him for some reason and he fumbles his attack. The redcap jabs Gal once with his pike. He barely deflects the second pike attack and the follow-up bite. The redcap also attempts a solid kick, but the stalwart barbarian is strong enough to resist toppling over. Restricted by the small doorway, Fenler deftly dives over Altair, Galinis, and the redcap. He lands in a roll and comes up to slash the warlock with both weapons. Gal goes into rage mode and hacks away at the redcap.

The unseelie seems to be resistant to mundane weapons, however, and the wounds partially close. Seeing Galinis once again wounded, Luvese fires up the healing magic to patch him up. Knowing how dangerous redcaps are and how dire the situation is in such cramped quarters. Izalea and Altair must trust Fenler’s reflexes and unleash magic that could hurt him in addition to their foes. Iza calls upon a flaming sphere that Fenler and the redcap mostly dodge but scorches the warlock. Altair casts a hail of thorns spell on his next arrow that assaults the warlock. The thorns also shower Fenler and the redcap, though. The thorns accomplished their task, however, for the warlock loses his focus on his conjuration spell and the redcap is freed.

It turns on its former master. It impales the warlock with its pike, then pounces and bites the man’s throat out. Soaked in blood, the redcap returns its attention to the group, who surround the creature and start pummeling it. In the melee, Galinis and Fenler both take pike hits, Fenler being dropped by his. As the unseelie rears up for another attack, however, the necrotic energy left by Luvese’s previous punishing strike bursts upon the redcap and kills it. Izalea casts a healing spell to wake Fenler and the group prepares to enter what they hope will be the last chamber of this forsaken hideout.

The heroes cautiously enter the next room, expecting another assault. When they see nobody present, they visibly relax. Prematurely, however, as a cone of frigid energy blasts them from nowhere. Surprisingly, the only one who doesn’t dodge out of the way to mitigate the damage is the dexterous Altair, who is knocked unconscious by the cold. Everyone else is still seriously wounded. Fenler wastes no time and wildly swings at the area of the spell’s origin, connecting with one blow. This breaks the villain’s concentration, ending the greater invisibility spell, and the mage appears. The woman is cloaked in mage armor and appears to be quite the formidable adversary, her eyes glowing with magical power. Gal returns to his enraged state and slashes the mage. Luvese, not appreciating the cold, retaliates with her own cold spell, Frostbite. Izalea decides to play her hand and casts Contagious Madness from the scroll she obtained previously. The mage tries to counterspell it but fails and succumbs to the madness.

She joins Gal in raging and pricks him with her dagger. While the stab barely phases him, the madness infects him as well. The mage then sobers up and resolves the spells effects for her. Fenler risks catching a stray blow from Galinis and cuts deep into the mage with both of his weapons. Galinis luckily attacks the mage and shakes the madness right after. The hit was enough, though, as Luvese manages a deadly blow with her punishing strike. The mage tries a last-ditch effort to divert the blow with a Shield spell, but even this isn’t enough. Luvese’s blade runs her through and she collapses, dead, on the floor. The girls rush to Altair’s side, and stabilize him in the nick of time. Gal and Luvese then tend to him while Fenler and Izalea investigate the fussing they hear across the room. 

> Squeaky Voice: Hey! Over here! Get me out of this damn cage!

> Fenler: Well that is sure a small cage… Must be a tiny fairy or something. Wait…. That doesn’t look like a fairy. What the hell is it?

> Izalea: It looks like a cat. But it has wings.. Wait! I’ve heard of these. It’s a..

> Charlemagne: (Interrupting) Tressym. Yes, yes. Just get me loose.

> Izalea: Right, right. (Gets the cage open). So, you’re Charlemagne, correct?

> Charlemagne: Why, yes. Although I prefer Charlie.

> Fenler: We went through all that for a flying kitten?

> Charlemagne: (Balking) Why, I never met a lunk so rude. (To Izalea) Gather your oafish friends and let’s go outside. We have much to discuss…..