---
title: "The Horadric Conflict"
description: "The Horadrim Wizard's Council formed to combat evil in Sanctuary faces internal strife as young mage Liliana rises, leading to a deadly clash with its members."
tags: [DND5E, Lore]
---

```
Author: Austin
```

### The Horadric Conflict

The Horadrim Wizard's Council was founded in an early age of the world of Sanctuary to protect it from evil forces.  In another age, adventurers battled against prime evils that came to the world seeking its conquest and destruction -- the Horadrim were some of the world's greatest spellcasters and allied their forces in combat.  However, as with all orders of power, conflict eventually arose within the council.

The actions of one member, Zulton Kulle were no longer condoned by his compatriots.  It had been well documented that he oft experimented on corpses and practiced other foul arts.  Helpful as he may have been in the imprisonment of the prime evils, the council declared him a liability to their council and to the well being of Sanctuary.  Kulle was eventually defeated and imprisoned within a tomb outside of Tristram, the tomb of a once ally whom Kulle was accused of betraying.

However, the order found that their old league of spell casting and nobility was giving way to a ruthless pursuit of pure power in their homeland of Dexa.  One, particularly troubling concern for the Horadrim was the rise of a young mage named Liliana; earning several monikers -- "the defiant necromancer" and "the heretical healer."

The Horadrim again had a civil dispute about how to manage this mage's quickly growing power and unscrupulous nature.  Of course, magical forces in the world are limited and the decision came down to lessening the restraints on the prison entombing Zulton Kulle so that a new prison could be constructed on the island of Dexa itself.  All of the original Horadrim, save Kulle himself, died in the battle against Liliana; however, their sacrifice was not in vain.

Rather, their battle had weakened the necromancer such that the Horadrim's pupils could conceal Liliana within the mage prison before reaching her much sought after artifact -- The Chain Veil.  An artifact she had previously used to seek retribution against the archdemons who had been the source of her power.

**Reference:**

* [Diablo Wiki, Horadrim](https://diablo.fandom.com/wiki/Horadrim)