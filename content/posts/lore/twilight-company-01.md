---
title: "The Twilight Company - Escapism"
description: "A cursed faeriefolk and a hermit ally against lycans, while a fallen prince and his brother hunt forest threats. Their paths converge in a battle for survival."
tags: [DND5E, Lore]
---

```
Author: Ryan
```

### Chapter 1: Escapism

A cursed and forsaken faeriefolk with supernatural ties to her slumbering celestial mistress lithely navigates a deep secluded forest, well aware of the bloodthirsty lycans that tail her. She comes upon a shelter built among the trees in a peaceful glen.

She instantly recognizes the inhabitant as one of few remaining children that a powerful goddess left in the wake of her assassination, partly at the hands of the deity of the vile beasts pursuing her. She quickly realizes her mistake, she led the pack right to this kindred spirit of hers.

The option for flight is spent, now is the time to ally and stand their ground. The hermit obliges, as these beasts took not only her mother, but also her adopted druidic family.

A stone's throw away, an elf and his nontraditional brother, a being that appears half man and half cervine, stalk the same woods. The elf is the former crown prince of a fallen kingdom a few days journey from their present locale. The duo hunts the woods for the savage humanoids that terrorize the peaceful residents of the neighboring lands.

The prince is searching for the ones responsible for the fall of his nation. The savages so far have been unconnected, but his rage is subdued slightly in their destruction.

Both pairs are vagrant wanderers in search of their destinies. As fate would have it, their seemingly aimless paths are due to cross. Will they be friends or foes?

Prince Galinis and his brother, Altair, continue tracking a small group of forest goblins when one tries to snipe Galinis. His split-second reflexes allow him to dodge the arrow at the last second, and Altair returns fire. The goblin falls in a single hit. Another goblin, this one slightly more skilled, hears his brethren’s death cry. He spots the culprit and looses an arrow at Altair. This one harmlessly clinks off of his scale mail and Galinis charges. The nimble forest goblin thief ducks the greataxe swing but catches an arrow from Altair. The goblin counters with a stake coated with poison, sticking it into Gal’s leg. The regal prince shakes off most of the poison’s effects but can’t rally well enough to land a blow. Luckily, Altair’s aim is true and sinks another arrow into the goblin’s skull, killing it. 

Meanwhile, Izalea and Luvese have their hands full with a hostile wolf that’s attempted to ambush the pair. The wolf resists Luvese’s frostbite spell and dodges Izalea’s divine bolt but is unable to land its own attack. Contact is made when Luvese calls on her punishing strike, coating her blade in dark energy and transferring it to the beast with a slash. Iza attempts a word of radiance, but the wolf resists this as well. It stands posed to strike when the dark energy from Luvese’s spell overwhelms it and brings the animal down. 

Unbeknownst to the pair, a jackal is hiding in the bush. Quietly, it starts to grow and change into a beast that looks like a hybrid between its previous form and a man. Its sickly yellow eyes meet Iza’s and she feels the lull of sleep trying to overcome her. She is able to shake it off but the drowsiness prevents her from accurately placing her divine bolt. Luvese, however, is still sharp from the last fight and unleashes a frostbite spell on the source of the gaze. The chill hurts the monster but hardly slows it. The jackalwere charges and strikes Iza with its scimitar. Luvese retorts with her punishing strike. The blade hits true, but the jackalwere is strangely unphased by the blade. Regardless, it is still enveloped in the inky black energy that slayed the wolf. The jackalwere gets one final bite off on Iza, before a combination of the dark energy and another slice from Luvese ends the monster.

Across the forest, Altair and Gal face another forest goblin ambush. The sneak succeeds in sticking Altair with a poisoned dart from his blowgun. The goblin doesn’t even have time to recognize its mistake before its catches an arrow from Alt and a javelin from Gal in quick succession and the lights go out. 

It appears the goblin was patrolling for a pair of its allies, as another goblin and an adolescent ogre are resting and feasting on a few caught rabbits nearby. Thanks to their quickly dispatching the sentry, Gal and Alt are able to get the drop on the hostile pair. Gal tries to slash the goblin while Alt follows with an arrow. Luck is with the goblin, however, and both strikes miss. The ogre swings a nasty claw at Galinis, at least where the nimble eladrin was standing. The momentum of the ogre’s charge carries him into biting range of Altair, but the scrappy ogre is off balance and can’t connect. Gal takes advantage and slams his greataxe into the ogre. Altair continues the struggle to peg the goblin as it retreats, just as the ogre continues to struggle to strike a blow to either of its foes. The goblin’s luck holds out and puts an arrow right into Alt’s chest, barely avoiding his vital organs. Alt’s arrow hits truer and pierces the goblin’s small heart. The ogre and Gal trade blows with Gal taking a rake from a claw. Gal’s axe catches the ogre in the neck and ends the beast. The pair is bloody and fatigued, but just as they settle to rest, the sound of nearby combat pushes them onward to investigate.

The telltale sounds are in fact Iza and Luv in combat with another Therianthrope. This time a foxwere. The therianthropes are a lesser lycanthrope allegedly created by Malar, by infusing his tainted and blackened blood into ordinary animals. His goal was to find an additional source for frontline soldiers. Many of the animals could not survive the process, however. The ones that did became beast far weaker than traditional lycans. This particular group had a mission fairly common to the therianthropes, to hunt low-profile targets in regions where traditional lycanthropes are heavily persecuted. Therianthropes can evade the standard detection methods thanks to their unorthodox creation and do not share the silver allergy with their stronger kin. The foxwere plays a clever trick, pretending to be a wounded and scared forest resident. Iza approaches and attempts to calm it by calling on her divine patron. She quickly realizes her mistake when she meets its charming gaze. She feels an enchantment washing over, but quickly clears her mind. Luvese senses trouble and rushes in, dodging a retaliatory bite from the foxwere. Iza blasts the beast with a divine bolt. It responds by shifting into a hybrid form and stabs Luvese during a short sword duel. Iza takes the opportunity to blast another divine bolt point blank into the monster’s gut, ending its life. 

As Gal and Altair catch sight of the other pair in the distance, they witness another attack in progress. A wolf charges Luvese, while an owl swoops in on Iza from behind and digs its beak into her shoulder, shifting into its hybrid form immediately after. Seeing the owlwere as a bigger threat, Luvese steps in and cuts into it with a punishing strike, just dodging the snapping fangs of the wolf as she does. With both foes so close, Iza calls a word of radiance to blast them. The owlwere fights through that radiance and the slithering residual energy from Luvese’s spell to pierce her with a poison-laced talon. She resists the minor toxin and cuts the beast again, killing it. Now alone the wolf makes a final effort to catch Iza’s throat and instead catches a divine bolt down its own as it goes down.

Wearied and injured, the ladies stop to catch their breath. This only last a short moment as they spring back into attack mode as Galinis and Altair approach. Fortunately, the men are smart enough to recognize the others as the victims of the assualt and throw a gesture of peace. Izalea and Luvese lower their weapons and collapse back to their resting positions, inviting the boys to join them. 

> Galinis: “Hail, I am Prince Galinis of the Nevarum kingdom. This is my brother Altair. It seems you’ve a few enemies… Perhaps you could use some allies?” 

> Izalea: “Oh, yes. It seems I foolishly led them right to my compatriot’s home here. I am Izalea and this is Luvese Arith. You say you’re both from the kingdom of Nevarum. I heard that tragedy befell the nation. So, you escaped the fall? 

> Galinis: “Yes, and sadly we have no solid leads on the perpetrators.”

> Altair: “Well, we believe that giants and other savage races are involved. Hence our being in this particular forest today. We’ve been tracking a goblin tribe that seemed to be searching for something.”

> Luvese: “Or someone. Iza tells me my kind has quite the bounty on it. I wonder if the goblins are involved with the lycans that have been after her and now myself.”

> Galinis: “It’s a definite possibility. Especially, if a powerful master is pulling the strings.”

> Izalea: “Well it could be in our best interest to team up then, at least for the time being.”

A hearty cheer from the men indicates their approval and thus began the Twilight Company. They were not out of the woods yet, however. As they teamed up, so did some of their adversaries. A half-ogre traveling with the goblin tribe and the leader of this dispatch of therianthropes, a wolfwere, were waiting to end the fledgling alliance at the shallowing edge of the forest. The half-ogre issues a battle cry as a challenge. Both groups charge. Altair strikes first. Loosing an arrow into the beastly half-ogre. The savage responds with a powerful javelin throw that drops Altair on impact. The wolfwere, in its wolf form, locks eyes with Iza and mesmerizes her, effectively leaving the nymph stunned. On seeing his brother down and unmoving, he flies into a rage and sprints towards the half-ogre. Luvese uses her frostbite spell to chill the half-ogre. The frigid spell causes him to slow its greataxe swing and give Gal time to dodge. The wolfwere attempts to bite Luvese, but she slinks away, leaving the lycan to chew a bit of her leather armor. Galinis lands a powerful greataxe strike on the half-ogre, just as Luv blasts the wolfwere with a punishing strike. The half-ogre responds with a blow of his own greataxe. His is stronger, and Galinis joins his brother in the dirt. The lycan shifts to a hybrid form and tries its swordplay against Luvese. She parries the blows and retorts with another punishing strike. The half-ogre takes advantage of the melee and lands a cheap shot that drops Luvese. With only the stunned Izalea remaining, the monsters charge. She snaps out of the haze and unleashes a word of radiance just as her foes get into range. The blast kills the wolfwere, but the half-ogre escapes unscathed and slashes at Izalea. She rolls under the blow and calls her most powerful spell, celestial fist. This creates a hand of radiant energy that closes upon the half-ogre and crushes the life from it. 

Izalea must move quickly to save her three dying friends. She sees Altair is starting to stir, so prioritizes the other two. She manages to stabilize them with some effort and drags all three to a makeshift camp to nurse them back from the brink. Several tough days pass, and the three regain some ability to move. With the lycans probably still on their trail, the group decides the most prudent option is to move on. Luvese recalls rumors of a small group of centaurs living in a nearby wood mentioned by her deceased druidic mentor. The trek is slow and uneventful, but the group finally picks up signs of the peaceful inhabitants. The sign being a centaur leveling an arrow at Galinis’s head and demanding they state their business. Quick introductions ensue, and the centaur sentry leads them to the chief, Oganda. 

> Oganda: My apologies friends, we can never be too careful these days. Goblins and the like have been pressing into our territory. Six of our sentries have been captured or killed in the last two moons. 

> Galinis: Understandable. You would be happy to know that myself and Altair have been hunting the group. We believe they may have something to do with the tragedy that befell my kingdom.

> Oganda: it is our way to be accepting, but these goblins have been nothing but trouble. I hate to be so cynical, but I’m glad to see some decent folk come through the area. Say, Altair, how does one of your kind come to land in this region? To my knowledge the last cervitaur tribe disappeared from the region several centuries ago…

> Altair: It’s really a mystery to me as well. My liege’s family took me in as an infant and I was never enlightened to the details. 

> Izalea: I guess we are all really separated from our more peaceful pasts. Its probably a common theme amongst so-called adventurers. 

> Oganda: Well we can provide you shelter until you heal and a few supplies in return for helping rid the woods of the menace. But if the black-blooded are after you, my hospitality must end there. We are simply too weak after our last few skirmishes to deal with an organized threat. 

> Luvese: We understand and are certainly grateful for everything. 

The group enjoys the short vacation in the modest accommodations provided by the centaurs. The chief provides them with a few potions passing wizard left behind as payment. These included a potion of heroism, a beetle elixir and a potion of beast mind control. Oganda also included a pouch of magical seeds called seeds of wealth. As the end of their stay approaches, something spectacular occurs one fateful evening. A shooting star streaks across the sky, drawing the attention of the group. It streaks overhead and strikes in the vicinity of some old ruins a few miles into the woods. Something deep inside Izalea is stirred by this event, something overwhelming that pushes her to go to the site. The group obliges, and heads for the ruins.
