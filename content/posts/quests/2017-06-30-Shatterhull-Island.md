---
title: "Shatterhull Island"
date: 2017-06-30
description: "After battles with ogres, spiders, harpies, and a water elemental, Cas, Digoria, and Link continue training to face the drow mage Ellion Stryfe in his fortress."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 04 - Shatterhull Island
Session: 01
DM: Austin
```

After the adventure in the Cave of Wonders, Cas, Digoria, and Link set sail upon a ship that Digoria had "rented" (though with no intention of returning) from a merchant.  While Cas isn't happy about this behavior, he agrees to accompany Digoria, believing that she has friends in high (and low) places that may be able to help in him in regaining his lost memories.

Link, unsure of his own history accompanies the party as well.  He states that he remembers leaving his homeland (though he can't recall anything about it) and meeting Cas in a tavern in Tristram while adventuring.  They struck up a friendship an when Cas did not quickly return from investigating rumors about a haunted mansion, Link pursued his friend.

At present, Digoria informs her acquittances that the "map" is more of a "compass", except that instead of pointing North, it points to *a very special ship* which is what they are sailing towards.  While Link and Cas have agreed to accompany Digoria, they remain suspicious of her history, character, and motivation.

As the crew sails around the southwest islands, they draw close to one of hte smaller and notice that there are many wercked ships along the coast.  They also hear faint, soft music.  They crew decides to dock in a safe area and start exploring the beach area of the island.

Cas notices a couple ogres fishing on the shore along the wreckage and he and Digoria decide to attack head on while Link sneaks along the rocks, bow readied.  After brief combat, the ogres are easily charred by Digoria's lightning bolt.  Link, however, feels ill (he did, in fact drink quit a bit of wine and mead aboard the ship last night) and must retire back to the docked ship for rest, leaving Digoria and Cas to continue alone.

Upward, through the hills of the mountains middle of the island they go as the music they heard upon docking grows ever more clear.  Cas and Digoria notice some sort of creatures flying around the top of the mountainous hill that appear to be the source of the music they are hearing.  However, they find it a better option to venture into the cavern system below and work their way upward from inside.

As they work their way through the cavernous paths they come to several forks in the path.  Thanks to their fortitude, they are able to stick together.  Fortunately so, for they are ambushed by a number of phase spiders!  Apparently, the creatures' songs from above were meant to lure adventurers apart and make for easy prey for the spiders below.  Showing great skill and tact, Cas and Digoria make quick work of the spiders and find a few potions and a few assorted alchemical ailments and remedies for their trouble.

Atop the mountain where the two assume they must go next, they are greeted with a volley of harpies who immediately swarm to attack once they realize they've been spotted.  Cas works his way to the foreground and draws the attention of many of the feyfolk.  Digoria barks orders for Cas to draw more fire and concentrate as many harpies as possible within a small area.

Cas grunts and groans, but goes along with the plan.  However, his frustration grows large and his temper short with Digoria "apparently doing nothing" -- his perception of her readying a powerful spell.  Just as Cas falls to a knee Digoria unleashes a powerful lightning bolt of critical mass that reduces the entire flock of harpies to ash and bone (likely to have killed them without any additional damage from Cas, yet the plan would have surely failed had they fey not been in a concentrated area).

Link, apparently feeling better, has worked his way to the mountaintop and joins Cas and Digoria.  Together they notice that there is a long chute atop the mountain.  Seeing no where else to go, they chug a minor potion of healing and jump down the long dark chute.

Splash!  The chute lands them in a bay cavern at the base of the island.  Digoria, thanks to her ring of water walking, rises to the surface and waits for her accomplices.  As they observe their surroundings, the group is attacked by a water elemental that Digoria is able to fend off while Cas and Link swim to the shore.

Digoria lures that elemental creature back to the shore so that her allies can be of use in the combat.  Though the creatures land several crushing waves upon the party, it ultimately meets its end.  Exhausted for the day's work, the crew reconvenes aboard their temporary ship.  As they look out to the horizon, Digoria parts to a far away mountain range.  There(!), she says, is the tower fortress of the drow mage Ellion Stryfe.

Digoria tells Cas and Link that she brought them to this island in order to further refine their adventuring and combat skills enough to lay seige to the mage's castle.  With and exasperated sigh, she announces her belief that the crew is *still not nearly strong enough* to make an attempted attack on the castle.

---

**Notes:**

* Quest Credit: [Stormwrack, 3.5e](https://dtdnd.neocities.org/books/player/Stormwrack.pdf)