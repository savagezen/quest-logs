---
title: "Adventures in Charn"
date: 2017-05-07
description: "Digoria trains under Jadis, The White Witch, in exchange for retrieving a legendary ship. She learns the sword and battles a white dragon, but questions Jadis’s motives."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 01 - Adventures in Charn
Session: 01
```

#### Preface:

~ 10 years before presents day

The time had come when Digoria had entered through the wardrobe at beckoning of Jadis, The White Witch and Goddess of Charn.  Jadis, finding Digoria to be of a noble bloodline of sorcerers, yet incompetent in the ways of the sword, offers to train her in such a discipline. 
 
The price to be paid is that Digoria will seek out a ship in her home world, one that she has captianed prior (The Entropy Tide) and can travel between planes and realms.  Jadis states that at the end of all worlds in her realm, The Realm of narnia, a great lion ([Aslan](http://narnia.wikia.com/wiki/Aslan)), destroyed all creation.

However, her "Deep Magic" is allegedly strong enough to persists beyond physical destruction.  Having been eternally defeated by Aslan, Jadis wishes to command The Entropy Tide so that she can wage eternal war against her nemesis, the chaotic evil god of her realm, [Tash](https://narnia.wikia.com/wiki/Tash) -- who she find repulsive.

Jadis has further stated that only mage, every several generations, is capable of commanding The Entropy Tide, even whilst donning The Mempo of Twilight.  Additionally, Tash appears to have made a similar bid to a demigod from Digoria's realm (Cain, The Necromancer) whom has made an alliance with Digoria's drow mage nemesis, Ellion Stryfe.

While Digoria is eager to learn the sword, she is leery of such a powerful promise.  However, seeing as Jadis had, moments earlier banished Digoria's master from all existence (ethereal and corporeal)... Digoria does what she does best, whatever she needs to to survive.

Even now Jadis appears extremely powerful, capable of interplanar communication and transporting objects (other than herself) to / from realms and creating a pocket realm for her "force ghost" to reside in.  She states, though, she exists presently as only shard of her former glory.

---

#### Once Upon a Time in Charn:

Jadis is skeptical of Digoria and her alignment at first, but knows that the nature of her power, and thus has gone through painstaking efforts to bring Digoria to the pocket realm she has created.

As the two walk through the recreated version of Jadis's world Digoria looks around and finds the place to have a strange and morbid haunting beauty.  Her sense of the territory is that it is somewhere between a Winter Wonderland and a charnel house full of skeletal remains.  But, there are also fringes on the horizon, as if the world has not yet been completely created... half finished, or falling apart.

Jadis tells Digoria that there was once a river in The Wood Between Worlds that lead to her homeland of Charn.  She states that though the river dried up just before Aslan's undoing of the realm, Charn did not completely die.  Therefore, she remained partially connected to the world and has been able to use, what she calls 'Deep Magic from darker and much older days' to recreate the world she knew.

```
Jadis:  You see, young tiefling, true evil never dies.  So long as there 
is light, there exists a presences where it is absent."

Digoria:  Sounds charming...

Jadis:  All life has remained extinguished here until I brought you in.  
As you see, our sun, being of a much older realm, is larger, red, and 
very much colder than the sun of your world.

Digoria:  So, you've brought me here to train me to steal a ship for you?

Jadis:  Indeed, as I've explained.  I cannot yet transport a being of my 
status between realms.  A peasant, such as yourself, is a much more 
simple task.

Digoria:  So, where is that you come from Ms... us... Jadis?

Jadis:  Once, when all realms were young and even the Faerie folk were 
just learning to use magic, there were far fewer races in existence.  
I am of a royal heritage consisting of noble Storm Giants and the most 
powerful Djinn.

Digoria:  You're obviously very powerful in the ways of magic and 
skilled on the battlefield as well.  What is this 'training' going to 
cost me?  I don't suspect you're the type that has much use for gold?

Jadis:  Indeed... perhaps you are smarter than you appear.  You are 
correct.  I have no use for gold.  What I need is time and a vesse... um
 (starts pacing), a vehicle.

Digoria:  ... right then... so, the price?

Jadis:  You shall bring me The Entropy Tide first and foremost.  However 
such a trophy will not come easily nor is your time amid it's captainship 
without it's cost.  I shall grant you permission to retain the mantle of 
Captain of the Entropy Tide and make you a proper swordmage of Charn if 
you bring the me the ship and swear allegiance to my army; of which, of 
course you will put put in the highest command of.

Digoria:  And I don't supponse you're the type of goddess who 
appreciate's being crossed?

Jadis:  Think not of betraying me!  It would behoove you to not triffle 
with a goddess known for her ill temperment and short tolerance for 
traitors and unorderly behavior.  Have you thoughts of betraying me?

Digoria:  I have thoughts of surviving one day at a time.

Jadis:  Very astute of you.  Come, I shall grant you your weapon and we 
shall begin your training immediately!
```

And so it began, Digoria's training under the projected avatar of Jadis.  A cruel master she is, and a very Emporer-Anakin type of relationship ensues.  Jadis, loving to talk of herself provided much information to Digoria about the history of Charn and it's people.

```
Jadis:  In truth, I disdain the Sons of Ada... er ... humans of your 
world.  Their kind was made an end of in Charn a thousand years ago 
when my grandfather laid waste to 700 of your world's mightiest warriors 
before tye'd drawn a single sword.

Digoira:  (asks whay makes Then Entropy Tide able to travel between worlds)

Jadis:  There are two types of rings, and at least two of each in 
existence.  Green ones are for traveling back from worlds, and yellow ones 
are for traveling to worlds.  

I once encountered a meek magic user from your world, Andrew Ketterly 
who crafted the rings from faerie dust sealed in a box from the world of 
Atlantis that he said he had gotten from his half-faerie godmother.

The rings have transported me to other world before, and so they shall 
again.  Over the eons I have grown in power, and as you're an example of, 
I can transport many things and have small climate effects on other 
worlds, but no where near the power to bring myself into corpreal form

... yet.
```

Jadis also explains that once upon a time dragons used to exist in Charn.  This would be the training tool for Digoira.  Jadis presents Digoria before a white dragon wyrmling.  Each day Digoria battled the dragon.

However, the dragon was particularly enchanted to age quickly.  Thus, as Digoria grew in strength and power, so did the dragon.  So, in fact, rather than building an edge over the white dragon, each week, each challenge became more and more difficult as the dragons abilities grew faster than her own.

Of course, the dragon was at the command of Jadis herself and would always stop short of the fatal blow to Digoria.  Then, rest and recovery would ensue and when healed -- though oft prematurely -- Digoria would resume her training against the dragon.

As the challenges grew, Jadis progressively provided Digoria with pieces of equipments as she deemed Digoria worthy to use them.  First, for survival purpose in Char, and particularly in recurring battle with a white dragon, Digoria was given [Boots of Winter Walking](https://open5e.com/equipment/magic-items/boots-of-the-winterlands.html) -- also known as "Ice Breakers" in some realms.

Next, a mighty weapon.  Though Jadis did not tell Digoria of the weapon's full potential it was legendary no less.  What Digoria perceived to be a finely crafted and stylish rapier was actually crafted with a [Frost's Bite Blade](https://www.dandwiki.com/wiki/Frost%27s_Bite_(5e_Equipment)).

In addition, the 'grand' nature of the weapon would allow strong damage than normal weapons of it's size and dexterity.  It was not until many years later, with the help of a Djinn back in her home world that Digoria was able to identify the weapon's concealed nature.

Nevertheless, the training continued with these tools and Digoria's skills grew both in terms of her magic use and proficiency with a blade.  Indeed, she was becoming a proper swordmage.

Perhaps The White Witch was not able to perfeclty recreate a full strength young white dragon in her pocket realm.  Perhaps it was by strong magic and, with no doubt, the aide of enhanced healing enchantments from Jadis (she did, after all revel in prolonged bloody battles), but one day the dragon lay slain at the hand of Digoira.  indeed, the time had come for her to return to her world as Jadis now believed she was ready.  Jadis stating that Digoria's hatred for beasts and the world had grown and that her mind had become deafened to all but the thirst for battle. 

Jadis had trained a mage with a historic bloodline to now use a blade, however, this particular sorceress -- by fate it seems -- abides by the nature of chaos and has no taste for the bickering between good and evil.

The legends and lore grow less clear here.  Some state that this time marked a clear descent into madness for Digoria.  Others say she became a perfect weapon, a pure instrument of battle.  Yet others still claim that her mind fractured; never completely loyal to the masters of her world and yet never succumbing to the laws of any law or lord.

Even all legends combined cannot tell this tale in full and there are many holes that remain to be filled and are unknown even to Digoria herself.  All that is known next is that she was cast, violently back into her world in a bay where there was a vicious storm and she was brought to town and exiled for her demonic appearance.

Apparently the travel between worlds was quite tax as, at that time, Digoria appeared to only have a fraction of her memories as well as sliver of her power.  It was as if she has started all over again as modest youngling.  However, her adventures soon came to her as she dusted of her skills on the battlefield and evocative dreams elicited memories and abilities long forgotten.