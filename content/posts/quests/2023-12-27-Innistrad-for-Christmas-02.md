---
title: "Innistrad for Christmas, Part II"
description: "Digoria, Liliana, and Sorin battle hezrou demons and the Krampus atop a cathedral. After intense combat and strategic magic, they trap the Krampus, but Zoltun Kulle's threat remains."
date: 2023-12-27
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 01 - Innistrad for Christmas
Session: 02
DM: ChatGPT
```

---

### ChatGPT Prompt:

> [Reference Guide](https://oracle-rpg.com/2023/03/solo-dm-guide-part-3-chatgpt-as-assistant-ai-dungeon-master/): Role, Capacity, Setting, Insight.

**Role and Capacity (user input):**

> Assume the role of dungeon master for D&D 5e. I will instruct you directly if needed and declare my character's actions by first stating the character's name. Players will manually roll dice and track character sheets. Your job is to guide the story and fulfill the role of NPCs.

**Setting (user input):**

> The Campaign Setting is: the world of Sanctuary from the Diablo video game series, incorporating lore from Forgotten Realms, Magic the Gathering, and D&D 5e.

> Player Characters are: Digoria Antica (level 20, tiefling warlock), Liliana Vess (level 18, human necromancer), and Sorin Markov (level 15 vampire fighter).

> Antagonists and Personal Stakes are: Zoltun Kulle a lich who has recreated the Immortal Sun in Tristram, stopping any innate or magical planeshifting from away from Sanctuary. Kulle is attempting to rebuild the Black Soulstone after it was destroyed in the Reaper of Souls events. Kulle has learned of the planeswalker Nicol Bolas ([formerly CR30](https://chatgpt.com/share/174d37cf-7c8c-4e65-8c2d-e16898067d18)) and sees him as a reasonable threat to Sanctuary, so he's actually trying to summon Bolas to Sanctuary, with the party as bait, to then be trapped by the Black Soulstone. Kulle is hoping that whoever survives, Bolas or The Party, is weak enough to be defeated by Kulle and also be absorbed by the Black Soulstone and augment Kulle's power. Kulle doesn't trust the track record of angels in Sanctuary; thus he's skeptical of Ugin being able to keep Bolas at bay. Nichol Bolas now nameless and imprisoned ([current CR7](https://chatgpt.com/share/174d37cf-7c8c-4e65-8c2d-e16898067d18)), cannot be summoned by normal means. Kulle doesn't have faith in Ugin the Spirit Dragon (CR24), due to the track record of angels in the world of sanctuary, to keep Bolas contained. Bolas has been greatly de-powered and cannot escape the meditation realm, but the MTG universe has not specified if Bolas can communicate to or from it. Cunning as ever, Bolas, the once Pharoh King, would be willing to deal with any mortal or undead ally that could break his brother Ugin's grasp over him."

> The Current Situation is: The player party is currently trying to flee a battle with a level 20 Krampus. The Krampus is severely wounded, about 25% life, on a rooftop. The party descending a ladder towards the inner part of a building to escape the battle. There are two hezrou demons on the rooftop with the Krampus.

---

### Played Adventure:

> Full Transcript: [Chat Link](https://chatgpt.com/share/0dafbf39-404d-4491-9798-c00d1d16bd1c)

> Narrative Prompt: "give me a narrative story of this session that reads like a action novel"

The night was thick with the scent of smoke and blood as the battle raged on the rooftop of the crumbling cathedral. The moonlight cast long, eerie shadows over the scene, illuminating the fierce combat between the adventurers and their monstrous foes. Digoria Antica, a tiefling warlock with glowing eyes and a mane of wild hair, moved with supernatural speed, her shadow blade flickering in and out of existence as she struck at the hulking hezrou demons.

Beside her, Liliana Vess, a human necromancer, stood defiantly despite her injuries. Dark tendrils of necrotic energy coiled around her fingers, lashing out at the demons with deadly precision. Each strike sapped the life force from her enemies, leaving them reeling.

Sorin Markov, a vampire fighter, carried Liliana to safety, his fangs bared in a snarl. He set her down gently, then turned to face the remaining hezrou with a grim determination. With a swift, fluid motion, he drew his longsword and leaped back into the fray, his blade gleaming in the moonlight.

The drow allies, expert marksmen from the shadows, fired their hand crossbows with deadly accuracy. Bolts flew through the air, striking the hezrou and chipping away at their tough hides. Despite their valiant efforts, the demons were relentless, their claws and teeth gnashing and tearing at anything in their path.

Digoria, sensing the tide of battle turning against them, called upon her dark magic. She cast a maddening hex on the Krampus, the twisted demon who towered above them all, and followed it with a hellish rebuke. The Krampus roared in pain, its eyes burning with fury, but it was not enough to stop it.

The celestials summoned by Sorin unleashed their divine wrath upon the Krampus, their radiant strikes searing its flesh. Despite their valiant efforts, Sorin's concentration wavered under the hezrou's relentless assault, and the celestials dissipated into the ether.

With a final surge of necrotic energy, Liliana cast two tendrils of agony at the Krampus. The dark magic struck true, and the demon staggered, its strength waning. Digoria, in a last-ditch effort, lunged at the Krampus, attempting to grapple it and shove it into the magic sack she had obtained.

The Krampus, weakened and dazed, struggled feebly against Digoria’s grip but ultimately failed. With a triumphant shout, Digoria shoved the demon into the sack, sealing it away. As the sack’s magic took hold, the Krampus’s roars were muffled, and the sack fell silent.

Meanwhile, Sorin, battered but unyielding, struck the remaining hezrou with his longsword. The blade cut deep, and the hezrou let out a final, guttural cry before collapsing to the ground. The drow allies fired their crossbows one last time, ensuring the demon was truly defeated.

As the dust settled, the adventurers took a moment to catch their breath. Digoria, her energy spent, felt the weight of her exertions and leaned heavily against the ladder. Liliana moved to her side, casting a healing spell to mend her wounds. Sorin, ever watchful, scanned the area for any remaining threats.

The battle was won, but the war was far from over. Zoltun Kulle’s dark plans still loomed over them like a shadow, and the party knew they had to regroup and prepare for the challenges ahead. With the Krampus captured, they had gained a valuable asset, but they also knew that greater dangers awaited them in the days to come.

In the eerie silence that followed the battle, the adventurers exchanged determined glances. They were bloodied but unbowed, ready to face whatever horrors the lich Zoltun Kulle had in store for them next. The fate of Sanctuary rested on their shoulders, and they would not falter."

---

### ChatGPT Summary Prompt for Next Session:

User Input:

> "Give me a summary that I can prompt you with for next time; including your role and capacity, player characters, antagonists, personal stakes, and the current situation."

Summary Prompt (to copy and paste):

> "You are the Dungeon Master for a D&D 5e campaign set in the world of Sanctuary from the Diablo series, incorporating lore from Forgotten Realms, Magic the Gathering, and D&D 5e."

> "Player Characters: Digoria Antica (level 20, tiefling warlock), Liliana Vess (level 18, human necromancer), and Sorin Markov (level 15, vampire fighter)"

> "Antagonists: Zoltun Kulle (CR21 lich), attempting to recreate the Black Soulstone and summon Nichol Bolas to trap him.  Krampus (CR20 demon) defeated and captured by the party.  Hezrou demons, minions of the Krampus, that were defeated by the party."

> "Personal Stakes:  Zulton Kulle''s Plan: Summon Nichol Bolas to Sanctuary, use the party as bait, and trap Bolas in the Black Soulstone to augment his own power.  Party's Goal: To stop Kulle's plans and survive the traps and creatures he sets against them."

> "Current Situation:  The party has just defeated a battle on a rooftop, with Digoria capturing the Krampus in a magic sack.  Digoria is exhausted from the haste spell's end.  The last Hezrou was critically injured and finally defeated.  Sorin and the Drow allies contributed to the final blows against the enemies.  The party needs to regroup, heal, and decide on their next actions.  Next steps could include interrogating the captured Krampus, planning the next move against Zoltun Kulle, and ensuring the party is ready for the challenges ahead."