---
title: "World Marshals, Act 3"
date: 2016-10-05
description: "So far Green Goblin is trying to make an infantry of cyborg superheroes in cahoots with Sebastian Shaw who is using second hand Stark Tech to create a calvary of metal gear sentinels.  Loki is scheming somewhere.  Is there another big evildoer tying things together or is this the crescendo?"
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 02 - World Marshals
Session: 03
```

### Event:  World Marshals

*In the fall out of the Civil War several events took place as a world-scale practice run for the villains who would later spearhead the events  of Siege and Fear Itself.  The Metal Gear Solid crossover, Raiden, sets out to stop them; making friends and foes along the way.*

#### Act III:

*So far Green Goblin is trying to make an infantry of cyborg superheroes in cahoots with Sebastian Shaw who is using second hand Stark Tech to create a calvary of metal gear sentinels.  Loki is scheming somewhere.  Is there another big evildoer tying things together or is this the crescendo?*

**Doom Pool:  2d8**

* Join a Team (1XP)
* Stop the Sentinel Attack (3XP)
* Stop the Metal Gear Excelsus (10XP)

-----

#### Scene 1:

Returning from the Green Goblin and Loki's lair, Raiden asks Nick Fury how he manage to swing a Helicarrier with Tony Stark still having so much control over SHIELD.  Fury replies that he still has a lot of string to pull *in the old guard* and has a lot of favors he's calling in *"because why the hell not at this point."*

Fury asks Raiden if he still uses the alias Mr. Lightningbolt.  When Raiden affirms, Fury opens a door revealing the teammates he has found for Raiden and remarks; *"The Lightningbolts" has a certain ring to it"*.  The team largely consists of past and future members of The Thunderbolts and X-Force, adding to the candidacy of the team name.

[The Punisher](http://marvelheroicdatafiles.wikidot.com/es:punisher) starts to bicker briefly with [Deadpool](http://stuffershack.com/wp-content/uploads/2012/04/Deadpool1.png) because of his prior Pro-Registration stance, but Deadpool counters by saying; "Like any other merc, I can bought, bribed, and bamboozled."  Raiden is somewhat skeptical because he has mostly functioned on his own suffered quite a bit of betrayal for FoxHound and other teams. Before he can give it more thought an alarm sounds that [Metal Gear Sentinels](http://marvelheroicrp.wikia.com/wiki/Sentinel_Mark_VI_(Watcher_Datafile)) are attacking a small city in the Middle East.  It looks like the team will get an early test as they're deployed to combat Crossbones (BR37), and three Metal Gears.

There is slight arguing between Raiden and Storm bout leadership, mostly related to Raiden favoring a free-for-all while Storm wants a more organized approach.  The first Metal Gear Ray attacks Raiden and lands an energy blast, but Raiden is able to seek cover.  The Punisher opens 
fire on Corssbones and lands a few glancing shots before Crossbones finds cover.  At the same time Deadpool and [Fantomex](http://marvelheroicrp.wikia.com/wiki/Fantomex_(X-Force,_Hero_Datafile)) tag team the other Metal Gear Ray.  Initially Deadpool underestimates it's toughness, but Fantomex is able to confuse it with misdirection; allowing Deadpool to get another shot that's a critical hit -- incapacitating the second Metal Gear Ray.

Meanwhile Storm (BR92) rallies a ferocious elctro-thunderstorm that severely weakens the first Metal Gear's defenses.  Raiden is then able to attack and a land a heavy blow though not quite incapacitating the machine.  The Metal Gear fires it's machine guns at Storm, but her agile flight allows her to avoid serious injury.  She answers with a lightning strike that blows the head off the Metal Gear, putting it to rest.

Crossbones then opens fire on the Punisher which yields a series of narrow misses.  Deadpool then teleports next to Crossbones and small scuffle ensues.  While Crossbones is distracted with Deadpool, Punisher is able to get close enough to stab Crossbones.  Crossbones is about to lash out, but Fantomex joins in and tells Crossbones that he's outnumbered and outgunned.  Just then an large explosion (doom pool) is detonated from within each of the fallen Metal Gears.  This creates a distraction for Crossbones to escape, though the heroes regroup and are 
hot on his trail.

-----

#### Scene 2:

The team decides to check for injuries before chasing after Crossbones.  Mostly only minor wounds, except for Raiden, though he knows he still has yet to tap into his Ripper form during this adventure and the stakes are high.  However, he is trying to forget that he also has Goblin Serum implants in his brain.  The team decides to push on.  However, Riaden lags a bit behind so that he can try and salvage some energy units from the fallen Metal Gears.  He's getting low on resources.

They chase Crossbones through a series of allies and find him standing (and waiting) in an open field.  The team finds it suspicious and stalls before chasing after him.  Good thing too, because suddenly a [Metal Gear Excelsus](http://marvelheroicrp.wikia.com/wiki/Tri-Sentinel_(Watcher_Datafile)) burrows up from underneath the ground!

Storm quickly begins to charge a weather attack, while Raiden readies his Rail gun.  Deadpool makes a witty remark about *"burrowing metallic spiders getting erect."*  Jean-Pierre and Punisher head into the thick of things.  All the while, Sebastian Shaw is piloting the Excelsus and going on a rant about Crossbones leading the heroes right to him and that he plans to work through them while the rest of Earth's heroes are busy fighting each other.

Jean-Pierre is easily able to incapacitate the already wounded Crossbones.  The Punisher throws a grenade that does little damage to a heavily plated section of the Metal Gear Excelsus.  Storm unleashes a lightning attack, that does no damage, but distracts the Metal Gear.  Raiden then unloads the rail gun, dealing a stunning hit!  The effects of which temporarily shutdown the Metal Gear's energy shield.

Deadpool tries to decide on the best measure and settles on singing to himself while he "thinks it over", making superficial sword scratches on the underbelly of the Metal Gear.

Fantomex tries to get the attention of the Metal Gear via misdirection, but he fails.  The Metal Gear notices The Punisher running away, but decides to try and stomp Fantomex who runs in front of it.  It is a crushing blow to Jean-Pierre, but his dampening plates save his life.  Storm unleashesa fury of electrical attacks that severely damage Excelsus!  Deadpool teleports atop the Metal Gear, planning to attack the cockpit.

Raiden then dashes in to close combat in a rage and slashes at the Metal Gear.  He lands a heavy blow, but it causes a huge electrical feedback current that brings Excelsus's energy sheilds back online and knocks both Storm and Raiden down for one round.  Deadpool then sees The Punisher driving back in a vehicle and teleports in to join him.  After a brief discussion, Frank jumps out while Deadpoll crashes the vehicle into  the metal gear (since he has healing factor and Frank does not).  The ensuing explosion shuts down one of the metal gear's legs.

Meanwhile, Raiden tries to free Fantomex from under Excelsus's other leg.  He is unable to lift the heavy machine and Excelsus stomps harder still, incapacitating Fantomex.  Excelsus then fires an energy blast at Storm which lands a heavy blow.  Raiden yells for Storm to start a rain an lightning storm. She obliges as Raiden and The Punisher again to lift the Excesus's leg off of Fantomex.  However, they fail and Fantomex is smashed even harder.  As a resultof the ensuing trauma, E.V.A. teleports Jean-Pierre out of the combat area.

Excelsus fires an energy blast at Deadpool which he acrobatically dodges.  Raiden tells The Punisher to stab him so that his *Ripper Mode* will be triggered. Initially, Frank argues, saying that it may trigger the Goblin Serum as well.  Meanwhile, Excelsus is trying to hit Deadpool with energy blasts, but keeps missing; whack-a-mole style.  The Punisher readies a knife, but Storm flies in to stop him.

Now angry at Storm, Raiden walks out to point blank range and starts taunting Shaw who is piloting the Metal Gear.  Storm tries to fly after Raiden to stop him, but The Punisher tackles her.  Just as Excelsus is about to fire an energy blast, Deadpool teleports over and impales Raiden in the midsection from behind; remarking "This is what you always wanted right?"  Raiden replies, "Thanks Wade.  I hope..."

Raiden is then agonizingly triggered into his *Jack the Ripper* personality.  "Wade! What have you done?!", exclaims Storm.  Raiden looks up, eyes red, and strongly says, "I am the lightning.  The rain transformed."