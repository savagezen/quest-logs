---
title: "Metal Gear Rising, Introduction"
date: 2016-06-06
description: "Metal Gear Solid x Marvel Crossover"
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 01 - Introduction
Session: 00
```

I've long been a fan of classic pen and paper RPGs.  Nothing beats classic D&D, but I've recently picked up Marvel Heroic Roleplaying again and started a solo campaign featuring [Metal Gear Solid's Raiden](http://metalgear.wikia.com/wiki/Raiden) crossed into the Marvel (comic) Universe.

If you're familiar with both MGS and Marvel, then watching the [MGS4 cutscene with Raiden versus several "Gekkos"](https://www.youtube.com/watch?v=8UMb-ubySec)
looks an awful lot like taking down some early era [Sentinels](http://comicvine.gamespot.com/sentinels/4060-4296/).

Here's a link to [Raiden's Data File](/posts/character-raiden/).  There's a page for his XP log, so you can regress all the way to the MGS4 state that I started in.  It isn't an exact match, but some things had to be proportionate to other characters and their respective compromises between comic feats and MHRP stat sheets.

### Framing the Crossover

After the events of Metal Gear Solid 4, Raiden returns to "civilian life."  He begins working for private militarized "security companies."  (Similar to the start of Metal Gear Rising)  However, he finds employment under Tony Stark who sends him on errands and security detail for situations that are deemed unwrothy of Iron Man's attention.  Raiden eventually finds out about Stark's (prior) support of "the War Economy" and the two have a falling out.  Naturally, many villains find the market for Private Military Companies (thematic in MGS4 and MGR) convenient and profitable.

Some classic Metal Gear Characters also make nice parallels to the Marvel Universe:

* [Vamp](https://metalgear.fandom.com/wiki/Vamp) and [Morbius](https://marvel.fandom.com/wiki/Michael_Morbius_(Earth-616))
* [Solid Snake](https://metalgear.fandom.com/wiki/Solid_Snake) and [The Punisher](https://marvel.fandom.com/wiki/Francis_Castle_(Earth-616))
* [Armstrong](https://metalgear.fandom.com/wiki/Steven_Armstrong) and [Sebastian Shaw](https://marvel.fandom.com/wiki/Sebastian_Shaw_(Earth-616))
* [Normal Osborne](https://marvel.fandom.com/wiki/Norman_Osborn_(Earth-616)) as a pawn of [The Patriots](https://metalgear.fandom.com/wiki/The_Patriots)

> *2024 Update: With all the multiversal (madness) content that's been created since this post was originally written; it's entirely possible that in an alternate time line Tony Stark / Iron Man never became as "hero" and continued as an arms dealer.  Eventually, he may have died or his whereabouts otherwise became unknown.  As a result you have many Iron-Man-esque armed militias or private militaries around the world.*