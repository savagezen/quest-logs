---
title: "Superunkonwn, Part 3b"
description: "In Entsteig, the party faces an erinyes and flinds while uncovering secrets about Corros's death. Allies unite to battle dark forces threatening the town."
date: 2018-03-12
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 07 - Superunkonwn
Session: 03 - Part B
DM: Austin
```

Upon returning to Entsteig Kulle, Digoria, and Alodel venture into Corros's keep and find Cas half naked and badly beaten, though apparently his mouth hasn't stopped running the entire time.  Naturally, innuendos and tongue-in-cheek insults pass between Cas and Digoria.  Tiring of "this foolishness" the erinyes snatches Cas's face and forcibly closes his mouth while raising her sword.  Digoria is able to somewhat parry the blow as it would have otherwise surely been fatal to Cas, asserting that "if anyone is going to kill that lunkhead it's going to be me!"

Battle ensues!  Just prior though, Kulle reminds the party that he is by no means required to aid them as "any other mortal could reunite my body just as well."  Unsure of the outcome of what they've just started, the accept Kulle's help -- making for two "favors" that are owed to him.

Between of Alodel's healing and divine magic, Kulle's debuffs, Digoria's vigor, and Cas's renewed spirits the erinyes proves only a moderate challenge.  Afterwords, Alodel is a bit shaken up from the combat -- never before seeing such a foe and hardly such a level of combat.  Digoria and Cas continue to bicker, naturally, but Digoria's point is made that while she and Cas are "even" for saving each other, she (unlike him) was under no moral obligation to do so and as such "payment is in order."  Begrudgingly agreeing to the logic.

Before departing Corros's keep, Digoria accidentally thinks out loud, saying something about wanting to open they Abyss (!).  Kulle catches this and tries to instigate among the party that she had something to do as well with the freeing of the vampire spellcaster Karasu who had previously been sealed in the Kokiri Forest Temple and is now "on her tail as well."  Fortunately, thanks to Digoria's knack for deception she is able to convince the party that these statements of hers are no more than a result of her quasi-psychosis and Kulles aging absent mindedness.

As the crew prepares to leave Corros's keep, they find that approximately 30 villagers have been hiding in the underground tunnels beneath the city.  The people have come out and are accusing the party of "bringing more bad luck, monsters, and destruction" to their village.  Digoria considers fighting the town's people, but thinks better of it -- though, they insist on hanging her and "don't understand why Corros let her off the hook in the first place."  "And now", they say "Corros is dead, slaughtered!"

Cas tries to reason with the mob as well, but takes heat himself for associating with a harrlett whom they just hung last week for practicing dark magic.  Alodel tires to reason and is able to aide Cas's case that while the part has brought some unsavory folk to town, they have also stopped many great evils who have attacked it.  With this reasoning, the mob's anger subsides and, in fact, they bring forth a chest of assorted goods that they have been able to salvage after the town's attack.

Much to Alodel's delight the party is able to search through the chest and find several rare weapons and trinkets (prayer beads and the like) for him to use.  And no time too soon as just when the group makes their way to the town for some rest, a messenger rushes through the gates frantically rambling on that he's spreading word of great evil forces gathering in the East Deserts and on in his way back he couldn't help but notice that the Kokir Forest is no longer burning and went to investigate.  The messenger goes on to report that there was an evil so great there, as he stood before the now opened Forest Temple, that he felt it consume the very air around him and he ran for his life, fearing that whatever was inside has now been loosed on the world with Corros's death.

No sooner do the last words leave the messenger's lips than he viciously pounced on and dismembered by a flind!  Much to the horror of the town's folk, another flind joins the feeding.  Cas, Digoria, and Alodel decide to try and stop the flinds for the sake of the town though they know this will be no easy match and they are all already in a weakened state.  To make things worse, Zulton Kulle refuses to aid them -- asserting that this is their chance to prove their worth in his presence and if they can defeat the flinds without aid he will remove one of the favors they owe him.

Through careful tact with Alodel's healing spells, Digoria's distracting antics, and near-death paralysis, the party is able to persist.  The flinds push them to their limits, viciously growling and yelping.  Cas, remaining paralyzed for the longest time is able to get back to his feet, but no sooner does this happen then is Digoria struck by a paralyzing blow from on of the flind's flails before she can think to have used a shielding spell.

Some quick thinking on Alodel's part allows him to cast a radiant area attack over the area which buys enough time for the melee combatants to do their part and conquer the flinds.  Washed in blood, Digoria and Cas stand over the corpses and thank Alodel for his healing capacities.  At last, it is time to rest, before setting out on the next adventure.

Kulle, most impressed keeps his bargain and removes one of the favors he hold over the party -- a most appealing turn of events for Alodel, given his divine alignment.