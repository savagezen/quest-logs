---
title: "Liches Be Schemin', Part 3"
description: "As the party nears Leoric’s manor, a dark fog laced with eerie butterflies swirls. Tensions rise, Digoria snaps, Serra falls, and angels descend—too late."
date: 2025-01-31
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 02 - Liches Be Schemin'
Session: 03
Author: Austin
```

### Current Situation:

> Current Situation: The *Black Label Society* has uncovered that Kulton Zulle is the lich Zulton Kulle. The party found the Wisemen (Dec, Kard, and Cain) turned to stone by a fey magic spell linked to blood, likely cast by Maghda. Despite growing tensions within the party and the discovery that the spell might be a trap, they press on towards the haunted cathedral outside Tristram, where Kulton has set up his grand portal operation. They are determined to stop the lich and unravel Maghda's plans, knowing time is running out.

---

As the party approaches the manor of the former King Leoric to no confront Maghda and disrupt, or at least gain information about, Zoltun Kulle's plans, they notice a thick black-ish fog surrounding the area.  The fog appears to be accompanied by neon black and green butterflies.

Sorin comments that they fog resembles the smoke they saw whirlwind out of the Wisemen's hut.  However, when Zakk attempts to detect magic in the nearby area, he informs the party that the spell is weak and weakening, *"like a lingering exhaust."*

The party has studied the books they obtained from the Wisemen's hut and is well acquainted with both [Maghda](https://diablo.fandom.com/wiki/Maghda)'s and Sacntuary's history with lesser and greater evils.  While they don't have much of a plan, short of "guns blazing", they won't be easily tricked or deceived either.

Suddenly, Serra, cringes in pain.  She says that she feels like she's trying to receive a telepathic message from the remaining fleet of angels in Westmarch, but Maghda's foggy magic around the magic is interfering with "reception."

Zakk is able to *dispel magic* long enough to Serra to connect her communication orb.  Serra informs the party that there was some sort of battle at Westmarch while the part was away, the details aren't clear, but Liliana has apparently been cleared of all charges as the Chain Veil has been returned to [Niv-Mizzet](https://mtg.fandom.com/wiki/Niv-Mizzet) by the other Angels, and Lilian is on her way meet up with the party.

The call is interrupted by Alodel, once Digoria's adventuring colleague, now abjuration representative of the Mage Council of Dexa and Guardian of the Citadel of Entsteig.  Al demands and update from Serra as "there's been an absurd increase in cosmic activity around Tristram in the last 24 hours."

Serra, struggling to maintain focus, replies that she's maintained her charge of watching Digoria, who -- so far -- hasn't been causing any mischief.  Uncharacteristically moved by emotion, Digoria jumps into view of the frame and curses at Al about "not needing a babysitter any more than the world needs policing by the mage council."

It seems that the once comrades have birthed some tension between each other in their time apart.

Al comments that there "may be a perfectly rational reason for the increase in magical activity that council can manage, particularly with the oversite of Niv's fleet of angels (of which Serra is a part of)."

The aura of hatred begins to fill the air.  Zakk struggles to continue dispelling the ethers that surround the party.  Sorin begins to get nervous, clinching his teeth and the hilt of his sword, feeling the pressures of the evils that threaten to beset the party.

Digoria continues to seethe towards Al and Serra.  Alodel comments that "it's just a matter of principle, nothing personal, but since you (Digoria) have had previous dealing with the lich Zoltun Kulle, some surveillance is warranted."

With Zakk and Sorin looking outward, that seems to be the last straw for Digoria.  She ignites her shadow blade and strikes Serra's hand, knocking the communication orb to the ground and injuring Serra!

"Al, can you confirm that both Liliana and I are cleared of all charges by the Council of Dexa?", Digoria asks, eyes fixed on Serra.

Serra reaches towards her weapon.

Al stutters, not being present to fully see what's unfolding.  Digoria shouts and repeats herself, drawing the attention of Sorin and Zakk.

Confined by his morality and position within the council, Alodel is compelled to answer honestly.  "Yes", he says, "With the sealing of Jadis' Temple and the return of the Chain Veil, Digoria Antica and Liliana Vess are cleared of all charges by the Council of Dexa."

Digoria, still focusing on Serra, asks Alodel, "... and what level threat would the council rate Zoltun Kulle if he were to have a lithomancer and a God Pharaoh at his beckon call?"

Alodel gulps audibly, "... beyond cosmic."

"Then it seems The Council will need assistance.  Though I'm sure they will not like any answer or solution I propose to resolve such a 'beyond cosmic' threat.  And you should be reminded Alodel, that I'm under no obligation to aide the council.  If you should find my assistance, or methods, worth inquiry... they can be bought, but not spied upon any longer... *it's just a matter of principle, nothing personal.*"

With that, Digoria unleashes a flurry of shadow-blade strikes on the communication orb, shattering it and ending the call.  In an flurry of rage she dashes towards Serra, who is ready for the attack.  Serra narrowly avoids the full force of Digoria's onslaught as Zakk and Sorin intervene to shove Digoria out of the way.  However, several of Serra's angel feathers fall to the ground as she's again been wounded.

Serra, noble in her charge, stands her ground.

"Some kind of magic has taken you over", Sorin says to Digoria, "you're never this emotionally tied to something, what gives?"

In a voice, seemingly not her own:

> Tears of blood rained on a desert jewel...Hell was torn asunder...a spear of light, piercing Hatred’s heart...a wise man with seven arms...a fog of lies...plagues of every name...

As Digoria appears to be under some sort of quasi-mind-control Sorin and Zakk look at each other confused.  Zakk taps his head, thinking out loud; "Now wait a minute, I've heard this somewhere before..."

Digoria continues:

> I saw a child give birth to a mother, as Hatred’s sun set and that of Terror and Destruction dawned...

"Ah ha!", Zakk proclaims, "It's [the prophecy of the first born](https://diablo.fandom.com/wiki/Rathma's_Prophecy), the first necromancer, Rathma!"

Digoria launches another attack on Serra, but appears more interested in Alodel and the communication orb.  The offensive sufficiently distracts Serra and Digoria is able to retrieve the orb (and a few of Serra's feathers) after knocking it to the ground.

Alodel stammers and tries to maintain an austere tone, "Digoria, you cannot seriously be considering summoning one of [The Great Evils](https://diablo.fandom.com/wiki/Great_Evils)!  Yourself, Cas, and I already had to clean up an inter-planar mess with another demigod because of your, your... your reckless and short-sighted shenanigans!"

It's seems there is indeed an aura of lies and hatred festering in the ether.

"You're in no position to be making demands, or trading insults Alodel.  Consider this 'goodbye', for now.  The only reason I'd consider saving the world is to be free of it's judgment... apparently no authority in Sanctuary has any ability to see beyond their own perceived benevolence."

Alodel continues making demands, his voice fading, as Digoria smashes the orb to the ground, clicking here bracers of haste and unleashing a flurry of shadow blades before anyone can intervene.

A burst of energy and shards of broken crystals fly through the air.  The thick, dark fog that had covered the area seems to have dissipated slightly.  Once everyone has regained their bearings, Digoria seems somewhat confused herself.

"Enough of this!", Sorin shouts, "I don't know what kind of madness, more than usual anyway, has beset you Digoria, but we can't keep fighting each other, especially out in the open like this."  Gesturing with spirit fingers, Zakk agrees with Sorin, "Yeah man, it's like some sort of spooky essence is still lingering around this mansion.  You'd think like one of those great evils of lies or hatred or something had ties to this place."

Everyone looks around awkwardly at each other, then back at Zakk, and back at each other.  "Just a hunch", he says shrugging.

Breaking the awkward moment, the sound of a galloping horse approaching breaks the silence.  It's Liliana!  She jumps in front of the party, unaware of the current tension and announces Maghda's defeat and briefly describes the battle that took place.  Specifically, she references the smoke, ash, and butterflies that departed the scene thereafter; somewhat explaining the fog that returned to the manor.

With Digoria, Zakk, and Sorin keeping their eyes fixed on Serra and Serra on them, Serra says; "... and the veil?"

"It's been returned to the rest of your fleet... Wait, what's going on here?!", Lilly says noticing the tension between the party.

"I will not stand for any alliance with the great evils of the cosmos!", Serra shouts.  With that she lets out a shrieking sound and a beacon of light shoots to the sky.  It seems that with the fog of lies and hatred lifted, her telepathic link with the rest of the angels has been restored and she's sent out an emergency SOS call.

"I never said I would summon a greater evil, only I that I would do whatever it takes to stop Kulle, because he's owed an ass whooping and I'd rather not have a council of mages and fleet of angels watching over my shoulder.", Digoria retorts.

Taking everything in Lilly composes herself and majestically strides towards Digoria, placing a hand on her shoulder, and boasts, "The blood feud between my brother and I is no secret to The Mage Council.  If they'd like him stopped, they'd best not get in my way."

Zakk looks at the brightening stars in the distance, indicative of the approaching fleet of angels and says to Sorin, "I'm trying to get my mojo back mate, and those don't look like the choir type of angels."  Zakk sides with Lilly and Digs.

Snidely looking over her shoulder towards Soring, Lilly says, "What's it going to be toothy-boi?"

Sorin, now drawing his sword and stepping to the front of the party, says, "I only want to get back to Innistrad, but killing Nahiri first seems a lot easier without a fleet of angels holding my hands..."

The stars in the distance continue to brighten as the angels draw closer and the party inches their way backwards towards the front door of the Maghda-Leoric Manor.  Unable to contain her righteous furry, Serra shouts "The might of the heavens will stop you!" and leaps towards Sorin to attack.

With little effort she is quickly shredded by Digoria's shadow blade and Liliana's tendrils of agony.  Sorin stands over Serra to issue the killing blow.  Zakk, first starts to hum, then louder, then begins singing [a fitting battle hymn](https://music.youtube.com/watch?v=xpM59e6lyws&si=cE8dJjTTKT1ZdIIB) under his breath.

The fog of lies and hatred weighs heavy in the air.  Terror seems to conquer the day as it fills the once faithful eyes of the wounded celestial.  With a final blow from Sorin's vampiric great sword, Serra is dead.  The forces of evil grow stronger, but the party remains free.

Meteor like forces begin to crash into the ground around the party as the fleet of angels descend, too late but, to rescue Serra.  "Let's get the fook out of here!", Zakk yells with a tone of inspiration.

The party flees inside The Manor's unlocked front door, slamming it shut, hoping it can withstand the onslaught of radiant energy from the angelic forces.  Eventually, the light diminishes, much to the party's surprise.

Everyone gasps a audible sigh of relief.

The party looks around at each other, all equally surprised.  Scoffing, and trying to play it cool, Zakk says, "Welp, that's that.  [All that shines turns to rust](https://music.youtube.com/watch?v=A40AGZ9tPcw&si=Jcm1c2nzCytlVqmB)!"

The party composes themselves, though it's apparent that they have not acted alone and that [other forces, divine or profance](https://diablo.fandom.com/wiki/Adria), linger heavily in the air.

---

### Notes:

> The above is a narrative "cutscene" between playable adventures.  ChatGPT and real die rolls (for randomization) were used for assistance.

