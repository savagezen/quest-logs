---
title: "World Marshals, Act 2"
date: 2016-09-26
description: "Raiden follows a spider tracer to the Green Goblin's lair and bites off more than he can chew while trying to see the bigger picture with Sedbastian Shaw involved."
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 02 - World Marshals
Session: 02
```

### Event:  World Marshals

*In the fall out of the Civil War several events took place as a world-scale practice run for the villains who would later spearhead the events  of Siege and Fear Itself.  The Metal Gear Solid crossover, Raiden, sets out to stop them; making friends and foes along the way.*

#### Act II:

*Raiden follows a spider tracer to the Green Goblin's lair and bites off more than he can chew while trying to see the bigger picture with Sebastian Shaw involved.*

**Doom Pool: 2d6**

* Convince SHIELD to Provide Transportation (1XP)
* Capture and Interrogate the Green Goblin (3XP)
* Obtain the whereabouts of Sebastian Shaw (4XP)

-----

After he and Spider-Man defeated Carnage, Raiden is introduced to [Nick Fury](https://marvelplotpoints.files.wordpress.com/2012/04/datafilenickfury.jpg). Spider-Man leaves Raiden to contend with SHIELD while he returns to his friendly neighborhood duties.  Raiden asks Nick for SHIELD's help in tracking down the [Green Goblin](http://marvelheroicrp.wikia.com/wiki/Green_Goblin_(Norman_Osborn,_Watcher_Datafile)) who Spider-Man was able to land a tracking device on.

He is initially unsuccessful in getting SHEILD's help due to Fury believing him unstable and reckless.  However, Raiden uses his codec to play back recorded audio from his missions.  Raiden uses this to convince Fury that SHIELD already has their hands full with post Civil War business and that these recorded events can't go unattended to.

Fury agrees to provide transportation for Raiden to follow the tracker Spider-Man was able to land on the Green Goblin.  Raiden also mentions that it might be a good idea for Fury to "start looking for calvary to back him up in case things get messy."

Spider-Man's tracking device leads Raiden to northern Pakistan, near Gilgit Baltistan.  The tracer leads to a large cavernous recess where Raiden is dropped off to pursue on his own.  Security within the cavern is suspiciously non-existent.  However, Raiden wanders through a gate that leads to an apparently vacant city.  He is cautious and remains hidden as there are several Sentinels wandering around.

Raiden has an encounter with Wolverine and a vicious battle ensues.  The combat was captured on surveillance cameras.  The footage has been 
uploaded to the internet by an unknown source and is featured in the link below.

Shortly after the battle, Raiden discovers that the Wolverine he encountered was in fact a cyborg.  Raiden concludes that the Green Goblin has been working to create cyborg versions of super heroes.  Raiden turns around and sees a handful of gruesomely killed SHIELD agents.  It is then that he wakes up and realizes that he has been dreaming during this entire time.

As he comes back to consciousness, he sees that he is restrained via magical bonds to an operating table.  Loki and Norman Osborn are in the room and Loki, as well as the corpses of the SHIELD agents Raiden saw in his dream.  Loki tells Raiden that Raiden has been subject to a reality warping experiment and that The Green Goblin, with Loki's help, easily defeated the SHIELD helicopters and that they never even landed.

Loki then states that he "has other business to attend to" and disappears, and with him the binds holding Raiden captive.  Raiden immediately jumps up to rush after him, but Osborn emerges in full goblin mode and warns that Raiden has been given implants of Goblin Serum that could explode when detonated, turning him into a psychotic killing machine with further augmented power.  Obviously a fight ensues between the two.

Green Goblin physically charges Raiden.  Raiden overpowers him, but experiences severe mental pain as a result of the Goblin Serum implants.  Apparently they can be triggered by severe stress.  The Goblin lets out a maniacal laugh and throws a pumpkin bomb at Raiden, which he is able to dodge though the explosion temporarily delays combat.

Raiden looks around for his sword.  The Goblin laughs again and says that "it isn't anywhere close."  Raiden is severely wounded as a pair of The Goblin's razor rings pierce through the bomb's smoke and hit Raiden.  Raiden charges Green Goblin and but is losing strength due to his wounds.

Raiden dodges a punch and lands a heavy blow of his own that knocks down The Goblin.  Raiden has the upper hand and lands another hard punch that stuns its target.  Raiden throws a hard kick, but narrowly misses The Goblin's head.  The Goblin rallies with a ghost grenade that stuns and creates great mental stress for Raiden; nearly triggering the Goblin Serum implants.

The Green Goblin rushes for his glider and makes an attempt to escape.  Raiden acrobatically leaps after him, but, in his current condition, is no match for the glider's speed.  The Green Goblin having gotten away, Riaden decides to have a look around the room he is in.  He finds a locked chest which he is able to break open though it takes several tries.  Inside, he finds his HF Sword as well as a silenced AK-47, a powerful rail gun, and a few spare parts to patch himself up.

Soon after, a SHIELD Helicarrier arrives outside the lair -- apparently in response to the previously downed helicopters.  Raiden uses his tact to find his way out, though he is concerned about the Goblin Serum implants the Green Goblin had warned about.  Certainly, this would make most unpleasant circumstance if Raiden were to enter his already unstable *Jack the Ripper* mode.

Raiden is taken to the infirmary for the serum implants to be assessed.  He is told that Nick Fury has been working on a finding him "backup."  Meanwhile, SHIELD examination of the lairs yields no information about Sebastian Shaw's whereabouts, though it does reveal that he has been working with the Green Goblin to create both an army of Metal Gears as well as Superhero Cyborg footsoldiers -- Raiden, Wolverine, and Goblin Serum being some of the primary components for their creation.