---
title: "Born Under a Bad Sign, Part V"
description: "The party battles Jadis in her palace, only to face betrayal from Digoria, who joins forces with Liliana. They defeat Jadis, but Digoria’s journey takes a darker turn."
date: 2023-11-09
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 15 - Born Under a Bad Sign
Session: 05
Author: Austin
```

> This "quest" is written as "fan fiction."  The original Dawn Treaders campaign never finished it's original final climactic quest which was intended to be titled "Born Under a Bad Sign" and conclude with the characters Cas, Alodel, and Digoria eventually going their separate ways after defeating the big-bad.  The personality and spirit of each character has been attempted to be preserved.

---

### Part 5 - The White Witch:

The party arrives at the ruins of Jadis's palace, fighting through some of the lesser monsters of her army to get there.  The scent of evil is strong.  Cas and Al gain confidence and conviction, while the usually sharp-witted and haphazard Digoria seems hesitant.

The party pushes onward, descending through the palace, finding oddly few obstacles in the way.  They arrive at an enormous hall far within the depths of the palace.  Within it, at the far end from the entrance is a wooden [wardrobe](https://narnia.fandom.com/wiki/Wardrobe).  Al isn't able to recall many details about such a furnishing from his library findings.

The opened wardrobe appears to be some kind of [eternal vortex](https://i.imgur.com/s3wkIvx.jpg) from which [Jadis is being resurrected](https://narnia.fandom.com/wiki/Jadis?file=PC-Witch.JPG)!  She gloats and greets the party, welcoming them to her chamber, "where many brave and foolish warriors have come to meet their end."  The party realizes there is no turning back and they must enter and engage in direct combat.  As they approach, the stone doors of the hall slam shut, sealing them in with the Specter of Jadis.

In her [current form](https://forgottenrealms.fandom.com/wiki/Kostchtchie) Jadis is far from her godly status, but a menacing presence nonetheless; though the party quickly realizes that she appear tethered to the wardrobe from which the specter is emanating.  The party begins to feel the full effects of eternal winter, weakening due to the environmental conditions.

Prepared as best as they could, and even with their combined might, the party is falling behind.  Then, suddenly Cas wrenches over and begins vomiting.  Al asks what's wrong and Cas replies that "the smell he sensed from the super cell in the prison... it's close!"  A powerful presence booms and bangs on the heavily fortified doors of the hall.

Undeterred by the attempted intrusion, Jadis continues to attack the party, appearing to continue to gain further upper hand; but the doors to the hall appear to be weakening.  Al's mace glow with a brightness he's never seen before, indicating the strong presence of the undead.  The doors to the hall explode with necrotic energy and "tendrils of agony" spread throughout the room!

A [smug and vengeful necromancer](https://i.imgur.com/DjmKELZ.jpg) presents herself from the rubble as the dust settles.  Jadis, scorned by the intrusion makes several attacks at the necromancer, who, fresh to the battle, is a formidable foe to Jadis in her current condition.

Alodel has the brilliant idea to attack the wardrobe itself directly while Jadis is distracted by the necromancer.  However, before he attacks, he notices Digoria standing still, not fighting, staring at the necromancer.  He can barely hear her voice amid the sounds of battle, the slightest utterance; "mother..."

Cas continues to be nauseous and his celestial nature causes him to lash out and attack at the necromancer so that he can be of more use in the fight against Jadis.  Much to everyone's surprise, Digoria intervenes, attacking Cas to defend the necromancer!  Digs comes clean with her plan, telling Cas that "Liliana (the necromancer) needed freed from the prison because the party wouldn't be able to stop Jadis alone."

"You!  You... You wanted to go to prison?!  You set us up!?"; Cas asks and yells simultaneously.  Digoria makes a sly comment about, "just being an old fashioned pirate."  Al, on the other hand, can't decide if he has developed a goth-fetish and is more than slightly attracted to Liliana, putting him in quite the moral predicament.

In his anger and disbelief, Cas is distracted, and sucker punched by a powerful attack by Liliana.  Alodel is beyond unhappy with being allied with a necromancer, even if he "is a fan of spandex and fishnets."  He demands that Digoria "come clean with anything else she's been hiding."  Before Digs can answer Liliana begins reanimating the dead foes Jadis has slain and had sacrificed in the hall in order to strengthen her in this realm; first a few, then a few more, then dozens, and then dozens more.

With the party divided, Jadis manages a sneak attack of her own before being confronted by her re-animated foes and having to again deal with Liliana directly.  Al snaps back to his senses and realizes that Jadis is again distracted and the wardrobe is left vulnerable.  He launches the few fireballs he has remaining to severely damage the wardrobe.  Doing so appears to have an exponential damaging effect on Jadis!

Liliana tells the party that they must seal the wardrobe shut with a legendary item from the world that it is a portal to.  Digoria's rapier!  Distracted by the reanimated foes and Liliana, Jadis isn't able to properly defend the wardrobe and the party rushes to attack it directly.  With the wardrobe weakened by fire, Cas tries to manually close the doors, but Jadis still isn't weak enough for the wardrobe to be physically overcome.

Liliana tells Digs that she must "repent to [Kiranasalee](https://forgottenrealms.fandom.com/wiki/Kiaransalee), in order to *embrace her true nature* and *full potential as a shadow mage*; adding that "the law and order of the cold never suited Digoria... that traveling to Charn in first encountering Jadis has corrupted and crippled Digoria."  Part of this feels manipulative to Digoria, part of it feels true.  How does Liliana know all of this?  Why did she never come to Digoria's aide in the past?  Liliana throws a potion to Digoria, a thick dark liquid emitting a vaporous smog.

Cas slaps Digoria upside the head and yells for her to "snap out of it and get back in the fight!" while he continues to struggle with the wardrobe doors.  Alodel ponders the recent events and gasps as he recognizes that Liliana is performing a "graverendering" and doesn't want to be further beholden or indebted to any undead or fiends.  He yells for Digoria to *not* drink the potion!

Digoria reads the bottom of the bottle which has imprinted letters reading "[re-imagine](https://www.5esrd.com/gamemastering/magic-items/potions-oils/)."  She looks up at Al and says, "Sorry Al, I'm good with it!"  She sloshes some of the potion down the blade of her rapier before chugging the rest down -- commenting how it tastes worse than the smell of Cas's "beer shits."

The potion-coated blade appears to have an added effect as the re-animated hordes along with Liliana's necrotic magic attacks begin to overwhelm Jadis.  Cas strains one last time and is able to bring the doors of the wardrobe close enough together to notice that when they do, there is a key-notch formed by the two halves.

As Jadis's specter begins to flicker as it weakens, Al calls for Digoria to use her rapier as a key to lock the wardrobe shut.  She inserts the rapier, locks the wardrobe, and before she can withdraw the blade is shoved out of the way by Cas!  Cas, in a tremendous effort, clashes both of his swords against the rapier, breaking it an it's magic as well as making it impossible to be removed from the wardrobe -- remarking to Digoria that a legendary weapon is the least she could pay for having Cas bail her out of Azkaban.

At last, Jadis is defeated!  However, as the entire palace is a construct of her magic and presence in this realm, it begins to shake and crumble around the party.  Liliana launches agonizing tendrils into both Al and Cas!  She walks up to Digoria and offers her another, smaller potion of the same variety as before -- commenting that she's had "centuries" to make them and walked across multiple planes to gather the necessary ingredients.

Liliana begins a planeshift spell and tells Digoria to make a choice.  Digoria looks to her compass of desire, which spins frantically -- offering no insight.  Digoria notices the ethers and shadow energy gathering around her; an effect of the previous potion.  Digoria calls out to Liliana that she "wants to double down" -- one good turn of deceit deserves another.

Liliana smiles and obliges, tossing Digoria another re-imagining potion, though Digoria's sleight of hand is swift enough to kick the second re-imagining potion to Cas.  Digoria chugs the third re-imagining potion and before entering the planeshift portal with Liliana, turns and comments to Cas and Al; "See you in another life boys!"  With that, the ladies disappear into the ether.

Cas and Alodel are able to escape the palace before it collapses into ruin.  They return safely to Dexa to find that, while there were some causalities, all members of the council have survived and that the city has been successfully defended thanks two groups of traveling adventurers who said they'd met Cas and Al before.  The groups called themselves strage things like "The Avengers" and "Guardians of the Galaxy."

Cas and Alodel contract a with a group of sailors to return to Entsteig.  After a brief adventure at sea, they arrive safely.  Cas holds the re-imagining potion in his hand and thinks about his quest for revenge and his old compatriots.  Al looks at Cas optimistically and tells Cas, "You already knwo what you have to do."  Al begins preparing a planeshift spell for Cas to rejoin the TWC in the Feywilds and pursue Sitariel.

Before departing Sanctuary, Cas comments to Al, that Entsteig will need its wards renewed and someone to protect it and keep watch over Sanctuary "especially with Digoria's crazy ass running around between planes now.. who knows what's going to get stirred up!"  Al smiles sheepishly and lowers his voice to say; "Challenge accepted!  I shall send an owl to Dexa to propose that I join Corros's vacant seat on their council."

Back in Tristram, Zoltun Kulle (previously known as Josu, brother to Liliana Vess) curses fate as Jadis has been sealed away.  In his ongoing udead quest for evermore power he seeks a new ally, the planeswalking dragon Nichol Bolas.

Meanwhile, reunited with and under the tutelage of *The Defiant Necromancer*, Liliana Vess, Digoria prepares to search for the Chain Veil and a legendary weapon to replace her destroyed rapier so that the two dark mages can fulfill their lust for revenge against Zoltun Kulle.