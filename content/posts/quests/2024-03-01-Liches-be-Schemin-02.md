---
title: "Liches Be Schemin', Part 2"
description: "Four warriors, led by tiefling warlock Digoria, battle through fire giants and witches in their quest for the Chain Veil, uncovering dark secrets in Tristram."
date: 2024-03-01
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 02 - Liches Be Schemin'
Sessoin: 03
DM: ChatGPT
```

---

**Scene 1: The Departure from Westmarch**

The sun was setting over the battered city of **Westmarch** as four battle-hardened warriors made their way through the gates. Leading the group was **Digoria**, a tiefling warlock with a devilish smirk, her eyes blazing with determination. Beside her, the brooding vampire **Sorin** gripped his greatsword, itching for a fight. **Zakk**, a dwarf bard with a flask in hand and a love for heavy metal, followed close behind, his eyes sharp and ready for anything. Bringing up the rear was **Serra**, the celestial paladin, her radiant armor glowing even in the dim light.

Their mission: to reach **Tristram**, consult the **Wisemen**, and secure the **Chain Veil**. But as they left Westmarch, their path was blocked by two massive **fire giants**. Without hesitation, Digoria stepped forward, her voice cutting through the tension like a knife. "Move, or you'll regret it."

The giants, sensing the danger, backed down. Sorin’s hand twitched on his sword hilt as he eyed them. "I would’ve enjoyed that fight," he muttered.

Serra shot him a disapproving look. "We don’t have time for that."

With the giants out of the way, the Black Label Society continued on, navigating the familiar terrain with ease. By the time night fell, they were at the outskirts of Tristram, ready to face whatever horrors awaited them.

**Scene 2: The Battle in the Forest**

As they approached Tristram, the group stumbled upon a ritual in progress—three **blood witches** chanting around a fire. Zakk didn't waste a second, his fingers dancing over his lute as he cast **Hold Person** on the first witch. Sorin moved like a shadow, his greatsword slicing through the air as he banished the second witch to another plane. Digoria was a blur of motion, her shadow blade striking true with deadly precision.

Serra, though usually restrained, couldn’t resist the thrill of battle. Her **Guiding Bolt** blasted into the third witch, illuminating the dark forest with holy light. The witches barely had time to react before Digoria and Sorin cut them down, their attacks relentless and precise.

As the last witch fell, Digoria wiped her blade clean with a smirk. "Why do they always make such a mess?" she quipped.

Sorin glanced at Zakk, who was tuning his lute. "Not bad music, dwarf."

Zakk grinned, taking a swig from his flask. "And here I thought vampires didn’t have taste."

The banter faded as they searched the crude camp. Finding little of value, they pressed on, knowing their mission was far from over.

**Scene 3: The Truth in Tristram**

Tristram’s gates creaked open, and the group was greeted by the indifference of the town guards. Adventurers came and went, but few left a mark. The Black Label Society made their way to **Headrig’s Inn**, where the blacksmith greeted them with a nod. Headrig’s eyes lingered on Digoria; their shared past was a story for another day.

“Kulton Zulle,” Headrig spat, the name dripping with disdain. “He’s moved his operations to the cathedral. The Wisemen don’t trust him—neither do I.”

Serra’s jaw tightened at the mention of a lich. It was her sworn duty to destroy such evils, and the thought of leaving Zulle unchecked gnawed at her. But Digoria, quick on her feet, convinced them to check on the Wisemen first.

As they approached the Wisemen’s hut, screams filled the air. The group burst inside, only to find **Dec, Kard, and Cain** turned to stone, their faces twisted in terror. Wisps of black smoke and butterflies filled the room—a clear sign of fey magic.

Zakk, always the scholar, cast **Legend Lore**, piecing together the spell’s origins. “This isn’t undead magic,” he said, his voice rough. “It’s fey. And it’s tied to blood.”

Sorin’s eyes narrowed. “Nahiri didn’t do this. There would’ve been much more destruction.”

Before they could delve further, Digoria made a move that caught everyone off guard. With a swift flick of her blade, she cut Sorin’s arm, collecting his blood on a cloth. Sorin roared in anger, but Digoria just smirked. “You’ll heal. And besides, patrons don’t pay themselves.”

Before Sorin could retaliate, Serra pointed to one of the statues. “Look!”

The blood Digoria had spat on the statue began to move, causing the stone figure to point towards a stack of books. The group followed the statue’s direction, uncovering a book on fey magic and undead rituals. The text warned of “unintended consequences,” a clear sign of danger. They grabbed what they could, including a journal that revealed **Kulton Zulle** was actually the lich **Zulton Kulle** in disguise.

As they continued their search, Sorin’s blood began to defy gravity, pooling on the ceiling in a twisted display of magic. The group knew they were in over their heads, but there was no turning back now.

**Scene 4: The Black Label Society**

Serra, torn between her duty and the mission, wanted to help the Wisemen. But Digoria’s cold laughter cut through the air. “Help? Clearly you’ve mistaken us for some type of heroes.”

Zakk, finishing off his flask, let out a roaring belch. “Yeah, it’s like we’re some kind of **Black Label Society** or something.”

The name stuck. With a new sense of identity and determination, the Black Label Society left the hut, their eyes set on finding **Maghda**. The road ahead was filled with darkness and danger, but they were ready for whatever came next.

---

### Notes:

> Full Transcript: [Chat Link](https://chatgpt.com/share/a1fcfc53-e36a-45ea-a9ad-60153fa61d87)

**User Input:**

> "You are the Dungeon Master for a D&D 5e campaign set in the world of Sanctuary from the Diablo series, incorporating lore from Forgotten Realms, Magic the Gathering, and D&D 5e."

> "Player Characters include: Digoria (level 20, tiefling warlock), Sorin (level 15, vampire fighter), Zakk (level 9, dwarf bard), Serra (level 13, celestial paladin).  Each member has a riding horse."

> "Current Situation: The party is currently in Westmarch and need to travel to Tristram to consult with the three wisemen, Dec, Kard, and Cain regarding the location of the Chain Veil; which unknown to the party is located in the Pandemonium Fortress.  The party has 8 days to travel from Westmarch to Tristram to Pandemonium retrieve the Chain Veil, and return to Westmarch, or one of their compatriots, who is being held hostage, will be executed."

> "Monster types for Westmarch include ruins, underdark, upper planes, and lower planes. Monster types for Tristram include forest, mountains, and cities. Pandemonium monsters include gothic horror, ruins, dungeons, and underdark."

> **User Input:** "Give me a summary that I can prompt you with for next time; including your role and capacity, player characters, antagonists, personal stakes, and the current situation."

**Summary for Next Session:**

> DM Role: You are the Dungeon Master for a D&D 5e campaign set in the world of Sanctuary from the Diablo series, blending lore from Forgotten Realms, Magic: The Gathering, and D&D 5e.

> Player Characters:
> 1. **Digoria** - Level 20 Tiefling Warlock, 4'10", cunning and sarcastic.
> 2. **Sorin** - Level 15 Vampire Fighter, seeking information on his nemesis Nahiri.
> 3. **Zakk** - Level 9 Dwarf Bard, heavy metal enthusiast, and Sorin's "bro-mance" partner.
> 4. **Serra** - Level 13 Celestial Paladin, driven by duty, often at odds with the group's moral ambiguity.

> Antagonists:
> 1. **Kulton Zulle** (actually **Zulton Kulle**, a powerful lich in disguise).
> 2. **Maghda** - A CR 17 Witch Queen, responsible for a dark fey spell.

> Personal Stakes: The party must retrieve the Chain Veil from the Pandemonium Fortress within 7 days or one of their compatriots, held hostage, will be executed.

> Current Situation: The **Black Label Society** has uncovered that Kulton Zulle is the lich Zulton Kulle. The party found the Wisemen (Dec, Kard, and Cain) turned to stone by a fey magic spell linked to blood, likely cast by Maghda. Despite growing tensions within the party and the discovery that the spell might be a trap, they press on towards the haunted cathedral outside Tristram, where Kulton has set up his grand portal operation. They are determined to stop the lich and unravel Maghda's plans, knowing time is running out.