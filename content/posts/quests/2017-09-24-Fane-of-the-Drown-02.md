---
title: "Fane of the Drow, Part II"
date: 2017-09-24
description: "Cas and Link, joined by Balfador and rescued elf Alodel, battle drow, duergar, and giant spiders in the caverns, using clever traps and teamwork to survive the dangers."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 05 - Fane of the Drow
Session: 02
DM: Ryan
```

Cas and Link awake in Warlock's study and find Digoria missing.  Instead, they are greeted by a dwarf, named Balfador, who states that he sympathizes with the dwarves of the cast and travels about as an adventurer for hire.

Balfador joins the company and they proceed through the caverns.  Link, not thinking clearly, breaks down a closed door, alerting the drow on the other side.  Link and Cas are forced to engage, while Balfador elects to sneak around the perimeter of the room.

There is a bound elven cleric in the far corner who seems friendly, but somewhat curt and obnoxious.  While Link and Cas are battling the drow, Balfador is able to sneak around and return the elf's equipment whilst the elf slips his bonds.

The elf and Balfador join the others and the drow are overcome.  Thanks, in no small part to Link finally mustering enough courage to unsheathe his sword and put it to use!

After the combat settles, the elf introduces himself as Alodel.  He states that he was adventuring nearby and taken captive by the drow.  Alodel also recalls a story the drow told while he was captive about the drow *"wanting to sacrifice / feed him to some creature."*

Continuing still through the caverns, the group is ambushed by a pair of deurgar.  Just when the party seems to have the upper hand, a [draegloth](https://imgur.com/wZ3QBmY) appears from the shadow and attacks!  Cas and Balfador are able to finish off the deurgar while Alodel and Link distract the draegloth with their arrows.  The draegloth tries to escape, but is caught and killed by the party.

Balfador proceeds to sneak around and explore further.  He passes a large, stinking room, and eventually comes to a large open room with many large holes the ground.  Holes filled with large spiders.  Unfortunately, he slips and startles the spiders and they start to awake!

Balfador dashes back to the others who have started to explore another room.  Just as they are inspecting several large bulbous knots of webbing, Balfador informs them of the spiders.  The group is able to deduce that the room they are in is full of spider eggs about to hatch!  Thanks to some quick thinking from Balfador, he is able to bar the door with some nearby debris.

The group moves on to the large stinking room and notice a two very, very large spiders starting to move around.  It seems that the "parents" of the nest are awakening.  Though a great deal of time is taken, the group waits patiently outside while Balfador rigs a trap with his dungeoning and stoneowrking tools (and tact in stonework) to rig a trap for the room to collapse.

The trap is triggered, the room collapses, and the spiders are severely injured.  And, now angry!  In a turn of fortunes, it is Link and the neophyte adventurer, Alodel, who are able to finish off the spiders.

---

**Notes:**

* Quest Inspiration: [Fane of the Drow, 3.5e](https://dnd-wiki.org/wiki/Publication:Fane_of_the_Drow)