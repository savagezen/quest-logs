---
title: "Training Day, Act 3"
date: 2016-06-10
description: "After returning to the U.S. and recovering, Raiden sets out to confront Iron Man about Shaw's statements.  Raiden is either planning to fight or quit working for Iron Man due to his support of the war economy."
tags: [MHRP, Metal Gear Rising, Quests]
---

#### Event:  Training Day

Raiden's first "solo gig."  Tony Stark has sent Raiden to be chief of security to transport a Prime Minister in the Middle East.  The Prime Minister has been pro-mutant and this has divided his country.  Several Private Military Companies have taken opposition to this.  Riaden needs to find out who is at the bottom of this and stop them if he can.*

### Act 3:

*After returning to the U.S. and recovering, Raiden sets out to confront Iron Man about Shaw's statements.  Raiden is either planning to fight or quit working for Iron Man due to his "support of the war economy."*

**Doom Pool:  2d6**

* Find out Iron Man's involvement with Sebastian Shaw (1XP)
* Gain access to Avengers Tower beyond your clearance (3XP)
* Stop or convince Iron Man to stop supporting the PMCs (10XP)

-----

Raiden is able to use some of his experience to further upgrade the reflex skills of his cybernetic body.  He is approached by Tony Stark who tells Raiden that he "*pulled some stunt back here*."  Raiden fires right into asking about Shaw.  Tony is caught off guard and admits doing business with Shaw.  Raiden presses further, but some slick talking and excuses get Tony out of the conversation.  Saying, that information is "above Riaden's pay grade."

Later Raiden goes snooping around Avengers Tower.  He goes to a storage room that he's seen Stark go in and out of a lot.  Surprisingly, Raiden's retina grants him access to the room.  It's a one stall elevator that goes to the top floor, adjacent to the top floor lobby of the tower.

Raiden notices a large cabinet that is electronically locked.  Raiden tries to use his tech skills to hack into it, but can't.  He also does not notice that tampering with the cabinet has triggered an alarm.  This is because he receives a mysterious codec call from someone saying they have plenty of "dirt on Stark Industry's arms sales."

The call is cut short by Iron Man (BR76) standing in a hidden doorway to the room and confronting Raiden.  Raiden again presses about Stark's involvement with Shaw.  Stark says that "terrorists killing each other keeps them from thinking about killing us."  he also states he has kill switches in all of his devices and, besides, "he's a one man army, no need to worry about Sentinels."

Raiden storms out of the room into the lobby when Stark mutters a jab that "Raiden is just like his father, too much of an idealist to do what needs to be done."  Raiden lashes out and tries to punch Iron Man, but Iron Man catches his fist.  He tells Raiden that he needs to be able to trust him, or else he'll "*let him go*."

Raiden tries to wrench his hand free, but Iron Man starts to crush it.  But, Raiden gets pissed and slashes Iron Man with his sword, freeing his hand.  Iron Man fires his repulsor laser from his off hand, but Raiden dodges the blast.  Raiden lands a hefty slash wound, but this angers Iron Man enough to use his Unibeam.

The blast blows Raiden through a wall, out the window, and off the top of Avengers Tower.  Iron Man flies after him, but catches him in a one-hand choke-slap position.  Iron Man says; "*Do you think you're immortal?  You're not even an Avenger yet.*"  Raiden replies; "*No, I just don't fear death.*"

Iron Man hurls Raiden through another window.  Raiden acrobatically lands, but he is very near passing out from damages.  Iron Man says that he has "nothing against Raiden", but "he just can't trust him anymore."  Iron Man gives Raiden the chance to walk away from there, which he accepts.

In the closing scene Raiden is met by a man in a buiness suite, whose face he can't see.  The many says he's quite wealthy and is "intimately familiar" with Stark's weaponry.  He's also the one who called Raiden earlier.