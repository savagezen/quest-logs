---
title: "Liches Be Schemin', Part 1"
description: "The party learns from a bartender about Nahiri’s distress and rumors of the Black Soulstone. Angels demand Liliana's location, giving the party 8 days to find the Chain Veil."
date: 2024-02-01
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 02 - Liches Be Schemin'
Session: 01
Author: Austin
```

---

### Played Adventure:

> This session is "fan-fiction" authored; i.e. plot development storytelling.  ChatGPT was used for random rolls, skill checks, and other assistance.

While taking in a short rest at [the inn](https://diablo.fandom.com/wiki/Wolf_City_Tavern), the party questions [Bartender Bailey](https://diablo.fandom.com/wiki/Bartender_Bailey) about [Nahiri](https://www.dndbeyond.com/characters/117915508).  Bailey is actually quite well-informed, stating that "he's been doing a lot of reading since the adventuring business has slowed down after [Mathel](https://diablo.fandom.com/wiki/Malthael)'s destruction."

Bailey states that Nahiri wasn't in Westmarch long and seemed to have a lot on her mind, somewhat distressed, almost worried, in between bouts of threats and a flaring temper.  Bailey also notes that there have been rumors of someone trying to rebuild [The Black Soulstone](https://diablo.fandom.com/wiki/Black_Soulstone) -- "but of course, that would take bending or shaping inter-planar power..."

As for how Nahiri and Sorin were brought to Innistrad, he's short on information, but speculates that "plane walking" is typically an extra-ordinary feat; even with rings like Digoria's "Fey Ring of Atlantis" has to meet special conditions to tear riffs in space and time.  However, there are three legendary sages, [Dec-Kard-and-Cain](https://diablo.fandom.com/wiki/Deckard_Cain), in the town or Tristram that would likely know much more about these things.

As the party exits the inn, they notice that two stars in the night sky seem to be getting bright... no, closer!  With an eruption of radiant power, two [Serra Angels](https://i.pinimg.com/originals/3a/00/9c/3a009c3f1d14a23986d2e188c35a44af.png) descend upon the party and stand in wait -- appearing to want to talk more than to fight.  Wylde Zakk stumbles up behind the party and Lilly quips -- "what the hell do you think you're doing?"

Still very intoxicated, and in no condition for playing an instrument, Zakk belches and asserts that the party may be in need of inspiration along the way and that he could provide "head-banging heavy licks" in times of need.  Lilly is not thrilled by this proposal, Sorin is indifferent, but Digoria makes the case who they may need to betray -- err... convince -- when dealing with potentially inter-planar threats.  So, Zakk officially joins the party!

The angels announce that they have come to deliver a message.  One of the angels holds a scroll in an outreached hand, which Digs spryly creeps towards, then cautiously snatches away from the angel.  Unrolling the scroll, Digoria reads aloud:

> "You are hearby put on notice that your actions are being monitored by the [Angiris Council](https://diablo.fandom.com/wiki/Angiris_Council).  Due to certain technologies from the [Phrexian race](https://mtg.fandom.com/wiki/Phyrexia) having gone missing from The Council's vault on [Ravinca](https://mtg.fandom.com/wiki/Ravnica), notice from the Council of Dexa that one Liliana Vess, who previously possessed the Chain Veil and recently escaped Dexa's prison is a prime suspect in this account."
> 
>  [Niv-Mizzet](https://mtg.fandom.com/wiki/Niv-Mizzet) ([CR27](https://dnd-5e.fandom.com/wiki/Niv-Mizzet))

Digs and Lilly briefly consider fighting the angels and running, which would be a simple task for the party, but after a brief history check from Sorin, he warns that "pissing off a god-level dragon who appears able to communicate across realms might not be a great idea right now."

One of the angels inquires if the party has any knowledge of the whereabouts of The Chain Veil; thanks to Digoria's aptitude at lying, she's able to talk them in circles until they lose interest.  Angels aren't the type to be bargained with, but nevertheless Sorin comments that the two of them alone can't possibly hold the party back.

No sooner does Sorin finish speaking than both angels look to the sky.  The party's gaze follows.  More than 20 (3d10) stars start to brighten in the same fashion as the first two angels arrived in.  The second angel states that "they skies will light up" if the order to remain in place until either "The Vault Items" are returned to Niv, or Liliana can be exonerated.

Sorin jeers Lilly by asking the angels if there's a bounty for Lilly so he "can just turn her in?"  Lilly scoffs, Digs chuckles, and the angles dryly reply that "justice cannot be purchased with money."  Lilly uses one of the charges of her Scroll of Naughty and Nice; however it reveals that she is still "mostly naughty."

Seizing the opportunity, Digoria spots an opportunity to capitalize on words.  She remarks that "since justice can't be bought with money, can it be bought with blood?  ... Since angels tend to be the vengeful smiting types."  On that note, Zakk puts his arm around Lilly and gives her a nuggie, saying to the angels -- still drunkenly slurring:

> *"Shouldn't youz guyz have some kind of divine wisdom or sumpin'? Like, can't you just tell us where this chain-veil-thing-ah-mah-jig is or who has it so we can kill the.. otherwise obtain it from them, bring it back to you, and baddah-bing-baddah-boom Lilly here is free to go?*

The two angels contemplate the proposition and begin to lose themselves in thought and prayer to prepare a response.  While the party is growing bored and frustrated with the delayed response, Bailey runs out from the inn, shouting that he forgot to tell them something.

Bailey says that Nahiri was accompanied by a "Wizard of Ill Repute."  Who didn't say much, didn't announce his name, but he had "profoundly foul aura about him that was as evil as it was potent; almost like it reflected a calmness beyond life and death."

"Kulle..." Digoria says under her breath, but loud enough to catch Lilly's attention.  Liliana snaps, "What did you say?!"  After an awkward few moments of Liliana explaining that Zulton Kulle is her brother (formerly known as Josu) and the two have a long standing feud because of their differences in "dark magic" -- necromancy being the control of death, and lich-dom succumbing to it -- Sorin takes the opportunity to comment on Lilian's family being as dysfunctional as it is vain.

The angels return from deliberation and announce that they have reached a decision.  Since landing in the world of Sanctuary, the angles have communicated with the Mage Council of Dexa -- of which Liliana was formerly a member before being imprisoned -- and decided since there are 8 schools of magic, each represented in the council, they party will have 8 day to return locate and return the Chain Veil to Westmarch.

However, Liliana is not permitted to leave and will remain under the watch of the angels overhead.  The party negotiates that in Liliana's place in the party, one of the Serra Angels will join the party in their quest.  After another lengthy mediation, the angels state that this is a good idea since their "point of contact" within the council thinks it's a good idea for "someone to watch out for Digoria to not cause too much trouble -- i.e. collateral damage."

Since only the accompanying angel can fly, Bailey offers to provide the party with a number of [riding horses](https://roll20.net/compendium/dnd5e/Monsters:Riding%20Horse?expansion=0#content) in exchange for a date with Lilly -- winking obscurely and making puckering gestures towards Lilly while propositioning the party.  Lilly groans with begrudging disgust because she knows the offer cannot be declined.  Never being bashful to tease their companions, both Sorin and Digs giddily accept Bailey's offer. 

---

### Player Knowledge:

> *The Mage Council Member advocating for Liliana's reconciliation and wanting Digoria watched over (and kept safe) is Alodel, Digoria's former compatriot.*
