---
title: "World Marshals, Act 1"
date: 2016-06-16
description: "Most of Earth's heroes are tied up with the fall out of the Civil War, however, Raiden suspects there is something much bigger brewing while everyone is distracted and sets out to find and stop it."
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 02 - World Marshals
Session: 01
```

### Event:  World Marshals

*In the fall out of the Civil War several events took place as a world-scale practice run for the villains who would later spearhead the events  of Siege and Fear Itself.  The Metal Gear Solid crossover, Raiden, sets out to stop them; making friends and foes along the way.*

#### Act I:

*Most of Earth's heroes are tied up with the fall out of the Civil War, however, Raiden suspects there is something much bigger brewing 
while everyone is distracted and sets out to find and stop it.*

**Doom Pool: 2d6**

* Discover a scheming plan (1XP)
* Defeat *The Assassin* (3XP)
* Stop *The Stranger* (10XP)

-----

The Stranger Raiden met at the end of the Training Day event leads him to a dark warehouse.  He tells Raiden that *"Stark is no friend of mine either, but I have an enemy I'd like to have your assistance against."*  He offers Raiden a mechanical / cybernetic upgrade that enhances his strength.

Raiden notices that The Stranger will frequently have ticks and make comments as if he's talking to another person inside his head, but also elsewhere in the room.

Raiden starts to walk away, stating he's *done fighting other people's battles*; but The Stranger says; "*The real question is, what do you have against our common enemies?"*

Raiden's interest is peaked, so he stays.  The Stranger says he *"has spies all over*" and asks one to *"come out of hiding."*  Raiden's cybernetic sense cue him off to something behind him.  He jumps out of the way as Carnage (BR12) emerges from the shadows.

Carnage tries to intimidate Raiden, but he is unafraid; but anxious and attacks the creature.  He lands a massive blow with his sword.  Carnage retaliates with an equally heavy blow, catching Raiden in his tentacles.

Just then, Spider-Man (BR88) breaks through the window.  Carnage turns his attention to him and lands a glancing blow.  Raiden swings his sword to free himself, but Carnage traps the sword in another tentacle.  Next, Spider-Man fires a web ball that barely misses Carnage.  However, Raiden is able to regain control of his sword and slash himself free.

Suddenly, the roof of the building explodes and is engulfed in flames as the [Green Goblin](http://marvelheroicrp.wikia.com/wiki/Green_Goblin_(Norman_Osborn,_Watcher_Datafile)) flies over head.  Raiden notices The Stranger is not in the room and Spider-Man says that he had tracked the Green Goblin here.

Carnage attacks Raiden for slashing him.  The hit stresses Raiden out, but a burning rafter falls on Carnage.  Spider-Man rifles through the boxes in the room and finds an electrolyte pack to recharge Raiden.  Raiden is then able to attack and subdue Carnage with a heavy blow.

With Carnage incapacitated, Raiden and Spider-Man quickly look around the burning building.  Spider-Man notices a back door, but a pile of burning debris falls in front of it.  The two are easily able to clear the debris before Raiden slashes through the door itself.

The door leads to a testing lab (already engulfed in flames) that appears to be a breeding ground for *"goblin soldiers"*, or cyborgs of some kind.  However, the building quickly begins to cave in and the two flea the scene.