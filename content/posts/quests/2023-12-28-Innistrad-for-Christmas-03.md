---
title: "Innistrad for Christmas, Part III"
description: "After defeating the Krampus, Digoria secures a hexblade pact, and Santa Claus rewards the party with magical gifts. Sorin’s past catches up to him when Nahiri’s trap is revealed."
date: 2023-12-28
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 01 - Innistrad for Christmas
Session: 03
Author: Austin
```

> This session is "fan-fiction" authored; i.e. plot development storytelling.  Random rolls, skill checks, and ChatGPT as needed.

After defeating the Krampus and defending the Basillica, the presiding Drow Matriarch agrees that Krampus, now ironically restrained within the "magic sack" of a lesser Krampus, will make a suitable sacrifice to complete Digoria's transition from sorcerer to warlock.  The hexblade variety of warlocks are particularly notorious for require a sacrificial pact to enable one's mystical powers.

To celebrate their victory and obtain some rest and relaxation, the party returns to the [Wolves' Den Inn](https://diablo.fandom.com/wiki/Wolf_City_Tavern) to chat with [Bartender Bailey](https://diablo.fandom.com/wiki/Bartender_Bailey); who apparently still has a crush on Liliana -- though relentless in his pursuits he's unable to charm her.

The bards at the inner are a helter-skelter group with lack-luster talent, though their renditions of "[Heavy Metal Christmas](https://www.youtube.com/playlist?list=OLAK5uy_lrnWIg6EhCtvUMEoi5FqLnGNf69jwWNrk)" ballads are seasonally appreciated.  Sorin notices on particularly famous bard, though he isn't playing music, he's sullen and incredibly intoxicated at the bar.

Being an enamored fan of the [Heavy Metal Bard School](https://www.dndbeyond.com/forums/dungeons-dragons-discussion/homebrew-house-rules/2066-archetype-college-of-metal) in general, Sorin feels the need to approach the legendary [Wylde Zakk](https://i.pinimg.com/originals/18/c2/8d/18c28d0bb4a367c3908aadb439b6c06c.jpg)!  Zakk reports that he's returned to Westmarch to lament the hiatus of his band, Sabbath of Black, that was previously touring around Sanctuary.

Just then, a cold chill bursts through the door, accompanied by the smell of cookies holly berries, and the sound of sleigh bells.  The patrons look to the door and see large, jolly character who announces himself as [Nicholas Claus](https://i.pinimg.com/originals/a8/63/45/a86345996483a14584f40bf32fc2ab2e.jpg).

Claus announces that he's been trying to spread "holiday cheer", but an unruly demonous impersonator has been flying around the skies wreaking havoc.  Claus says that, he's gathered that the impersonator has already been defeated here in Westmarch and he's looking to reward the adventurers responsible.

Despite his bold and robust physical stature, Claus is immensely charismatic and his bellowing "ho-ho-ho" laughter bring a contagious optimism to an otherwise gloomy city.  Never one to squander an opportunity to obtain mystical items and worry about consequences and curses later, Digoria jumps up and announces that she, her mother (Liliana), and "Emo Knight over there" (Sorin) are the responsible parties.

After an intimidating, magically driven stare-down, Claus determines Digoria's truthfulness.  He snaps his fingers and a large red velvet bag appears in front of him.  Claus asks the party members to come forth and draw their reward (1d4 for rarity) from his magic bag-of-gifts.  Through a game of rock-paper-scissors, officiated by Bartender Bailey, it's decided that Lilly will draw her gift first, Digs will draw hers second, and Sorin last.

Liliana initially draws a [Bottomless Bag of Coal](https://www.tribality.com/2019/12/17/dd5e-christmas-themed-magic-items-to-gift-your-players/).   Claus lets out a bellowing laugh, and Lilly is jeered by Sorin and Digoria, however she's allowed to draw another gift.  Lilly's second gift is a [Scroll of Naughty and Nice](https://www.wallydm.com/dnd-holiday-magic-items-christmas/#item6) which possesses several charges of a spell intended to detect where someone is on the spectrum of good and evil.

On Digoria's turn, she draws a [Snow Globe](https://www.wallydm.com/dnd-holiday-magic-items-christmas/#item6) which can temporarily conceal the shaker of the globe inside of the miniature scene depicted inside the globe.  On Sorin's turn, he draws his lost sword, [The Vampiric King's Greatblade](https://www.dandwiki.com/wiki/Vampiric_King%27s_Greatblade_(5e_Equipment)), which prompts Claus to cryptically comment that Sorin "must have lost it when he was brought to this plane."

Sorin stumbles over his words, both curious and concerned how Claus possesses this information, but he's interrupted by Bailey who brings Sorin a wrapped gift, stating that was left at the inn by a woman named... Bailey stammers in thought as Sorin begins to unwrap the gift as there are no external markings to indicate who it's from.  Once unwrapped, Sorin see's that it's a statue of his home world, [Innistrad](https://mtg.fandom.com/wiki/Innistrad_(plane))!

As Bailey continues to guess incorrect names, out loud, Sorin looks closer at the statue and notices that it is beginning to move!  The statue of his homeworld begins to twist and contort, before eventually exploding in his hands at the exact moment Bailey says the giver's name; [Nahiri](https://mtg.fandom.com/wiki/Nahiri) ([CR20](https://www.dndbeyond.com/characters/117915508)).

Sorin's teeth flare with rage as he pins the harmless Bailey against the wall, picking him up by his throat and demanding to know Nahiri's whereabouts.  Claus intervenes by giving a loud whistle summoning two [reindeer](https://www.d20pfsrd.com/bestiary/monster-listings/animals/reindeer/) who blindside Sorin and trample him, interrupting the attack on Bailey.  Santa encourages the party to refrain any mischief on this particular evening and save the adventuring and avenging for another time.

Before anyone can asky any further questions, the sound of sleigh bells fills their, a cold snowy wind whisks into the room from seemingly nowhere; and with a white-out in effect, Claus bellows another jolly laugh, and disappears.

Lilly approaches the confused Sorin and smugly mocks him, knowing full well the complicated history between Nahiri and Sorin.  Sorin clinches his fists and snarls his teeth, but restrains himself, knowing that his realm has left him weakened and whether he likes it or not, continuing to ally with Liliana seems to be his best way home.

---

Noteworthy Items and Add-Ons:

* Liliana
	* [Bottomless Bag of Coal](https://www.tribality.com/2019/12/17/dd5e-christmas-themed-magic-items-to-gift-your-players/)
	* [Scroll of Naughty and Nice](https://www.wallydm.com/dnd-holiday-magic-items-christmas/#item6), 12 charges remaining
* Sorin
	* [Vampiric King's Greatblade](https://www.dandwiki.com/wiki/Vampiric_King%27s_Greatblade_(5e_Equipment))