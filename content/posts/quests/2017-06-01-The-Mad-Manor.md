---
title:  "The Mad Manor"
date: 2017-06-01
description: "Dawn Treaders Campaign, Quest 2"
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons and Dragons 5e
Campaign: Dawn Treaders
Quest: 02 - The Mad Manor
Session: 01
DM: Austin
```

A young fighter adventurer, named Cas finds himself outside [The Mad Manor of Astabar](http://watermark.rpgnow.com/pdf_previews/138937-sample.pdf).  He wakes not quite sure of where he's at and feels amnesiac.  However, recalls having ventured from the nearby town to investigate the manor.

His memory clears a bit and he recalls that the mansion was once owned by a powerful sorcerer who allegedly died trying to harness some sort of alchemical power.  Many adventurers have dies trying to cease the spirits that haunt the mansion, including one poltergeist and the sorcerer's imp familiar.

Navigating a volley of traps and trickery, Cas is able to discern that the dinner table in the dining room of the mansion had eight skeletons (each without their head) sitting at the table.  He finds many creatures in the mansion, mostly giant beasts, and is able to convince the traveler's poltergeist to give some insight as to what to do with the skulls he's been finding that appear to have symbols representing the stages of the moon carved on their forehead.

Cas is able to defeat the imp familiar and, through a series of puzzles, finds that all the rooms in the manor have been unlocked, save one in the attic.  Before continuing, he decides to take a much needed rest.  Cas wakes to the sound of his fellow adventurer, Link, coming in the front door of the manor and calling for his friend.

A few more giant beast creatures are killed en route to their meeting and Cas relays the information he's gathered to Link.  Several more puzzles need to be solved, most notably a chess table requiring a single move to win -- lest any other move fills the room with poison gas!  The pair perseveres and prevails!  They gather the skulls and place them on the heads of the dinner guest skeletons.  Upon doing so in the correct order, a secret compartment under the table opens.  It contains a key!

Immediately the two take the key to the final room in the attic.  They don't seem to notice that the poltergeist is no longer in the attic where she once resided.  They enter the room carefully, but are attacked by a [doppelganger](https://roll20.net/compendium/dnd5e/Doppelganger#content)!

Cas openly attacks the doppelganger who appears to resemble a painting of the sorcerer that was found in the mater bedroom.  Cas fights valiantly though the young Link is skittish and his nerves frequently causes his loosed arrows to miss.

As Cas nearly dies in combat, the ghost of the former adventurer attacks and distracts the sorcerers doppelganger!  She is able to buy enough time for Link to focus and make a precise shot!  The doppelganger lies slain!  The ghost confirms that that "thing" is what became of the sorcerer (named Astabar) who owned the manor upon his mistakes and failures dabbling with alchemy.  She graciously thanks them for freeing her spirit and bringing rest to the manor.

Cas and Link approach a large chest which, to their surprise, is found to be unlocked.  Inside it the find a large sum of gold as well as a small box. The try several keys that were found in the mansion, but the box appears to not even have a key hole.  Perhaps, they think, it is sealed by magic.  Surely, something like that is of value.  The two adventurers congratulate themselves and descend from the manor's estate back to town to celebrate their accomplishments!

---

**Notes:**

* In this campaign, Astabar is the name of the wizard who owned the manor, not the name of the town.
* The manor mentioned in this story is not the manor later referenced in the [Black Label Society](/tags/black-label-society/) campaign; though it's possible that Astabar was a an unerling of Leoric or Maghda at some point.