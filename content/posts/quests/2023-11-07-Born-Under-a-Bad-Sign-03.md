---
title: "Born Under a Bad Sign, Part III"
description: "Cas faces seduction and betrayal in his luxurious chamber, then triumphs in a brutal gladiator tournament, while Al uncovers secrets in the library to thwart a dark ritual."
date: 2023-11-07
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 15 - Born Under a Bad Sign
Session: 03
Author: Austin
```

> This "quest" is written as "fan fiction."  The original Dawn Treaders campaign never finished it's original final climactic quest which was intended to be titled "Born Under a Bad Sign" and conclude with the characters Cas, Alodel, and Digoria eventually going their separate ways after defeating the big-bad.  The personality and spirit of each character has been attempted to be preserved.

---

### Part 3 - The Coliseum and The Library:

Cas is escorted to his "Warrior's Chamber", a luxurious suite with abundant food, a huge bed, and a private bathing pool; which apparently includes four "women of universal beauty."

Unable to resist their temptation and his own desires, Cas is drawn into the pool.  He attempts to persuade the women towards the bed, but is charmed into entering the pool.  Cas finds the women's skin unusually slippery.

With his guard down, he is attacked, and ... tickled!  Combat ensues as Cas realizes he's been seduced by a party of [Rasulka](https://www.instagram.com/p/BpVrDYrD-AO/) -- water-bound feyborn temptresses.  Cas is unquestionably successful, though not without wounds.  No doubt, this will make the gladiator tournament more difficult.  He wonders if the food is also poisoned and decides to abstain from that, ironing his will and survival instincts.

After a short rest, guards approach Cas and inform him that "the games" will be beginning early and that he will only receive a "short rest" instead of a full night; "that is, if he's really the warrior he claims to be."

Cas finds himself entered into a random bracket of combatants, each round increasing in difficulty with only a short rest between.  There are four rounds.

Al is forced to watch by the council, though he feels uneasy, as if he should be doing research to "find a way out of this mess."  Al learns that the people of Dexa and Xiansai do not shun any type of magic or refute any source of power, which separates their "wizard-like" nature from that of other 'sourcerer-ish' counterparts.  However, summoning multi-dimensional beings, deities, etc. is strictly prohibited.

Cas handily survives the first round, only having to thrash a few simple monsters.  Moving to the semi-finals, Cas is not faced with more difficult monsters and traps and hazards have been added to the arena.

New-Whoo, who is sitting on Al's shoulder, is cooing frantically, seeming increasingly uneasy.  While Al does not have a telepathic bond with the creature, he seems to resonate well in personality with the owl.  Al, in an uneasy joking manner, asks the council; "Say... uh... I don't think we ever agreed on a prize or reward for my man Cas if he wins the tournament!"  One of the council members replies, "Oh, well it's simple.  He will be sacrificed in order to keep the winter strong and 'her' permanently sealed in the prison."  Al starts to stammer, "You... you mean Digoria?"  They reply, "no, not her, but 'her' who shall not be named."

Al tries to protest, that "these types of sacrifices rarely end well for the good of all."  However, the council remains steadfast in their decision.  Al states that he was under the assumption that The Dawn Treaders could leave freely if the tournament was won their representative, Cas.  The council states that Al himself, may go freely "a generous offer for someone consorting with such criminals", but that Digoria is one of the city's most wanted criminals, and the "blood of a celestial will make a fine sacrifice."

Al's perception is that something is amiss with the elders, but cannot get clear information from them.  Specifically his attention is drawn to the royal guards.  Al is able to persuade the elders to take an intermission before the final round of the battle tournament under the premise that it will build suspense for the championship round.

During the intermission, New-Whoo directs Al's attention towards a sign pointing towards the city library -- ostensibly the library in a millennia-old city of mages would have valuable information!  Within the library, Al overcomes the defending librarian, an [Azure Mind Sculptor](https://imgur.com/S6g0PqC), who is then cryptically helpful.  The librarian reports that she is "not fond of the cold", and finds Al's mastery of magic "endearing."  Al blushes and "feels a tingle between his knees."

Walking through the library a [painting of a ship on rugged seas](https://narnia.fandom.com/wiki/Ship_Painting) catches Al's attention.  The painting appears enchanted and appears as some sort of "motion picture."  The ship looks like the Entropy Tide, but the crew appear to be the Dexa council members, including Corros.  Al recognizes all but one character who appears to be the .  Al is able to pass an intelligence check which allows him to interpret and continue watching the painting.

The story of the council unfolds and Al gains the valuable information that:

* The city council cannot hold a sacrifice without a vote.
* The city council cannot vote without all schools of magic represented.
* City history about "[Liliana's Revolt](/posts/homebrew-lore-liliana-revolt/)"
* Lore of different realms indicates that the Jadis the Dawn Treaders are trying to prevent from coming to the world of Sanctuary is sometimes called by the name Auril.

While Al was at the library, Cas was doing his best to recover despite the worsening winter conditions; often remarking that "he could cut glass with these nipples."   Between bouts Cas is ambushed by the council's royal guards who reveal themselves as spies, [priestesses of Jadis](https://i.pinimg.com/originals/10/ec/f5/10ecf5683024de5af2db15919cdaf786.jpg) who have been hypnotizing the council members to ignore the worsening weather and coming of Jadis. 

Cas is able to defeat one of the priestesses, making sideways comments about their "attractiveness being wasted on such a cold hard bitch."  Cas is able to kill one of the priestesses, but ultimately falls to the second and is subsequently tortured before his final and [most difficult bout](https://i.pinimg.com/originals/81/5e/e3/815ee33d30714c908348008310aee00b.png).

As the fabric between realms weakens, Cas is empowered by the influence of his TWC compatriots and defies all odds by winning the coliseum tournament!  As the audience roars and cheers, the council rises to initiate the sacrificial ritual.  Al bursts into the room, throwing the library materials he found at them and confronting the council with their own legislation; that a sacrifice cannot commence without a vote and a vote cannot be held without a representative from each school of magic -- currently necromancy and abjuration are missing.

All parties leave the coliseum for the Great Hall to deliberate in trial.  Cleverly, Al uses New-Whoo to send a note to Cas saying that he (Al) will try to stall the council in deliberation while Cas should use the opportunity to try and free Digoria from the Mage Prison.  Again, Cas is not happy with this sort behavior, "because he's tired of always getting stuck in or cleaning up after Digoria's messes", however, he's continually impressed by Al "finally growing a pair."

Cas returns a note to Al via New-Whoo informing him of Jadis and her priestess's impersonation of the royal guards and spell over the city council.  Cas heads for the prison to break Digoria free.