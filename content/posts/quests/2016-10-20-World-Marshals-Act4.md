---
title: "World Marshals, Act 4"
date: 2016-10-20
description: "Raiden has formed a team, The Lightningbolts... They responded to a Sentinel attack, but after dealing with them and the deadly Crossbones, they encounter the global threat of Sebastian Shaw piloting a Metal Gear Excelsus!"
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 02 - World Marshals
Session: 04
```

### Event:  World Marshals

*In the fall out of the Civil War several events took place as a world-scale practice run for the villains who would later spearhead the events  of Siege and Fear Itself.  The Metal Gear Solid crossover, Raiden, sets out to stop them; making friends and foes along the way.*

#### Act IV:

*Raiden has formed a team, The Lightningbolts, with Storm, Fantomex, Deadpool, and The Punisher.  The responded to a Sentinel attack, but after dealing with them and the deadly Crossbones, they encounter the global threat of Sebastian Shaw piloting a Metal Gear Excelsus!  The battle was enduring, but the team seemed to be slipping Raiden asked a favor from an ally that would trigger his Jack the Ripper personality and improve the team's chances of success. Will it also trigger the Goblin Serum in his brain?*

**Doom Pool: 3d8**

* Find a cure for the Goblin Serum (1XP)
* Stop the Attack on the Helicarrier (3XP)
* Defeat Sebastian Shaw (4XP)

-----

#### Scene 1: Somewhere in a remote location...

There is bickering between another group of villains.  Norman Osborn, Mr. Sinister, and Carnage have gathered to discuss the nature of their plans with Sebastian Shaw.  Sinister believes the trio needs Shaw for his resources and the technology he has acquired from black market Stark Tech.  Osborn objects, stating that his intellect is sufficient and that the present three can cut Shaw out of their plans since he's decided to "just go blow shit up a head of schedule."

Sinister pauses and recalls his on-and-off feud with the Hellfire Club, which of course includes Shaw.  Carnage is willing to side with whoever will let him cause the most, well, carnage.  Norman then points out that while The Lightningbolts are fighting Shaw, there's no one defending the rogue helicarrier that  Nick Fury is riding around on -- that is, the base for the Lightningbolts.

"Oh, and one more thing Mr. Sinister and Mr. Cassidy", Norman says, "I have a couple surprises for both of you.  As well as a couple friends."  Norman pulls a veil off a table and reveals two goblin gliders for his comrades.  While they marvel and their new toys, Norman pushes a button which activates two Spider Slayers.  "These two creations will be joining us.  I hope you don't mind."

*Que menacing laughter.*

---

#### Scene 2:  Back on the battlefield...

Raiden has entered his Ripper personality and begins approaching and taunting [Sebastian Shaw](http://marvelheroicrp.wikia.com/wiki/Black_King_(Sebastian_Shaw,_Watcher_Datafile)) who is piloting the Metal Gear Excelsus.  The Excelsus swings one of it's massive foreblades at Raiden who catches it and stops it in place!  Raiden begins to grunt and strain as if he is trying to lift the entire Metal Gear.

Meanwhile The Punisher has the idea for Storm to help him *"super charge"* Raiden's rail gun.  She agrees and she and Frank ready the rail gun.  Suddenly,there are explosive electrical sounds as Riaden, in a massive feat of enraged strength, rips the entire blade arm off of Excelsus!  With a weakness in the machine's armor exposed, Punisher fires the Storm-charged rail gun and does notable damage.  Deadpool, then acrobatically swings his way up the blade arm (which Raiden is still holding) so that he can jump into the hole blown in Excelsus.  There he begins slashing away Excelsus's internals.

Punisher tells Storm to ready another shot "in case... they have to turn it on Raiden."  She is somewhat hesistant, but turns to see Raiden drop the blade arm but begins making efforts to pick it back up.  With an engraged yell he starts to succeed.  Storm again agrees with Frank.  However, before the rail gun is fully charged, Raiden starts to swing the the arm at the exploded weakness in Excelsus.  When it hits, parts start to explode and Storm flies up help Deadpool escape the explosive mess.

Shaw begins cursing from the cockpit about the weapons platform and locomotion devices shutting down.  Raiden yells out *"The Ripper's comin for ya!"* and begins running up the arm (no leaning from the ground on one end and crashed into Excelsus on the other).  Storm swoops up and grabs Deadbpools hand to fly him away, to which he makes a lude comment about her *"firm grip."*  While flying away, Deadpool throws Raiden's sword to him as Riaden summersaults through an explosion to the top of Excelsus.  With the machine all but entirely shut down, Shaw states *"If you want something done right, you gotta get your own hands dirty."*

On the ground, Storm is alerted on her X-communicator.  It is Nick Fury who says that E.V.A. has gotten Fantomex back to their Helicarrier, but that the ship has detected an incoming threat; Green Goblin, Carnage, and Sinister all equiped with Goblin Gliders and accompanied by two spider slayers.  Storm assertively states that Deadpool and Punisher need to fly back with her and that Raiden, for better or worse, can handle himself with Shaw.  Deadpool points out, *"Wouldn't it be easier Storm, if you just waited for Shaw to come out, wisk him away in a whirlwind, then we drop Raiden off at the Helicarrier and hope he gets geeked out enough on [Goblin Serum](http://marvelheroicrp.wikia.com/wiki/Goblin_Formula) to waylay the genetic freaks?"*

*"Would that include pitting him against you Wade?"*, she asks.  *"No thanks!"*, Deadpool replies, and under his breath says, *"That's not as good of a story anyway."*  And so, Storm, The Punisher, and Deadpool fly off to help Fantomex and Nick Fury defend the Helicarrier.  *"By the way"*, Furry says over the communicator, *"... I've notified a 'contingency plan' for Raiden, they're standing by just in case."*

---

#### Scene 3:  Atop the collapsed Metal Gear Excelsus...

Raiden continues making mild slashes to the top of Metal Gear Excelsus while Shaw opens the heavily armored cockpit bay.  Shaw begins to rant:

> *"You still don't get it twerp!  War is a business and business is great right now.  Osborn is off the deep end thinks all these machines, all these mutant 
cyborgs, will actually stand up in an attack on Asg....  There are bigger players in motion than you can imagine Raiden."*

Raiden replies:

> *"Everything I ever said about my sword being used for justice, to protect the weak... garbage.  I learned from a young age that killing my enemies felt good.  And now, Jack is back!  I'm sure you've met a few 'Rippers' in your day shaw, but none like me.  This is who I am, I'm not pretending anything anymore."*

Sebastian gains an enthusiastic smile and says:

> *"That's the spirit boy!  All the people you think you ever saved, they went off and fought for causes they didn't believe in.  They died for causes they
didn't understand.  If everything goes according to plan, I'll be presient of this pathetic planet, by force or by politics -- it doesn't matter.  I keep my
eye on the prize and don't get distracted by grandiose meta-narratives.*

> War as a business is the perfect campaign to end commercialize, government sponsored
wars and bring about utter chaos and personal wars.  Look at your heroes in America.  Where did their ideals get them?  In a Civil War of their own and no one
cares about anything but winning."*

Shaw assumes sumo-type stance and is able to use stray cable protruding from Excelsus to electrocute and enhance his powers -- fortunately Raiden is able to gain a peripheral charge from the energy waves as well.  Even in his Ripper state Raiden is barely able to hold his own against Shaw, evading most strikes, but taking several heavy blows and initially takes a beating from Shaw.  Surveillance drones in the area captured the below footage.

Sebastion grabs and begins to squeeze Raiden's head, saying *"Lets see if we can get those goblin juices in your brain flowing faster."*  Sure enough, the serum is triggered by the physical stress.  Raiden is able to slap Shaw's hands free and lands a heavy punch, but it causes a massive kinetic explosion blowing apart parts of the Metal Gear, and landing the combatants on the ground below.

Raiden strikes Shaw with his sword, but the blow is scuffed off.  The Black King smirks and says,

> *"You can't hurt me Jack.  You may have god-like strength  now, but I've always had god-like durability.  We could be destined to do this forever you know. 
But you're barely human anymore, you've forgotten how to fight for something you believe in."*

Raiden replies, 

> *"Not forever Sebastian.  Only the strongest survive in nature... and in the wild... there are never two strongest."*

Raiden and Shaw trade blows for a while, but in his enhanced state, Raiden is able to evade most strikes, almost with ease!  Eventually, Raiden counters and lands a near fatal blow!  Shaw apparently has the upper-hand in tank-ablility, but he lacks tact and diversity that Raiden excels in and, now, on top of that his current *Goblin Rage*.

Jack yells with full rage, completely overtaken by the Goblin Serum further enhancing his Ripper State, and plunges his fist into Sebastian's chest.  Shaw make fading comments about Jack *"ensuring the 'status quo' will continue for a while longer, but at least he (Shaw) has left a 'worthy successor' in (Raiden)."*  This triggers a great deal of emotional stress for Raiden, and he begins to come down from his Goblin State.  However, this is not before he rips Shaw's heart from his chest and crushes it.  Shaw's dying comments further torment Raiden as Shaw's fading breath echoes, 

> *"Deep inside... we're kindred spirits, you... and I."*