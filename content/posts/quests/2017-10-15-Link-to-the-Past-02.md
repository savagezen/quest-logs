---
title: "Link to the Past, Part II"
date: 2017-10-15
description:  "Link, Cas, and Digoria awaken The Great Deku Tree and uncover secrets of Link’s past. With two Spiritual Stones found, they face dangerous paths, fire, and tough truths."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 06 - Link to the Past
Session: 02
DM: Austin
```

After venturing through The Lost Wood and into the Kokiri Village, Link and company continue their search for The Great Deku Tree, the three Spiritual Stones, and any insight they can manage as to Link's past -- given that much mystery surrounds it.

One Spiritual Stone had been found in the wreckage of the town shop and another turned out to be in the pack of one of the hobgoblin warlords guarding the shop.  With two of three stones already found Cas, Digoria, and Link continued to wander through the village paths.

They came to fork on the far end of the village, the far left path appearing to be sealed by wooden door of vine wrappings, tightly sealed.  The middle path revealed a narrow and daunting path weaving through intense walls of fire at least 20 feet high and several feet thick.  The right path, seemed less dangerous as it only offered a slightly haunting chill.

The group continued down the path and, behold, the remnant stump of The Great Deku Tree!  Carefully inspecting the tree stump, they're fairly certain it was once The Great Deku Tree, but are not sure what to do with this finding.  Cas notices that some of the ground is soft beneath his feet and pokes around with his sword until it hits something hard, like a rock.  A bit of digging reveals the third spiritual stone!

Link recalls that he has a special musical instrument in his pack and remembers it having some unique relationship to the spiritual stones.  He places the stones on top of the stump and begins to play The Ocarina of Time.  Mystically the stones begin to sink into The Great Deku Tree and it gasps with life!

Now awakened, the treant greets Link and tells him that he is not one of the Kokiri, but that his mother brought him to the village for protection.  Apparently there was a great deal of discord between the families of Link's mother and father, particularly due to their alignment and also due to his father's half-human blood.  The Deku Tree confirms what Navi had said about a powerful vampire sorcerer coming for The Master Sword in the forest temple and that Saria and The Deku Tree needed to sacrifice their energies to seal him inside.

The Great Deku Tree tells Link that he must seek help for the forest as his purpose is indeed to protect it.  Then, The Great Deku Tree fades and flutters, returning to an ordinary tree stump.

Link is feeling energized while Digoria is looking more suspicious than usual.  Cas, apparently more weary than he led on has fallen asleep sitting against the tree stump.  Digoria and Link decide to do a bit more exploring, perhaps one of the other paths before waking Cas.

They come to the burning path and Link self-righteously, though foolhardy, asserts that this must be the path to the forest temple where *"it is his duty to draw the master sword and slay the vampire"*.  Digoria promptly informs him of his stupidity and states that he wouldn't stand a chance against a spell-casting vampire even if he had The Master Sword.

Only slightly swayed, Link has his heart set on trying to make it through the blazing walls of fire.  Digoria, giving in (as she often does) to a mischievous nature, antagonizes Link's naiveté and takes advantage of his hubris:  suggesting that he try to belly slide on his shield through the blaze.

Link thinks it's a great idea!  He readies himself and takes fully charge.  However, he makes it about 15 feet into the 50+ fleet of blazing forest before coming to standstill and realizing that now he will likely die before he can make it out of the fire.  After much pleading with Digoria, she is able to delicately vault into the fire and pull Link out, though the she (concealing the fact) is badly wounded in the process as well.

Dismayed, Link asks why she let him do that?  She says that it was because she need to tell him that she was once a "trusted acquaintance" of the mage that created the the fire and laid waste to Link's beloved forest home.  Although enraged, he is too frail and weak to raise a fuss or dare to fight back.  He sleeps, and Digoria too takes rest.  Tomorrow, they rejoin and take the last unadventured path.