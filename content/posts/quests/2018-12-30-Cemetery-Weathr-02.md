---
title: "Cemetery Weather, Part II"
description: "The party confronts Karasu and Valik in a circus tent, battling dire wolves, and ultimately defeating their foes while planning a strategic retreat into the mountains."
date: 2018-12-30
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 14 - Cemetery Weather
Session: 02
DM: Austin
```

### I'm looking out for your nuts Cas... someone's got to!

The party exchanged some words with , and Digoria jumps onto a sarcophagus and threatens Karasu with an interplanar demigod if he tried to leave. While Karasu paused at this, he quickly determined it was a bluff, and turned to leave, but not before stabbing Cas with a dagger and then magically convincing him to reveal the protective wards over Entsteig. Luckily, Cas does *not* have perfect recall, but he was still able to remember that there were glyphs of warding and some hallowed ground in a few places.

As Karasu leaves, Alodel attacks Phage, and Valik blows his hunting horn, as two dire wolves tear through the tent siding and attack Cas. This continues for a few more rounds, when suddenly Karasu appears behind Cas, biting him on the shoulder. Cas was able to kill one dire wolf quickly. Shortly after that, Alodel was able to kill Phage and one of the dire wolves, and then turned to help Cas and Digoria with the remaining two foes.

Cas managed to obliterate Karasu with a few particularly well-placed strikes with Dawnbringer and the Heavenly Mace he borrowed from Alodel. With his last breath before he turned to mist, Karasu mocked the party, saying "you don't even know how to kill me...". With their attention turned solely to Valik, the party quickly dispatched him and gathered their thoughts.

Before long, however, they heard a return hunting call, signaling more hunters coming, likely for Digoria. The party quickly looted Valik and Phage for any gold or useful items, and then made some rough plans to retreat into the mountains long enough for a brief respite, before heading on to Dexa to deal with the bounty for capturing Digoria.