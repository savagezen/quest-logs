---
title: "Born Under a Bad Sign, Part IV"
description: "Cas navigates Azkaban’s time-warping traps to rescue Digoria, revealing dark secrets. Al brokers a desperate deal with Dexa’s council as a deadly army and dragon attack."
date: 2023-11-08
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 15 - Born Under a Bad Sign
Session: 04
Author: Austin
```

> This "quest" is written as "fan fiction."  The original Dawn Treaders campaign never finished it's original final climactic quest which was intended to be titled "Born Under a Bad Sign" and conclude with the characters Cas, Alodel, and Digoria eventually going their separate ways after defeating the big-bad.  The personality and spirit of each character has been attempted to be preserved.

---

### Part 4 - The Great Hall and Azkaban:

Cas approaches the the prison and is surprised to find that there are no guards outside.  He then remembers that this is a Mage Prison and is likely to have many traps and magical protections and wards rather than physical deterrents.  Upon careful investigation he is able to unlock a sign that molds the stone above the doorway and reads: "Welcome to Azkaban: Abandon All Hope Ye Who Enter."

Cas snubs the sign and opens the front door which is casually unlocked.  Inside there is a room with dozens of clocks, everywhere.  There is a strange, unbothered creature sitting at a desk in the middle of the room who identifies himself as the [Book Keeper](https://i.pinimg.com/originals/28/78/62/2878629580bdeae5f01c520385b63199.jpg).

The Book Keeper looks up from his desk and raises a bony finger towards Cas who immediately feels as though he is "stuck in time."  Cas looks down at his hands, he's growing weaker, barely able to hold his swords, watching his body age before his very eyes.

The Book Keeper spins his time wheel obsessively in one hand while remarking what a "rare abomination to existence" Cas is, that "across all reality he is something quite rare."  The Book Keeper makes it clear that he is not looking for a fight, that he stops wanderers from entering the prison because so few do so willingly.

As The Book Keeper returns to his desk, he pulls a small hour glass from his pocket and places it on his desk in the "start" position.  The Book Keeper points to ladder leading up to a doorway on the ceiling, as strange warp in reality, and informs Cas that he will have 1 hour to exit Azkaban, or he will subsequently age 1,000 years over the following hour.  With that said, Cas can now move freely and before ascending the ladder, makes a sly comment to The Book Keeper that he "hopes they can meet again on better terms, winking and flashing his swords at the time keeper."

As Cas leave The Book Keeper's office, he finds himself in a random room.  He returns through the door from which he came, and rather than finding The Book Keeper's office he finds and adamantine vault with random adventuring supplies.  This process repeats for a while and Cas starts to lose his temper, smashing some of the supplies only to reveal they have name tags.  Cas can't read the written language, but cleverly uses his lens of truth to find that the names are actually instructions that read "burn by breath."

It's then that Cas realizes he has just that very potion!  He proceeds to drink the breath of fire potion, burns the supplies, which melts the stone floor as well, revealing grated bars that have common prisoners in their cells.  Digoria isn't among them.  However, there is a narrow stairwell revealed as well which appears to lead to the second floor.  Cas presses on, knowing time is of the essence.

Meanwhile, the Council of Elders is deliberating with Al on what to do about Cas who is supposed to be sacrificed, but not being able to hold a sacrifice without a vote.  Al is getting frantic and jittery knowing that the remaining royal guard is a spy and needs confronted, but that he cannot do so so long as the entire council is hypnotized.

Al notices that the royal guard appears to be mumbling something under her breath, but Al can't make out the words or context, only that she appears to be concentrating rather intently.  Al recalls the librarian's reference to "the cold taking control over the city" meant much more than the weather!  At the time Al shouts out, "I fucking love fireballs!"  Everyone looks confused at his exclamation as he takes aim at a royal guard, then above her as to not injure the other council members, only break the guard's concentration over them.

The fire partially weakens the priestess who's true identity has been revealed, and Alodel is easily able to overtake her due to her susceptibility to fire.  The council members are initially confused by time and place, but are able to collect their senses and are able to restrain both Al and the (almost dead) priestess.  Not knowing who to believe, it is New-Whoo who speaks, telepathically, with the divination representative to update the council on recent events.

The priestess is then subdued and sent to the prison.  Alodel is freed from his bonds.  While Cas is off the hook and no longer needs to be sacrificed, Al will still need to convince the council to free Digoria.  They are not willing to do so lightly.

Cas continues to pursue Digoria within second floor of Azkaban where he navigates a multitude of puzzle doors and hallways patrolled by [harlequin demons](https://66.media.tumblr.com/81e104f876c6e41c2228b8b69645f0c2/tumblr_oys76ouMgf1vu5c8fo5_1280.jpg).  Again, he comes up empty, but finds yet another stairwell leading to the third floor.  In his haste he misses the sign that says "maximum security ahead."

Cas stops in his tracks, looking down a hallway that appears to go on forever.  Additionally, there is some sort of [eerie music playing](https://www.youtube.com/watch?v=JpUevK17OD0).  As Cas walks, somewhat mesmerized by the  music, a hallway to his left appears that didn't seem to be there before, but also seems to be the origin of the music.  Unable to shake his curiosity, Cas follows the music down the sheer and polished stone walls and floors.  Without noticing, the hallway's diameter shrinks progressively until Cas is stooped, then on hands and knees, then on his belly.

Cas gets himself stuck in the ever shrinking hole as he's able to get his head through to a room where "he's been expected" and is greeted by the warden of the jail, who identifies himself as [Oogie Boogie](https://78.media.tumblr.com/b4baeab840836175652d19a480ec9b16/tumblr_oys76ouMgf1vu5c8fo9_1280.jpg).  Oogie tells Cas that he "would normally look forward to a formidable engagement (referring to torture and interrogation), but that he has other engagements to attend to."

Cas is utilizes his vow of enmity to amplify his divine channeling, causing Oogie to be stricken with fear before he can leave the room.  In a startled clamor, Oogie knocks over a shelf, and clumsily trips a lever that loosens the stonewall trap's hold on Cas.  Oogie is able to escape, but Cas is eventually able to wrench himself free and begins to investigate the [interrogation room](https://grimoire.thebombzen.xyz/spells/zone-of-truth).

Cas finds the floor plan map of the prison and also learns that there is a "super cell" for "maximum security" inmates on the third floor.  The super cell consists of an adamantine body cast for the inmate placed in a 10' [anti-magic field](https://grimoire.thebombzen.xyz/spells/antimagic-field) which is then enclosed in 5' of [magic stone](https://grimoire.thebombzen.xyz/spells/stone-shape) and finally surrounded by a 15' zone of [darkness](https://grimoire.thebombzen.xyz/spells/silence) and [silence](https://grimoire.thebombzen.xyz/spells/silence).

Cas's divine sense is going bonkers, on one hand he feels drawn to investigate a fiendish odor in the direction of the super cell.  However, a murmur catches his ear from within one of the adamantine casks in the interrogation room.  Cas is eventually able to find the physical keys -- commenting on the poor execution of mages with physical restraints.  Cas finds Digs inside the cask.  Digoria, dazed and confused from Oogie's interrogation isn't phased by Cas's joke that "apparently Digs wasn't badass enough to be put in the super cell."

This is when Cas knows something's wrong.  Digoria's attention is fixated in the direction of the super cell, almost as if trans-vexed.  Cas and Digs decide it's best to leave the prison before getting further stuck, trapped, or attacked.  However, Cas notices that as they pass the hallway leading to the super cell, the profane odors emanating from the super cell are now are now surrounding Digs as well, though in a greatly diluted potency.

Digs and Cas use Oogie's map to escape the prison and head to the Great Hall to meet up with Alodel.  When they arrive, they find that Al has been busy arguing and haggling with the council.  Unsurprisingly, the council is now furious that Digs has been freed against their will.

Just then, there are screams of turmoil and destruction outside the Great Hall.  A [great army](https://narnia.fandom.com/wiki/Jadis%27_Army) of duergar, ghouls, and giants has descended upon Dexa from the icy mountains, lead by a white dragon.  Using the opportunity and the information Al obtained from the library, the party is able to negotiate with the council that since they cannot keep Jadis contained to her realm, maintain Azkaban's fortifications, and fight Jadis's army all at once; that Al, Cas, and Digs will go and fight Jadis.  If they are able to defeat and conceal her, which would cause her army to dissipate from this realm, then Digs will be pardoned and the party can leave Dexa freely.

The council knows Al is right, that they cannot fight on this many fronts and begrudgingly agrees to Al's terms.  The party leaves for the mountains to battle The White Witch!

Al didn't now how right he was though.  While the council and townspeople were able to hold their ground fighting against Jadis's army, *most* of the prison's integrity was maintained.  Unknown to anyone in the city, the super cell had weakened significantly; significantly enough that it's inhabitant had now been freed, in part due to a once lost power source being brought near.
