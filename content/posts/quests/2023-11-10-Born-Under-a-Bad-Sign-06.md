---
title: "Born Under a Bad Sign, Part VI"
description: "The Dawn Treaders have parted ways.  May the dice be with you."
date: 2023-11-10
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 15 - Born Under a Bad Sign
Session: 06
Author: Austin
```

> This "quest" is written as "fan fiction."  The original Dawn Treaders campaign never finished it's original final climactic quest which was intended to be titled "Born Under a Bad Sign" and conclude with the characters Cas, Alodel, and Digoria eventually going their separate ways after defeating the big-bad.  The personality and spirit of each character has been attempted to be preserved.

---

### Part 6 - Aftermath:

**Epilogue:**

Cas and Alodel are able to return to refuge in their fortified citadel of Entsteig.  In a surprising show of "good faith" and with a request to not be pursued, Digoria -- by unknown means -- had the [Mempo of Twilight](/posts/homebrew-item-mempo-of-twilight/) returned to Cas and Al.  Cas would be able to to use the helmet to captain the [Entropy Tide](/posts/homebrew-item-entropy-tide/) and pursue his quest again Sitariel in the Feywild.

Alodel will come to replace Corros's seat (Abjuration representative) among the Council of Dexa's mages; though remotely as he also protects the Kokiri Forest surrounding Entsteig.

**Rewards:**

With the completion of the campaign and three years of adventuring, the player have earned their level-20 characters and are free to pursue *"epic level"* adventures.

---

### One-Shot Opportunities and Future Campaigns:

**One-Shot Quests:**

* Cas's gladiator battle bracket.
* Avengers and GOTG vs. Jadis's Winter Army
* Cas, Alodel, Digoria, and Liliana vs. [Jadis](https://www.dndbeyond.com/monsters/417094-kostchtchie)
* Cas and Alodel travel by ship from Xiansai to Entsteig, but the seas are no longer calm.

**Future Campaigns:**

* Low Level
  * The campaign left a loose end of an Abyssal portal engulfing the city of Caldeum.  The party fled as the city castle collapsed into the Abyss.
  * The [Dawn Treaders](/tags/dawn-treaders) campaign takes place after Diablo 3, during which there's a 50 year gap before the events of Diablo 4.  *See below regarding Digoria's future adventures.*
* Mid Level
  * The Council of Dexa is probably not happy about a max security prisoner (Liliana) being out and about, even if she is technically a council member, and even though Digoria has been vindicated for helping stop Jadis.  The city may hire other adventurers to investigate.
  * [Karasu (CR20)](https://www.dandwiki.com/wiki/Vampire_Lord_(5e_Creature)) is still at large and is a natural antagonist, since Alodel has already warded Entsteig against him.  A vampire lord would probably like to get his hands on a planeshifting ship!
* High Level
  * Cas and The Twilight Company vs. Sitariel
  * Alodel (or Cas, separate or together) vs. an [Ancient Leshen, CR20](https://i.pinimg.com/736x/92/2a/aa/922aaa4f8d5500af040ab1953468d193.jpg) (or a group of them) in they Feywild.
  * The events of [Dawn Treaders](/tags/dawn-treaders) are followed by the [Black Label Society](/tags/black-label-society) campaign; during which Digoria and Liliana pursue confrontation with Zoltun Kulle (CR20 lich).