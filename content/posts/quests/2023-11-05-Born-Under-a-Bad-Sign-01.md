---
title: "Born Under a Bad Sign, Part I"
description: "Outside Dexa, the crew plans their entry into the mage city. A familiar owl brings a letter from an Archdruid, warning of shifting cosmic tides. Secrets and tensions rise as danger looms."
date: 2023-11-05
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 15 - Born Under a Bad Sign
Session: 01
Author: Austin
```

> This "quest" is written as "fan fiction."  The original Dawn Treaders campaign never finished it's original final climactic quest which was intended to be titled "Born Under a Bad Sign" and conclude with the characters Cas, Alodel, and Digoria eventually going their separate ways after defeating the big-bad.  The personality and spirit of each character has been attempted to be preserved.

---

### Part 1 - Xiansai:

Outside the walls of Dexa, capital city of [Xiansai](https://diablo.fandom.com/wiki/Xiansai) the crew have successfully escaped the hunters and are planning how to enter the city.  A white owl, identical in name and personality to Digoria's deceased familiar (Whoo), lands on Digoria's shoulder with a letter from an Archdruid.  The letter states that the Archdruid saw the party's actions in defending the Kokiri Forest and as a reward he sent his familiar to accompany them.

There is another letter addressed to Cas, stating that the abyssal portal in Caldeum (chaotic evil) and the strengthening of Jadis in Xiansai (lawful evil) have caused a shift in the cosmic tides, weakening the veil between realms.  Thus, "the time is right for Cas to join TWC and pursue his quest to avenge his twins."

The team postulates that Jadis' growing power and rigid nature have frozen, or rather calmed, the seas around Xiansai, explaining the party's relatively safe travels throughout Xiansai and Sanctuary as a whole.

A shooting start catches Cas's eye and he feels the taunting presence of Sitariel headed west, towards Entsteig and his hatred for Sitariel and thirst for revenge begins to boil as he questions his loyalties to the Dawn Treaders versus TWC.

Aldoel is increasingly frustrated by the multiple incidents of entering a new town only to find Digoria's face on a wanted poster.  This time, however, the stakes appear higher as Digoria herself is more unnerved and uncharacteristically tense -- determined as she's been to get the crew to the city.

The team decides to purchase costumes and disguises from a shady merchant's cart outside the city.  The merchant seems mostly harmless and has a great knowledge of the city, though there's a hauntingly familiar presence about him that the team can't put their finger on.  The team's attempts to probe their memory and question the merchant can't seem to penetrate the merchant's coy deceptiveness.

The merchant does however warn the team that magic means of concealment (and other comparative engagement) are likely to be ineffective within the city -- it is, after all, a city state founded exclusively by mages over a millennia ago.  He also warns that magical combat within the city is "possible, but unwise" as the city contains a prison that is collectively held in place by all magic practitioners within the city.  That is, if the concentration is broke, all the prisoners would be freed -- "with dire consequences."

The merchant further warns that the collective energies of the city's mages "cannot indefinitely sustain both their prison and keeping Jadis from entering the realm", "a choice will have to be made" he says.

Digoria appears unusually shaken by this reference, Cas is having emotional diarrhea, so Alodel makes the executive decision that it's best to venture into the city and gain more information.

On a final note, before disappearing into the ether, the merchant also mentions that generations of mages all across Sanctuary have come to Xiansai to recruit prodigies, "though of course not all who are trained or recruited here are of noble intent."  The merchant drops the nugget that the collective magic of the city also creates a "[Mage Prison](https://harrypotter.fandom.com/wiki/Azkaban)."