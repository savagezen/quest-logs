---
title: "Superunknown, Part 3a"
description:  "In Lut Gholein, the party uncovers dark secrets tied to Tal Rasha's tomb. With a deadly erinyes after Cas, they must race against time to save him."
date: 2018-03-11
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 07 - Superunkonwn
Session: 03 - Part A
DM: Austin
```

The party has docked in Lut Gholein and learned from the castle guards, who are actually hired mercenaries, that many of the town's people and actual guards have been being killed or disappearing as of late. They sate that there are rumors that these and other strange things have been happening since there were rumors of someone snooping around Tal Rasha's tomb again.

The party also meets Fara, a paladin blacksmith and friend of Headrig, who offers Cas a ring of strength in exchange for his greater gauntlets. She states that these gauntlets will grant Headrig the strength he needs to finish working on splitting Cas's fume sword -- which is also given to her by Cas.

Digoria leaves the party, cryptically stating that she "has other business to attend to in town." As she walks away, Cas throws slanted comments about "not needing her help anyway" and that "he can take care of and do whatever he wants on his own" and other such assertions of independence. No doubt pampering his frail ego.

Fara informs Cas and Altorin that they need to go to Corros, if he is still alive, and retrieve some sort of magical blacksmithing tool and return it to herself or Headrig so that he can begin work on the sword. Cas and Altorin arrive in Entsteig to find the castle doors wide open and a person claiming to be and looking like Corros to be inside. However, this is later revealed to be the Oni after he successfully leads the party through a series of explosive traps!

Cas and Altorin discover the severed corpse of Corros, but are able to avenge him by killing of the Oni -- barely. Severely wounded, they find that Corros's private study has been opened by the Oni and that there is puzzle box on the table inside. After a bit of reflection, Cas is able to open the box using the signet ring he had taken from Corros deceased apprentice after the corpse was found when the party first visited Entsteig.

There is also a letter in the box that tells Altorin that if he's reading this, then Corros himself is dead and that Altorin should travel back to Lut Gholein and join the ranks of The Iron Wolves -- the mercenaries hired to protect the city. The note also states that there is a magical item affixed to the underside of the table, and there Cas finds a Horadric Maul. The box also contains one of the Fey Rings of Atlantis that Digoria has long since sought.

No sooner does Cas place the maul in his pack and turn to a corner to urinate, than do the windows and walls begin to shake as an erinyes erupts violently through the window. She spreads her wings and points her sword at Cas -- trousers still at his ankles -- demanding that he "stay... for someone has placed a high price on your head, and a higher one if you're alive."

Thanks to some quick thinking. Cas bargains to send the maul with Altorin and begrudgingly instructs him to send for help from Digoria. At the last second, Cas thinks it best to hold on to the ring so that Digoria has "added" (or any) incentive to come back for him. Altorin sets out, but the erinyes asserting that she will only allow Cas to live and / or remain in this realm of 11 days.

Alodel has spent much time studying at an undisclosed location -- at the hire of Digoria.  The rejoin and Alodel tells her that he's learned something of "soul stones" as well as the location of and how to enter Tal Rasha's tomb.  The two go to the tomb and there are met by a series of random doors each presenting either a reward or challenge.  After an encounter with a wraith -- concealed behind one of the doors -- the crew is lucky enough to open the door to Tal Rasha's Chamber.

There, however, they do not find Tal Rasha nor any of his remnants.  Instead they find a demilich stating his name is Zulton Kulle.  Kulle asserts his advocacy for mankind and, after some mudslinging with Digoria and her failed attempts to persuade and deceive him, he states that he seeks to be reunited with his body.  Specifically, a vile of his blood (presented to party) must be poured over his head, which currently rests in the crypt below the Tristram Cathedral.

In exchange for this favor Kulle offers that upon completing the task they will be rewarded with the "powerful ring of transport" Digoria came seeking in Tal Rasha's tomb.  The party accepts the offer.  No sooner have they returned to town than does Altorin come running and panting up to them.  He tells Digoria and Altorin what has happened to Cas and that he is in desperate need of their help.

Altorin also tells the party that Corros has died and his final wish was for Altorin to join the ranks of The Iron Wolves here in Lut Gholein.  Altorin also updates Digoria and Co. that he and Cas were able to find a magic maul and that he was able to give the maul and Cas's fume sword to Fara to take to Headrig so that they can begin working on forging it into a new weapon.

Unfortunately, Altorin has taken much too long to travel due to his wounded state.  He makes the party aware that the erinyes that holds Cas captive only allowed 11 days for him to be saved and that there is no way for the party to travel across the country in time.  Kulle, being a wizard of great magic, offers to mystically transport the party -- "if they owe him another favor."

Digoria, noting that she is already "in far greater debt to far more powerful persons" , obliges.  Aldoel accepts as well, though little to his knowing this causes great displeasure to the deity he worships.  Altorin also neigh forgets to mention that Corross bequeathed a "Fey Ring of Atlantis", though Cas thought it best to "hold on to as motivation for Digoria to come to his aid."