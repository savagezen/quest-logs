---
title: "Training Day, Act 1"
date: 2016-06-08
description: "The Prime Minister's daytime transport has been attacked on an empty street by sentinels and Tony Stark has sent Raiden to be cheif of security."
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 01 - Training Day
Session: 01
```

#### Event:  Training Day

Raiden 's first "solo gig."  Tony Stark has sent Raiden to be chief of security to transport a Prime Minister in the Middle East.  The Prime Minister has been pro-mutant and this has divided his country.  Several Private Military Companies have taken opposition to this.  Raiden needs to find out who is at the bottom of this and stop them if he can.*


#### Act 1:

*The Prime Minister's daytime tranport has been attacked on an empty street by Sentinels.*

**Doom Pool: 2d6**

* Make a philosphical statement (1XP)
* Kill a Sentinel (2XP)
* Find out who attacked the Prime Minister (10XP)

-----

Raiden is riding with the Prime Minister in a limo.  The limo is attacked by a [Sentinel](http://marvelheroicrp.wikia.com/wiki/Sentinel_Mark_VI_(Watcher_Datafile)) who blows up the front of the car.  Raiden is able to summersault out, taking the Prime Minister with him.  The Sentinel fires a retractable hand at Raiden which catches him because he was checking on the Prime Minister.

Raiden tries to break free from the grasp but cannot.  He tries to reach his sword but can't do that either.  Under duress he goes into his "Jack the Ripper" personality and is able to wrestle enough space to reach his sword and slash himself free.  The Sentinel tries to blast Raiden, but the attack is dodged and Raiden slashes the Sentinel.  Whilst the Sentinel tries to grab Raiden with its remaining hand, Raiden stabs the palm, then proceeds to decapitate
the Sentinel.

As Raiden is in a crazed mode, [War Machine](http://vignette3.wikia.nocookie.net/marvelheroicrp/images/1/1c/Wm-data-file1.jpg/revision/latest?cb=20120729204251) flies onto the scene and opens gatling gun fire on Raiden. But, Raiden is agile enough to avoid taking hits.  War Machine lands and says that he was sent by Iron Man as reinforcement and "wanted to be sure whose side Raiden was
on."  Raiden has calmed down, but isn't happy with Iron Man for sending a babysitter.

Before the two can argue, they're attacked by another Sentinel.  War Machine provides cover fire while Riaden rushes in and slashes a leg off the Sentinel.  The Sentinel tries to grab Raiden, but he cuts of the hand.  War Machine fires his repulsor beams and that finishes off the
Sentinel.

However, a menacing force glides by, sticking to the shadows.  War Machine's sensors fail to detect it.  While War Machine is separate from Raiden, the mysterious foe in the shadows attempts to control War Machine's mind and threaten him to leave, that he is far worse than any Sentinel, and that they can't even "what his master has in store for them."

Raiden notices the figure and runs back, yelling at War machine.  War Machine snaps out of the daze and opens fire on the shadows.  The figure appears wounded, but fleas into a dark building.  War Machine wants to pursue, but Raiden convinces him to take the Prime Minister somewhere safe since War Machine can fly.

Before leaving War Machine is able to run a video recording of what he saw and heard through a database and his computer identifies the villain as [Morbius](https://marvelplotpoints.files.wordpress.com/2014/12/morbiuswatcher.png). War Machine flies away with the Prime Minister while Raiden sets out to pursue Morbius into the building.