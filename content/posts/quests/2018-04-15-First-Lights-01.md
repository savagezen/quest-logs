---
title: "First Lights, Part I"
description: "Adventurers tackle the Squirrel Forest, face a giant squirrel, and uncover a surprising secret about Big Red's infamous drinks in this wild escapade!"
date: 2018-04-15
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 11 - First Lights
Session: 01
DM: Ryan
```

### Here Cas, eat this squirrel shit...

The party embarks on the Entropy Tide with Big Red and heads to what he calls the Squirrel forest. Once there, he gives just a few details on what they are hunting. With that, they set out into the woods, tracking their mysterious target. Alodel briefly tricks Cas into eating squirrel droppings, and then moments later the party is beset by one squirrel swarm after another. While they took a lot of time to defeat, the adventurers eventually dispatched them without suffering much damage, incinerating most, and freezing a few. Digoria decided to run off at that point deeper into the woods, so Cas and Alodel gave chase, with Big Red slowly following behind them. 

Eventually, Digoria happened upon the ginormous squirrel, which made its presence known by letting out a cacophonous roar upon detecting intruders in its forest. The party engaged the giant beast with specific instructions from Big Red to only distract the creature rather than kill it... a challenging venture indeed.

A 7th squirrel swarm appeared in the midst of the battle, but the group was able to restrain the giant beast with magic and skill before the 7th squirrel swarm was able to join the fray. Big Red yelled to the group that he had everything he needed, and the party ex-filtrated the area post haste, not wishing to engage any more nasty squirrels or wait for the giant squirrel to break free of its trappings. 

Once they were safely away, Big Red explained to the group that the giant squirrel's droppings were the secret ingredient to his famed drinks... Alodel made a mental note to stop drinking it upon hearing that. Afterward, the party prepared to journey home.