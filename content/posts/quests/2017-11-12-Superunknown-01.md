---
title: "Superunknown, Part 1"
date: 2017-11-12
description: "As the party navigates Tristram, they tackle odd jobs, confront ghasts, and duel a deceptive mage. Tensions rise as they uncover dark secrets and plan their next moves."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 07 - Superunknown
Session: 01
DM: Austin
```

As the party travels to [Tristram](http://diablo.wikia.com/wiki/Tristram) Cas indulges his lunk-head nature by "hunting" and rummaging around in the bushes.  Meanwhile Link inquisitively asks Digoria about what she is looking for.  To which she, in a shifty manner as always, responds that she needs help *"killing a few ghouls."*

The party enters Tristram and meets the resident blacksmith, Haedrig.  After some bartering with Cas, Haedrig agrees to send a messenger to a friend to try and gather the skill and materials needed to work on a legendary weapon.  But the cost of forging a new one would be $1000 gp -- far too much for the party.

Link, gun-ho and foolhardy, catches a rumor of a lich (or something of the sort) now haunting the town's cathedral and wants to charge in after it.  Fortunately for him and his naivete, a peasant stops him and provides enough pause to recollect the idiocy of charging in unprepared and at such a low level against a powerful foe such as lich (of any sort).

The group decides to embark on some odd jobs around the town to build up their bank account.  Digoria, elects to study with the town's wise men to uncover lore, myths, and magical knowledge.  Cas is able to barter with Haedrig and work in his shop as a way to offset the cost of any potential work he may do for Cas's weaponry in the future.  In the mean time, he is paid with an onyx gemstone worth 50 gp.

Link... poor, poor foolish Link.  Link finds a community forum posting from a nearby farmer asking for a stable hand.  A task that Link, being a ranger, finds himself most interested in.  Link finds the farmer and agrees to work.  However, as he goes to the stable for the night, he finds the horses dead and is attacked by two ghasts!

After fending off the ghasts, Link runs to the farmers help to tell him what has happened.  The farmer is strangely calm and appears to be preparing a meal, though it is in the wee hours of the morning.  The farmer leaves the kitchen where Link is sitting and goes to fetch something from his quarters upstairs.  Link gets curious and takes a look inside the refrigerator where he is most disgusted to find several amputated body parts and a jar of eyeballs!

Link runs upstairs to confront the farmer, but he is nowhere to be found!  Link recalls the farmer saying something about a wife, and goes to check in the master bedroom where there is nothing but a bed and a single wardrobe.  Link tries to open the wardrobe, but it is locked so he resorts to slashing at it with his sword.  To which he finds a great deal of blood begins to run out of the doors and through the splintered cracks he can see more body parts.

Link then runs downstairs, and out the back door, thinking must either stop or get awy from this murderous person.  However, he is caught off guard and knocked into the Farmer's cellar where apparently two more ghasts have been being kept as "pets."  The farmer tries to lock Link inside and says that he's beginning a profane celebration under the full moon and reveals his own cannibalistic nature.

Fortunately, just as link is about to die from paralysis and at the hands of the creatures and his grip loosens on his grip on the sword barely keeping the cellar from shutting and locking completely -- there are sounds of combat outside and screams from the farmer.  Much to Link's relief Cas has come to Link's rescue along with the town's guardsmen who arrest the farmer.

Back in town there is news from Corros that he can indeed provide Haedrig with the tools and additional magical abilities needed to work on a legendary weapons.  While Cas and Digoria are gathering supplies (potions and armor), Link appears to have disappeared again.  Too much ale perhaps...

Unsure of his whereabouts, and needing him before they set out to speak with Corros again, Digoria and Cas decide to do some quick adventuring north of town, past where the farmer's house was.

In a bizarre way, they are most pleased to test their new equipment when they find a coven of hags attempting to summon something from their cauldron.  The witches prove nothing too complicated for the two warriors, but their mettle is a bit more tested when a hezrou emerges from the cauldron!  However, the amphibious fiend is ill equipped to take on such combatants in a head-to-head brawl.  Cas and Digoria successfully return to Tristram to find Link sleeping off a mighty hangover at Haedrig's Inn.

Rumors from scouts and adventures passing the inn say that there are many armies (presumably evil) gathering in the east desert near Caldeum.  Link becomes angry that the party is not immediately going to fight the drow mage that set fire to his beloved forest.  During their arguments, Digoria lets it slip that Ellion (the drow mage) is now a Priest of Lolth -- a fact that Digoria knowing raises suspicion among her peers.  When Cas and Digoria wake, they find that Link is once again missing.  They decide that they can wait for his foolishness no longer, no search for him and hold his hand.  He is on his own, and the two depart westward.

Upon being welcomed back to Entsteig, Cas and Digoria persuade Corros that it's best for them to go to Ellion's (the drow mage turned priest of Lolth) Castle and try and find any information they can about his plans before going to attack the gathering armies -- *"besides, if they're all evil, why not let them whack each other?"*  Corros emphasizes the need for the party to "mature" first.  he states that he will engage in a *training sparring session* with Digoria, while Cas should ask for the same from Altroin, the captain of the guard.  Cas obliges the opportunity.

A vicious mage duel ensues between Digori and Corros, one with fire and lightning and all kinds of explosive debris.  Digoria becomes severely weakened and realizes she is outgunned, so she begins to hide behind bookshelves to better avoid Corros's blasts.  However, his ability to slow time makes it easy to find her.  Though, Digoria is able to somewhat trick Corros in to blasting open the doorway to his private study, where Digoria is further able to taunt him into chasing after her.  Her deceptive "surrender" serves her well as she is able to evade Corros and make a break for the building's exit -- casting a misty step scroll (found on the table in Corros' study), spraying a grease potion, and casting expeditious retreat.

Corros, furious at the trickery casts [Mordenkainen's Sword](https://thebombzen.com/grimoire/spells/mordenkainens-sword) and firebolt which both miss.  Digoria nearly makes to the exit, but falls just short of the doorway as she is hit by final firebolt from an invisible Corros.

Corros is not happy about being tricked by Digoira and orders an underling to follow her around and keep watch, stating that her attempts to deceive him were *"foolish, but admirable."*  He also describes her as "having the character of a rogues, which seems incongruent with her forced brutish fighting style."*  He offers to let her read his books on rogues to see if they spark some new inspiration for her.  He seems to know something of her past life, but does not explicitly say so.

Later that evening, Cas is most satisfied with his scrimmage against Altorin -- having bested him in only a few blows.  Though they may have been lucky, they were true and hit their mark and hit it well.  Digoria grows suspicious of Corros, stating that the purpose of her fight with him, wasn't to win, but to test his limits.

Cas agrees that it does seem odd that a powerful mage such as Corros wouldn't have killed off a drow mage who set fire to the neighboring forest long ago -- especially given that Corros is half elven and everyone knows that elves hate the drow.  They spend the night researching lore and local legends to try and find out what Corros might be afraid of, and they come to the conclusion of a Vampire Spellcaster.

Digoria wants to summon a demon to interroage Corros, and Cas suggest an angel instead.  Reaching no conclusion, Digoria suggests that the two of them can "beat the information out of" Corros and kill him if necessary.  Cas's morality is strongly tested in this manner and he again objects as tensions once again build between the two.

Cas reminds Digoria that, thanks to his help, Corros made the decision to spare her life.  Digoria contends that this was *"a mistake perhaps, and decision that he knew full well may cost him dearly."*