---
title: "Fane of the Drow, Part I"
date: 2017-09-23
description: "Cas, Digoria, and Link explore the Til A’Sperna Cavern near Tristram, facing drow, duergar, and spider swarms, uncovering secrets of a warlock's teleportation chamber."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 05 - Fane of the Drow
Session: 01
DM: Ryan
```

Cas, Digoria, and Link dock and travel to the nearby, larger, town of [Tristram](http://diablo.wikia.com/wiki/New_Tristram).  The old town has been rebuilt several times, though most of it's adventuring worth has been used up by heroes of centuries past.  At any rate, the trio goes to the local tavern to celebrate their accomplishments and reflect on the equipment awarded to them from the djinni.

At the tavern, they hear chatter of Stonehelm Castle not too far away in plains surrounding the town.  It seems that the castle was once full of dwarvish trade people, blacksmiths, and farmers, though lacking in skilled fighters.  The group listens closely for more rumors and learns of the Til A'Sperna Cavern near the castle which has been an area of much conflict.  The caverns, rich in raw ore and materials as well as loot and riches has been the contest of many duergar, drow, and dwarves.

As the are is rich in material, a truce has temporarily been made that neither the the duergar or the Drow will try to explore or exploit the area.  However, neither side truly abides by this treaty and the dwarves who rightfully own the land are still at a loss.

Within a day or two's light travel, the team approaches what's left of the castle, found west of Tristram.  There they meet local drawves who, not being fighters themselves, are interested in hiring some adventurers to investigate and bring back information about the Til A'Sperna Cavern.  After some haggling, the dwarves agree to pay 120 gp for information about the drow / duergar in the cavern.

However, given this brief encounter, they are suspicious of the tiefling's tact in haggling, assume that the human fight is grossly overcompensating for lacking masculinity, and the half-elf is rather hot tempered and ill mannered.

Venturing into the caves, the trio find a fair number of drow as well as a handful of duergar whom they battle and conquer without too much effort.  Following the cavern system to a few larger corridors, the floor becomes soft and squishy... and sticky... like spider webs.

Finding their slashing weapons neigh useless against the swarm of spiders, the triad eventually finds a way to squash and smash their way through the spiders.  Though, finding the task more difficult than they had initially imagined.

The caverns seem to weave their way around a large portal-like area that has a nearby office.  After careful examination, it appears to office is abandoned or, at least unoccupied at this time.  The group enters and begins searching the many papers, potions, and books inside.

They learn that the large portal appears to be some sort of teleportation chamber in use by a warlock.  The crew decides that this is a good resting point to nurse their wounds, get some rest, and review the many notes and materials they've found in the warlock's study.

---

**Notes:**

- Quest Inspiration: [Fane of the Drow, 3.5e](https://dnd-wiki.org/wiki/Publication:Fane_of_the_Drow)