---
title: "Innistrad for Christmas, Part I"
description: "Lilly and Digoria broker a deal with a Drow Matriarch for Digoria's warlock pact. After gathering rare items, they battle demons and encounter a fearsome Krampus atop a basilica."
date: 2023-12-26
tags: [Black Label Society, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Black Label Society
Quest: 01 - Innistrad for Christmas
Session: 01
DM: ChatGPT
```

### ChatGPT Prompt:

Role, Capacity, Setting, Insight ([ref.](https://oracle-rpg.com/2023/03/solo-dm-guide-part-3-chatgpt-as-assistant-ai-dungeon-master/)):

> Assume the role of dungeon master for D&D 5e.  I will instruct you directly if needed and declare my character's actions by first stating the character's name.  Players will manually roll dice and track character sheets.  Your job is to guide the story and fulfill the role of NPCs.

> The campaign setting is the world of Sanctuary from the Diablo video game series. Lore sources should also include Forgotten Realms, Magic the Gathering, and all 5e source materials.

> Keep all responses under 100 words.

> We're starting in the city of Westmarch, about 20 years after the defeat of Mathael.

> *Additional DM Note:*  This campaign starts about 1 - 2 years after [The Dawn Treaders](/tags/dawn-treaders) campaign ended.

---

### Player Characters:

* Digoria Antica (Lvl 20, tiefling warlock)
* Liliana Vess (Lvl 18, human necromancer)

---

### Antagonists and Personal Stakes:

* **[Zoltun Kulle](https://diablo.fandom.com/wiki/Zoltun_Kulle)**, has recreated the [Immortal Sun](https://mtg.fandom.com/wiki/Immortal_Sun) in Tristram, stopping any innate or magical planeshifting from away from Sanctuary.  Kulle is attempting to rebuild the [Black Soulstone](https://diablo.fandom.com/wiki/Black_Soulstone) after it was destroyed in the [Reaper of Souls](https://diablo.fandom.com/wiki/Diablo_III:_Reaper_of_Souls) events. Kulle has learned of the planeswalker Nicol Bolas and sees him as a reasonable threat to Sanctuary, so he's actually trying to summon Bolas to Sanctuary, with the party as bait, to then be trapped by the Black Soulstone.  Kulle is hoping that whoever survives, Bolas or The Party, is weak enough to be defeated by Kulle and also be absorbed by the Black Soulstone and augment Kulle's power.  Kulle doesn't trust the track record of angels in Sanctuary; thus he's skeptical of [Ugin](https://mtg.fandom.com/wiki/Ugin) being able to keep Bolas at bay.
* **[Sorin Markov](https://mtg.fandom.com/wiki/Sorin_Markov)**, a planeswalker from Innistrad doesn't know how long or why he's been trapped in Sanctuary, but suspects it's for a reason similar to the [War of the Spark](https://mtg.fandom.com/wiki/War_of_the_Spark).  While Sorin finds Westmarch particularly familiar to Innistrad, he has learned of the destruction caused by  [Mathael](https://diablo.fandom.com/wiki/Malthael) in Sanctuary, specifically Westmarch.  While Mathael was already defeated long before Sorin's arrival, Sorin vowed that so long as he was trapped in Sanctuary, he would not let another Black Soulstone be developed again -- imagining what could have happened if someone like Bolas or Mathael not only absorbed trapped planeswalkers, but all the inhabitants of the  [Helvault](https://mtg.fandom.com/wiki/The_Helvault) as well.
* **[Nicol Bolas](https://mtg.fandom.com/wiki/Nicol_Bolas)**, now nameless and imprisoned, cannot be summoned by normal means.  Kulle doesn't have faith in [Ugin the Spirit Dragon (CR24)](https://i.pinimg.com/originals/00/57/31/00573169db99eaf7f6193750274007e3.jpg), due to [the track record of angels in the world of sanctuary](https://diablo.fandom.com/wiki/Eternal_Conflict), to keep Bolas contained.  Bolas has been greatly de-powered and cannot escape the meditation realm, but the MTG universe has not specified if Bolas can communicate to or from it.  Cunning as ever, Bolas, the once Pharoh King, would be willing to deal with any mortal or undead ally that could break his brother Ugin's grasp over him.

---

### Current Situation:

* After the events of [Born Under a Bad Sign](https://gitlab.com/savagezen/quest-logs/-/tree/main/DND5E/Dawn%20Treaders?ref_type=heads), Digoria and Liliana planeshifted to an undisclosed location.  Presumably Liliana is on a quest for revenge against her brother, Josu, who now goes by the name [Zoltun Kulle](https://diablo.fandom.com/wiki/Zoltun_Kulle) -- a lich who Digoria has become familiar with.  Though, they do not know the two identities are connected.  Digoria's consumption of multiple "re-imagining potions" has transmogrified her abilities from a sorcerer to warlock, but a pact is needed to complete the transition.

---

### Played Adventure:

Lilly and Digs start at the basilica and broker with the [Drow Matriarch](https://i.pinimg.com/736x/0c/a1/38/0ca1382c5bc232470a636b98a444b141.jpg) to complete Digoria's transition from sorcerer-rogue to warlock via hexblade pact.  The Matriarch, accompanied by several subordinate [drow diviners](https://www.reddit.com/media?url=https%3A%2F%2Fi.redd.it%2Flsr1m74p8o291.jpg), informs the party they will need to retrieve the blood of an envy demon and the tooth of a Templar in order to complete the ritual.

Lilly and Digs proceed to explore the basilica before departing.  In some sort of sacrificial room they find a pentagram etched on the floor and complete a puzzle, placing skull lanterns on each of the corners of the pentagram.  Suddenly, the encircled pentagram transforms into a reflecting pool where the characters can see their reflection.

Lilly is able to resist the allure of the reflecting pool, but Digoria is drawn close to the water.  Her reflection transforms into a hideous creature that then leaps from the pool and pulls Digs in!  Fortunately, the party is able to defeat the [envy demon](https://i.pinimg.com/736x/c5/67/8d/c5678d2f4e79b09a9f5eb8c6459e40f6--tabletop-rpg-tabletop-games.jpg) with  minimal resources.

The party gives the blood they collect from the demon and give it to the Matriarch before
proceeding to the city courtyard in search of the remaining Templars, though the city is mostly abandoned, dead, or deserted.  At the entry to the courtyard Lil notices a spiked trap above the threshold and Digs disarms it.

Before entering, Digs and Lilly notice two Templars outside the Wolves' Den Inn.  Lilly comments on their decrepit state, "they're basically just drunk paladins down on their luck."  The party spots that one of Templars has a silver tooth!  Lilly is unusually hesitant to start a fight, so Digs has the brilliant idea to "just ask nicely" for the tooth.

In a shocking display of charisma, the Templar with the silver tooth readily hands it over to Digs.  Shocked and baffled by the absurdity of the situation, Lilly stands confused as Digs staggers into the inn.  Lilly makes frequent commentary about having spawned (Digoria) either an pirate idiot or a drunken genius.

Inside the inn, the party is met by [Bartender Bailey](https://diablo.fandom.com/wiki/Bartender_Bailey) who instantly has a crush on Lilly, but "doesn't have enough teeth to whistle" cat calls at her.  Apparently the adventuring business has been slow, so Bailey has "had a lot of time to read" and is quite knowledgeable about lore and history, specifically taking a liking to stories about liches and the Black Soulstone.  Bailey also notes that he's been hearing, and others have reported as well, some sort of wizard "flying around in a sleigh and playing bells this time of year."

Unfortunately, there are no habitable rooms at the inn, so the party returns to the basilica where Digs and Lilly encounter Sorin.  Sorin and Lilly certainly don't get along or like each other, though the respect each other's potential as a powerful ally.  Sorin joins the party and the three are able to find a small room upstairs in the basilica.

However, only a short rest is able to be obtained as the sounds of battle erupt downstairs!  The party rushes to the scene and finds that two [hezrou](https://www.dndbeyond.com/monsters/16922-hezrou) have attacked the drow.  Most of the diviners are dead, while the Matriarch is attending to the wounds of the one surviving diviner.

Somewhat shocked, the party defeats the demons with relative ease, and look confused at the drow's inability to defend themselves.  The Matriarch comments that the hezrou did not act alone, but were summoned by the sound of bells playing and the drow were stricken with fear before being ambushed.  Suddenly, there is a tremendous "thud!" on the roof of the basilica.

Lilly steps outside to investigate, but cannot see the roof from the street level as it is getting colder and snowing heavily.  The party continues from an inside ladder towards the roof to investigate the source of the noise, snow, and demons.  To their surprise, the arrive on the roof to see the Matriarch they left on the ground floor dancing and singing carols on the roof -- *Making Christmas!  Making Christmas!*

Lilly dispels the magical disguise from the impersonating creature who is revealed to be wearing a read and white cloak, has long finger claws, hoof feet, and large horns atop it's head.  The creature makes an attempt to disappear but is able to be tracked through footprints on in the snow.

Lilly attempts to advance towards the creature, but slips on the difficult terrain and slides to the edge of the roof, nearly falling to her death.  Sorin is able to probe his insight and recall that this creature is a particularly powerful form of [Krampus](https://i.pinimg.com/originals/a4/98/1b/a4981ba595cd683f7bbca3f1e8b65bf2.jpg).

Both Lilly and Digs are stricken with fear and only able to inflict minimal damage from afar.  Lilly struggles to return to her feet, frequently falling, while Sorin -- currently  hungry and largely depleted -- has only marginal means of physically hurting the Krampus.

Lilly is able to get to her feet and land some substantial blows on Krampus, though both Lilly and Digs are seriously wounded in the process.  Angered by his opponents, the Krampus summons two more hezrou to attack the party.  Further grievous wounds are inflicted on Digs and Lilly by the Krampus's poisonous, piercing, and grappling chains!

With two of the three party members almost dead, they make the tactical decision to retreat.  Though, the Krampus appears on his last leg himself, it's better to live to fight another day.  Sorin is able to get to the edge of the roof and pick up Lilly, and the two retreat down the ladder from which they came.  Digoria then follows, all three party members suffering opportunity attacks from the hezrous.

---

### GPT Summary End Prompt:

*Setting:*

The world of Sanctuary from the Diablo video game series, incorporating lore from Forgotten Realms, Magic the Gathering, and D&D 5e.

*Player Characters:*

* Digoria Antica (Lvl 20, tiefling warlock)
* Liliana Vess (Lvl 18, human necromancer)
* Sorin  Markov (Lvl 15, vampire fighter)

*Antagonist:*

* Zulton Kulle (above)
* Nichol Bolas (above)

*Personal Stakes:* (above)

*Current Situation:*

The party is currently trying to escape battle with a high-level Krampus.  Though the Krampus is severely wounded, the party is trying to play tactically and funnel their opponents in to the ladder chute.