---
title: "Training Day, Act 2"
date: 2016-06-09
description: "Raiden follows Morbius into a shadowy buildling, believing him to be the who sent the Sentinels to attack the Prime Minister."
tags: [Quests, MHRP, Metal Gear Rising]
---

```
Game: Marvel Heroic Roleplaying
Campaign: Metal Gear Rising
Quest: 01 - Training Day
Session: 02
```

#### Event:  Training Day

Raiden's first "solo gig."  Tony Stark has sent Raiden to be chief of security to transport a Prime Minister in the Middle East.  The Prime Minister has been pro-mutant and this has divided his country.  Several Private Military Companies have taken opposition to this.  Riaden needs to find out who is at the bottom of this and stop them if he can.*

#### Act 2:

*Raiden follows Morbius into a shadowy buildling, believing him to be the who sent the Sentinels to attack the Prime Minister.*

**Doom Pool: 2d6**

* Use a non-combat skill (1XP)
* Catch and kill Morbius (3XP)
* Stop further Sentinel attacks (10XP)

-----

Before chasing after Morbius, Riaden notices that War Machine has left him a package.  the package contiane an electorlyte pack for Raiden to recover as well as an "upgrade pack" that Raiden uses his tech expertise to patch onto himelf, effectively giving him moderate cybernetic senses.

Raiden uses his cybernetic senses to follow Morbius in the dark, but Raiden is eventually ambushed by Morbius in an open (though dimly  lit) cavern.  Morbius is able to surprise Raiden and scratches him.  Raiden tries to slash Morbius with his sword, but Morbius is amazingly able to stop the blade.  He states that his molecular structure is already unstable, so Raiden's HF blade has little effect without enough force behind it.

Morbius tries to take the sword away, but Raiden is able to wrench if free from Morbius's grasp.  He slashes, but Morbius evades and counters with a scratch.  Raiden then impales Morbius, who colapses from the stress.

A figure appears from the shadows who reveals himself to be [Sebastian Shaw](http://marvelheroicrp.wikia.com/wiki/Black_King_(Sebastian_Shaw,_Watcher_Datafile)).  Shaw charges Raiden, who acrobatically evades.  Not knowing Shaw's power, Raiden cuts him with his sword, which Shaw redirects to empower a heavy body punch.  Raiden slashes again and this time Shaw redirects to a near KO punch that stresses Raiden out.

Shaw goes on an overlord monologue about how Morbius wanted (to drink and study) Raiden's (artificial) blood so that he did not have to feed on "real people."  Shaw reveals himself as the one who ordered the Sentinel attack, but it was a ploy to lure Raiden to him.  Shaw states that "this is a warning to Raiden" to "stay out of his business, the business of war."  Raiden glances around the room and notices that Morbius's body is missing.

Before leaving, Shaw tells Raiden to "thank Stark Industries", because "their former 'government only arms' have fallen into the hands of Private Military Companies."  Others without these weapons "keep Shaw in business" by being able to sell Sentinels to them.

Shaw gets away and Raiden is grabbed from behind by Morbius who states his healing factor to Raiden before biting him.  Raiden goes into his Ripper Mode, exclaiming:  *Pain... this is why I fight!*  While Morbius still has his teeth sunk in, Raiden impales him.  Morbius yells and viciously chokes the wounded, though now berserk, Raiden.  Raiden slashes off Morbius's arm and, before he can react, also decapitates him -- additionally dicing his head into pieces before it hits the ground.

Just then, a S.H.E.I.L.D. Helicarrier blows open the top of the cavern and opens machine gun fire in Raiden.  Raiden effectively dodges the bullets due to his heightened state.  He is rapidly decelerating though and finds it wiser to just jump on to the deck of the Hellicarrier, show signs of "coming down" and being "in control of himself" whilst asking to be let in.