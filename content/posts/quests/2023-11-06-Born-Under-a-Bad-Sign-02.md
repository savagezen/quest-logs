---
title: "Born Under a Bad Sign, Part II"
description: "As the weather worsens, disguises fail, and tensions rise in Dexa. Alodel bargains with the council, proposing Digoria’s imprisonment while Cas fights in a gladiator tournament."
date: 2023-11-06
tags: [Dawn Treaders, DND5E, Quests]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 15 - Born Under a Bad Sign
Session: 02
Author: Austin
```

> This "quest" is written as "fan fiction."  The original Dawn Treaders campaign never finished it's original final climactic quest which was intended to be titled "Born Under a Bad Sign" and conclude with the characters Cas, Alodel, and Digoria eventually going their separate ways after defeating the big-bad.  The personality and spirit of each character has been attempted to be preserved.

---

### Part 2 - Dexa:

The weather continues to worsen... As the team makes their way around the town, their disguises eventually fail.  Cas weighs his options whether his survival skills can aide the team enough to make a break for the mountains and flee the town -- it would be risky.  Alodel thinks they should try to make peace with the townspeople to gather more information about the demigod the merchant spoke of.  Both Cas and Alodel are frustrated with Digoria's unusual silence compared to her usual quick with, penchant to getting into trouble, and excessive sarcasm.

The City Council comes forth and identifies themselves as being formed to represent each school of magic (Abjuration, Conjuration, Necromancy, Illusion, Enchantment, Transmutation, Divination, and Evocation) and two royal guards.  Cas counts the schools of magic and points out that while there are 8 schools of magic, there are only 6 council members present.

The council states that the abjuration representative, Corros "stick in the mud he was, had noble intentions to further extend aid to Sanctuary outside of Xiansai."  The council is aware that the party is acquainted with Corros, that Corros is dead, and that the party has taken up Corros' citadel of Entsteig.

The council reads a list of crimes Digoria is being accused of including "murdering innocents in Entsteig, piracy, theft, extortion and terrorism (in Heythorp) and being the last remaining child of 'her'."  As a result, the council proposes that all members of the Dawn Treaders party be sacrificed immediately, "to bring order" to the realms.

 The elders state that they refuse to speak "her" real name, but declare that "she" refused to abide by the decree agreed on by all mages after their previous [clan wars](https://diablo.fandom.com/wiki/Mage_Clan_Wars).  Apparently the violations were so great, that "her" children were all either confined to servitude within the city, imprisoned, or killed if they protested the former.

Alodel, in a major character development moment, defiantly says, "No!"  Cas, comments on Al "[making his voice deeper](https://youtu.be/It67i5uKqos?si=hLrPwn-17p-5Nd8A&t=90)".  Without consulting the team, Al proposes that Digoria be "temporarily" imprisoned "under more favorable conditions" while Al speaks with the elders and reviews lore and recent developments around Sanctuary -- including the team's interactions with [Zoltun Kulle](https://diablo.fandom.com/wiki/Zoltun_Kulle) and Karasu.  As a bargaining chip, Al volunteers Cas to fight in a gladiator tournament within the city's coliseum.  Cas, while unhappy with being spoken for, appears to be out of his funk and is "happy to oblige with good-old-fashioned hack-and-slashery."

The people of Dexa and it's elders seem appeased by this decision.  the guards strip Digs of her possessions and escort her towards the prison.  Al makes one last concession that Digs' possessions should be left with him so that he, with the elders' help of course, can further assess them for any threat.  Much to everyone's surprise, this act of deception is permitted.

Cas is ushered towards The Coliseum and Al towards The Great Hall with the elders.