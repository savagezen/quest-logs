---
title: "Fane of the Drow, Part III"
date: 2017-09-25
description: "Digoria reunites with Cas and Link after visiting a fortune teller. The trio battles drow, rescues prisoners, and faces a powerful shaman in a dangerous underground cavern."
tags: [Quests, DND5E, Dawn Treaders]
---

```
Game: Dungeons & Dragons 5e
Campaign: Dawn Treaders
Quest: 05 - Fane of the Drow
Session: 03
DM: Ryan
```

Digoria declares to the group that she had left the party because Chuck, her familiar, told her that a fortune teller was traveling through town.  Digoria went to visit the fortune teller and hired the dwarf, Balfador, and the elf that was rescued, Alodel, to "attend to some other errands."

Curiously, Cas and Link ask Digoria what the fortune teller revealed to her.  Digoria states that the mystic drew a set of cards and Digoria's dealing featured a tree, a fire, an elf, and a ring.

Shortly after Cas and Link admit that they are puzzled as to why they decided to start following Digoria in the first place.  Cas suspects that, for him, he believes that she has friends in high (and low) places that may help him recover his lost memories.  Link remains passive and agrees with Cas.

Taking ornery advantage of Link's timid behavior, Digoria persuades him to go heading into the next room.  However, Link refuses to fight and wants the group to go togther should they venture further.

With rolling eyes and a sigh, Digoria vaults over Link and attacks the drow around the corner of the room.  With a ruckus stirred, Cas is forced to charge in as well and help Link and Digoria fight the drow.

Digoria charges further into the towards a throne-like area; killing a slew of drow on her way.  Much to her surprise she is met by a summoned drow cleric!

After the combat settles, Cas and Link finishing off the minions while Digoria slays the cleric (after a fairly close call mind you!), the group decides to split up.  Digoria sends Chuck to go explore with Cas, while she and Link go the other direction.

Heading south, Digoria and Link find two drow sleeping outside of prison cells.  They are easily killed and one of the drow conveniently has a key to one of the jail cells.  A modest dwarf is inside, and he states that he does not have a key for his friend's jail cell.

Meanwhile, north in the cave, Cas has been spotted by a powerful drow shaman who has been guarding the cave.  Seeing no help readily available, he readies for combat and tells Chuck to go find the others!  Fast!

Back on the south end of the cave, after Chuck's flight, Digoria tells Link to take the dwarf and search for the other key while she goes to help Cas.  Cas almost dies at the hand of the doppelganger risen by the shaman as it provides a good enough shield for her to openly cast spells upon Cas.

With Digoria's help, the shaman is easily killed, though Cas (ever needing to soothe his bruised and overcompensating ego), assures Digoria that "he had the situation completely under control."  A key is found on the shaman which opens the Dwarf's friend's jail cell.

Inside is a (barely) alive human.  Apparently, the dawrf and his friend the human were exploring the cave to learn about dwarven heritage and got captured.  As the two civilians run back to the surface, the adventures decide to push further still into the depths of the cave.

Coming to the lowest level they find themselves approaching the tomb level of the cave.

---

**Notes:**

* Quest Inspiration: [Fane of the Drow, 3.5e](https://dnd-wiki.org/wiki/Publication:Fane_of_the_Drow)