# Phage Elung - cr06

Ruthless, relentless, bloodthirsty, and unpredictable.  Phage's history is largely unknown.  She posesses a hearthstone which she states was given to her by her mother.  Phage has also said that she was born of a vampire spellcaster, however, this appears untrue as she has made several references to elaborate efforts to free herself from her sire.

### Abilities

**Regeneration:**  Phage regains 10 hit points at the start of her turn if she has at least 1 hit point and isn't in sunlight or running water.  If Phage takes radiant damage or damage from holy water, this trait doesn't function at the start of her next turn.
<hr/>
**Spider Climb:**  Phage can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
<hr/>
**Vampire Weakness:**  Phage has the following flaws:
- *Forbiddance:*  Phage can't enter a residence without an invitation from one of the occupants.
- *Harmed by Running Water:*  Phage takes 20 acid damage if she ends her turn in running water.
- *Stake to the Heart:*  Phage is destroyed if a piercing weapon made of wood is driven into her heart while she is incapacitated in her resting place.
- *Sunlight Hypersensitivity:*  Phage takes 20 radiant damage when she starts her turn in sunlight.  While in sunlight, she has disadvantage on attack rolls and ability checks.

<hr/>
**Etherrealness:**  Phage magically enters the Ethereal Plane from the Material Plane, or vice versa.  To do so, she must have a hearthstone in her posession.

### Actions

**Multiattack:**  Phage makes two attacks, only one of which can be bite.
<hr/>
**Claws:**  Mele Weapon Attack: +7 to hit, reach 5 ft., one creature.  Hit: 9 (2d4 + 4) slashing damage.  Instead of dealing damage, Phage can grappe the target (escape DC 13).
<hr/>
**Bite:**  Melee Weapon Attack: +7 to hit, reach 5 ft., one willing creature, or a creature that is grappled, incapacitated, or restrained.  Hit: 7 (1d6 + 4) piercing damage plus 7 (2d6) necrotic damage.  The target's hit point maximum is reduced by an amount equal to the necrotic damage taken, and Phage regains hit points equal to that amount.  The reducion lasts until the target finishes a long rest.  The target dies if this effect reduces its hit point maximum to 0.

### Notable Items

- [Hearthstone](https://www.dandwiki.com/wiki/Hearthstone_(5e_Equipment))
- [Ring of Warmth](https://open5e.com/equipment/magic-items/ring-of-warmth.html)
- [Gauntlets of Ogre Power](https://roll20.net/compendium/dnd5e/Gauntlets%20of%20Ogre%20Power#content)

### Alternate Versions
- [Archive / Quests / Pre-Retcon](http://rockandcode.ga/rpg/DND5E-NPC-Phage-CR06/)
- [Base, Vampire Spawn CR5](https://chisaipete.github.io/bestiary/creatures/vampire-spawn)
- 

** Medium Undead, Chaotic Evil**

| | |
| -- | -- |
| **Armor Class:** | 15 (natural armor) |
| **Hit Points:** | 82 |
| *Speed:* | 30 ft. |
| *Skills:* | Acrobatics, +5 |
|           | Perception, +2 |
|           | Stealth, +5 |
| *Damage Resistances:* | necrotic, bludeoning, piercing, and slashing from non-magical attacks |
| *Senses:* | - darkvision, 60 ft. |
|           | - passsive Perception 13 |
| *Languages:* | common, elvish, drow sign |
| *Challenge:* | CR6 (2,300 XP) |

| | |
| -- | -- |
| **Strength:** | 19 (+4) |
| **Dexterity:** | 16 (+3) |
| **Constitution:** | 16 (+3) |
| **Intelligence:** | 11 (+0) |
| **Wisdom:** | 10 (+0) |
| **Charisma:** | 12 (+1) |
