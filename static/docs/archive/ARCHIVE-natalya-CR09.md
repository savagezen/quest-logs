# Natalya Josen

| | | | |
| -- | -- | -- | -- |
| **Race:** | Human (medium) | **Age:** | 29 |
| **Class:**| Blood Hunter | **Height:** | 5' 11"|
| **Gender:**| Female | **Weight:** | 140 lbs. |

| | | | | | |
| -- | -- | -- | -- | -- | -- |
| **Hit Points:** | 77 (9d10) | **Armor Class:** 17 (half plate) | | **Initiative:** +5 | |
| **Proficiency Bonus:** | +3 | **Speed:** | 30 | **Passive Perception:** | 12|

| STR | DEX | CON | INT | WIS | CHA |
| --- | --- | --- | --- | --- | --- |
| 11 (+0) | 20 (+5) | 15 (+2) | 13 (+1) | 15 (+2) | 13 (+1) |


| | |
| -- | -- |
| **Alignment:** | Lawful Good with Chatoic tendencies |
| **Proficiencies:** | light armor, medium armor, shields, simple weapons, martial weapons |
| **Tools:** | Alchemist's supplies |
| **Languages:** | common, abyssal |
| **Saving Throws:** | *Strength +4*, Dexterity +5, Constitution +2, Intelligence +1, *Wisdom +6*, Charisma +1 |
| **Skills:** | Athletics +0, *Acrobatics +9*, Sleight of Hand +5, Stealth +5, Arcana +1, History +1, Investigation +1, Nature +1, Religion +1, Animal Handling +2, Insight +2, Medicine +2, Perception +2, *Survival +3*, Deception +1, Intimidation +1, Performance +1, Persuasion +1 |

### Abilities:

**Hunter's Bane:**  You have advantage on *Wisdom (Survival)* checks to track Fey, Fiends, and Undead as well as on *Intelligence* checks to recall information about them.  if you are actively tracking one of these creature types, you cannot be surprised by any creatures of that type.  You can only be tracking one type of creature at a time.

---
**Crimson Rite:** As a bonus action you can imbune a single weapon you hold with your own life force, temporarily reducing your maximum hit points a number equal to your character level.  The lost maximum hit points are returned once the rite fades and cannot be restored otherwise.  Until the rite fades, that weapon flares with (elemental) energy.  Attacks from that weapon deal an additional 1d6 rite damage (magical).  The rite fades after the attack.  *Rite of the Flame:  fire damage*

---
**Fighting Style:**  *Two Weapon Fighting* - When you engage in two-weapon fighting, you can add your ability modifier to the damage of the second attack.

---
**Blood Hunter Order:**  *Order of the Mutant*
Advanced Mutagen Craft:  Each formula takes a short rest to craft and a bonus action to consume.  Persons othe than yourself only suffer the side effects of consuming.
 *Mutation Score: 2*
- *Celerity Formula:*  Your *Dexterity* score increases by an amount equal to your mutation score, as does your Dexterity maximum.  Your *Wisdom* score decreases by your mutation score. 
- *Sagacity Formula:*  Your *Wisdom* score increases by an amount equal to your mutation score, as does your Wisdom maximum.  Your *armor class* is reduced by your mutation score.

---
**Blood Maledict (2/rest):**  Blood curses can be *amplified* by suffering damage equal to one's *Crimson Rite* damage.
- *Blood Curse of the Marked* - As a bonus action, you can mark an enemy within 30 feet.  Until the end of your turn, all cirmson rite damage you deal to the target is doubled.
*Amplify: You cause the marked target to also lose resistance to your rite dmamage type until the beginning of your next turn.* 
- *Blood Curse of Mutual Suffering* - As a bonus action, you can link to a creature within 30 feet.  Each time the cursed creature damages you, the cursed creature takes necrotic damage equal to half of the damage you suffered.  This curse lasts a number of rounds equal to your *Wisdom* modifier (minimum 1).
*Amplify: You double the duration of this curse, and this curse's damage ignores necrotic resistance.*

---
**Grim Psychometry:**  Take 10 minutes to meditate on an object regarding lingering evil or wicked past, make a *Wisdom* check.

### Weapons & Attacks

**Multiattack:**  Natalya makes two attacks, one with each hand crossbow, or two with the heavy crossbow if it is drawn.

---
**Hand Crossbow (L, first):**  +9 to hit, Ranged weapon attack, range 30 / 120, Hit: 1d6 +4 piercing, +1d6 fire (crimson rite, can be doubled with *Curse of the Marked*).

---
**Hand Crossbow (R, second):**  +9 to hit, Ranged weapon attack, range 30 / 120, Hit: 1d6 +11, piercing

---
**Heavy Crossbow:**  +9 to hit, Ranged weapon attack, range 100 / 400, Hit: 1d10 piercing, +1d6 fire (crimson rite, can be doubled with *Curse of the Marked*).

### References:

- [Natyalya's Origin Story](https://us.diablo3.com/en/game/lore/short-story/demon-hunter/)
- [Video of Natalya in combat](https://youtu.be/qis_X2jiXCU)
- [Blood Hunter Class v2.1](http://www.dmsguild.com/product/170777/Blood-Hunter-Class?term=blood+hunter?affiliate_id=237976)
- [Quest / Archive](http://rockandcode.ga/rpg/DND5E-NPC-Natalya/)
