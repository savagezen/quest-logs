# Gachev Zhota

| | | | |
| -- | -- | -- | -- |
| **Race:** | Half-Dwarf (medium) | **Age:** | 40 |
| **Class:** | Monk | **Height:** | 5' 8" |
| **Gender:** | Male | **Weight:** | 180 lbs |


| | | | | | |
| -- | -- | -- | -- | -- | -- |
| **Hit Points:** | 84 (9d8) | **Armor Class:** | 18 | **Initiative:** | +5 |
| **Proficiency Bonus:** | +4 | **Speed:** | 40 | **Passive Perception:** | 16 |

<!-- Raw Rolls: 16, 12, 17, 7, 11, 16

| STR | DEX | CON | INT | WIS | CHA |
| --- | --- | --- | --- | --- | --- |
|  11 | 17  |  16 | 12  | 16  | 7   |

-->

| STR | DEX | CON | INT | WIS | CHA |
| --- | --- | --- | --- | --- | --- |
|  12 (+1) | 20 (+5) | 18 (+4) | 14 (+2) | 16 (+3) | 7 (-2) |

| | |
| -- | -- |
| **Alignment:** | Lawful Good |
| **Proficiencies:** | no armor, simple weapons, shortswords |
| **Tools:** | pan flute, carpenter's tools, mason's tools |
| **Languages:** | common, dwarvish, celestial |
| **Saving Throws:** | Strength +1, Dexterity +5, Constitution +4, Intelligence +2, Wisdom +3, Charisma -2 |
| **Skills:** | Athletics +1, Acrobatics +5, Sleight of Hand +5, Stealth +5, *Arcana +6*, History +2, Investigation +2, *Religion +3*, Animal Handling +3, *Insight +7*, Medicine +3, *Perception +7*, Survival +3, Deception -2, Intimidation -2, Performance -2, Persuasion -2 |
| **Senses:** | Darkvision 60' |

### Abilities

**Dwarven Resilience:**  Advantage on saving throws against being poisoned and resistance to poison damage.

---
**Unarmored Defense:**  Without armor, AC 10 + *Dexterity* modifier + *Wisdom* modifier

---
**Martial Arts:**  Use Dexterity instread of strength for attack and damage rolls with unarmed strikes and monk weapons.  You can use 1d4 instead of unarmed strike or monk weapon.  You can make an unarmed strike as a bonus action.

<!-- Ki save DC = 8 + proficiency bonus + wisdom modifier -->

---
**Ki:**  *Ki Points:* 9, *Ki Save DC:* 15, recover Ki points after 30 minutes of rest.
- Flurry of Blows: after attacking spend 1 Ki point to make 2 unarmed stries as a bonus action
- Patient Defense: spend 1 Ki point to take the Dodge action as a bonus action
- Step of the Wind:  spend 1 Ki point to take Disengage or Dash action as a bonus action
- Empowered Strikes: unarmed strieks count as magical for the purpose of overcoming resistances and immunities to nonmagical attacks and damage.

---
**Unarmored Movement:**  Your speed increases by 10' while not wearing armor or a sheild. You can move aloing vertial suraces or across liquids without falling during the move.  +15 ft. to movement

---
**Monastic Tradition:**  *Way of the Elemental Fist (fire)*
- Elemental Fist: you can control, mold, or activate (cost 1 Ki point) traces of your element nearby.
- Elemental Wall: you can summon a wall of your element 30' long and 20' high that does 3d6 elemental damage.  Elemental Fist must be activated first.

---
**Deflect Missiles:** Can use reaction to deflect or catch projectile when hit by a ranged weapon attack.  Damage is reduced by 1d10 + dex_mod + monk_level

---
**Slow Fall:**  Use your reaction to reduce fall damage by 5 x monk_level

---
**Stunning Strike:**  When you hit another creature with a melee attack, you can spend 1 Ki point to make a stunning strike.  Target must succeed on a *Constitution* save or be stunned until the end of your next turn.

---
**Stillness of Mind:**  You can use your action to end one effect on yourself that is causing you to be *Charmed* or *Frightened*.

---
**Evasion:**  When you are subject to an attack that allows you to make a *Dexterity* save, you take only half damage on a fail or no damage on a success.

### Weapons & Attacks

**Multiattack:**  Gachev makes 2 attacks (either unarmed or with Bo Staff) and one Unarmed Strike as a bonus action (2 if 1 Ki point is spend).

---
**Bo Staff:** +9 to hit, melee weapon, Hit: 1d6 bludgeoning

---
**Unarmed Strike:** +9 to hit, melee weapon Hit: 1d4 bludgeoning (magical)

### References

- [Gachev battling a demon](https://youtu.be/4cJfxhBgq8c)
- [Original NPC / CR8](https://imgur.com/HIvPzSj)
- [Half-Dwarf - Homebrew Race](https://www.dandwiki.com/wiki/Half-Dwarf_(5e_Race))
- [Way of the Elemental Fist - Homebrew Monastic Tradition](https://www.dandwiki.com/wiki/Way_of_the_Elemental_Fist_(5e_Subclass))
